#pragma once
#include <time.h>
class CNeural
{
public:
	CNeural(int inputCnt);
	~CNeural(void);

public:
	bool	m_init;
	int		m_inputCount;
	double* m_input;
	double* m_weight;
	double	m_NeuralCore;
	double	m_reference;
	double	m_error;
	double  m_delta;
};