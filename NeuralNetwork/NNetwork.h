#pragma once
#include "Neural.h"

#define ERR_RES 0.001
#define LERN_C_POINT 0.3

class CNNetwork
{
public:
	CNNetwork(int intputLayerCnt, int hideLayerCnt, int OutLayerCnt);
	~CNNetwork(void);

	int			m_intputLayerCnt;
	int			m_hideLayerCnt;
	int			m_OutLayerCnt;

	CNeural**	m_HideLayer;
	CNeural**	m_OutLayer;

	double		learnCurrentPoint;
	int			learnSampleCount;
	int			m_learnSampleCount;

	double** m_sampleInput;
	double** m_sampleOutput;

	void initLearnSample(int Count);
	void LearnSample(double* input, double* output);
	void Learning();

	void showAlldata(void);
	void inToOut(double* input);
};

