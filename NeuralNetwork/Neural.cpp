

#include "StdAfx.h"
#include "Neural.h"



CNeural::CNeural(int inputCnt)
{
	m_init = false;
	m_inputCount	= inputCnt;
	m_input			= NULL;
	m_weight		= NULL;
	m_NeuralCore	= 0.0;
	m_error			= 0.0;
	m_reference		= 0.0;

	m_input			= new double[m_inputCount];
	m_weight		= new double[m_inputCount];
	
	for (int i = 0 ; i < m_inputCount ; i++)
	{		
		m_weight[i] = (double)(( rand() % 2000 ) - 1000) / 1000.0;
	}
	m_init = true;
}

CNeural::~CNeural(void)
{
	delete [] m_input;
	delete [] m_weight;		
	m_input			= NULL;
	m_weight		= NULL;
}
