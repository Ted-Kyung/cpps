#include "StdAfx.h"
#include "NNetwork.h"

#include <math.h>
inline double sigmoid(double x)
{
	return (double)( 1.0 / (1.0 + exp(-x)) );
}

CNNetwork::CNNetwork(int intputLayerCnt, int hideLayerCnt, int OutLayerCnt)
{
	m_learnSampleCount = 0;
	learnSampleCount = 0;
	printf("%d input %d Hide %d Out ",intputLayerCnt, hideLayerCnt, OutLayerCnt);
	learnCurrentPoint = LERN_C_POINT;
	printf("%lf Learning Point\n", learnCurrentPoint);
	m_HideLayer		= NULL;
	m_OutLayer		= NULL;

	m_intputLayerCnt	= intputLayerCnt;
	m_hideLayerCnt		= hideLayerCnt;
	m_OutLayerCnt		= OutLayerCnt;

	srand((unsigned)time(NULL));
	printf("HideLayer \n");
	m_HideLayer		= new CNeural* [m_hideLayerCnt];
	for(int i = 0 ; i < m_hideLayerCnt ; i ++)
	{
		m_HideLayer[i]		= new CNeural(m_intputLayerCnt);
	}
	
	printf("\nOutLayer \n");
	m_OutLayer		= new CNeural* [m_OutLayerCnt];
	for(int i = 0 ; i < m_OutLayerCnt ; i ++)
	{
		m_OutLayer[i]		= new CNeural(m_hideLayerCnt);
	}
}


CNNetwork::~CNNetwork(void)
{
	delete [] m_HideLayer;
	delete [] m_OutLayer;
}

void CNNetwork::Learning()
{
	//no Learning Data
	if(learnSampleCount == 0)
	{
		printf("No Learning Data\n");
		return;
	}

	printf("start Leaning");

	double total_err = 0.0;

	double* input;
	double* output;

	int		sampleCnt = 0;
	while(1)
	{
		if(sampleCnt > m_learnSampleCount)sampleCnt = 0;
		input = m_sampleInput[sampleCnt];
		output = m_sampleOutput[sampleCnt];

		//input -> hide calculation
		{
			double buf_beta = 0.0;
			for(int j = 0 ; j < m_hideLayerCnt; j++)
			{
				for(int i = 0 ; i < m_intputLayerCnt ; i++)
				{
					buf_beta += input[i] * m_HideLayer[j]->m_weight[i];
				}
				m_HideLayer[j]->m_NeuralCore = sigmoid(buf_beta);
				buf_beta = 0.0;
			}		
		}

		// hide -> output calculation
		{
			double buf_beta = 0.0;
			for(int k = 0 ; k < m_OutLayerCnt; k++)
			{
				m_OutLayer[k]->m_reference = output[k];
				for(int j = 0 ; j < m_hideLayerCnt; j++)
				{
					buf_beta += m_HideLayer[j]->m_NeuralCore * m_OutLayer[k]->m_weight[j];
				}
				m_OutLayer[k]->m_NeuralCore = sigmoid(buf_beta);

				//error calculation
				m_OutLayer[k]->m_error = m_OutLayer[k]->m_reference - m_OutLayer[k]->m_NeuralCore;
				m_OutLayer[k]->m_delta = m_OutLayer[k]->m_NeuralCore * (1.0 - m_OutLayer[k]->m_NeuralCore) * m_OutLayer[k]->m_error;
				buf_beta = 0.0;
			}		
		}

		//Wjk update 
		{
			for(int k = 0 ; k < m_OutLayerCnt; k++)
			{
				for(int j = 0 ; j < m_hideLayerCnt; j++)
				{
					m_OutLayer[k]->m_weight[j] = m_OutLayer[k]->m_weight[j] + (learnCurrentPoint * (m_HideLayer[j]->m_NeuralCore * m_OutLayer[k]->m_delta));
				}
			}	
		}
		//Wij update
		{
			double buf_delta = 0.0;
			// hidden layer delta calculation
			for(int j = 0 ; j < m_hideLayerCnt; j++)
			{		
				for(int k = 0 ; k < m_OutLayerCnt; k++)
				{
					buf_delta += m_OutLayer[k]->m_weight[j] * m_OutLayer[k]->m_delta;
				}			
				m_HideLayer[j]->m_delta = m_HideLayer[j]->m_NeuralCore * (1.0 -m_HideLayer[j]->m_NeuralCore) * buf_delta;
				buf_delta = 0.0;
			}

			for(int j = 0 ; j < m_hideLayerCnt; j++)
			{
				for(int i = 0 ; i < m_intputLayerCnt ; i++)
				{
					m_HideLayer[j]->m_weight[i] = m_HideLayer[j]->m_weight[i] + (learnCurrentPoint * input[i] * m_HideLayer[j]->m_delta);
				}
			}
		}

		total_err = 0.0;
		for(int k = 0 ; k < m_OutLayerCnt; k++)
		{
			total_err += (double)(m_OutLayer[k]->m_error) * (double)(m_OutLayer[k]->m_error);
		}

		printf("e : %lf\n",total_err);
		if (total_err < ERR_RES) 
		{
			printf("e : %lf break\n",total_err);
			break;
		}
		//showAlldata();
		sampleCnt++;
	}
	printf("Learn Complate\n");
}

void CNNetwork::showAlldata(void)
{
	printf("HIDE LAYER \n");
	for(int j = 0 ; j < m_hideLayerCnt; j++)
	{
		printf("[%d]neural %lf\n",j,m_HideLayer[j]->m_NeuralCore);
		for(int i = 0 ; i < m_intputLayerCnt ; i++)
		{
			printf("W%d%d = %lf\t",i,j,m_HideLayer[j]->m_weight[i]);
		}
		printf("\n\n");
	}

	printf("OUTPUT LAYER \n");
	for(int k = 0 ; k < m_OutLayerCnt; k++)
	{
		printf("[%d]neural %lf\n",k,m_OutLayer[k]->m_NeuralCore);
		for(int j = 0 ; j < m_hideLayerCnt ; j++)
		{
			printf("W%d%d = %lf\t",j,k,m_OutLayer[k]->m_weight[j]);
		}
		printf("\n\n");
	}
}

void CNNetwork::inToOut(double* input)
{
	printf("\n");
	//input -> hide calculation
	{
		double buf_beta = 0.0;
		for(int j = 0 ; j < m_hideLayerCnt; j++)
		{
			for(int i = 0 ; i < m_intputLayerCnt ; i++)
			{
				buf_beta += input[i] * m_HideLayer[j]->m_weight[i];
			}
			m_HideLayer[j]->m_NeuralCore = sigmoid(buf_beta);
			buf_beta = 0.0;
		}		
	}

	// hide -> output calculation
	{
		double buf_beta = 0.0;
		for(int k = 0 ; k < m_OutLayerCnt; k++)
		{
			for(int j = 0 ; j < m_hideLayerCnt; j++)
			{
				buf_beta += m_HideLayer[j]->m_NeuralCore * m_OutLayer[k]->m_weight[j];
			}
			m_OutLayer[k]->m_NeuralCore = sigmoid(buf_beta);
			printf("%lf\n",m_OutLayer[k]->m_NeuralCore);
			buf_beta = 0.0;
		}		
	}
}
void CNNetwork::initLearnSample(int Countx)
{
	m_sampleInput = new double*[Countx];
	m_sampleOutput  = new double*[Countx];

	for(int i = 0 ; i < Countx ; i++)
	{
		m_sampleInput[i] = new double [m_intputLayerCnt];
		m_sampleOutput[i] = new double [m_OutLayerCnt];
	}
	printf("%d sample\n",Countx);
	learnSampleCount = Countx -1 ;
}
void CNNetwork::LearnSample(double* input, double* output)
{
	for(int i = 0 ; i < m_intputLayerCnt ; i++)
	{
		m_sampleInput[m_learnSampleCount][i] = input[i];
	}
	for(int k = 0 ; k < m_OutLayerCnt ; k++)
	{
		m_sampleOutput[m_learnSampleCount][k] = output[k];
	}
	m_learnSampleCount++;
	if(m_learnSampleCount > learnSampleCount) m_learnSampleCount = learnSampleCount;
}