
// NeuralNetworkDlg.h : 헤더 파일
//

#pragma once
#include "NNetwork.h"

// CNeuralNetworkDlg 대화 상자
class CNeuralNetworkDlg : public CDialogEx
{
// 생성입니다.
public:
	CNeuralNetworkDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NEURALNETWORK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
	CNNetwork* m_nnetwork;

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnCreate();
	afx_msg void OnBnClickedBtnLearn();

public:
	CEdit* m_Linput;
	CEdit* m_Lref;
	CEdit* m_input;
	CEdit* m_sampleCnt;
	afx_msg void OnBnClickedBtnInput();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
