#include "sockNetwork.h"

sockNetwork::sockNetwork()
{
	m_server = NULL;
	m_client = NULL;
}

//서버
sockNetwork::sockNetwork(int port)
{
	CreatSock(port);
}
//클라이언트
sockNetwork::sockNetwork(char* ip, int port)
{
	CreatSock(ip,port);
}

sockNetwork::~sockNetwork(void)
{
	if(m_server != NULL)
	{
		closesocket(m_server);
	}
	if(m_client != NULL)
	{
		closesocket(m_client);
	}
	WSACleanup();
}

void sockNetwork::CreatSock(char* ip, int port)
{	

	if(WSAStartup(MAKEWORD(2,2),&m_winsock) != 0)
	{
		printf("m_winsock2 init err!!\n");
	}
	printf("m_winsock2 init success!!\n");

	m_client = socket(AF_INET,SOCK_STREAM,0);

	m_server_addr.sin_family		= AF_INET;
	m_server_addr.sin_port			= htons(port);
	m_server_addr.sin_addr.s_addr	= inet_addr(ip);

	int addr_len = sizeof(m_server_addr);

	printf("any Key push -> connect server\n");

	while(kbhit() == 0);


	connect(m_client,(SOCKADDR*)&m_server_addr,addr_len);
	m_sock = m_client;

	printf("Server Connect Success!\n");
}
void sockNetwork::CreatSock(int port)
{

	/********************************************************************/
	/*						m_winsock Open								*/
	/********************************************************************/

	int err = 0;
	err = WSAStartup(MAKEWORD(2,2),&m_winsock);
	if(err == 0){printf("WSAStartup success!!\n");}

	/********************************************************************/
	/*						m_server Open								*/
	/********************************************************************/

	m_server =socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(m_server != 0)
	{
		printf("sock open!!\n");
	}
	m_server_addr.sin_family			= AF_INET;
	m_server_addr.sin_port			= htons(port);
	m_server_addr.sin_addr.s_addr		= htonl(INADDR_ANY);

	bind(m_server,(SOCKADDR*)&m_server_addr,sizeof(m_server_addr));

	/********************************************************************/
	/*						m_server listen								*/
	/********************************************************************/
	printf("Listen!!\n");
	listen(m_server,SOMAXCONN);

	int addr_size;

	addr_size = sizeof(m_client_addr);

	/********************************************************************/
	/*						Accept m_client								*/
	/********************************************************************/

	m_client = accept(m_server,(SOCKADDR*)&m_client_addr,&addr_size);
	printf("accept!!\n");
	m_sock = m_client;
}


int sockNetwork::SendData(char* data, int size)
{
	return send(m_sock,data,size,0);
}
int sockNetwork::RecvDATA(char* data, int size)
{
	int len = recv(m_sock, data, size, 0);
	data = NULL;
	return len;
}