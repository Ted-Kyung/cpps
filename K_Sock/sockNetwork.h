#pragma once
#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

class sockNetwork
{
public:
	sockNetwork();
	sockNetwork(int port);
	sockNetwork(char* ip, int port);
	~sockNetwork(void);

public:

	void CreatSock(char* ip, int port);
	void CreatSock(int port);
	int SendData(char* data, int size);
	int RecvDATA(char* data, int size);
	WSADATA m_winsock;

	SOCKET m_server;
	SOCKET m_client;
	SOCKET m_sock;

	SOCKADDR_IN m_server_addr;
	SOCKADDR_IN m_client_addr;
};

