#include "stdafx.h"
#include "K_ImageCameraCtrl.h"


#define CLIP(x) (((x) <0)?0:(((x)>255)?255:(x)))
#define RED		0
#define GREEN	1
#define BLUE	2

unsigned char* pImageBuffer;
BITMAPINFO		capinfo;	

ImageCameraCtrl::ImageCameraCtrl(void)
{
}
ImageCameraCtrl::ImageCameraCtrl(CWnd* mainwindow, int nID)
{
	m_Cwnd = NULL;
	if(m_Cwnd != NULL)
	{
		m_Cwnd = mainwindow;
		m_hWndCap = m_Cwnd->GetSafeHwnd();
	}	

	CreateDC(mainwindow,nID);
	printf("Cam & DC init\n");
}

ImageCameraCtrl::~ImageCameraCtrl(void)
{
	free(lpBits);
	
	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	lpBits				= NULL;
	image				= NULL;	
	m_bitmap.DeleteObject();
	m_mDC.DeleteDC();

	capDriverDisconnect(m_hWndCap);
	free(pImageBuffer);
	m_hWndCap = NULL;
}
bool ImageCameraCtrl::init_Cam(void)
{
	m_hWndCap = capCreateCaptureWindow("Capture Window", WS_CHILD | WS_VISIBLE ,0,0
		, GetImageParm().dcRight, GetImageParm().dcBottem
		,(m_mainWindow->GetDlgItem(GetresourceID()))->GetSafeHwnd(), GetresourceID());

	if(capSetCallbackOnFrame(m_hWndCap,capCallbackOnFrame) == false) return false;

	if (capDriverConnect(m_hWndCap, 0) == FALSE) return FALSE;

	capGetVideoFormat(m_hWndCap,&capinfo,sizeof(BITMAPINFO));
	if(capinfo.bmiHeader.biBitCount != 24)
	{
		capinfo.bmiHeader.biBitCount = 24;
		capinfo.bmiHeader.biCompression = 0;
		capinfo.bmiHeader.biSizeImage = capinfo.bmiHeader.biWidth * capinfo.bmiHeader.biHeight * ((capinfo.bmiHeader.biBitCount)/8);
		BOOL tf = (BOOL)capSetVideoFormat(m_hWndCap,&capinfo,sizeof(BITMAPINFO));
		if(tf == TRUE)
		{
			printf("cam setting Success\n");
		}
		else
		{
			printf("cam setting faild\n");
		}		
	}
	capGetVideoFormat(m_hWndCap,&capinfo,sizeof(BITMAPINFO));
	pImageBuffer = new unsigned char[capinfo.bmiHeader.biWidth * capinfo.bmiHeader.biHeight * ((capinfo.bmiHeader.biBitCount)/8)];

	capPreviewRate(m_hWndCap, 33);
	capOverlay(m_hWndCap, false);	//
	capPreview(m_hWndCap, true);	//GDI 에 바로 보여줄것인지 설정 

	buf_size = capinfo.bmiHeader.biSizeImage;

	
	return TRUE;  // return TRUE  unless you set the focus to a control
}
unsigned char* ImageCameraCtrl::getCapture()
{
	return pImageBuffer;
}

LRESULT CALLBACK ImageCameraCtrl::capCallbackOnFrame(HWND hWnd, LPVIDEOHDR lpVHdr)
{	
	memcpy(pImageBuffer,lpVHdr->lpData,sizeof(unsigned char) * capinfo.bmiHeader.biSizeImage );

	unsigned int uiBuflen = lpVHdr->dwBufferLength;
	unsigned char RGB[240][360][3]={0,};
	unsigned int nWidth, nHeight;
	unsigned int i, j;
	int Y0, U, Y1, V;

	//     The "YUY2" YUV pixel format looks like this: 
	//         As a series of BYTES:    [Y0][U][Y1][V] (reverse it for a DWORD) 
	//
	//     As you can see, both formats pack two pixels into a single DWORD.
	//     The pixels share U and V components and have separate Y components. 

	nWidth = capinfo.bmiHeader.biWidth ;
	nHeight = capinfo.bmiHeader.biHeight;

	// YUY2 ---> RGB
	for(j = 0; j < nHeight; j ++)
	{	// height
		for(i = 0; i < nWidth; i += 2 ) 
		{	//width
			Y0 =	lpVHdr->lpData[(nWidth*j + i) * 2];
			U  =	lpVHdr->lpData[(nWidth*j + i) * 2 + 1];
			Y1 =	lpVHdr->lpData[(nWidth*j + i) * 2 + 2];
			V  =	lpVHdr->lpData[(nWidth*j + i) * 2 + 3];

			pImageBuffer[(j*nWidth) + i + RED] = (int) CLIP( Y0 + (1.4075*(V-128)));
			pImageBuffer[(j*nWidth) + i + GREEN] = (int) CLIP( Y0 - 0.3455*(U-128) - 0.7169*(V-128));
			pImageBuffer[(j*nWidth) + i + BLUE] = (int) CLIP( Y0 + 1.7790*(U-128));

			pImageBuffer[(j*nWidth) + (i+1) + RED] = (int) CLIP( Y1 + (1.4075*(V-128)));
			pImageBuffer[(j*nWidth) + (i+1) + GREEN] = (int) CLIP( Y1 - 0.3455*(U-128) - 0.7169*(V-128));
			pImageBuffer[(j*nWidth) + (i+1) + BLUE] = (int) CLIP( Y1 + 1.7790*(U-128));
		}
	}
	//// RGB ---> YUY2
	//for(j = 0; j < nHeight; j ++) 
	//{	// height
	//	for(i = 0; i < nWidth; i += 2 ) 
	//	{	//width
	//		Y0 = (int) CLIP(0.2999*RGB[j][i][RED] + 0.587*RGB[j][i][GREEN] + 0.114*RGB[j][i][BLUE]);
	//		Y1 = (int) CLIP(0.2999*RGB[j][i+1][RED] + 0.587*RGB[j][i+1][GREEN] + 0.114*RGB[j][i+1][BLUE]);
	//		U  = (int) CLIP(-0.1687*RGB[j][i][RED] - 0.3313*RGB[j][i][GREEN] +0.5*RGB[j][i][BLUE] + 128.0);
	//		V  = (int) CLIP( 0.5*RGB[j][i][RED] - 0.4187*RGB[j][i][GREEN] - 0.0813*RGB[j][i][BLUE] + 128.0);

	//		lpVHdr->lpData[(nWidth*j + i) * 2] = Y0;
	//		lpVHdr->lpData[(nWidth*j + i) * 2 + 1] = U;
	//		lpVHdr->lpData[(nWidth*j + i) * 2 + 2] = Y1;
	//		lpVHdr->lpData[(nWidth*j + i) * 2 + 3] = V;

	//	}
	//}
	return (LRESULT)TRUE;
}
int ImageCameraCtrl::Getcap_width()
{
	return capinfo.bmiHeader.biWidth;
}
int ImageCameraCtrl::Getcap_height()
{
	return capinfo.bmiHeader.biHeight;
}
BITMAPINFO ImageCameraCtrl::Getcap_BITMAPINFO()
{
	return capinfo;
}