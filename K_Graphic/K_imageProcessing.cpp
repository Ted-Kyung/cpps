#include "StdAfx.h"
#include "K_imageProcessing.h"
#include <math.h>

imageProcessing::imageProcessing(void)
{
	mp_callback = NULL;
	m_StaticpictureFile		= NULL;
	m_pictureFile			= NULL;
	bitCnt					= 0;
	lowDataCnt				= 0;
	picBit					= NULL;
	staticpicBit			= NULL;
}
imageProcessing::imageProcessing(CWnd* mainwindow, int nID, ImageProcessing_CallBack* callback)
{
	mp_callback = callback;
	m_StaticpictureFile		= NULL;
	m_pictureFile			= NULL;
	bitCnt					= 0;
	lowDataCnt				= 0;
	picBit					= NULL;
	staticpicBit			= NULL;
	CreateDC(mainwindow,nID);
}
imageProcessing::~imageProcessing(void)
{
	if(lpBits != NULL)	free(lpBits);
	if(staticpicBit != NULL)	free(staticpicBit);
	if(picBit != NULL)	free(picBit);

	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	lpBits				= NULL;
	image				= NULL;	
	picBit					= NULL;
	staticpicBit			= NULL;
	m_bitmap.DeleteObject();
	m_mDC.DeleteDC();
}
void imageProcessing::offsetLine(int x , int y , int toX, int toY, COLORREF clr)
{
	x = x - GetImageParm().dcXoffset;
	toX = toX - GetImageParm().dcXoffset;
	y = y - GetImageParm().dcYoffset;
	toY = toY - GetImageParm().dcYoffset;

	if((x<0)
		||(y<0)
		||(x>=m_Image_Width)
		||(y>=m_Image_Height)
		||abs((x-toX)+(y-toY))<2
		) return;

	int			distx	= toX - x; 
	int			disty	= toY - y;
	double		dist	= (double)sqrt((double)(distx*distx)+(double)(disty*disty));
	double		mov_x = (double)distx/(double)dist;
	double		mov_y = (double)disty/(double)dist;

	double		ang	= (double)disty / (double)distx;

	double		bufX = 0.0;
	double		bufY = 0.0;

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));
	int			cnt = 0;

	bufX = x;
	bufY = y;

	bool chkx = false , chky = false;
	while(1)
	{
		setpixCLR((int)bufX,(int)bufY,red,green,blue);
		if(abs(bufX-(double)toX) < 1.0)
		{
			chkx = true;
		}
		else
		{
			bufX = (double)bufX + (double)mov_x;
		}
		if(abs(bufY-(double)toY) < 1.0)
		{
			chky = true;
		}
		else
		{
			bufY = (double)bufY + (double)mov_y;
		}
		if((chkx & chky) == true) break;
	}
}
void imageProcessing::Line(int x , int y , int toX, int toY, COLORREF clr)
{
	if((x<0)||(y<0)||(x>=m_Image_Width)||(y>=m_Image_Height)) return;

	int			distx	= toX - x; 
	int			disty	= toY - y;
	double		dist	= (double)sqrt((double)(distx*distx)+(double)(disty*disty));
	double		mov_x = (double)distx/(double)dist;
	double		mov_y = (double)disty/(double)dist;

	double		ang	= (double)disty / (double)distx;

	double		bufX = 0.0;
	double		bufY = 0.0;

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));
	int			cnt = 0;

	bufX = x;
	bufY = y;
	bool chkx = false , chky = false;
	while(1)
	{
		setpixCLR((int)bufX,(int)bufY,red,green,blue);
		if(abs(bufX-(double)toX) < 1.0)
		{
			chkx = true;
		}
		else
		{
			bufX = (double)bufX + (double)mov_x;
		}
		if(abs(bufY-(double)toY) < 1.0)
		{
			chky = true;
		}
		else
		{
			bufY = (double)bufY + (double)mov_y;
		}
		if((chkx & chky) == true) break;
	}
}
void imageProcessing::setpixCLR(int non_offset_x,int non_offset_y, int red,int green,int blue)
{
	if((non_offset_x<0)||(non_offset_y<0)||(non_offset_x>=m_Image_Width)||(non_offset_y>=m_Image_Height)) return;

	if(bitcnt_size == 4)	//32bit
	{
		lpBits[ (non_offset_x*4 + (non_offset_y * m_bit_info.bmWidthBytes) ) + 2] = red;
		lpBits[ (non_offset_x*4 + (non_offset_y * m_bit_info.bmWidthBytes) ) + 1] = green;
		lpBits[ (non_offset_x*4 + (non_offset_y * m_bit_info.bmWidthBytes) ) + 0] = blue;
	}
	else if(bitcnt_size == 2)	//16bit
	{
		unsigned short rgb16Buf = 0;
		rgb16Buf = (0x1F) & (red >> 3);		
		rgb16Buf = (rgb16Buf<<5) | (0x3F) & (green >> 2);
		rgb16Buf = (rgb16Buf<<6) | (0x1F) & (blue >> 3);
		BYTE up		= (BYTE)(0xFF & rgb16Buf>>8);
		BYTE low	= (BYTE)(0xFF & rgb16Buf);
		lpBits[non_offset_x*2 + (non_offset_y * m_bit_info.bmWidthBytes)] = low;
		lpBits[non_offset_x*2 + (non_offset_y * m_bit_info.bmWidthBytes)+1] = up;
	}
	
}
void imageProcessing::setpixCLR(int x,int y, COLORREF clr)
{
	x = x - m_Parm.dcXoffset;
	y = y - m_Parm.dcYoffset;

	if((x<0)||(y<0)||(x>=m_Image_Width)||(y>=m_Image_Height)) return;

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));

	if(bitcnt_size == 4)	//32bit
	{
		lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 2] = red;
		lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 1] = green;
		lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 0] = blue;
	}
	else if(bitcnt_size == 2)	//16bit
	{
		unsigned short rgb16Buf = 0;
		rgb16Buf = (0x1F) & (red >> 3);		
		rgb16Buf = (rgb16Buf<<5) | (0x3F) & (green >> 2);
		rgb16Buf = (rgb16Buf<<6) | (0x1F) & (blue >> 3);
		BYTE up		= (BYTE)(0xFF & rgb16Buf>>8);
		BYTE low	= (BYTE)(0xFF & rgb16Buf);
		lpBits[x*2 + (y * m_bit_info.bmWidthBytes)] = low;
		lpBits[x*2 + (y * m_bit_info.bmWidthBytes)+1] = up;
	}
}
void imageProcessing::setPicture(char* path)
{
	HBITMAP picture = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),path,IMAGE_BITMAP,0,0,LR_LOADFROMFILE|LR_CREATEDIBSECTION);
	m_mDC.SelectObject(picture);	

	m_pictureFile = NULL;
	m_pictureFile = fopen(path,"rb");
	if(m_pictureFile == NULL) 
	{
		printf("file open faild\n");
		return;
	}
	fread(&m_pictureBitFileHead,sizeof(BITMAPFILEHEADER),1,m_pictureFile);
	fread(&m_pictureBitInfo,sizeof(BITMAPINFOHEADER),1,m_pictureFile);
	fclose(m_pictureFile);
}
void imageProcessing::setBITPicture(char* path)
{
	m_pictureFile = NULL;
	m_pictureFile = fopen(path,"rb");
	if(m_pictureFile == NULL) 
	{
		printf("file open faild\n");
		return;
	}
	fread(&m_pictureBitFileHead,sizeof(BITMAPFILEHEADER),1,m_pictureFile);
	fread(&m_pictureBitInfo,sizeof(BITMAPINFOHEADER),1,m_pictureFile);

	picBit = (unsigned char*)malloc(GetLowDataCnt(m_pictureBitInfo));

	int err = fread(picBit,sizeof(unsigned char) * GetLowDataCnt(m_pictureBitInfo),1,m_pictureFile);
	fclose(m_pictureFile);

	bInfo.bmiHeader = m_pictureBitInfo;

	m_mDC.SetStretchBltMode(HALFTONE);
	StretchDIBits(m_mDC.GetSafeHdc(),0,0
		, GetImageParm().dcRight,GetImageParm().dcBottem
		,0
		,0
		,m_pictureBitInfo.biWidth
		, m_pictureBitInfo.biHeight
		,picBit,&bInfo,DIB_RGB_COLORS,SRCCOPY);

	free(picBit);
}
void imageProcessing::setBITPicture(unsigned char* data,int width, int height)
{
	m_mDC.SetStretchBltMode(HALFTONE);
	StretchDIBits(m_mDC.GetSafeHdc(),0,0
		, GetImageParm().dcRight,GetImageParm().dcBottem
		, 0,0,m_pictureBitInfo.biWidth
		, m_pictureBitInfo.biHeight,data,&bInfo,DIB_RGB_COLORS,SRCCOPY);
}
void imageProcessing::setBITPicture(BITMAPINFO info,unsigned char* data,int width, int height)
{
	m_mDC.SetStretchBltMode(HALFTONE);
	StretchDIBits(m_mDC.GetSafeHdc(),0,0
		, GetImageParm().dcRight,GetImageParm().dcBottem
		, 0,0,m_pictureBitInfo.biWidth
		, m_pictureBitInfo.biHeight,data,&info,DIB_RGB_COLORS,SRCCOPY);
}
void imageProcessing::setBITPicture(char* path,int shift_x,int shift_y,int zoom)
{
	m_pictureFile = NULL;
	m_pictureFile = fopen(path,"rb");
	if(m_pictureFile == NULL) 
	{
		printf("file open faild\n");
		return;
	}
	fread(&m_pictureBitFileHead,sizeof(BITMAPFILEHEADER),1,m_pictureFile);
	fread(&m_pictureBitInfo,sizeof(BITMAPINFOHEADER),1,m_pictureFile);

	int i = GetLowDataCnt(m_pictureBitInfo);

	picBit = (unsigned char*)malloc(GetLowDataCnt(m_pictureBitInfo));

	fread(picBit,sizeof(unsigned char) * GetLowDataCnt(m_pictureBitInfo),1,m_pictureFile);
	fclose(m_pictureFile);

	bInfo.bmiHeader = m_pictureBitInfo;

	m_mDC.SetStretchBltMode(HALFTONE);
	StretchDIBits(m_mDC.GetSafeHdc(),0,0
		, GetImageParm().dcRight + zoom,GetImageParm().dcBottem + zoom
		, shift_x
		, shift_y
		, m_pictureBitInfo.biWidth
		, m_pictureBitInfo.biHeight
		,picBit,&bInfo,DIB_RGB_COLORS,SRCCOPY);

	free(picBit);
}

int	imageProcessing::GetBitCount(BITMAPINFOHEADER info)
{
	return info.biBitCount;
}
int	imageProcessing::GetLowDataCnt(BITMAPINFOHEADER info)
{
	return info.biWidth * (GetBitCount(info) / 8) * info.biHeight;
}
int	imageProcessing::GetPicWidth()
{
	return m_pictureBitInfo.biWidth;
}
int	imageProcessing::GetPicHeight()
{
	return m_pictureBitInfo.biHeight;
}
void imageProcessing::GraphShiftLeftBitMap(int centor, COLORREF clr)
{

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));


	if(bitcnt_size == 4)	//32bit
	{
		for(int x = 0; x<m_Parm.dcRight ; x++)
		{
			for(int y = 0; y<m_Parm.dcBottem ; y++)
			{
				if(x < m_Parm.dcRight-2)
				{
					lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 2] = lpBits[ ((x+2)*4 + (y * m_bit_info.bmWidthBytes) ) + 2];
					lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 1] = lpBits[ ((x+2)*4 + (y * m_bit_info.bmWidthBytes) ) + 1];
					lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 0] = lpBits[ ((x+2)*4 + (y * m_bit_info.bmWidthBytes) ) + 0];
				}
				else if(x >= m_Parm.dcRight-2)
				{
					if(y == centor)
					{
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 2] = red;
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 1] = green;
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 0] = blue;
					}
					else
					{
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 2] = 0;
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 1] = 0;
						lpBits[ (x*4 + (y * m_bit_info.bmWidthBytes) ) + 0] = 0;
					}
				}			

			}
		}
	}
	else if(bitcnt_size == 2)	//16bit
	{

		unsigned short rgb16Buf = 0;
		rgb16Buf = (0x1F) & (red >> 3);		
		rgb16Buf = (rgb16Buf<<5) | (0x3F) & (green >> 2);
		rgb16Buf = (rgb16Buf<<6) | (0x1F) & (blue >> 3);
		BYTE up		= (BYTE)(0xFF & rgb16Buf>>8);
		BYTE low	= (BYTE)(0xFF & rgb16Buf);


		for(int x = 0; x<m_Parm.dcRight ; x++)
		{
			for(int y = 0; y<m_Parm.dcBottem ; y++)
			{
				if(x < m_Parm.dcRight-2)
				{
					lpBits[x*2 + (y * m_bit_info.bmWidthBytes)] = lpBits[(x+2)*2 + (y * m_bit_info.bmWidthBytes)];
					lpBits[x*2 + (y * m_bit_info.bmWidthBytes)+1] = lpBits[(x+2)*2 + (y * m_bit_info.bmWidthBytes)+1];
				}
				else if(x >= m_Parm.dcRight-2)
				{
					if(y == centor)
					{
						lpBits[x*2 + (y * m_bit_info.bmWidthBytes)] = low;
						lpBits[x*2 + (y * m_bit_info.bmWidthBytes)+1] = up;
					}
					else
					{
						lpBits[x*2 + (y * m_bit_info.bmWidthBytes)] = 0;
						lpBits[x*2 + (y * m_bit_info.bmWidthBytes)+1] = 0;
					}
				}			

			}
		}
		int as = 1;
	}
}

void imageProcessing::setStaticBITPicture(char* path)
{
	if(m_StaticpictureFile != NULL) 
	{
		delete m_StaticpictureFile;
		m_StaticpictureFile = NULL;
	}
	if(staticpicBit != NULL) 
	{
		free(staticpicBit);
		staticpicBit = NULL;
	}	

	m_StaticpictureFile = fopen(path,"rb");

	if(m_StaticpictureFile == NULL) 
	{
		C_RED;printf("MAP Image Open Error\n");C_WHITE;
		mp_callback->callback_ERR_MapFileOpen();
		return;
	}
	mp_callback->callback_ERRNONE_MapFileOpen();

	fread(&m_pictureBitFileHead,sizeof(BITMAPFILEHEADER),1,m_StaticpictureFile);
	fread(&m_pictureBitInfo,sizeof(BITMAPINFOHEADER),1,m_StaticpictureFile);

	staticpicBit = (unsigned char*)malloc(GetLowDataCnt(m_pictureBitInfo));

	int err = fread(staticpicBit,sizeof(unsigned char) * GetLowDataCnt(m_pictureBitInfo),1,m_StaticpictureFile);
	fclose(m_StaticpictureFile);

	bInfo.bmiHeader = m_pictureBitInfo;
}
void imageProcessing::StaticBITPictureMove(int shift_x,int shift_y,int zoom)
{
	m_mDC.SetStretchBltMode(COLORONCOLOR);

	if(shift_x < 0) shift_x = 0;
	if(shift_y < 0) shift_y = 0;
	if(shift_x+zoom > m_pictureBitInfo.biWidth) shift_x = m_pictureBitInfo.biWidth - zoom;
	if(shift_y+zoom > m_pictureBitInfo.biHeight) shift_y = m_pictureBitInfo.biHeight - zoom;

	StretchDIBits(m_mDC.GetSafeHdc(),0,0
		, GetImageParm().dcRight,GetImageParm().dcBottem
		, shift_x
		, shift_y
		, zoom
		, zoom
		, staticpicBit,&bInfo,DIB_RGB_COLORS,SRCCOPY);	

	ReDrawDC();
}
void imageProcessing::setStaticpixCLR(int non_offset_x,int non_offset_y, int red,int green,int blue)
{
	if((non_offset_x<0)||(non_offset_y<0)||(non_offset_x>m_pictureBitInfo.biWidth)||(non_offset_y>m_pictureBitInfo.biHeight)) return;

	non_offset_y = m_pictureBitInfo.biHeight - non_offset_y;

	//if(bitcnt_size == 4)	//32bit
	{
		staticpicBit[ ((non_offset_x)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ ((non_offset_x)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ ((non_offset_x)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ ((non_offset_x-1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ ((non_offset_x-1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ ((non_offset_x-1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ ((non_offset_x+1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ ((non_offset_x+1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ ((non_offset_x+1)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ (non_offset_x*3 + ((non_offset_y+1) * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y+1) * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y+1) * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ (non_offset_x*3 + ((non_offset_y-1) * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y-1) * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y-1) * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ ((non_offset_x-2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ ((non_offset_x-2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ ((non_offset_x-2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ ((non_offset_x+2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ ((non_offset_x+2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ ((non_offset_x+2)*3 + (non_offset_y * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ (non_offset_x*3 + ((non_offset_y+2) * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y+2) * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y+2) * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

		staticpicBit[ (non_offset_x*3 + ((non_offset_y-2) * m_pictureBitInfo.biWidth * 3) ) + 2] = red;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y-2) * m_pictureBitInfo.biWidth * 3) ) + 1] = green;
		staticpicBit[ (non_offset_x*3 + ((non_offset_y-2) * m_pictureBitInfo.biWidth * 3) ) + 0] = blue;

	}
	//else if(bitcnt_size == 2)	//16bit
	//{
	//	unsigned short rgb16Buf = 0;
	//	rgb16Buf = (0x1F) & (red >> 3);		
	//	rgb16Buf = (rgb16Buf<<5) | (0x3F) & (green >> 2);
	//	rgb16Buf = (rgb16Buf<<6) | (0x1F) & (blue >> 3);
	//	BYTE up		= (BYTE)(0xFF & rgb16Buf>>8);
	//	BYTE low	= (BYTE)(0xFF & rgb16Buf);
	//	staticpicBit[non_offset_x*2 + (non_offset_y * m_pictureBitInfo.biWidth * 2)] = low;
	//	staticpicBit[non_offset_x*2 + (non_offset_y * m_pictureBitInfo.biWidth * 2)+1] = up;
	//}
}
void imageProcessing::StaticLine(int x , int y , int toX, int toY, COLORREF clr)
{
	if((x<0)||(y<0)||(x>m_pictureBitInfo.biWidth)||(y>m_pictureBitInfo.biHeight)) return;
	if((toX<0)||(toY<0)||(toX>m_pictureBitInfo.biWidth)||(toY>m_pictureBitInfo.biHeight)) return;

	int			distx	= toX - x; 
	int			disty	= toY - y;
	double		dist	= (double)sqrt((double)(distx*distx)+(double)(disty*disty));
	double		mov_x = (double)distx/(double)dist;
	double		mov_y = (double)disty/(double)dist;

	double		ang	= (double)disty / (double)distx;

	double		bufX = 0.0;
	double		bufY = 0.0;

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));
	int			cnt = 0;

	bufX = x;
	bufY = y;
	bool chkx = false , chky = false;
	while(1)
	{
		setStaticpixCLR((int)bufX,(int)bufY,red,green,blue);
		if(abs(bufX-(double)toX) < 1.0)
		{
			chkx = true;
		}
		else
		{
			bufX = (double)bufX + (double)mov_x;
		}
		if(abs(bufY-(double)toY) < 1.0)
		{
			chky = true;
		}
		else
		{
			bufY = (double)bufY + (double)mov_y;
		}
		if((chkx & chky) == true) break;
	}
}
void imageProcessing::StaticCircle(int x , int y ,int R, COLORREF clr)
{
	if((x<0)||(y<0)||(x>m_pictureBitInfo.biWidth)||(y>m_pictureBitInfo.biHeight)) return;

	double		bufX = 0.0;
	double		bufY = 0.0;

	int			red = (int)(0x000000FF & (BYTE)(clr));
	int			green = (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue = (int)(0x000000FF & (BYTE)(clr>>16));
	int			cnt = 0;

	bufX = (double)x;
	bufY = (double)y;

	setStaticpixCLR((int)bufX,(int)bufY,red,green,blue);

	for(int i = 0 ; i < 360 ; i++)
	{
		bufX = (int)(R * cos((double)i /180.0 * 3.141592));
		bufY = (int)(R * sin((double)i /180.0 * 3.141592));

		bufX = bufX + (double)x;
		bufY = bufY + (double)y;

		setStaticpixCLR((int)bufX,(int)bufY,red,green,blue);
	}
}
void imageProcessing::DeleteStaticBITPicture()
{
	if(m_StaticpictureFile != NULL) 
	{
		delete m_StaticpictureFile;
		m_StaticpictureFile = NULL;
	}
	if(staticpicBit != NULL) 
	{
		free(staticpicBit);
		staticpicBit = NULL;
	}	
}