#pragma once
#include "K_ImageCtrl.h"
#include "K_ImageCameraCtrl.h"

class ImageProcessing_CallBack
{
public:
	virtual void callback_ERR_MapFileOpen(void) = 0;
	virtual void callback_ERRNONE_MapFileOpen(void) = 0;
};

class imageProcessing : public ImageCtrl
{
public:
	imageProcessing(void);
	imageProcessing(CWnd* mainwindow, int nID,ImageProcessing_CallBack* callback);
	~imageProcessing(void);

	ImageProcessing_CallBack* mp_callback;

	void	Line(int x , int y , int toX, int toY, COLORREF clr);
	void	StaticLine(int x , int y , int toX, int toY, COLORREF clr);
	void	StaticCircle(int x , int y ,int R, COLORREF clr);
	void	offsetLine(int x , int y , int toX, int toY, COLORREF clr);
	void	setpixCLR(int x,int y, COLORREF clr);
	void	setpixCLR(int non_offset_x,int non_offset_y, int red,int green,int blue);
	void	setStaticpixCLR(int non_offset_x,int non_offset_y, int red,int green,int blue);

	void	setPicture(char* path);
	void	setBITPicture(char* path);	
	void	setBITPicture(char* path,int shift_x,int shift_y,int zoom);
	void	setBITPicture(unsigned char* data,int width, int height);
	void	setBITPicture(BITMAPINFO info,unsigned char* data,int width, int height);

	void	setStaticBITPicture(char* path);
	void	StaticBITPictureMove(int shift_x,int shift_y,int zoom);
	void	DeleteStaticBITPicture();
	
	int		GetPicWidth();
	int		GetPicHeight();

	void	GraphShiftLeftBitMap(int centor, COLORREF clr);

protected:
	int		GetBitCount(BITMAPINFOHEADER info);
	int		GetLowDataCnt(BITMAPINFOHEADER info);

	FILE*				m_pictureFile;
	FILE*				m_StaticpictureFile;
	BITMAPINFOHEADER	m_pictureBitInfo;
	BITMAPFILEHEADER	m_pictureBitFileHead;
	int					bitCnt;
	int					lowDataCnt;
	unsigned char*		picBit;
	unsigned char*		staticpicBit;
	BITMAPINFO			bInfo;
};

