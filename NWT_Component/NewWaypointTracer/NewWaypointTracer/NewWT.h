#pragma once
#include "sockNetwork.h"
#include "NWT_data.h"

struct Robot
{
	int rx;
	int ry;
	int rh;
};
struct Waypoint
{
	int wx;
	int wy;
};

class NewWT : public sockNetwork
{
public:
	NewWT(void);
public:
	~NewWT(void);

	NWT_data positionData;
	NWT_Driving_Data drivingData;

	Robot robot;	
	Waypoint waypoint;
	double m_speed;
	double robot_to_waypoint_heading_radian	;		//로봇에서 포인트를 바라보기위한 헤딩 라디안각도
	int	   robot_to_waypoint_heading_degree ;		//로봇에서 포인트를 바라보기위한 헤딩 각도

	void Control_Driving(void);
	double GetWaypointHeading(Robot robot,Waypoint waypoint);
	NWT_Driving_Data Control_Accelation(Robot robot,Waypoint waypoint,double m_speed,int steering);
	int Control_Steering(Robot robot,Waypoint waypoint,double m_speed);

};
