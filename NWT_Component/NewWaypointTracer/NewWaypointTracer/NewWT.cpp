#include "NewWT.h"
#include "NWT_data.h"
#include <math.h>
#define PI 3.141592
#define HALF_PI PI/2
#define POINTSIGHT_DEGREE 90
#define PERCENTAGE 100
#define VEHICLE_MAX_SPEED 14


NewWT::NewWT(void)
{
}

NewWT::~NewWT(void)
{
}

void main(void)
{
	int w;
	NewWT* NWT = new NewWT;
	sockNetwork* simulator = new sockNetwork("127.0.0.1",8888);						//소켓네트웍 활성화

	while(1)
	{
		if(simulator->RecvDATA((char*)&(NWT->positionData),sizeof(NWT_data)) != NULL)
		{
			printf("rx = %d, ry = %d, rh =%d, wx = %d , wy = %d speed = %.1lf\n"
				,NWT->positionData.rx
				,NWT->positionData.ry
				,NWT->positionData.rh
				,NWT->positionData.wx
				,NWT->positionData.wy
				,NWT->positionData.speed);
			NWT->Control_Driving();			// Recv후 알고리즘 주함수 호출 
			w = simulator->SendData((char*)&NWT->drivingData, sizeof(NWT_Driving_Data));
		}
	}
}

void NewWT::Control_Driving(void)
{
	int steering;

	m_speed = positionData.speed;		//GPS 에서 주는 속도 값 대신 상수로 처리

	//robot 정보 저장 
	robot.rx = positionData.rx;
	robot.ry = positionData.ry;
	robot.rh = positionData.rh;
	
	//포인트 정보 저장
	waypoint.wx = positionData.wx;
	waypoint.wy = positionData.wy;

	steering	= Control_Steering(robot,waypoint,m_speed);
	drivingData = Control_Accelation(robot,waypoint,m_speed, steering);
	
}

int NewWT::Control_Steering(Robot robot,Waypoint waypoint,double m_speed)
{
	double heading;
	int steering;
	bool leftright =  FALSE;


	heading	= GetWaypointHeading(robot,waypoint);

	if(heading >= 0)
	{
		leftright = TRUE;
	}
	else 
	{
		leftright = FALSE;
	}
	
	heading = abs(heading);
		
	double p1 =  6.984e-014 ;
	double p2 = -4.933e-011 ;
	double p3 =  1.391e-008 ;
	double p4 = -1.985e-006 ;
	double p5 =   0.0001507 ;
	double p6 =   -0.005995 ;
	double p7 =      0.1263 ;
	double p8 =     -0.5922 ;
	double p9 =      0.1135 ;

	steering = (int)(p1*pow(heading,8) + p2*pow(heading,7) + p3*pow(heading,6) + p4*pow(heading,5) + 
		p5*pow(heading,4) + p6*pow(heading,3) + p7*pow(heading,2) + p8*heading + p9);

	if(steering >= 100 )
	{
		steering = 100;
	}
	if(steering <= 0)
	{
		steering = 0;
	}

	if(leftright == FALSE)
	{
		steering = steering * -1;
	}

	return steering;
}
NWT_Driving_Data NewWT::Control_Accelation(Robot robot,Waypoint waypoint,double m_speed,int steering)
{
	
	memset((char*)&drivingData,NULL,sizeof(NWT_Driving_Data));

	int Accelation = 0;
	int out_accelation = 0;
	double str = abs(steering);

	double p1 =  2.003e-013 ;
	double p2 = -8.464e-011 ;
	double p3 =  1.479e-008 ;
	double p4 = -1.385e-006 ;
	double p5 =  7.534e-005 ;
	double p6 =   -0.002422 ;
	double p7 =     0.04441 ;
	double p8 =     -0.4258 ;
	double p9 =       1.435 ;
	double p10 =        100 ;
		
	Accelation = (int)(p1*pow(str,9) + p2*pow(str,8) + p3*pow(str,7) + p4*pow(str,6) + 
		p5*pow(str,5) + p6*pow(str,4) + p7*pow(str,3) + p8*pow(str,2) + p9*str + p10);


	out_accelation = (int)((m_speed / VEHICLE_MAX_SPEED) * 100.0) * (double)Accelation / 100.0  ;
 
	if(Accelation <= 0)
	{
		out_accelation = 0; 
	}

	drivingData.Accelation = out_accelation;
	drivingData.Steering = steering;

	printf("Accelation => %d , steering => %d\n",drivingData.Accelation, drivingData.Steering);

	return drivingData;
	
}

double NewWT::GetWaypointHeading(Robot robot,Waypoint waypoint)
{
	double difference_x		= 0.0;						//원점 기준으로의 거리를 구하기 위한 변수
	double difference_y		= 0.0;						//원점 기준으로의 거리를 구하기 위한 변수
	double direct_distance	= 0.0;						//원점 기준으로의 거리

	double rotate_x;									//로봇 회전된 좌표의 x 
	double rotate_y;									//로봇 회전된 좌표의 y
	char   direction = '0';								//좌우 구분 변수 0 : Right 1:Left

	
	bool check = FALSE;									//헤딩을 -180~ 180 으로 표현하기 위한 예외 처리 
	while(1)
	{
		if(robot.rh > 360)
		{
			robot.rh = robot.rh - 360;
			check = TRUE;
		}
		else if(robot.rh > 180)
		{
			robot.rh = -360 + robot.rh;
			check = TRUE;
		}
		else if(robot.rh == -180)
		{
			robot.rh = 180;
			check = TRUE;
		}
		else
		{
			check = FALSE;							//모든것에 해당이 안되면 예외처리완료로 생각하고 Break;
		}
		
		if(check == FALSE) break;
	}

	//삼각함수는 radian 을 매개변수로 받으므로 degree / (180.0 * PI) 해주어서 입력. 
	//로봇헤딩각이 회전 될때의 회전 x,y좌표	
	rotate_x = (double)((waypoint.wx - robot.rx) * cos((double)(robot.rh * PI/180.0)) - (waypoint.wy - robot.ry) * sin((double)(robot.rh * PI/180.0)));
	rotate_y = (double)((waypoint.wx - robot.rx) * sin((double)(robot.rh * PI/180.0)) + (waypoint.wy - robot.ry) * cos((double)(robot.rh * PI/180.0)));	

	if(rotate_x >= 0)
	{
		direction = '1';	//회전시 x 좌표가 양수면 1 
	}
	else if(rotate_x < 0)
	{
		direction = '2';	//회전시 x 좌표가 음수면 2
	}

	difference_x = (double)(waypoint.wx - robot.rx);		//로봇점 기준부터 x 축으로 포인트가 떨어진 거리
	difference_y = (double)(waypoint.wy - robot.ry);		//로봇점 기준부터 y 축으로 포인트가 떨어진 거리
	direct_distance = (double)(sqrt((double)(difference_x*difference_x) + (double)(difference_y*difference_y)));	//로봇점에서 포인트까지의 직선 거리
	double r_buf = 0;

	r_buf= rotate_y / direct_distance;
	robot_to_waypoint_heading_radian = (double)acos(r_buf);
	if(direction == '1')
	{
		robot_to_waypoint_heading_degree = (int)(robot_to_waypoint_heading_radian /PI*180.0);	//라디안값 -> 디그리 변환
	}
	if(direction == '2')
	{
		robot_to_waypoint_heading_degree = (int)(-robot_to_waypoint_heading_radian /PI*180.0);	//라디안값 -> 디그리 변환
	}
	if(robot_to_waypoint_heading_degree < 0 )
	{
		int a = 1;
	}

	printf("robot to point degree => %d\n",robot_to_waypoint_heading_degree);

	return robot_to_waypoint_heading_degree;	//디그리값 리턴 
}