// NWT_SimulationDlg.h : 헤더 파일
//

#pragma once
#include "SockNet.h"
#include "afxwin.h"
#include "NWT_data.h"
#include "resource.h"


// CNWT_SimulationDlg 대화 상자
class CNWT_SimulationDlg : public CDialog, public Interface
{
// 생성입니다.
public:
	CNWT_SimulationDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NWT_SIMULATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CSocket server;
	SockNet* litner;
	BOOL check_Network;
	NWT_data positionData;
	NWT_Driving_Data drivingData;
	int center_x,center_y;
	CPoint wpoint;

	CBitmap r_bit;
	BITMAP r_bit_info;
	HBITMAP hbit;
	double h_rad;

	int cpoint_x;
	int cpoint_y;

	int m_movex;
	int m_movey;
	int m_moveh;


	void GRID(int x, int y, double heading);
	void bitBKgnd(void);
	void robotgrid(int x, int y, double heading);
	HBITMAP GetRotatedBitmap( HDC hdc, HBITMAP hBitmap, double radians, COLORREF clrBack ); 


public:
	CStatic m_image;
	CDC* m_imageDC;
	CRect m_windowRect;
	CDC m_mDC;
	CBitmap m_bit;
	CDC m_off;
	CBitmap m_off_bit;
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButton3();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton1();

public:
	virtual void On_DlgRecv(NWT_Driving_Data data);
};
