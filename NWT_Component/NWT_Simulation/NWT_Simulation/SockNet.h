#pragma once
#include "NWT_data.h"
//#include "NWT_SimulationDlg.h"

// SockNet 명령 대상입니다.

//데이터 인터페이스 만들기 
class Interface
{
public:
	virtual void On_DlgRecv(NWT_Driving_Data data) = 0;
};

class SockNet : public CAsyncSocket
{
public:
	SockNet(Interface* pInterface);
	~SockNet();

	Interface* m_pInterface;
	CDialog* m_poW;
	NWT_Driving_Data drivingData;
	virtual void OnReceive(int nErrorCode);
};


