//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NWT_Simulation.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_NWT_SIMULATION_DIALOG       102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDC_BUTTON3                     1005
#define IDC_BUTTON4                     1006
#define IDC_EDIT5                       1007
#define IDC_EDIT6                       1008
#define IDC_EDIT8                       1010
#define IDC_EDIT9                       1011
#define IDC_IMAGE                       1013
#define IDC_EDIT1                       1014
#define IDC_EDIT2                       1015
#define IDC_BUTTON1                     1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
