#pragma once

struct NWT_data 
{
	int rx;
	int ry;
	int rh;
	int wx;
	int wy;
	double speed;
};

struct NWT_Driving_Data
{
	int Accelation;
	int Steering;
};