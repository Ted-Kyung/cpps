// NWT_SimulationDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NWT_Simulation.h"
#include "NWT_SimulationDlg.h"
#include "NWT_data.h"
#include <math.h>

#define STEERING_INT 4.7
#define MOVING_INT 10

#define PI 3.141592
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CNWT_SimulationDlg 대화 상자




CNWT_SimulationDlg::CNWT_SimulationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNWT_SimulationDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNWT_SimulationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE, m_image);
}

BEGIN_MESSAGE_MAP(CNWT_SimulationDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_RBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON4, &CNWT_SimulationDlg::OnBnClickedButton4)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON3, &CNWT_SimulationDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1, &CNWT_SimulationDlg::OnBnClickedButton1)
//	ON_WM_TIMER()
END_MESSAGE_MAP()


// CNWT_SimulationDlg 메시지 처리기

BOOL CNWT_SimulationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	r_bit.LoadBitmap(IDB_BITMAP1);
	r_bit.GetBitmap(&r_bit_info);
	r_bit_info.bmHeight= r_bit_info.bmHeight +10;
	r_bit_info.bmWidth = r_bit_info.bmWidth +10;


	check_Network = FALSE;

	m_imageDC = m_image.GetWindowDC();
	m_image.GetWindowRect(&m_windowRect);
	m_windowRect.left = 0;
	m_windowRect.top  = 0;
	m_windowRect.right = 828;
	m_windowRect.bottom = 584;
	center_x = m_windowRect.right/2;
	center_y = m_windowRect.bottom/2;
	cpoint_x=center_x;
	cpoint_y=center_y;
	m_mDC.CreateCompatibleDC(m_imageDC);
	m_bit.CreateCompatibleBitmap(m_imageDC,m_windowRect.right,m_windowRect.bottom);
	
	m_mDC.SelectObject(&m_bit);
	m_mDC.PatBlt(m_windowRect.left,m_windowRect.top,m_windowRect.right,m_windowRect.bottom,WHITENESS);


	GRID(center_x,center_y,0);


	SetDlgItemText(IDC_EDIT2,"0");
	SetDlgItemText(IDC_EDIT5,"127.0.0.1");
	SetDlgItemText(IDC_EDIT6,"8888");
	SetDlgItemText(IDC_EDIT8,"0");
	SetDlgItemText(IDC_EDIT9,"0");
	SetDlgItemText(IDC_EDIT1,"1.0");

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CNWT_SimulationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNWT_SimulationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
		m_imageDC->BitBlt(0,0,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
	}
	else
	{		
		m_imageDC->BitBlt(0,0,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNWT_SimulationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CNWT_SimulationDlg::OnRButtonDown(UINT nFlags, CPoint point)
{

	AfxMessageBox("초기화!!");
	SetDlgItemText(IDC_EDIT2,"0");
	GRID(center_x,center_y,0);
	CDialog::OnRButtonDown(nFlags, point);
}

void CNWT_SimulationDlg::OnBnClickedButton4()
{
	if(check_Network == TRUE)
	{
		AfxMessageBox("이미 연결 되어 있습니다");
		return;
	}
	char buf[50];
	litner = new SockNet(this);

	GetDlgItemText(IDC_EDIT6,buf,sizeof(buf));

	server.Create(atoi(buf));
	server.Listen(5);
	server.Accept(*litner);
	check_Network = TRUE;
}


void CNWT_SimulationDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//Centor x = 414, y = 292
	char cbuf[10];
	char buf[64];
	int heading = 0;

	if(point.y > 584) return;

	printf("x = %d , y = %d\n",point.x - center_x,center_y - point.y);
	
	itoa(point.x - center_x,cbuf,10);
	SetDlgItemText(IDC_EDIT8,cbuf);

	itoa(center_y - point.y,cbuf,10);
	SetDlgItemText(IDC_EDIT9,cbuf);

	GetDlgItemText(IDC_EDIT2,buf,sizeof(buf));

	heading = atoi(buf);
	h_rad = heading / 180.0 * PI;

	//회전 함수가 반대임 -1 곱함

	h_rad = h_rad * -1;

	wpoint = point;

	GRID(cpoint_x, cpoint_y,h_rad);
	m_mDC.FillSolidRect(wpoint.x-2,wpoint.y-2,5,5,RGB(255,0,0));
	m_imageDC->BitBlt(0,0,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
				
	__super::OnLButtonDown(nFlags, point);
}

void CNWT_SimulationDlg::OnBnClickedButton3()
{
	OnBnClickedButton1();

	if(check_Network == FALSE)
	{
		AfxMessageBox("Network가 비활성화 입니다 '접속'으로 서버 오픈해 주세요");
		return;
	}
	char buf[10];
	int heading = 0;
	
	positionData.rx = cpoint_x - center_x;
	positionData.ry = center_y - cpoint_y;

	GetDlgItemText(IDC_EDIT2,buf,sizeof(buf));
	heading = atoi(buf);

	positionData.rh = heading;

	memset(buf,NULL,sizeof(buf));
	GetDlgItemText(IDC_EDIT8,buf,sizeof(buf));
	positionData.wx = (int)atoi(buf);
	memset(buf,NULL,sizeof(buf));
	GetDlgItemText(IDC_EDIT9,buf,sizeof(buf));
	positionData.wy = (int)atoi(buf);
	memset(buf,NULL,sizeof(buf));
	GetDlgItemText(IDC_EDIT1,buf,sizeof(buf));
	positionData.speed = (double)atof(buf);

	//litner->Send((char*)&positionData,sizeof(NWT_data),0);
	litner->Send("123456789",9,0);
	
}


void CNWT_SimulationDlg::robotgrid(int x, int y , double heading)
{
	
	m_off.CreateCompatibleDC(&m_mDC);
	m_off_bit.CreateCompatibleBitmap(&m_off,r_bit_info.bmWidth,r_bit_info.bmHeight);

	hbit = GetRotatedBitmap(m_mDC,r_bit,heading,RGB(255,255,255));

	
	m_off.SelectObject(hbit);
	m_mDC.StretchBlt(x -r_bit_info.bmWidth/2*0.8 ,  y - r_bit_info.bmHeight/2*0.8
					,r_bit_info.bmWidth*0.8,        r_bit_info.bmHeight*0.8,      &m_off
					,0,0,r_bit_info.bmWidth,r_bit_info.bmHeight,SRCCOPY);
	
	m_mDC.BitBlt(m_windowRect.left,m_windowRect.top,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
	m_off.DeleteDC();
	m_off_bit.DeleteObject();


}
void CNWT_SimulationDlg::bitBKgnd(void)
{
	m_mDC.PatBlt(m_windowRect.left,m_windowRect.top,m_windowRect.right,m_windowRect.bottom,WHITENESS);
	m_mDC.MoveTo(m_windowRect.right/2,m_windowRect.top);
	m_mDC.LineTo(m_windowRect.right/2,m_windowRect.bottom);
	m_mDC.MoveTo(m_windowRect.left,m_windowRect.bottom/2);
	m_mDC.LineTo(m_windowRect.right,m_windowRect.bottom/2);

}

HBITMAP CNWT_SimulationDlg::GetRotatedBitmap( HDC hdc, HBITMAP hBitmap, double radians, COLORREF clrBack ) 
{ 
	// Create a memory DC compatible with the display 
	HDC sourceDC, destDC;
	sourceDC	= CreateCompatibleDC(hdc);
	destDC		= CreateCompatibleDC(hdc);

	// Get logical coordinates 
	BITMAP bm; 
	GetObject( hBitmap, sizeof( bm ), &bm ); 


	float cosine = (float)cos(radians); 
	float sine = (float)sin(radians); 


	//회전된 이미지의 영역을 구한다.
	// Compute dimensions of the resulting bitmap 
	// First get the coordinates of the 3 corners other than origin 
	int x1 = (int)(bm.bmHeight * sine); 
	int y1 = (int)(bm.bmHeight * cosine); 
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine); 
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine); 
	int x3 = (int)(bm.bmWidth * cosine); 
	int y3 = (int)(-bm.bmWidth * sine); 


	int minx = min(0,min(x1, min(x2,x3))); 
	int miny = min(0,min(y1, min(y2,y3))); 
	int maxx = max(0,max(x1, max(x2,x3))); 
	int maxy = max(0,max(y1, max(y2,y3))); 


	int w = maxx - minx; 
	int h = maxy - miny; 


	// Create a bitmap to hold the result 
	HBITMAP hbmResult = CreateCompatibleBitmap(hdc, w, h); 


	HBITMAP hbmOldSource = (HBITMAP)SelectObject( sourceDC, hBitmap ); 
	HBITMAP hbmOldDest = (HBITMAP)SelectObject( destDC, hbmResult ); 


	// Draw the background color before we change mapping mode 
	HBRUSH hbrBack = CreateSolidBrush( clrBack ); 
	HBRUSH hbrOld = (HBRUSH)SelectObject( destDC, hbrBack ); 
	PatBlt(destDC, 0, 0, w, h, PATCOPY ); 
	DeleteObject( SelectObject( destDC, hbrOld ) ); 


	// We will use world transform to rotate the bitmap 
	SetGraphicsMode(destDC, GM_ADVANCED); 
	XFORM xform; 
	xform.eM11 = cosine; 
	xform.eM12 = -sine; 
	xform.eM21 = sine; 
	xform.eM22 = cosine; 
	xform.eDx = (float)-minx; 
	xform.eDy = (float)-miny; 


	SetWorldTransform( destDC, &xform ); 


	// Now do the actual rotating - a pixel at a time 
	BitBlt(destDC, 0,0,bm.bmWidth, bm.bmHeight, sourceDC, 0, 0, SRCCOPY ); 


	// Restore DCs 
	SelectObject( sourceDC, hbmOldSource ); 
	SelectObject( destDC, hbmOldDest ); 



	DeleteObject(sourceDC);

	DeleteObject(destDC);


	return hbmResult; 
} 



void CNWT_SimulationDlg::OnBnClickedButton1()
{
	char buf[64];
	int heading = 0;
	
	GetDlgItemText(IDC_EDIT2,buf,sizeof(buf));

	heading = atoi(buf);
	h_rad = heading / 180.0 * PI;

	//회전 함수가 반대임 -1 곱함

	h_rad = h_rad * -1;

	//GRID(cpoint_x,cpoint_y,h_rad);
	robotgrid(cpoint_x, cpoint_y, h_rad);
	m_imageDC->BitBlt(0,0,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
}

void CNWT_SimulationDlg::GRID(int x, int y, double heading)
{

	OnBnClickedButton1();
	cpoint_x = x;
	cpoint_y = y;
	bitBKgnd();
	robotgrid(x, y, heading);

	if(wpoint.x || wpoint.y != NULL)
	{
		m_mDC.FillSolidRect(wpoint.x-2,wpoint.y-2,5,5,RGB(255,0,0));
	}
	
	m_imageDC->BitBlt(0,0,m_windowRect.right,m_windowRect.bottom,&m_mDC,0,0,SRCCOPY);
}

void CNWT_SimulationDlg::On_DlgRecv(NWT_Driving_Data data)
{
	bool check;
	int x = 0;
	int y = 0;
	char headbuf[8];
	GetDlgItemText(IDC_EDIT2,headbuf,sizeof(headbuf));

	// Acc 에 대한 값 입력 
	x = 0;
	y = (int)(data.Accelation / 10);

	// 직진에 대한 헤딩값을 적용하여 (0,y)의 값을 회전 
	x = y * sin(h_rad) * MOVING_INT;
	y = y * cos(h_rad) * MOVING_INT;

	//헤딩이 반영된 좌표로 전진 좌표 설정 
	cpoint_x = cpoint_x - x;
	cpoint_y = cpoint_y - y;


	//Steering 값에 따른 헤딩각도의 변환
	int change_heading	= 0;
	double command_heading	= 0.0;
	int now_heading			= 0;

	command_heading = data.Steering / STEERING_INT;
	now_heading		= atoi(headbuf);

	change_heading	= (int)command_heading + now_heading;
	while(1)
	{
		if(change_heading > 360)
		{
			change_heading = change_heading - 360;
			check = TRUE;
		}
		else if(change_heading > 180)
		{
			change_heading = -360 + change_heading;
			check = TRUE;
		}
		else if(change_heading == -180)
		{
			change_heading = 180;
			check = TRUE;
		}
		else if(change_heading < -180)
		{
			change_heading = -1 * (change_heading + 180);
			check = TRUE;
		}
		else
		{
			check = FALSE;							//모든것에 해당이 안되면 예외처리완료로 생각하고 Break;
		}

		if(check == FALSE) break;
	}
	itoa(change_heading,headbuf,10);

	SetDlgItemText(IDC_EDIT2,headbuf);
	
	GRID(cpoint_x,cpoint_y,h_rad);
}