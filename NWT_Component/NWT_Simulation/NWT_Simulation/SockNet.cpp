// SockNet.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "NWT_Simulation.h"
#include "SockNet.h"
#include <stdlib.h>
// SockNet

SockNet::SockNet(Interface* pInterface)
{
	m_pInterface = pInterface;
	memset((char*)&drivingData,NULL,sizeof(NWT_Driving_Data));
}

SockNet::~SockNet()
{
}

void SockNet::OnReceive(int nErrorCode)
{
	int t = recv(this->m_hSocket,(char*)&drivingData ,(int) sizeof(NWT_Driving_Data), 0);
	m_pInterface->On_DlgRecv(drivingData);
	printf("recv => acc = %d \t str = %d\n",drivingData.Accelation, drivingData.Steering);
}