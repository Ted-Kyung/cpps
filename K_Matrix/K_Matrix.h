#pragma once
#include <stdio.h>
#include <Windows.h>

#ifdef DLLEXPORT
#define DLLCREATE __declspec(dllexport)
#else
#define DLLCREATE __declspec(dllimport)
#endif

// Creator : Kyong Jae Hyun
// purpose : MATLAB like Matrix Calculation
// if use  : if use this Class 
//           1: Class instance Create(raw, col)
//           2: if instance Create() -> use Create_Matrix(raw,col)
//           3: use SetMatrixData() -> input Data
//           4: use to -> instance to instance +,-,*,/,= and function [ .transe() , .inverse() ]
//           caution : inverse() func is required SqureMatrix
//           Tip : easy debug use to ShowMatrix();
// ex      : 
// CMatrix A(2,2);
// CMatrix B;
// B.Create_Matrix(2,2);
// CMatrix Ans; 
// Ans = ((A+B) * A).inverse()

class DLLCREATE CMatrix
//class CMatrix
{
public:
	CMatrix(void);
	CMatrix(int raw, int col);
	~CMatrix(void);

	//Create Matrix
	void Create_Matrix(int raw, int col);
	//Delete Matrix
	void Delete_Matrix();
	//show My Matrix Data
	void ShowMatrix();
	//return Err Code
	char* GetLastErrCode();
	//Set Matrix Data 
	void SetMatrixData(int* data,int DataCnt);
	void SetMatrixData(int** data,int DataCnt);
	void SetMatrixData(float* data,int DataCnt);
	void SetMatrixData(float** data,int DataCnt);
	void SetMatrixData(double* data,int DataCnt);
	void SetMatrixData(double** data,int DataCnt);
	void SetErrCode(char* code);

	double GetMatrixValue(void);
	double GetMatrixValue(int raw, int col);

	void InitMatrix(double x);
	void SetMatrixData(int raw, int col, double Value);

private:
	int		m_raw;
	int		m_col;	
	double** m_myMat;
	char	m_err_code[1024];
	CMatrix* Ans;

	//init Matrix
	void InitZeroMatrix();

public:
	CMatrix& operator+ (int constInt);
	CMatrix& operator+ (float constFloat);
	CMatrix& operator+ (double constDouble);
	CMatrix& operator+ (CMatrix& bMatrix);
	CMatrix& operator+ (CMatrix* bMatrix);

	CMatrix& operator- (int constInt);
	CMatrix& operator- (float constFloat);
	CMatrix& operator- (double constDouble);
	CMatrix& operator- (CMatrix& bMatrix);
	CMatrix& operator- (CMatrix* bMatrix);

	CMatrix& operator* (int constInt);
	CMatrix& operator* (float constFloat);
	CMatrix& operator* (double constDouble);
	CMatrix& operator* (CMatrix& bMatrix);
	CMatrix& operator* (CMatrix* bMatrix);

	CMatrix& operator/ (int constInt);
	CMatrix& operator/ (float constFloat);
	CMatrix& operator/ (double constDouble);
	CMatrix& operator/ (CMatrix& bMatrix);
	CMatrix& operator/ (CMatrix* bMatrix);

	//inverse
	CMatrix& inverse(void);
	//Transe
	CMatrix& Transe(void);

	CMatrix* operator= (CMatrix* bMatrix);
	CMatrix& operator= (CMatrix& bMatrix);
};

