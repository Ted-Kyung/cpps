#include "K_Matrix.h"
#define DLLEXPORT
CMatrix::CMatrix(void)
{
	m_myMat = NULL;
	Ans = NULL;
	memset(m_err_code,0,1024);
}
CMatrix::~CMatrix(void)
{
	if (Ans != NULL)
	{
		delete Ans;
	}
	this->Delete_Matrix();
}

CMatrix::CMatrix(int raw, int col)
{
	Ans = NULL;
	m_myMat = NULL;
	this->Create_Matrix(raw, col);
}
//Create Matrix
void CMatrix::Create_Matrix(int raw, int col)
{		
	if((raw < 1) || (col < 1))
	{
		return;
	}
	if (Ans == NULL)
	{
		Ans = new CMatrix;
	}

	m_raw = raw;
	m_col = col;
	m_myMat = NULL;

	m_myMat = new double* [m_raw];
	for(int cCnt = 0; cCnt < m_raw ; cCnt++)
	{
		m_myMat[cCnt] = NULL;
	}	

	for(int cCnt = 0; cCnt < m_raw ; cCnt++)
	{
		m_myMat[cCnt] = new double [m_col];
	}	
	InitZeroMatrix();
}
//Delete Matrix
void CMatrix::Delete_Matrix()
{
	if(m_myMat == NULL) 
	{
		return;
	}

	for(int raw = 0; raw < m_raw ; raw++)
	{
		if(m_myMat[raw] != NULL)delete [] m_myMat[raw];
		m_myMat[raw] = NULL;
	}	
	if(m_myMat != NULL)delete [] m_myMat;
	m_myMat = NULL;
}
//show My Matrix Data
void CMatrix::ShowMatrix()
{
	if(m_myMat == NULL) 
	{
		return;
	}
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			printf("%.2lf\t\t",m_myMat[raw][col]);
		}	
		printf("\n");
	}
	printf("\n");
}
//init
void CMatrix::InitZeroMatrix()
{
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = 0.0;
		}	
	}
}
void CMatrix::InitMatrix(double x)
{
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = x;
		}	
	}
}
//return Err Code
char* CMatrix::GetLastErrCode()
{
	return m_err_code;
}
//Set Matrix Data 
void CMatrix::SetMatrixData(int* data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	int dataCnt = 0;
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[dataCnt];
			dataCnt++;
		}	
	}
}
void CMatrix::SetMatrixData(int** data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[raw][col];
		}	
	}
}
void CMatrix::SetMatrixData(float* data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	int dataCnt = 0;
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[dataCnt];
			dataCnt++;
		}	
	}
}
void CMatrix::SetMatrixData(float** data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[raw][col];
		}	
	}
}
void CMatrix::SetMatrixData(double* data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	int dataCnt = 0;
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[dataCnt];
			dataCnt++;
		}	
	}
}
void CMatrix::SetMatrixData(double** data,int DataCnt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[SetMatrixData]Matrix Not Created");
		return;
	}
	int a = sizeof(*(data));
	int b = sizeof(int);
	if( (m_raw * m_col) != DataCnt ) 
	{
		sprintf(m_err_code,"[SetMatrixData]Data Size Not Match");
		return;
	}
	sprintf(m_err_code,"[Err none]");

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)data[raw][col];
		}	
	}
}
void CMatrix::SetMatrixData(int raw, int col, double Value)
{
	m_myMat[raw][col] = Value;
}
CMatrix& CMatrix::operator+ (int constInt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator+]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator+]Matrix Not Created");
		return *Ans;
	}

	sprintf(m_err_code,"[Err none]");

	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] + (double)constInt;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator+ (float constFloat)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator+]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator+]Matrix Not Created");
		return *Ans;
	}

	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] + (double)constFloat;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator+ (double constDouble)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator+]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator+]Matrix Not Created");
		return *Ans;
	}

	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] + (double)constDouble;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator+ (CMatrix& bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator+]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator+]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix.m_raw) || (m_col != bMatrix.m_col)) 
	{
		sprintf(m_err_code,"[operator+]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator+]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] + (double)bMatrix.m_myMat[raw][col];
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator+ (CMatrix* bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator+]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator+]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix->m_raw) || (m_col != bMatrix->m_col)) 
	{
		sprintf(m_err_code,"[operator+]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator+]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");	
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] + (double)bMatrix->m_myMat[raw][col];
		}	
	}
	return *Ans;
}

CMatrix& CMatrix::operator- (int constInt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator-]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator-]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] - (double)constInt;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator- (float constFloat)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator-]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator-]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] - (double)constFloat;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator- (double constDouble)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator-]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator-]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] - (double)constDouble;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator- (CMatrix& bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator-]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator-]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix.m_raw) || (m_col != bMatrix.m_col)) 
	{
		sprintf(m_err_code,"[operator-]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator-]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");	
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] - (double)bMatrix.m_myMat[raw][col];
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator- (CMatrix* bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator-]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator-]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix->m_raw) || (m_col != bMatrix->m_col)) 
	{
		sprintf(m_err_code,"[operator-]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator-]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");	
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] - (double)bMatrix->m_myMat[raw][col];
		}	
	}
	return *Ans;
}

CMatrix& CMatrix::operator* (int constInt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator*]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator*]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] * (double)constInt;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator* (float constFloat)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator*]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator*]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] * (double)constFloat;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator* (double constDouble)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator*]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator*]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] * (double)constDouble;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator* (CMatrix& bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator*]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator*]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix.m_col)) 
	{
		sprintf(m_err_code,"[operator*]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator*]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(bMatrix.m_raw,m_col);

	int rawoff = 0, coloff = 0;
	for(int col = 0; col < m_col*bMatrix.m_raw ; col++)
	{
		for(int raw = 0; raw < bMatrix.m_col ; raw++)
		{
			Ans->m_myMat[rawoff][coloff] += bMatrix.m_myMat[rawoff][raw] * m_myMat[raw][coloff];
		}	
		coloff++;
		if (coloff == m_col)
		{
			coloff = 0;
			rawoff++;
		}
	}
	return *Ans;
}
CMatrix& CMatrix::operator* (CMatrix* bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator*]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator*]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix->m_col) && (m_col != bMatrix->m_raw)) 
	{
		sprintf(m_err_code,"[operator*]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator*]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(bMatrix->m_raw,m_col);

	int rawoff = 0, coloff = 0;
	for(int col = 0; col < m_col*bMatrix->m_raw ; col++)
	{
		for(int raw = 0; raw < bMatrix->m_col ; raw++)
		{
			Ans->m_myMat[rawoff][coloff] += bMatrix->m_myMat[rawoff][raw] * m_myMat[raw][coloff];
		}	
		coloff++;
		if (coloff == m_col)
		{
			coloff = 0;
			rawoff++;
		}
	}
	return *Ans;
}

CMatrix& CMatrix::operator/ (int constInt)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator/]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator/]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] / (double)constInt;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator/ (float constFloat)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator/]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator/]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] / (double)constFloat;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator/ (double constDouble)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator/]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator/]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col);
	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[raw][col] = (double)m_myMat[raw][col] / (double)constDouble;
		}	
	}
	return *Ans;
}
CMatrix& CMatrix::operator/ (CMatrix& bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator/]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator/]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix.m_col) && (m_col != bMatrix.m_raw)) 
	{
		sprintf(m_err_code,"[operator/]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator/]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(bMatrix.m_raw,m_col);

	int rawoff = 0, coloff = 0;
	for(int col = 0; col < m_col*bMatrix.m_raw ; col++)
	{
		for(int raw = 0; raw < bMatrix.m_col ; raw++)
		{
			Ans->m_myMat[rawoff][coloff] += bMatrix.m_myMat[rawoff][raw] / m_myMat[raw][coloff];
		}	
		coloff++;
		if (coloff == m_col)
		{
			coloff = 0;
			rawoff++;
		}
	}
	return *Ans;
}
CMatrix& CMatrix::operator/ (CMatrix* bMatrix)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[operator/]Matrix Not Created");
		sprintf(Ans->m_err_code,"[operator/]Matrix Not Created");
		return *Ans;
	}
	if((m_raw != bMatrix->m_col) && (m_col != bMatrix->m_raw)) 
	{
		sprintf(m_err_code,"[operator/]Row & Col Not Match");
		sprintf(Ans->m_err_code,"[operator/]Row & Col Not Match");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(bMatrix->m_raw,m_col);

	int rawoff = 0, coloff = 0;
	for(int col = 0; col < m_col*bMatrix->m_raw ; col++)
	{
		for(int raw = 0; raw < bMatrix->m_col ; raw++)
		{
			Ans->m_myMat[rawoff][coloff] += bMatrix->m_myMat[rawoff][raw] / m_myMat[raw][coloff];
		}	
		coloff++;
		if (coloff == m_col)
		{
			coloff = 0;
			rawoff++;
		}
	}
	return *Ans;
}

//inverse
CMatrix& CMatrix::inverse(void)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[inverse]Matrix Not Created");
		sprintf(Ans->m_err_code,"[inverse]Matrix Not Created");
		return *Ans;
	}
	if(m_raw != m_col)
	{
		sprintf(m_err_code,"[inverse]Not Square");
		sprintf(Ans->m_err_code,"[inverse]Not Square");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_raw,m_col * 2);

	double Det;
	double ratio;
	int squareN;
	squareN = m_col;
	for(int i = 0; i < squareN; i++)
	{
		for(int j = 0; j < squareN; j++)
		{
			Ans->m_myMat[i][j] = (double)m_myMat[i][j];
		}
	}
	for(int i = 0; i < squareN; i++)
	{
		for(int j = squareN; j < 2 * squareN; j++)
		{
			if(i==( j - squareN ))
				Ans->m_myMat[i][j] = 1.0;
			else
				Ans->m_myMat[i][j] = 0.0;
		}
	}
	for(int i = 0; i < squareN; i++)
	{
		for(int j = 0; j < squareN; j++)
		{
			if(i!=j){
				ratio = Ans->m_myMat[j][i] / Ans->m_myMat[i][i];
				for(int k = 0; k < 2 * squareN; k++)
				{
					Ans->m_myMat[j][k] -= ratio * Ans->m_myMat[i][k];
				}
			}
		}
	}
	for(int i = 0; i < squareN; i++)
	{
		Det = Ans->m_myMat[i][i];
		for(int j = 0; j < 2 * squareN; j++)
		{
			Ans->m_myMat[i][j] /= Det;
		}
	}
	for(int i = 0; i < squareN; i++)
	{
		for(int j = squareN; j < 2 * squareN; j++)
		{
			Ans->m_myMat[i][j - squareN] = Ans->m_myMat[i][j];
		}
	}
	Ans->m_col = Ans->m_col / 2;
	return *Ans;
}
//Transe
CMatrix& CMatrix::Transe(void)
{
	if(m_myMat == NULL) 
	{
		sprintf(m_err_code,"[Transe]Matrix Not Created");
		sprintf(Ans->m_err_code,"[Transe]Matrix Not Created");
		return *Ans;
	}
	sprintf(m_err_code,"[Err none]");
	Ans->Delete_Matrix();
	Ans->Create_Matrix(m_col,m_raw);

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			Ans->m_myMat[col][raw] = (double)m_myMat[raw][col];
		}	
	}
	return *Ans;
}

CMatrix* CMatrix::operator= (CMatrix* bMatrix)
{
	this->Delete_Matrix();
	m_raw = bMatrix->m_raw;
	m_col = bMatrix->m_col;
	this->Create_Matrix(m_raw,m_col);
	sprintf(m_err_code,"%s",bMatrix->GetLastErrCode());

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)bMatrix->m_myMat[raw][col];
		}	
	}
	return this;
}
CMatrix& CMatrix::operator= (CMatrix& bMatrix)
{
	this->Delete_Matrix();
	m_raw = bMatrix.m_raw;
	m_col = bMatrix.m_col;
	this->Create_Matrix(m_raw,m_col);
	sprintf(m_err_code,"%s",bMatrix.GetLastErrCode());

	for(int col = 0; col < m_col ; col++)
	{
		for(int raw = 0; raw < m_raw ; raw++)
		{
			m_myMat[raw][col] = (double)bMatrix.m_myMat[raw][col];
		}	
	}
	return *this;
}
void CMatrix::SetErrCode(char* code)
{
	sprintf(m_err_code,"%s",code);
	sprintf(Ans->m_err_code,"%s",code);
}
double CMatrix::GetMatrixValue(int raw, int col)
{
	if((raw < 0)||(col < 0)||(raw > m_raw-1)||(col > m_col-1)) return -9999999.0;
	return (double)(m_myMat[raw][col]);;
}
double CMatrix::GetMatrixValue(void)
{
	return (double)m_myMat[0][0];
}