#pragma once
#include <stdio.h>
#include <Windows.h>
#include "K_Matrix.h"

#pragma comment(lib,"K_Matrix.lib")

#ifdef DLLEXPORT
#define DLLCREATE __declspec(dllexport)
#else
#define DLLCREATE __declspec(dllimport)
#endif

class DLLCREATE K_KalmanFilter : public CMatrix
{
public:
	K_KalmanFilter(void);
	~K_KalmanFilter(void);

	void SetMatrix_A_SystemModel(int raw, int col);
	void SetMatrix_H_SystemModelling(int raw, int col);
	void SetMatrix_Q_SystemModelling(int raw, int col);
	void SetMatrix_R_SystemModelling(int raw, int col);
	void SetMatrix_P_SystemModelling(int raw, int col);

	//System Modellring
	void Set_A_DataValue(int* value, int DataSize);
	//System Modellring
	void Set_H_DataValue(int* value, int DataSize);
	// Q increse -> Kalman Gain increse
	void Set_Q_DataValue(int* value, int DataSize);
	// R increse -> Kalman Gain Decrese
	void Set_R_DataValue(int* value, int DataSize);
	// P error Limit
	void Set_P_DataValue(int* value, int DataSize);

	void Set_A_DataValue(int** value, int DataSize);
	void Set_H_DataValue(int** value, int DataSize);
	void Set_Q_DataValue(int** value, int DataSize);
	void Set_R_DataValue(int** value, int DataSize);
	void Set_P_DataValue(int** value, int DataSize);

	void Set_A_DataValue(double* value, int DataSize);
	void Set_H_DataValue(double* value, int DataSize);
	void Set_Q_DataValue(double* value, int DataSize);
	void Set_R_DataValue(double* value, int DataSize);
	void Set_P_DataValue(double* value, int DataSize);

	void Set_A_DataValue(double** value, int DataSize);
	void Set_H_DataValue(double** value, int DataSize);
	void Set_Q_DataValue(double** value, int DataSize);
	void Set_R_DataValue(double** value, int DataSize);
	void Set_P_DataValue(double** value, int DataSize);

	CMatrix& Updata_KalmanFilter(int inputData);
	CMatrix& Updata_KalmanFilter(double inputData);

private:
	bool m_setA;
	bool m_setH;
	bool m_setQ;
	bool m_setR;
	bool m_setP;

	//System Modellring
	CMatrix A;
	//System Modellring
	CMatrix H;	

	// Q increse -> error Limit increse
	CMatrix Q;
	// R increse -> Kalman Gain Decrese
	CMatrix R;
	// P error Limit
	CMatrix P;

	CMatrix esitimateValueForeCast;
	CMatrix esitimateErrorDispersion;
	CMatrix KalmanGain;
	CMatrix esitimateCalculation;
	CMatrix esitimateErrorCalculation;

	CMatrix& Run_KalmanFilter(double inputData);
};

