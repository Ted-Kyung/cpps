#include "K_KalmanFilter.h"

#define DLLEXPORT

K_KalmanFilter::K_KalmanFilter(void)
{
	m_setA = false;
	m_setH = false;
	m_setQ = false;
	m_setR = false;
	m_setP = false;
	esitimateCalculation.Create_Matrix(1,1);
}
K_KalmanFilter::~K_KalmanFilter(void)
{
	
}

void K_KalmanFilter::SetMatrix_A_SystemModel(int raw, int col)
{
	A.Create_Matrix(raw,col);
}
void K_KalmanFilter::SetMatrix_H_SystemModelling(int raw, int col)
{
	H.Create_Matrix(raw,col);
}
void K_KalmanFilter::SetMatrix_Q_SystemModelling(int raw, int col)
{
	Q.Create_Matrix(raw,col);
}
void K_KalmanFilter::SetMatrix_R_SystemModelling(int raw, int col)
{
	R.Create_Matrix(raw,col);
}
void K_KalmanFilter::SetMatrix_P_SystemModelling(int raw, int col)
{
	P.Create_Matrix(raw,col);
}

void K_KalmanFilter::Set_A_DataValue(int* value, int DataSize)
{
	m_setA = true;
	A.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_H_DataValue(int* value, int DataSize)
{
	m_setH = true;
	H.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_Q_DataValue(int* value, int DataSize)
{
	m_setQ = true;
	Q.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_R_DataValue(int* value, int DataSize)
{
	m_setR = true;
	R.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_P_DataValue(int* value, int DataSize)
{
	m_setP = true;
	P.SetMatrixData(value,DataSize);
}

void K_KalmanFilter::Set_A_DataValue(int** value, int DataSize)
{
	m_setA = true;
	A.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_H_DataValue(int** value, int DataSize)
{
	m_setH = true;
	H.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_Q_DataValue(int** value, int DataSize)
{
	m_setQ = true;
	Q.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_R_DataValue(int** value, int DataSize)
{
	m_setR = true;
	R.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_P_DataValue(int** value, int DataSize)
{
	m_setP = true;
	P.SetMatrixData(value,DataSize);
}

void K_KalmanFilter::Set_A_DataValue(double* value, int DataSize)
{
	m_setA = true;
	A.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_H_DataValue(double* value, int DataSize)
{
	m_setH = true;
	H.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_Q_DataValue(double* value, int DataSize)
{
	m_setQ = true;
	Q.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_R_DataValue(double* value, int DataSize)
{
	m_setR = true;
	R.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_P_DataValue(double* value, int DataSize)
{
	m_setP = true;
	P.SetMatrixData(value,DataSize);
}

void K_KalmanFilter::Set_A_DataValue(double** value, int DataSize)
{
	m_setA = true;
	A.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_H_DataValue(double** value, int DataSize)
{
	m_setH = true;
	H.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_Q_DataValue(double** value, int DataSize)
{
	m_setQ = true;
	Q.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_R_DataValue(double** value, int DataSize)
{
	m_setR = true;
	R.SetMatrixData(value,DataSize);
}
void K_KalmanFilter::Set_P_DataValue(double** value, int DataSize)
{
	m_setP = true;
	P.SetMatrixData(value,DataSize);
}

CMatrix& K_KalmanFilter::Updata_KalmanFilter(int inputData)
{
	return (this->Run_KalmanFilter((double)inputData));
}
CMatrix& K_KalmanFilter::Updata_KalmanFilter(double inputData)
{
	return (this->Run_KalmanFilter((double)inputData));
}
CMatrix& K_KalmanFilter::Run_KalmanFilter(double inputData)
{
	if(!(m_setA & m_setH & m_setQ & m_setR & m_setP)) 
	{
		printf("Value Setting Faild\n");
		esitimateCalculation.SetErrCode("Value Setting Faild");
		return esitimateCalculation;
	}
	esitimateValueForeCast = (A * esitimateCalculation);																	//%추정값 예측
	//esitimateValueForeCast.ShowMatrix();
	esitimateErrorDispersion = (((A * P) * A.Transe()) + Q);											//% 오차 공분산 예측
	//esitimateErrorDispersion.ShowMatrix();
	KalmanGain = (esitimateErrorDispersion * H.Transe()) * ((H * esitimateErrorDispersion * H.Transe() + R).inverse());		//% 칼만 이득 계산
	//KalmanGain.ShowMatrix();
	esitimateCalculation = (esitimateValueForeCast + (KalmanGain * (((H * esitimateValueForeCast) * -1.0) + inputData )));				//% 추정값 계산
	//esitimateCalculation.ShowMatrix();
	esitimateErrorCalculation = (esitimateErrorDispersion - (KalmanGain * (H * esitimateErrorDispersion)));					//% 오차 공분산 계산
	//esitimateErrorCalculation.ShowMatrix();
	return esitimateCalculation;
}