#include "K_imaphicMemory.h"

K_imaphicMemory::K_imaphicMemory(void)
{
	m_Parm.FusionlpBits	= NULL;
	BuflpBits			= NULL;
}
K_imaphicMemory::K_imaphicMemory(int width, int height)
{
	CreateMemory(width, height);
}
K_imaphicMemory::~K_imaphicMemory(void)
{
	FreeMemory();
}
void K_imaphicMemory::ClearAll()
{
	memset(m_Parm.FusionlpBits,NULL,m_Parm.TotalBit);
	memset(BuflpBits,NULL,m_Parm.TotalBit);
}
void K_imaphicMemory::CreateMemory(int width, int height)
{
	CreateMemory(width, height, DeviceBitCount);
}
void K_imaphicMemory::CreateMemory(int width, int height, int bitcont)
{
	CreateMemory(width, height, DeviceBitCount, 1);
}
void K_imaphicMemory::CreateMemory(int width, int height, int bitcont, int reverse)
{	
	FreeMemory();
	m_Parm.Bitcount				= bitcont / 8;
	m_Parm.PixelSize.width		= width;
	m_Parm.PixelSize.height		= height;
	m_Parm.BitSize.width		= m_Parm.PixelSize.width * m_Parm.Bitcount;
	m_Parm.BitSize.height		= m_Parm.PixelSize.height;
	m_Parm.TotalBit				= m_Parm.BitSize.width * m_Parm.BitSize.height;

	m_Parm.FusionlpBits		= new unsigned char[m_Parm.TotalBit+1];
	BuflpBits				= new unsigned char[m_Parm.TotalBit+1];

	memset(m_Parm.FusionlpBits,NULL,m_Parm.TotalBit);
	memset(BuflpBits,NULL,m_Parm.TotalBit);


	FillBitmapInfo(&m_Parm.bInfo, m_Parm.PixelSize.width, m_Parm.PixelSize.height, bitcont, reverse);
}

void K_imaphicMemory::FreeMemory()
{
	if(BuflpBits != NULL)
	{
		delete BuflpBits;
		BuflpBits			= NULL;
	}
	if(m_Parm.FusionlpBits != NULL)
	{
		delete m_Parm.FusionlpBits;
		m_Parm.FusionlpBits		= NULL;
	}
}