#include ".\circleque.h"

CCircleQue::CCircleQue(callback_que* ptr)
{
	m_ptr = ptr;

	savepoint	= 0;
	savesize	= 0;

	loadpoint	= 0;
	datalen		= 0;	

	DWORD threadID;
	CreateThread(NULL,NULL,search,(LPVOID)this,NULL,&threadID);
}

CCircleQue::~CCircleQue(void)
{
}

void CCircleQue::inputdata(char* data)
{
	int len = (int)strlen(data);
	for(int i = 0 ; i < len ; i++ )
	{
		if(data[i] == '\n') data[i] = '*';
		circledata[savepoint] = data[i];
		savepoint++;
		savesize++;
		if(savepoint >= QUESIZE)
		{
			savepoint = 0;
		}

		if (savesize >= QUESIZE)
		{
			savesize = QUESIZE;
		}
	}
}

DWORD WINAPI CCircleQue::search(LPVOID lpvoid)
{
	CCircleQue* tthis = (CCircleQue*)lpvoid;
	return tthis->searchStartbit();
}

DWORD CCircleQue::searchStartbit(void)
{
	int startbitnum = 0;
	int endbitnum	= 0;
	bool findflag = false;
	char* data;

	int datasavenum = 0;

	while(1)
	{
		

		if(savesize <= 0)
		{
			Sleep(1);
		}
		else if(savesize > 0)
		{
			
			//startbitnum = 0;
			//endbitnum	= 0;
			findflag	= false;
			datalen		= 0;
			for(int i = loadpoint ; i <= savesize ; i ++)
			{
				if(i >= QUESIZE) i = 0;

				if (circledata[i] == '$')
				{
					startbitnum = i;
					findflag    = true;
				}
				if(findflag == true)
				{
					datalen++;
				}
				if ((circledata[i] == '*') && (findflag == true))
				{
					endbitnum = i - 1;
					break;
				}
			}

			if((startbitnum==0)&&(endbitnum==0)&&(datalen==0)&&(loadpoint==0))
			{
				int a =0;
			}
			
			if(datalen > 25)
			{
				data = new char[datalen + 1];
				memset(data,NULL,sizeof(*data) * datalen + 1);

				for(int i = 0 ; i < datalen ; i ++)
				{
					datasavenum = startbitnum + i;
					if(datasavenum > QUESIZE-1) datasavenum = datasavenum - QUESIZE;
					data[i] = circledata[datasavenum];
					circledata[datasavenum] = '\0';
				}
				printf("%d\t%5d~%5d\t",savesize,startbitnum,endbitnum);
				m_ptr->que_output(data);

				delete data;
				loadpoint = endbitnum;
				savesize -= datalen;
			}
			Sleep(1);

		}
	}
		return 0;
}
