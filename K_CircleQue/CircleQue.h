#pragma once
#include <Windows.h>
#include <stdlib.h>
#include <stdio.h>
#define QUESIZE 70

static char circledata[QUESIZE];

class callback_que
{
public :
	virtual void que_output(char* data) = 0;
};

class CCircleQue
{
private:
	static DWORD WINAPI search(LPVOID lpvoid);
public:
	CCircleQue(callback_que* ptr);
	~CCircleQue(void);

	callback_que* m_ptr;
	
	long savesize;
	long savepoint;
	long loadpoint;
	long datalen;	
	void inputdata(char* data);	
	DWORD searchStartbit(void);
};
