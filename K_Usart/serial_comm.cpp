#include ".\serial_comm.h"
#include <io.h>
serial_comm::serial_comm(void)
{
	que = new CCircleQue(this);

	data_buffer = new char[MAXSIZE];
	_hSerial = ((HANDLE)((LONG_PTR)-1));
}

serial_comm::~serial_comm(void)
{
	delete data_buffer;
}

//COM port Open
int serial_comm::COMport_OPEN(const char *portName, long m_sBaudRate, int dataBits, char parity, float stopBits)
{
	if (_hSerial != INVALID_HANDLE_VALUE) {
		printf("ERROR: Open(): %s, %d, Port is already opened\n", portName, m_sBaudRate);
		return false;
	}

	_hSerial = CreateFile (portName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
	if (_hSerial == INVALID_HANDLE_VALUE){
		printf("ERROR: CreateFile(): %s, %d\n", portName, m_sBaudRate);
		return false;
	}

	//SetupComm (_hSerial, 8192, 8192);
	PurgeComm(_hSerial, PURGE_TXABORT|PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);

	DCB dcb;
	if (!GetCommState (_hSerial, &dcb)) {
		printf("ERROR: GetCommState ()\n");
		return false;
	}
	dcb.BaudRate = m_sBaudRate;
	dcb.ByteSize = (BYTE)dataBits;
	if (parity == 'N')
	{
		dcb.Parity = NOPARITY;
	}
	else if (parity == 'E')
	{
		dcb.Parity = EVENPARITY;
	}
	else if (parity == 'O')
	{
		dcb.Parity = ODDPARITY;
	}

	if (stopBits == 1)
	{
		dcb.StopBits = ONESTOPBIT;
	}
	else if (stopBits == 1.5)
	{
		dcb.StopBits = ONE5STOPBITS;
	}
	else if (stopBits == 2)
	{
		dcb.StopBits = TWOSTOPBITS;
	}
	dcb.fBinary = true;
	dcb.fDsrSensitivity = false;
	dcb.fParity = 0;
	dcb.fOutX = false;
	dcb.fInX = false;
	dcb.fNull = false;
	dcb.fAbortOnError = false;
	dcb.fOutxCtsFlow = false;
	dcb.fOutxDsrFlow = false;
	dcb.fDtrControl = DTR_CONTROL_DISABLE;
	dcb.fDsrSensitivity = false;
	dcb.fRtsControl = RTS_CONTROL_DISABLE;
	dcb.fOutxCtsFlow = false;
	dcb.fOutxCtsFlow = false;

	if (!SetCommState(_hSerial, &dcb)) 
	{
		printf("ERROR: SetCommState()\n");
		return -1; // -1 is faild
	}
	printf("SUCCESS: Open(): %s, %d, open serial port\n", portName, m_sBaudRate);
	return 0;
}
//ALL COM port Close
int serial_comm::COMport_CLOSE(void)
{
	if (_hSerial != INVALID_HANDLE_VALUE) 
	{
		CloseHandle (_hSerial);
		_hSerial = INVALID_HANDLE_VALUE;
		printf("SUCCESS: Close(): close serial port\n");
		return 0; //success
	}
	return -1; // -1 is faild
}
//data Send
int serial_comm::COMport_Send(char* Send_data)
{
	return -1; // -1 is faild
}
//data Read
int serial_comm::COMport_Read(char* Read_data,int len)
{
	return -1; // -1 is faild
}
//non recv data Time Out Setting
int serial_comm::SetTimeout (int readTimeout, int writeTimeout, int readIntervalTimeout)
{
	COMMTIMEOUTS commTimeout;

	if (!GetCommTimeouts (_hSerial, &commTimeout)) {
		printf("ERROR: GetCommTimeouts()\n");
		return false;
	}

	commTimeout.ReadIntervalTimeout = readIntervalTimeout;
	commTimeout.ReadTotalTimeoutMultiplier = 0;
	commTimeout.ReadTotalTimeoutConstant = readTimeout;
	commTimeout.WriteTotalTimeoutMultiplier = 0;
	commTimeout.WriteTotalTimeoutConstant = writeTimeout;

	if (!SetCommTimeouts (_hSerial, &commTimeout)) {
		printf("ERROR: SetCommTimeouts()\n");
		return -1;
	}
	return 0;
}
void serial_comm::COMport_AutoRead(callback_recv* ptr)
{
	m_cb_recv_ptr		= (callback_recv*)ptr;
	m_cb_timeout_ptr	= (callback_timeout*)ptr;
	CreateThread(NULL,NULL,RecvThread,(LPVOID)this,NULL,&threadID);
	
}
DWORD WINAPI serial_comm::RecvThread(LPVOID lpvoid)
{
	serial_comm* f_ptr = (serial_comm*)lpvoid;
	return f_ptr->receiveThread();
}
DWORD serial_comm::receiveThread(void)
{	
	while(1)
	{
		readBytes = 0;
		memset(data_buffer,0 ,MAXSIZE);
		ReadFile(_hSerial, data_buffer, MAXSIZE-1, &readBytes, NULL);
		//m_cb_recv_ptr->OnRecv(data_buffer);
		que->inputdata(data_buffer);
	}
}
void serial_comm::que_output(char* data)
{
	m_cb_recv_ptr->OnRecv(data);
}