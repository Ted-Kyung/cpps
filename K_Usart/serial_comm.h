#pragma once
#include <stdio.h>
#include <Windows.h>

#include "G:\\8_useful_include\\K_CircleQue\\CircleQue.h"

#define MAXSIZE 100

typedef void *HANDLE;

class callback_recv
{
public:
	virtual void OnRecv(char* buf) = 0;
};

class callback_timeout
{
public:
	virtual void OntimeOut(char* buf) = 0;
};

class serial_comm : public callback_que
{
public:
	serial_comm(void);
	~serial_comm(void);
private:
	callback_recv* m_cb_recv_ptr;
	callback_timeout* m_cb_timeout_ptr;

	HANDLE _hSerial; 
	static DWORD WINAPI RecvThread(LPVOID lpvoid);
	DWORD receiveThread(void);
	DWORD readBytes;

	char* data_buffer;
	DWORD threadID;

	CCircleQue* que;
	void que_output(char* data);
public:
	//COM port Open
	int COMport_OPEN(const char *portName, long m_sBaudRate, int dataBits, char parity, float stopBits);
	//COM port Close
	int COMport_CLOSE(void);
	//data Send
	int COMport_Send(char* Send_data);
	//data Read
	int COMport_Read(char* Read_data,int len);
	//data Auto Thread Read
	void COMport_AutoRead(callback_recv* ptr);
	//non recv data Time Out Setting
	int SetTimeout (int readTimeout, int writeTimeout, int readIntervalTimeout);	
};
