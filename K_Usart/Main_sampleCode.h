#pragma once
#include "serial_comm.h"

class CMain	: public callback_recv
{
public:
	CMain(void);
	~CMain(void);
	void OnRecv(char* buf);
	serial_comm* uart;
};
