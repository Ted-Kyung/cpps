#include "ProxyLRFdata.h"

ProxyLRFdata::ProxyLRFdata(InterfaceLRF* pOwer)
{
	m_pOwer = pOwer;
	nerv_err_t err = this->create(JH_Virtual_LRF::c_standard_port,Nerv::inet_addr("127.0.0.1"),1,1);
	printf("proxy LRF Create => %s\n",get_err_str(err));
}

ProxyLRFdata::~ProxyLRFdata(void)
{
}
void ProxyLRFdata::on_object_create()
{
	nerv_err_t err = this->scan_data_connect(false,2,0);
	printf("connection => %s \n",get_err_str(err));
}

void ProxyLRFdata::on_object_destroy()
{
	printf("LRF Scaner destroyed\n");
}

void ProxyLRFdata::on_scan_data_disconnected()
{

}

void ProxyLRFdata::on_scan_data_sync( const sender_sync_model_t& in_sync_model,
							   const double& in_interval,
							   JH_Virtual_LRF::struct_lrf_data const* const pin_buffer )
{

	lrfData = new struct_lrf_data;
	memcpy(lrfData,pin_buffer,sizeof(JH_Virtual_LRF::struct_lrf_data));

	BITMAPFILEHEADER* bitmapfileheader = (BITMAPFILEHEADER*)lrfData->lrf_data();
	BITMAPINFOHEADER* bitmapinfoheader = (BITMAPINFOHEADER*)(bitmapfileheader + 1);
	unsigned short* pin_data = (unsigned short*)(bitmapinfoheader + 1);

	int width = (int)bitmapinfoheader->biWidth;

	unsigned short* RangeData;
	RangeData = new unsigned short[width];

	memcpy(RangeData,pin_data,sizeof(unsigned short) * width);

	m_pOwer->lrfrecv(RangeData, width);

	delete lrfData;
	delete RangeData;
}