#include "ObjectEstimation.h"
////////////////////////////////////////////////////////////
// 입력		: 장애물 융합된 정보 
// 장애물	: 정렬 되지 않은 상태 
// 속성 부여: 현재 클래스 에서 속성을 부여함 
// 추적 기능: 현재 클래스 에서 추적 기능 담당 
// 출력		: 누적된 장애물 정보를 토대로 추적하여 ID 부여 
// 발전		: 추적 + 추측하여 장애물 확장을 추측값으로 발전 
////////////////////////////////////////////////////////////
ObjectEstimation::ObjectEstimation(Interface_Estimation* pOwer)
{
	m_pOwer = pOwer;
	tick_count = 0;
	mp_cumulation_obstacle = m_estimateObstacle.begin();
}

ObjectEstimation::~ObjectEstimation(void)
{
	int cnt = 0 ;
	for(mp_cumulation_obstacle = m_estimateObstacle.begin() ; mp_cumulation_obstacle < m_estimateObstacle.end() ; mp_cumulation_obstacle++ )
	{
		m_estimateObstacle[cnt].clear();
		cnt++;
	}
	m_estimateObstacle.clear();
}
void ObjectEstimation::estimationClear( void )
{
	int cnt = 0 ;
	for(mp_cumulation_obstacle = m_estimateObstacle.begin() ; mp_cumulation_obstacle < m_estimateObstacle.end() ; mp_cumulation_obstacle++ )
	{
		m_estimateObstacle[cnt].clear();
		cnt++;
	}
	m_estimateObstacle.clear();
}
void ObjectEstimation::estimation(deque<ObjectRectangular>::iterator pObstacleBegin , deque<ObjectRectangular>::iterator pObstacleEnd)
{
	if (tick_count > 49999)
	{
		tick_count = 0;
	}
	tick_count++;

	for( pObstacleBegin ; pObstacleBegin < pObstacleEnd ; pObstacleBegin ++ )
	{
		pObstacleBegin->tick = tick_count;
		obstacle_Cumulation(pObstacleBegin);
	}
	
	obstacle_Delete();

	debug_datacheck();

	m_pOwer->estimationInterface();	// 장애물 정보 되돌려줌. 
}

void ObjectEstimation::obstacle_Cumulation(deque<ObjectRectangular>::iterator pobstacle)		// 장애물 누적
{
	bool search_success = true;
	
	// 누적 장애물이 있는지 확인 기존 누적 데이터 있으면 재 누적 
	search_success = obstacle_Search(pobstacle);	

	// 누적 장애물 없으면 새로운 장애물 생성 
	if(search_success == false)
	{
		obstacle_Create(pobstacle);
	}

	// 추적값 기반으로 입력된 장애물이 어떤 장애물인지 식별 

	// 누적 장애물 기반으로 추적값 생성 

}
bool ObjectEstimation::obstacle_Search(deque<ObjectRectangular>::iterator pobstacle)	//비교(입력 장애물이 기존 누적 장애물에 있는지 확인) 
{
	if ((int)m_estimateObstacle.size() == 0)											//비교 데이터 가 없을떄 무조건 저장 
	{
		return false;
	}	

	int pcnt = 0;
	double d_x;
	double d_y;

	bool find = false;

	//장애물 비교 검색


	for(mp_cumulation_obstacle = m_estimateObstacle.begin() ; mp_cumulation_obstacle < m_estimateObstacle.end() ; mp_cumulation_obstacle ++)
	{  
		d_x = abs(m_estimateObstacle[pcnt][0].centerXY.x) - abs(pobstacle->centerXY.x);
		d_y = abs(m_estimateObstacle[pcnt][0].centerXY.y) - abs(pobstacle->centerXY.y);
		

		if(abs(d_x)+abs(d_y) < SAME_RES)
		{
			if((int)m_estimateObstacle[pcnt].size() == BUFSIZE )
			{
				mp_cumulation_obstacle->erase(mp_cumulation_obstacle->begin()+BUFSIZE-1);
			}
			obstacle_Save(mp_cumulation_obstacle,pobstacle);
			find = true;
		}
		pcnt++;
	}
	return find;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	deque<deque<ObjectRectangular> >::iterator			mp_cumulation_obstacle;			// 누적 장애물 데이터 포인터 저장 (장애물 갯수별 구분)
//	deque<deque<ObjectRectangular> >					m_estimateObstacle;				// 누적 장애물 데이터 
//	deque<ObjectRectangular>							m_guessObstacle;				// 추측 데이터  
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ObjectEstimation::obstacle_Create(deque<ObjectRectangular>::iterator pobstacle)									// 새로운 장애물 생성
{
	deque<ObjectRectangular> buf;
	buf.resize(0);
	buf.push_front(*pobstacle);	

	m_estimateObstacle.push_front(buf);											// 가로열로 갯수 한개 증가

}

void ObjectEstimation::obstacle_Save(deque<deque<ObjectRectangular> >::iterator	mp_cumulation_obstacle , deque<ObjectRectangular>::iterator pobstacle)
{
	mp_cumulation_obstacle->push_front(*pobstacle);
}

void ObjectEstimation::obstacle_Delete(void)																			// 사라진 장애물 삭제 
{
	int pcnt = 0;
	for(mp_cumulation_obstacle = m_estimateObstacle.begin() ; mp_cumulation_obstacle < m_estimateObstacle.end() ; mp_cumulation_obstacle ++)
	{  
		if(tick_count - m_estimateObstacle[pcnt][0].tick >= 20)
		{
			m_estimateObstacle[pcnt].clear();
			m_estimateObstacle.erase(m_estimateObstacle.begin()+pcnt);
			pcnt=0;
			mp_cumulation_obstacle = m_estimateObstacle.begin() ;
		}
		pcnt++;
	}
}
void ObjectEstimation::debug_datacheck(void)			//데이터 디버깅용 함수 
{
	int height =0;
	int i = 0;
	
	printf(" Estimator Debug \n");	

	for(mp_cumulation_obstacle = m_estimateObstacle.begin() ; mp_cumulation_obstacle<m_estimateObstacle.end() ; mp_cumulation_obstacle++)
	{
		height = (int)mp_cumulation_obstacle->size();
		for (int j = 0 ; j < height ; j++)
		{		
			
			printf("%d => x=%.2f y=%.2f \n"
													,i
													,(double)m_estimateObstacle[i][j].centerXY.x			
													,(double)m_estimateObstacle[i][j].centerXY.y);

		}
		if(i%2 == 0)
		{
			consoleColor(0,1,0);
		}
		else
		{
			consoleColor(0,0,1);
		}
		i++;
		printf("||\n");
	}
	printf("\n");

}
