#pragma once
#include "stdafx.h"

#define BUFSIZE 10
#define SAME_RES 10.

using namespace std;
class Interface_Estimation
{
public:
	virtual void estimationInterface() = 0;
};

class ObjectEstimation
{
public:	
	ObjectEstimation(Interface_Estimation* pOwer);
	~ObjectEstimation(void);
	
	Interface_Estimation* m_pOwer;

protected:
	bool search_success;
	unsigned int tick_count;


	deque<deque<ObjectRectangular> >::iterator			mp_cumulation_obstacle;						// 누적 장애물 데이터 포인터 저장 (장애물 갯수별 구분)
	deque<deque<ObjectRectangular> >					m_estimateObstacle;							// 누적 장애물 데이터 
	deque<ObjectRectangular>							m_guessObstacle;							// 추측 데이터  
public:
	void estimation(deque<ObjectRectangular>::iterator pObstacleBegin,deque<ObjectRectangular>::iterator pObstacleEnd );			// StubObjectManager 와 연계되는 통로 함수 
	void estimationClear( void );
protected:
	bool obstacle_Search(deque<ObjectRectangular>::iterator pobstacle);															// 장애물 비교 
	void obstacle_Create(deque<ObjectRectangular>::iterator pobstacle);																		// 새로운 장애물 생성
	void obstacle_Save(deque<deque<ObjectRectangular> >::iterator	mp_cumulation_obstacle , deque<ObjectRectangular>::iterator pobstacle);
	void obstacle_Delete(void);																			// 사라진 장애물 삭제 
	void obstacle_Cumulation(deque<ObjectRectangular>::iterator pobstacle);							// 장애물 누적
	void debug_datacheck(void);																			// 데이터 확인용 변수 
};
