#pragma once
#include "stdafx.h"
#include <JH_Virtual_LRF.h>

class InterfaceLRF
{
public:
	virtual void lrfrecv(unsigned short* lrfdata, int width) = 0;
};


using namespace JH_Virtual_LRF;

class ProxyLRFdata : public JH_Virtual_LRF::proxy_method_scan_data
{
public:
	ProxyLRFdata(InterfaceLRF* pOwer);
	~ProxyLRFdata(void);

	InterfaceLRF* m_pOwer;
	
	struct_lrf_data* lrfData;		

	virtual void on_object_create();
	virtual void on_object_destroy();
	virtual void on_scan_data_disconnected();
	virtual void on_scan_data_sync( const sender_sync_model_t& in_sync_model,
		const double& in_interval,
		JH_Virtual_LRF::struct_lrf_data const* const pin_buffer );
};
