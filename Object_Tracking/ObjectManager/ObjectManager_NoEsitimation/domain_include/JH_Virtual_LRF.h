/****************************************************************************************************************************************************

    Document Header

    domain               : JH_Virtual_LRF
    creator              : Lemonade
    creator information  : 
    standardport         : 8430
    description          : Estimaor 작업용
                           
                                                                                                  by Lemonade, at Tue Apr 03 15:59:03 2012

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__JH_Virtual_LRF__JH_Virtual_LRF_h
#define Define__JH_Virtual_LRF__JH_Virtual_LRF_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace JH_Virtual_LRF
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 8430;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_lrf_data
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_lrf_data
        {
            /*  */
            variable_array_field_dctrl_t lrf_data_dctrl;
            ::Nerv::u_int8_t* lrf_data() { return (::Nerv::u_int8_t*)(dynamicbuffer+lrf_data_dctrl.dindex); }
            bool lrf_data_alloc(unsigned int array_size)
            {
                lrf_data_dctrl.dindex = dynamicsize;
                lrf_data_dctrl.dsize = array_size*sizeof(::Nerv::u_int8_t);
                if(_maxDynamicSize < dynamicsize + lrf_data_dctrl.dsize) return false;
                dynamicsize += lrf_data_dctrl.dsize;
                return true;
            }

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(::Nerv::u_int8_t)*36100};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(struct_lrf_data)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<struct_lrf_data> struct_lrf_data_history_elem;
        typedef template_history<struct_lrf_data> struct_lrf_data_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : scan_data
            Input      : ::
            Output     : JH_Virtual_LRF::struct_lrf_data
            Method Type: INFORMATION
            Code       : 0x0001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_scan_data : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_scan_data() :    nerv_proxy_object_information(0x0001) {}

            bool scan_data_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t scan_data_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void scan_data_shutdown() { shutdown(); }

            void scan_data_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t scan_data_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void scan_data_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t scan_data_read( JH_Virtual_LRF::struct_lrf_data* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(JH_Virtual_LRF::struct_lrf_data);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_scan_data_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_scan_data_sync(in_sync_model,in_interval ,(JH_Virtual_LRF::struct_lrf_data*)pin_buffer );
            }

            virtual void on_scan_data_disconnected() = 0; //{}

            virtual void on_scan_data_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             JH_Virtual_LRF::struct_lrf_data const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_scan_data : public ::Nerv::nerv_object_information
        {

        public:

            method_scan_data(int interval= 0) : nerv_object_information(0x0001) {}

            void scan_data_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void scan_data_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            JH_Virtual_LRF::struct_lrf_data* scan_data_get()
            {
                return (JH_Virtual_LRF::struct_lrf_data*)get(sizeof(JH_Virtual_LRF::struct_lrf_data) );
            }

            void scan_data_cancel()
            {
                cancel();
            }

            nerv_err_t scan_data_post(JH_Virtual_LRF::struct_lrf_data* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((JH_Virtual_LRF::struct_lrf_data*)pin_buf)->get_sizeof();
                return post( outSize);
            }

            nerv_err_t scan_data_write(JH_Virtual_LRF::struct_lrf_data* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((JH_Virtual_LRF::struct_lrf_data*)pin_buf)->get_sizeof();
                return write(pin_buf,outSize);
            }

            letter_case_t scan_data_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void scan_data_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t scan_data_read( JH_Virtual_LRF::struct_lrf_data* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(JH_Virtual_LRF::struct_lrf_data);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_scan_data_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_scan_data_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_scan_data_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_scan_data_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__JH_Virtual_LRF__JH_Virtual_LRF_h
