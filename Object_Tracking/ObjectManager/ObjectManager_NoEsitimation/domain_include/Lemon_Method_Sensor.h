/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Method_Sensor
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:30:22 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Method_Sensor__Lemon_Method_Sensor_h
#define Define__Lemon_Method_Sensor__Lemon_Method_Sensor_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Sensor.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Method_Sensor
{

        using namespace Lemon_Data_Sensor;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : set_sensor_mount
            Input      : Lemon_Data_Sensor::struct_sensor_mount
            Output     : Lemon_Data_Sensor::struct_sensor_mount
            Method Type: CALL
            Code       : 0x0000
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_set_sensor_mount : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_set_sensor_mount()    : nerv_proxy_object_call(0x0000) {}

            ::Nerv::nerv_err_t set_sensor_mount(Lemon_Data_Sensor::struct_sensor_mount* pIn,Lemon_Data_Sensor::struct_sensor_mount* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Lemon_Data_Sensor::struct_sensor_mount);
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Sensor::struct_sensor_mount);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void set_sensor_mount_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_set_sensor_mount : public ::Nerv::nerv_object_call
        {
        public:
            method_set_sensor_mount() :  nerv_object_call(0x0000,sizeof(Lemon_Data_Sensor::struct_sensor_mount)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_set_sensor_mount(in_client_addr,(Lemon_Data_Sensor::struct_sensor_mount*)pin_buf,(Lemon_Data_Sensor::struct_sensor_mount*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Sensor::struct_sensor_mount);
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_set_sensor_mount_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_set_sensor_mount(const ::Nerv::client_addr_t& client_addr,Lemon_Data_Sensor::struct_sensor_mount* pIn,Lemon_Data_Sensor::struct_sensor_mount* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_set_sensor_mount_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Method_Sensor__Lemon_Method_Sensor_h
