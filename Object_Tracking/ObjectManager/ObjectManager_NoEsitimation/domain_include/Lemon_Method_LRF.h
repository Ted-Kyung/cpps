/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Method_LRF
    creator              : Lemonade
    creator information  : 
    standardport         : 213
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:30:14 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Method_LRF__Lemon_Method_LRF_h
#define Define__Lemon_Method_LRF__Lemon_Method_LRF_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_LRF.h>
#include <Lemon_Method_Sensor.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Method_LRF
{

        using namespace Lemon_Data_LRF;
        using namespace Lemon_Method_Sensor;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 213;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : range_map
            Input      : ::
            Output     : Lemon_Data_LRF::struct_range_map
            Method Type: INFORMATION
            Code       : 0x0000
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_range_map : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_range_map() :    nerv_proxy_object_information(0x0000) {}

            bool range_map_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t range_map_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void range_map_shutdown() { shutdown(); }

            void range_map_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t range_map_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void range_map_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t range_map_read( Lemon_Data_LRF::struct_range_map* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_LRF::struct_range_map);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_range_map_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_range_map_sync(in_sync_model,in_interval ,(Lemon_Data_LRF::struct_range_map*)pin_buffer );
            }

            virtual void on_range_map_disconnected() = 0; //{}

            virtual void on_range_map_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_LRF::struct_range_map * pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_range_map : public ::Nerv::nerv_object_information
        {

        public:

            method_range_map(int interval= 0) : nerv_object_information(0x0000) {}

            void range_map_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void range_map_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_LRF::struct_range_map* range_map_get()
            {
                return (Lemon_Data_LRF::struct_range_map*)get(sizeof(Lemon_Data_LRF::struct_range_map) );
            }

            void range_map_cancel()
            {
                cancel();
            }

            nerv_err_t range_map_post(Lemon_Data_LRF::struct_range_map* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_LRF::struct_range_map*)pin_buf)->get_sizeof();
                return post( outSize);
            }

            nerv_err_t range_map_write(Lemon_Data_LRF::struct_range_map* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_LRF::struct_range_map*)pin_buf)->get_sizeof();
                return write(pin_buf,outSize);
            }

            letter_case_t range_map_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void range_map_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t range_map_read( Lemon_Data_LRF::struct_range_map* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_LRF::struct_range_map);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_range_map_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_range_map_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_range_map_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_range_map_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Method_LRF__Lemon_Method_LRF_h
