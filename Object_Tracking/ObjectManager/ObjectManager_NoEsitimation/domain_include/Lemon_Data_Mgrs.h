/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Mgrs
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:17:41 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Mgrs__Lemon_Data_Mgrs_h
#define Define__Lemon_Data_Mgrs__Lemon_Data_Mgrs_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Mgrs
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_mgrs_v1
            writer     : Lemonade
            description: size = 21
        ********************************************************************************************************************************************/
        struct packed struct_mgrs_v1
        {
            /* 전세계에 대한 60구역 ID */
            ::Nerv::u_int32_t global_zone :6;

            /*  */
            ::Nerv::u_int32_t latitude_zone :5;

            /* global Zone 의 서브 구역값, 2자리의 알파벳대문자 아스키값1 */
            ::Nerv::u_int32_t square_zone_1 :5;

            /*  */
            ::Nerv::u_int32_t square_zone_2 :5;

            /*  */
            ::Nerv::u_int32_t elevation_zone :7;

            /*  */
            ::Nerv::u_int32_t reserved_bit :4;

            /* 0.05 mm 해상도1 */
            static const double min_east() { return double(-100000.); }
            static const double max_east() { return double(100000.); }
            ::Nerv::real< ::Nerv::int32_t,min_east,max_east > east;

            /* 0.05 mm 해상도1 */
            static const double min_north() { return double(-100000.); }
            static const double max_north() { return double(100000.); }
            ::Nerv::real< ::Nerv::int32_t,min_north,max_north > north;

            /* 0.05 mm 해상도, 실제 elevation값은  elevationZone*100000 + elevation1 */
            static const double min_elevation() { return double(-100000.); }
            static const double max_elevation() { return double(100000.); }
            ::Nerv::real< ::Nerv::int32_t,min_elevation,max_elevation > elevation;

            /* 0.025 mm 해상도1 */
            static const double min_rms() { return double(0.); }
            static const double max_rms() { return double(100000.); }
            ::Nerv::real< ::Nerv::int32_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_mgrs_v1> struct_mgrs_v1_history_elem;
        typedef template_history<struct_mgrs_v1> struct_mgrs_v1_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Mgrs__Lemon_Data_Mgrs_h
