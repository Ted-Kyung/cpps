/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_LRF
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:17:37 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_LRF__Lemon_Data_LRF_h
#define Define__Lemon_Data_LRF__Lemon_Data_LRF_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Image.h>
#include <Lemon_Data_Navigation.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_LRF
{

        using namespace Lemon_Data_Image;
        using namespace Lemon_Data_Navigation;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_range_sensor_info
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_range_sensor_info
        {
            /* pi*rad */
            static const double min_hfov() { return double(0.0); }
            static const double max_hfov() { return double(2.0); }
            ::Nerv::real< ::Nerv::int16_t,min_hfov,max_hfov > hfov;

            /* pi*rad */
            static const double min_vfov() { return double(0.0); }
            static const double max_vfov() { return double(2.0); }
            ::Nerv::real< ::Nerv::int16_t,min_vfov,max_vfov > vfov;

            /* m */
            static const double min_resolution() { return double(0.0); }
            static const double max_resolution() { return double(65.534); }
            ::Nerv::real< ::Nerv::int16_t,min_resolution,max_resolution > resolution;

            /*  */
            Lemon_Data_Navigation::struct_navigation_v1 navigation;

        };
        typedef template_history_elem<struct_range_sensor_info> struct_range_sensor_info_history_elem;
        typedef template_history<struct_range_sensor_info> struct_range_sensor_info_history;

        /********************************************************************************************************************************************
            data name  : struct_range_map
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_range_map
        {
            /*  */
            Lemon_Data_LRF::struct_range_sensor_info sensor_info;

            /*  */
            Lemon_Data_Image::enum_image_format_v1 map_format;

            /*  */
            variable_array_field_dctrl_t map_dctrl;
            ::Nerv::u_int8_t* map() { return (::Nerv::u_int8_t*)(dynamicbuffer+map_dctrl.dindex); }
            bool map_alloc(unsigned int array_size)
            {
                map_dctrl.dindex = dynamicsize;
                map_dctrl.dsize = array_size*sizeof(::Nerv::u_int8_t);
                if(_maxDynamicSize < dynamicsize + map_dctrl.dsize) return false;
                dynamicsize += map_dctrl.dsize;
                return true;
            }

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(::Nerv::u_int8_t)*65000};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(struct_range_map)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<struct_range_map> struct_range_map_history_elem;
        typedef template_history<struct_range_map> struct_range_map_history;

        /********************************************************************************************************************************************
            data name  : struct_range_map_frame
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_range_map_frame
        {
            /*  */
            ::Nerv::u_int16_t range;

            /*  */
            ::Nerv::u_int16_t intensity;

        };
        typedef template_history_elem<struct_range_map_frame> struct_range_map_frame_history_elem;
        typedef template_history<struct_range_map_frame> struct_range_map_frame_history;

        /********************************************************************************************************************************************
            data name  : struct_range_map_ibeo_header
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_range_map_ibeo_header
        {
            /*  */
            ::Nerv::u_int32_t num_of_layer;

            /*  */
            ::Nerv::int32_t start_angle[4];

            /*  */
            ::Nerv::int32_t end_angle[4];

            /*  */
            ::Nerv::int32_t resolution;

            /*  */
            ::Nerv::int32_t angle_res;

        };
        typedef template_history_elem<struct_range_map_ibeo_header> struct_range_map_ibeo_header_history_elem;
        typedef template_history<struct_range_map_ibeo_header> struct_range_map_ibeo_header_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_LRF__Lemon_Data_LRF_h
