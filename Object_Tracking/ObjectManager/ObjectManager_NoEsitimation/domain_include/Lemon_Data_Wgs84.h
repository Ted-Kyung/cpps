/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Wgs84
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:18:05 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Wgs84__Lemon_Data_Wgs84_h
#define Define__Lemon_Data_Wgs84__Lemon_Data_Wgs84_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Wgs84
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : enum_longitude_zone_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_longitude_zone_v1;
        const enum_longitude_zone_v1 c_zone_east = 0; // description:
        const enum_longitude_zone_v1 c_zone_west = 1; // description:
        typedef template_history_elem<enum_longitude_zone_v1> enum_longitude_zone_v1_history_elem;
        typedef template_history<enum_longitude_zone_v1> enum_longitude_zone_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_wgs84_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_wgs84_v1
        {
            /*  */
            Lemon_Data_Wgs84::enum_longitude_zone_v1 longitude_zone :1;

            /*  */
            ::Nerv::int8_t elevation_zone :7;

            /*  */
            static const double min_latitude() { return double(-90.0); }
            static const double max_latitude() { return double(90.0); }
            ::Nerv::real< ::Nerv::int32_t,min_latitude,max_latitude > latitude;

            /*  */
            static const double min_longitude() { return double(0.0); }
            static const double max_longitude() { return double(180.0); }
            ::Nerv::real< ::Nerv::int32_t,min_longitude,max_longitude > longitude;

            /*  */
            static const double min_elevation() { return double(-100000.0); }
            static const double max_elevation() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_elevation,max_elevation > elevation;

            /*  */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_wgs84_v1> struct_wgs84_v1_history_elem;
        typedef template_history<struct_wgs84_v1> struct_wgs84_v1_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Wgs84__Lemon_Data_Wgs84_h
