#include "StubObjectManager.h"
#include <stdio.h>

StubObjectManager::StubObjectManager(void)
{
	m_FirstTime = TRUE;
#ifdef TRACKING
	m_Oes = new ObjectEstimation(this);
#endif
	
}

StubObjectManager::~StubObjectManager(void)
{

}

void StubObjectManager::ObstacleData(deque<ObjectRectangular> obstacleSet)
{

	//debug_datacheck(obstacleSet);									//수신데이터 확인용
	
	m_fusion_obstacle = ObstacleExtended(obstacleSet);				//안전상의 문제로 장애물의 확장 기능 수행후 저장 
	
	//debug_datacheck(m_fusion_obstacle);							//데이터 저장 확인용 

	m_fusion_obstacle = ObstacleFusion(m_fusion_obstacle);			//통과 불가능한 지역의 장애물을 하나로 만드는 작업을 수행
	
	debug_datacheck(m_fusion_obstacle);							//데이터 저장 확인용 

#ifdef TRACKING
	if((int)m_fusion_obstacle.size() > 0 )							//융합 장애물이 1개 이상일때만 추적 시작 				
	{
		m_Oes->estimation(m_fusion_obstacle.begin(),m_fusion_obstacle.end());						//장애물 추적 및 ID 부여 class
	}
	if ((int)m_fusion_obstacle.size() == 0)														
	{
		m_Oes->estimationClear();																		//장애물 갯수가 0이면 초기화 시킴 
	}
#endif 

	
	m_fusion_obstacle.clear();
}

//장애물 확장 
deque<ObjectRectangular> StubObjectManager::ObstacleExtended(deque<ObjectRectangular> fusedObstacle)
{
	if (fusedObstacle.empty())	//데이터가 비어있으면 하위 함수 처리 안함 
	{
		return fusedObstacle;
	}

	double Extend_distance;
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{
		Extend_distance = (fusedObstacle[i].Width / 2.) + EXTEND_DISTANCE;

		//장애물을 바라보는 각도에 의하여 확장 = 정의 별첨 
		fusedObstacle[i].startXY.x	= ((double)Extend_distance * sin((double)((-90. + fusedObstacle[i].Ang)/DR))) +(double)fusedObstacle[i].centerXY.x;
		fusedObstacle[i].startXY.y	= ((double)Extend_distance * cos((double)((-90. + fusedObstacle[i].Ang/DR)))) +(double)fusedObstacle[i].centerXY.y;
		fusedObstacle[i].endXY.x	= ((double)Extend_distance * sin((double)((90. + fusedObstacle[i].Ang/DR))))+(double)fusedObstacle[i].centerXY.x;
		fusedObstacle[i].endXY.y	= ((double)Extend_distance * cos((double)((90. + fusedObstacle[i].Ang/DR))))+(double)fusedObstacle[i].centerXY.y;	

		fusedObstacle[i].Width		= pdistance((double)fusedObstacle[i].startXY.x
			,(double)fusedObstacle[i].startXY.y
			,(double)fusedObstacle[i].endXY.x	
			,(double)fusedObstacle[i].endXY.y);
	}
	return fusedObstacle;
}

//장애물 융합
deque<ObjectRectangular> StubObjectManager::ObstacleFusion(deque<ObjectRectangular> obstacleSet)
{
	double  distanceBuf	= 0.;
	bool	validity	= TRUE;
	int		ObstacleCnt = (int)obstacleSet.size();
	

	for(int cnt = 0 ; cnt < ObstacleCnt ; cnt++)
	{
		validity	= TRUE;									// 장애물 유효성 처리 이벤트 알림 변수 

		// 1] deque obstacle 데이터 검사 ( 유효성 검사 ) *장애물의 폭			
		if(obstacleSet[cnt].Width < MIN_OBSTACLE_WIDTH)		// 지정된 장애물 최소폭 보다 작으면 장애물 처리 안함 
		{
			obstacleSet.erase(obstacleSet.begin()+cnt);		// 해당 벡터를 지운다 (이후 자동정렬됨)
			--cnt;											// 해당 자리를 위하여 같은 구간을 한번더 검색 
			--ObstacleCnt;									// 장애물 총갯수 수정 
			validity	= FALSE;	
		}
		
		//2] deque 순차적으로 center xy 값과 width 를 계산하여 거리를 계산후 통과하지 못하는 장애물들을 합침		
		
		if((validity == TRUE) && !(cnt+1 == ObstacleCnt))
		{
			// 스캔데이터의 방향성을 고려하여 장애물의 끝 점과 다음장애물의 시작점의 거리를 계산하여 장애물 사이의 거리값을 산출 한다 
			distanceBuf =	(double)pdistance(	(double)obstacleSet[cnt].endXY.x
										,(double)obstacleSet[cnt].endXY.y
										,(double)obstacleSet[cnt+1].startXY.x
										,(double)obstacleSet[cnt+1].startXY.y);
			
			if(distanceBuf < MIN_PASS_DISTANCE)					// 최소 통과 폭 보다 좁으면 다음장애물의 끝점을 저장시키고 순번을 저장한다. 
			{
				obstacleSet[cnt].endXY.x = obstacleSet[cnt+1].endXY.x;	
				obstacleSet[cnt].endXY.y = obstacleSet[cnt+1].endXY.y;	
				reSizingData(&obstacleSet[cnt]);
				obstacleSet.erase(obstacleSet.begin()+cnt+1);							
				--cnt;											// 지워진 자리를 위하여 같은 구간을 한번더 검색 
				--ObstacleCnt;									// 장애물 총갯수 수정
			}
		}
	}
	//3] 융합된 장애물을 Return 시킴 
	return obstacleSet;
}

//융합된 장애물의 사이즈및 정의 재정의
void StubObjectManager::reSizingData(ObjectRectangular* Data)
{
	Data->centerXY.x	=	((double)Data->startXY.x + (double)Data->endXY.x) /2.;	//장애물의 중간점 구함 
	Data->centerXY.y	=	((double)Data->startXY.y + (double)Data->endXY.y) /2.;


	Data->distance	=   (double)pdistance(Data->centerXY.x , Data->centerXY.y,0,0);	// 센서 기준 거리 -> 기준좌표 (0,0)
	Data->Ang		=	AngCalculator(acos((double)Data->centerXY.y / (double)Data->distance) / PI * 180.);
	if (Data->centerXY.x < 0)
	{
		Data->Ang = Data->Ang * -1;
	}
	Data->Width		=	sqrt(	(double)square((double)difference((double)Data->startXY.x,(double)Data->endXY.x)) + 
								(double)square((double)difference((double)Data->startXY.y,(double)Data->endXY.y)));
}

double StubObjectManager::AngCalculator(double Angle)
{
	double ang_buf = Angle;
	bool check = FALSE;

	while(1)
	{
		check = FALSE;
		if(ang_buf>360.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		else if(ang_buf > 180.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		if(check == FALSE) break;
	}
	return ang_buf;
}
																					
void StubObjectManager::debug_datacheck(deque<ObjectRectangular> obstacleSet)			//데이터 디버깅용 함수 
{
	consoleColor(0,0,1);
	printf("! Change %d ",(int)obstacleSet.size());	

	for(int i = 0 ; i < (int)obstacleSet.size() ; i++)
	{
		consoleColor(1,1,0);
		printf(" x=%.2f y=%.2f w=%.2f a=%.2f"	,(double)obstacleSet[i].centerXY.x			
												,(double)obstacleSet[i].centerXY.y
												,(double)obstacleSet[i].Width
												,(double)obstacleSet[i].Ang);
		consoleColor(0,1,0);
		printf("||");
	}
	printf("\n");
}
#ifdef TRACKING
	void StubObjectManager::estimationInterface()
	{
		
	}
#endif