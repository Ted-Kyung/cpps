#pragma once
#include <vector>
#include <deque>
#include <math.h>
#include <Windows.h>

//#define TRACKING

#define PI 3.141592
#define DR 180.0 * PI

inline double difference(double x, double _x)
{
	return (double)_x-(double)x;
}
inline double square(double x)
{
	return (double)x*(double)x;
}
inline double pdistance(double fx, double fy,double sx, double sy)
{
	return (double)sqrt((square(difference(fx,sx))) + (square(difference(fy,sy))));
}
struct Rectangular
{
	double x;
	double y;
};

struct ObjectRectangular
{
	int			ID;
	double		heading;
	double		speed;

	Rectangular	startXY;
	Rectangular	endXY;
	Rectangular	centerXY;

	double		Width;
	double		Ang;
	double		distance;	

	unsigned int tick;
};

/////////////////////////////////////////////////////////////////콘솔 색 입히기 
const HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);

inline int _drgb ( bool d, bool r, bool g, bool b)
{
	return (d << 3) + (r << 2) + (g << 1) + (b << 0);
}

inline int _clr ( int fc, int bc )
{
	return (fc << 0) + (bc << 4);
}

inline void consoleColor(bool r, bool g, bool b)
{
	int fore = _drgb (true,r,g,b);		// 명암값 , R , G  , B 
	int back = _drgb (0,0,0,0);
	int color = _clr (fore,back);
	SetConsoleTextAttribute(h,color);	
}


