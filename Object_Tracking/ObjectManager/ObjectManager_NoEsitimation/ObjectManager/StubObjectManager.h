#pragma once
#include "stdafx.h"
#include "RangeDataDivide.h"
#include "ObjectEstimation.h"
#include <deque>

#define EXTEND_DISTANCE		0.8									// Extend length  : Meter
#define MIN_OBSTACLE_WIDTH	EXTEND_DISTANCE * 2.			    // 장애물 폭 최소 거리 (폭이 작으면 장애물 무시)   미터 단위 
#define MIN_PASS_DISTANCE	2.5									// 장애물 사이 간격으로 로봇이 통과할수 있는 폭(못지나가면 합침)    미터 단위 


using namespace std;

class StubObjectManager	: public Interface_Estimation
{
public:
	StubObjectManager(void);
	~StubObjectManager(void);

public:
	BOOL m_FirstTime;	

#ifdef TRACKING
	ObjectEstimation* m_Oes;										// ObjectEstimation 저장 
#endif
	
	deque <ObjectRectangular>	m_fusion_obstacle;					// 데이터 융합 처리 후 저장 공간 

	void ObstacleData(deque<ObjectRectangular> obstacleSet);		//LRF 데이터를 분리 하여 만든 오브젝트를 받아옴	 
	void reSizingData(ObjectRectangular* Data);						// 융합된 장애물들을 사이즈 재정의 
	double AngCalculator(double Angle);								// 장애물 각도 유효성 검사
	
	deque<ObjectRectangular> ObstacleFusion(deque<ObjectRectangular> obstacleSet);		// 수신된 Obstacle(장애물) 데이터를 커다란 묶음으로 처리
	deque<ObjectRectangular> ObstacleExtended(deque<ObjectRectangular> fusedObstacle);	//장애물 확장 기능 
	
	void debug_datacheck(deque<ObjectRectangular> obstacleSet);	// 데이터 확인용 변수 

#ifdef TRACKING
	virtual void estimationInterface(void);
#endif
	};
