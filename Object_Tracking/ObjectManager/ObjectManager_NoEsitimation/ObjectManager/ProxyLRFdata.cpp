#include "ProxyLRFdata.h"

ProxyLRFdata::ProxyLRFdata(InterfaceLRF* pOwer)
{
	m_pOwer = pOwer;
	nerv_err_t err = this->create(Lemon_Method_LRF::c_standard_port,Nerv::inet_addr("127.0.0.1"),30,1);
	printf("proxy LRF Create => %s\n",get_err_str(err));
}

ProxyLRFdata::~ProxyLRFdata(void)
{
}
void ProxyLRFdata::on_object_create()
{
	nerv_err_t err = this->range_map_connect(false,2,0);
	printf("connection => %s \n",get_err_str(err));
}

void ProxyLRFdata::on_object_destroy()
{
	printf("LRF Scaner destroyed\n");
}


void ProxyLRFdata::on_range_map_disconnected()
{

}

void ProxyLRFdata::on_range_map_sync( const sender_sync_model_t& in_sync_model,
							   const double& in_interval,
							   Lemon_Data_LRF::struct_range_map * pin_buffer )
{
	lrfData = new Lemon_Data_LRF::struct_range_map;
	memcpy(lrfData,pin_buffer,sizeof(Lemon_Data_LRF::struct_range_map));

	BITMAPFILEHEADER* bitmapfileheader = (BITMAPFILEHEADER*)lrfData->map();
	BITMAPINFOHEADER* bitmapinfoheader = (BITMAPINFOHEADER*)(bitmapfileheader + 1);
	unsigned short* pin_data = (unsigned short*)(bitmapinfoheader + 1);

	int width = (int)bitmapinfoheader->biWidth;
	res = (double)pin_buffer->sensor_info.resolution;

	unsigned short* RangeData;
	RangeData = new unsigned short[width];

	memcpy(RangeData,pin_data,sizeof(unsigned short) * width);

	m_pOwer->lrfrecv(RangeData, width, res);

	delete lrfData;
	delete RangeData;
}
