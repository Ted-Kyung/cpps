#pragma once
#include "stdafx.h"
#include "ProxyLRFdata.h"

#define RESOLUTION 0.5		// 레이저 스캐너 해상도 
							
#define ObjectOFFSET 1.5	// 오브젝트로 구분할 LRF 포인터의 떨어진 간격(이 수치보다 멀면 장애물 출현으로 인지)  
							// 미터 단위 M (1.5 미터 ) 

#define MAX_RANGE 6000		// 레이저 최대 거리 (발산시)s  -> cm 단위 (60 미터) 
							
#define START 1				// 장애물 저장 시작
#define END   0				// 장애물 저장 종료 

using namespace std;		

class StubObjectManager;

class ObjectManagerInterface
{
public:
	virtual void ObstacleData(ObjectRectangular* obstacleSet,int cnt) =0;
};

class RangeDataDivide : public InterfaceLRF
{
public:
	RangeDataDivide(void);
	~RangeDataDivide(void);

	ProxyLRFdata* proxylrf;
	StubObjectManager*	ObjectManager;

	deque <ObjectRectangular>	obstacle;
	ObjectRectangular			O_buffer;

	int							objectcount;													// deque 로 구성된 데이터를 배열로 다시 정렬 하기위하여 데이터 갯수 카운트
	int							startend;

	double DENOMINATION;
	double startAng;
	double endAng;

	virtual void lrfrecv(unsigned short* lrfdata, int width, double res);

	void StartObject(double Data, int TotalAngle,int res);
	void EndObject(double Data, int TotalAngle,int res);
};
