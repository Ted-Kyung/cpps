#include "RangeDataDivide.h"
#include "StubObjectManager.h"
#include <stdio.h>
#include <conio.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sensor -> Fused Velodyne 2 Layer 
// Action -> Safe Pathe Define 용 
// 작성자 -> 경재현 
////////////////////////////////////////////////////////////////////////////////////////////////////////////


void main(void)
{
	
	RangeDataDivide*	datadivide		= new RangeDataDivide;
	while(1)
	{
		if(_kbhit() == 0)
		{
			if(_getch() == 'q')break;
		}
	}
	delete datadivide;
}

RangeDataDivide::RangeDataDivide(void)
{
	proxylrf		= new ProxyLRFdata(this);
	ObjectManager	= new StubObjectManager;

	DENOMINATION = 0.01;//(double)proxylrf->res;
}

RangeDataDivide::~RangeDataDivide(void)
{
	obstacle.clear();
	delete proxylrf;
	delete ObjectManager;
}


void RangeDataDivide::lrfrecv(unsigned short* lrfdata, int width, double res)
{
	objectcount = 0;														// 현재 LRF 데이터를 저장 중인지 아닌지를 판별 
	startend	= END;

	for(int i = 0 ; i < width ; i++)
	{
		if((MAX_RANGE > ((int)lrfdata[i] * res)) && (i == 0))						// 시작데이터가 최대 감지거리보다 짧을때. 장애물로 인식하고 저장 시작. 
		{																	// 시작데이터 예외 처림 
			StartObject((double)(lrfdata[i] * res),width,i);						
		}
		
		if(i>0 && (abs(((double)(lrfdata[i] * res)) - ((double)(lrfdata[i-1] * res))) > ObjectOFFSET))
		{
			
			switch (startend)
			{
			case 0:
				StartObject((double)(lrfdata[i] * res),width,i);						// 이전 데이터와 현재 데이터를 비교하여 오프셋보다 거리가 멀고  
				break;													
			case 1:															// 이전 데이터와 현재 데이터를 비교하여 오프셋보다 거리가 멀고 
				EndObject((double)(lrfdata[i-1] * res),width,i-1);						// 장애물 저장중이면 장애물 종료점으로 판단(저장값은 이전값 데이터를 저장)
				if((double)lrfdata[i] * res != MAX_RANGE)
				{
					StartObject((double)(lrfdata[i] * res),width,i);
				}
				break;													
			}
		}
		if((((double)lrfdata[i] * res) == MAX_RANGE) && (startend ==START))
		{ 
			EndObject((double)(lrfdata[i-1] * res),width,i-1);
		}
		if((obstacle.size() >= 0) &&(i== (width-1)) && startend==START)			
		{
			EndObject((double)(lrfdata[i] * res),width,i);
		}
		else if((i == (width-1)) && (startend==START))
		{
			EndObject((double)(lrfdata[i] * res),width,i);				
		}
	}
	
	if(objectcount > 0)								
	{
		consoleColor(1,1,1);
		printf("Divide Object %d>\n",objectcount);
		ObjectManager->ObstacleData(obstacle);
	}
	else
	{
		consoleColor(1,0,0);
		printf("LRF Data Clear(object none)\n");
		ObjectManager->ObstacleData(obstacle);
	}
	obstacle.clear();
}

void RangeDataDivide::StartObject(double Data, int TotalAngle,int res)
{
	startAng = (double)(res * RESOLUTION)  -  (double)(TotalAngle * RESOLUTION )/ 2.;
	
	O_buffer.startXY.x	= (double)Data * sin((double)(startAng/DR));		//  /DENOMINATION. = 미터단위로 표시하기 위해서  
	O_buffer.startXY.y	= (double)Data * cos((double)(startAng/DR));		//  /DENOMINATION. = 미터단위로 표시하기 위해서  
	startend = START;
}
void RangeDataDivide::EndObject(double Data, int TotalAngle,int res)
{
	endAng			= (double)(res * RESOLUTION)  -  (double)(TotalAngle * RESOLUTION )/ 2.;
	double angpm	= 1.;
	O_buffer.endXY.x	=	(double)Data * sin((double)(endAng/DR));		//  /DENOMINATION. = 미터단위로 표시하기 위해서  
	O_buffer.endXY.y	=	(double)Data * cos((double)(endAng/DR));		//  /DENOMINATION. = 미터단위로 표시하기 위해서  

	O_buffer.centerXY.x	=	((double)O_buffer.startXY.x + (double)O_buffer.endXY.x) /2.;	//장애물의 중간점 구함 
	O_buffer.centerXY.y =	((double)O_buffer.startXY.y + (double)O_buffer.endXY.y) /2.;

	if(O_buffer.centerXY.x<0)
	{
		angpm = -1.0;
	}

	O_buffer.distance	=   (double)sqrt(((double)square(O_buffer.centerXY.x)) + (double)square(O_buffer.centerXY.y));	// 센서 기준 거리 -> 기준좌표 (0,0)
	O_buffer.Ang		=	(startAng + endAng )/2.;
	O_buffer.Width		= sqrt((double)square((double)difference((double)O_buffer.startXY.x,(double)O_buffer.endXY.x)) + 
		(double)square((double)difference((double)O_buffer.startXY.y,(double)O_buffer.endXY.y)));
	objectcount++;
	startend = END;
 	obstacle.push_back(O_buffer);
}
