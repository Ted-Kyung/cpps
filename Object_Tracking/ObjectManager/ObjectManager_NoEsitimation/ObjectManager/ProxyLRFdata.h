#pragma once
#include "stdafx.h"
#include <Lemon_Method_LRF.h>

class InterfaceLRF
{
public:
	virtual void lrfrecv(unsigned short* lrfdata, int width, double res) = 0;
};


using namespace Lemon_Data_LRF;

class ProxyLRFdata : public Lemon_Method_LRF::proxy_method_range_map
{
public:
	ProxyLRFdata(InterfaceLRF* pOwer);
	~ProxyLRFdata(void);

	InterfaceLRF* m_pOwer;
	
	double res;	
	
	virtual void on_object_create();
	virtual void on_object_destroy();
	
	Lemon_Data_LRF::struct_range_map* lrfData;

	virtual void on_range_map_disconnected();

	virtual void on_range_map_sync( const sender_sync_model_t& in_sync_model,
		const double& in_interval,
		Lemon_Data_LRF::struct_range_map * pin_buffer );
};
