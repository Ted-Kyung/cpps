/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Estimator
    creator              : Lemonade
    creator information  : 
    standardport         : 10002
    description          : 장애물 추정
                           
                                                                                                  by root, at Wed Sep 16 15:07:20 2009

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Estimator__Lemon_Data_Estimator_h
#define Define__Lemon_Data_Estimator__Lemon_Data_Estimator_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_LRF.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Estimator
{

        using namespace Lemon_Data_LRF;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 10002;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : enum_obstacle_movement
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_obstacle_movement;
        const enum_obstacle_movement c_stop = 0; // description:고정 장애물
        const enum_obstacle_movement c_movement = 1; // description:이동 장애물
        typedef template_history_elem<enum_obstacle_movement> enum_obstacle_movement_history_elem;
        typedef template_history<enum_obstacle_movement> enum_obstacle_movement_history;

        /********************************************************************************************************************************************
            data name  : struct_point
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_point
        {
            /*  */
            ::Nerv::ieee754_64_t x;

            /*  */
            ::Nerv::ieee754_64_t y;

            /*  */
            ::Nerv::u_int16_t index;

        };
        typedef template_history_elem<struct_point> struct_point_history_elem;
        typedef template_history<struct_point> struct_point_history;

        /********************************************************************************************************************************************
            data name  : struct_obstacle
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_obstacle
        {
            /*  */
            ::Nerv::int32_t identifier;

            /*  */
            ::Nerv::ieee754_64_t sensing_time;

            /*  */
            Lemon_Data_Estimator::enum_obstacle_movement state_movement;

            /*  */
            ::Nerv::char_t state;

            /*  */
            ::Nerv::ieee754_64_t width;

            /*  */
            ::Nerv::ieee754_64_t velocity;

            /*  */
            ::Nerv::ieee754_64_t head;

            /*  */
            ::Nerv::ieee754_64_t distance;

            /*  */
            Lemon_Data_Estimator::struct_point center;

            /*  */
            Lemon_Data_Estimator::struct_point start;

            /*  */
            Lemon_Data_Estimator::struct_point end;

        };
        typedef template_history_elem<struct_obstacle> struct_obstacle_history_elem;
        typedef template_history<struct_obstacle> struct_obstacle_history;

        /********************************************************************************************************************************************
            data name  : struct_obstacle_set
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_obstacle_set
        {
            /*  */
            variable_array_field_dctrl_t obstacle_dctrl;
            Lemon_Data_Estimator::struct_obstacle* obstacle() { return (Lemon_Data_Estimator::struct_obstacle*)(dynamicbuffer+obstacle_dctrl.dindex); }
            bool obstacle_alloc(unsigned int array_size)
            {
                obstacle_dctrl.dindex = dynamicsize;
                obstacle_dctrl.dsize = array_size*sizeof(Lemon_Data_Estimator::struct_obstacle);
                if(_maxDynamicSize < dynamicsize + obstacle_dctrl.dsize) return false;
                dynamicsize += obstacle_dctrl.dsize;
                return true;
            }

            /*  */
            Lemon_Data_LRF::struct_range_map fixed_lrf;

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(Lemon_Data_Estimator::struct_obstacle)*300};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(struct_obstacle_set)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<struct_obstacle_set> struct_obstacle_set_history_elem;
        typedef template_history<struct_obstacle_set> struct_obstacle_set_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Estimator__Lemon_Data_Estimator_h
