/****************************************************************************************************************************************************

    Document Header

    domain               : FM_KJH_ON_GROUND_PATH
    creator              : Lemonade
    creator information  : 
    standardport         : 8508
    description          : 
                                                                                                  by Lemonade, at Tue Jun 19 16:22:37 2012

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__FM_KJH_ON_GROUND_PATH__FM_KJH_ON_GROUND_PATH_h
#define Define__FM_KJH_ON_GROUND_PATH__FM_KJH_ON_GROUND_PATH_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace FM_KJH_ON_GROUND_PATH
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 8508;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)
        /********************************************************************************************************************************************
            data name  : Rectangular
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed Rectangular
        {
            /*  */
            static const double min_x() { return double(-100000.); }
            static const double max_x() { return double(100000); }
            ::Nerv::real< ::Nerv::int32_t,min_x,max_x > x;

            /*  */
            static const double min_y() { return double(-100000.0); }
            static const double max_y() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_y,max_y > y;

        };
        typedef template_history_elem<Rectangular> Rectangular_history_elem;
        typedef template_history<Rectangular> Rectangular_history;

        /********************************************************************************************************************************************
            data name  : ObjectRectangular
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed ObjectRectangular
        {
            /*  */
            static const double min_Width() { return double(-100000.0); }
            static const double max_Width() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_Width,max_Width > Width;

            /*  */
            static const double min_Ang() { return double(-100000.0); }
            static const double max_Ang() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_Ang,max_Ang > Ang;

            /*  */
            static const double min_distance() { return double(-100000.0); }
            static const double max_distance() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_distance,max_distance > distance;

            /*  */
            FM_KJH_ON_GROUND_PATH::Rectangular startXY;

            /*  */
            FM_KJH_ON_GROUND_PATH::Rectangular endXY;

            /*  */
            FM_KJH_ON_GROUND_PATH::Rectangular centerXY;

        };
        typedef template_history_elem<ObjectRectangular> ObjectRectangular_history_elem;
        typedef template_history<ObjectRectangular> ObjectRectangular_history;


    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : ground_tracking_path
            Input      : ::
            Output     : FM_KJH_ON_GROUND_PATH::ObjectRectangular
            Method Type: INFORMATION
            Code       : 0xF8F8
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ground_tracking_path : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_ground_tracking_path() :    nerv_proxy_object_information(0xF8F8) {}

            bool ground_tracking_path_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t ground_tracking_path_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void ground_tracking_path_shutdown() { shutdown(); }

            void ground_tracking_path_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t ground_tracking_path_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void ground_tracking_path_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t ground_tracking_path_read( FM_KJH_ON_GROUND_PATH::ObjectRectangular* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(FM_KJH_ON_GROUND_PATH::ObjectRectangular);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_ground_tracking_path_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_ground_tracking_path_sync(in_sync_model,in_interval ,(FM_KJH_ON_GROUND_PATH::ObjectRectangular*)pin_buffer );
            }

            virtual void on_ground_tracking_path_disconnected() = 0; //{}

            virtual void on_ground_tracking_path_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             FM_KJH_ON_GROUND_PATH::ObjectRectangular const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_ground_tracking_path : public ::Nerv::nerv_object_information
        {

        public:

            method_ground_tracking_path(int interval= 0) : nerv_object_information(0xF8F8) {}

            void ground_tracking_path_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void ground_tracking_path_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            FM_KJH_ON_GROUND_PATH::ObjectRectangular* ground_tracking_path_get()
            {
                return (FM_KJH_ON_GROUND_PATH::ObjectRectangular*)get(sizeof(FM_KJH_ON_GROUND_PATH::ObjectRectangular) );
            }

            void ground_tracking_path_cancel()
            {
                cancel();
            }

            nerv_err_t ground_tracking_path_post(FM_KJH_ON_GROUND_PATH::ObjectRectangular* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(FM_KJH_ON_GROUND_PATH::ObjectRectangular);
                return post( outSize);
            }

            nerv_err_t ground_tracking_path_write(FM_KJH_ON_GROUND_PATH::ObjectRectangular* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(FM_KJH_ON_GROUND_PATH::ObjectRectangular);
                return write(pin_buf,outSize);
            }

            letter_case_t ground_tracking_path_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void ground_tracking_path_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t ground_tracking_path_read( FM_KJH_ON_GROUND_PATH::ObjectRectangular* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(FM_KJH_ON_GROUND_PATH::ObjectRectangular);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_ground_tracking_path_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_ground_tracking_path_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_ground_tracking_path_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ground_tracking_path_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__FM_KJH_ON_GROUND_PATH__FM_KJH_ON_GROUND_PATH_h
