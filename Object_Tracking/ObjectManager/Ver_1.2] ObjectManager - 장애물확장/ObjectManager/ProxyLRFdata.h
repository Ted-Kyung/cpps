#pragma once
#include "stdafx.h"
#include <Lemon_Method_LRF.h>

class InterfaceLRF
{
public:
	virtual void lrfrecv(unsigned short* lrfdata, int width, double east , double north, double heading) = 0;
};


//using namespace JH_Virtual_LRF;
using namespace Lemon_Data_LRF;

class ProxyLRFdata : public Lemon_Method_LRF::proxy_method_range_map//public JH_Virtual_LRF::proxy_method_scan_data
{
public:
	ProxyLRFdata(InterfaceLRF* pOwer);
	~ProxyLRFdata(void);

	InterfaceLRF* m_pOwer;

	//Lemon_Data_LRF::struct_range_map lrfData;
	Lemon_Data_LRF::struct_range_map* lrfData;
	
	//struct_lrf_data* lrfData;		

	virtual void on_object_create();
	virtual void on_object_destroy();


	virtual void on_range_map_disconnected();

	virtual void on_range_map_sync( const sender_sync_model_t& in_sync_model,
		const double& in_interval,
		Lemon_Data_LRF::struct_range_map * pin_buffer );

	//virtual void on_scan_data_disconnected();
	//virtual void on_scan_data_sync( const sender_sync_model_t& in_sync_model,
	//	const double& in_interval,
	//	JH_Virtual_LRF::struct_lrf_data const* const pin_buffer );
};
