#pragma once
#include "stdafx.h"
#include "RangeDataDivide.h"
#include "StubObjectManager.h"
#include "FM_KJH_ON_GROUND_PATH.h"

using namespace std;

class StubObjectManager : public FM_KJH_ON_GROUND_PATH::method_ground_tracking_path, public ObjectManagerInterface
{
public:
	StubObjectManager(void);
	~StubObjectManager(void);

	FILE* m_file;

public:
	BOOL m_FirstTime;	
	double m_east;
	double m_north;
	double m_heading;

	deque <ObjectRectangular>	m_fusion_obstacle;					// 데이터 융합 처리 후 저장 공간 

	void ObstacleData(deque<ObjectRectangular> obstacleSet, double east , double north,double heading);		//LRF 데이터를 분리 하여 만든 오브젝트를 받아옴 	
	deque<ObjectRectangular> ObstacleExtended(deque<ObjectRectangular> fusedObstacle);		//장애물 확장 기능 
	deque<ObjectRectangular> ObstacleFusion(deque<ObjectRectangular> obstacleSet);		// 수신된 Obstacle(장애물) 데이터를 커다란 묶음으로 처리 
	void reSizingData(ObjectRectangular* Data);						// 융합된 장애물들을 사이즈 재정의 
	double AngCalculator(double Angle);								// 장애물 각도 유효성 검사

	deque<ObjectRectangular> ObstacleAngleDistanceFilter(deque<ObjectRectangular> fusedObstacle);
	deque<ObjectRectangular> ObstacleGroundFilter(deque<ObjectRectangular> fusedObstacle, double east , double north);

	void ObstacleMakeID(deque<ObjectRectangular> fusedObstacle);	// 장애물마다 ID를 부여 (추적기능 포함시켜야 함) 
	
	void debug_datacheck(deque<ObjectRectangular> obstacleSet);	// 데이터 확인용 변수 

	virtual nerv_err_t on_ground_tracking_path_connect( const client_addr_t& in_client_addr,
		const bool& in_tcp_request,
		const double& in_elapsed_time,
		const double& in_limit_time );

	virtual void on_ground_tracking_path_shutdown( const client_addr_t& in_client_addr);
};
