#pragma once
#include "stdafx.h"
#include "ProxyLRFdata.h"



using namespace std;

class StubObjectManager;

class ObjectManagerInterface
{
public:
	virtual void ObstacleData(ObjectRectangular* obstacleSet,int cnt) =0;
};

class RangeDataDivide : public InterfaceLRF
{
public:
	RangeDataDivide(void);
	~RangeDataDivide(void);

	ProxyLRFdata* proxylrf;
	StubObjectManager*	ObjectManager;

	vector <ObjectRectangular>	obstacle;
	ObjectRectangular			O_buffer;

	int							objectcount;													// vector 로 구성된 데이터를 배열로 다시 정렬 하기위하여 데이터 갯수 카운트
	int							startend;

	double startAng;
	double endAng;

	double m_east;
	double m_north;
	double m_heading;
	double denomination;

	virtual void lrfrecv(unsigned short* lrfdata, int width, double east , double north , double heading);

	void StartObject(int Data, int TotalAngle,int res);
	void EndObject(int Data, int TotalAngle,int res);
};
