#include "StubObjectManager.h"
#include <stdio.h>



StubObjectManager::StubObjectManager(void)
{
	int id;
	char buf[128];
	GetPrivateProfileString("ObstacleManager","ID","41",buf,sizeof(buf),".\\ObjectManager.ini");
	id = atoi(buf);

	nerv_err_t err = this->create(FM_KJH_ON_GROUND_PATH::c_standard_port,id,1);
	printf("Ground Tracking Create => %s ID : %d\n",get_err_str(err),id);
	m_FirstTime = TRUE;
	m_file = fopen("Rord Log.log","w+");
}

StubObjectManager::~StubObjectManager(void)
{
	fclose(m_file);
}

void StubObjectManager::ObstacleData(vector<ObjectRectangular> obstacleSet, double east , double north, double heading)
{
	m_east		= east;
	m_north		= north;
	m_heading	= heading;

	//데이터가 비어있으면 하위 함수 처리 안함 
	if (obstacleSet.empty())	
	{
		return;
	}

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleAngleDistanceFilter START\n");
	consoleColor(1,1,1);
#endif

	//가용 Angle 값 과 희망거리값 이상의 장애물 데이터 삭제 
	m_fusion_obstacle = ObstacleAngleDistanceFilter(obstacleSet);

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleAngleDistanceFilter END\n");
	consoleColor(1,1,1);
	//수신데이터 확인용
	debug_datacheck(m_fusion_obstacle);	
#endif

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleFusion 처리 시작 \n");
	consoleColor(1,1,1);
#endif

	//통과 불가능한 지역의 장애물을 하나로 만드는 작업을 수행
	m_fusion_obstacle = ObstacleFusion(m_fusion_obstacle);	

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleFusion 처리 종료 \n");
	consoleColor(1,1,1);
	//수신데이터 확인용
	debug_datacheck(m_fusion_obstacle);	
#endif


#ifdef GROUND_FILTER
	//제일 가까운 장애물중 땅에 가까운 데이터만을 남기고 모두 삭제 하는 루틴 
	m_fusion_obstacle = ObstacleGroundFilter(m_fusion_obstacle,east,north);
	//printf("ObstacleGroundFilter\n");
	debug_datacheck(m_fusion_obstacle);	// 도로의 가운데 점만 저장 할수있도록 한후 데이터 체크 	
#endif



#ifdef GROUND_FILTER
	//데이터 서비스 해줄곳 (좌표 서비스)
	{
		FM_KJH_ON_GROUND_PATH::ObjectRectangular pinbuff;

		pinbuff.startXY.x		= (double)m_fusion_obstacle[0].startXY.x;
		pinbuff.startXY.y		= (double)m_fusion_obstacle[0].startXY.y;
		pinbuff.centerXY.x		= (double)m_fusion_obstacle[0].centerXY.x;
		pinbuff.centerXY.y		= (double)m_fusion_obstacle[0].centerXY.y;
		pinbuff.endXY.x			= (double)m_fusion_obstacle[0].endXY.x;
		pinbuff.endXY.y			= (double)m_fusion_obstacle[0].endXY.y;

		pinbuff.distance		= (double)m_fusion_obstacle[0].distance;
		pinbuff.Ang				= (double)m_fusion_obstacle[0].Ang;
		pinbuff.Width			= (double)m_fusion_obstacle[0].Width;

		ground_tracking_path_write(&pinbuff);
	}
#endif

	
	
	// 장애물 전체의 domain 작업후 데이터 전송 
	debug_datacheck(m_fusion_obstacle);	// 도로의 가운데 점만 저장 할수있도록 한후 데이터 체크 	
	m_fusion_obstacle.clear();
}

vector<ObjectRectangular>  StubObjectManager::ObstacleFusion(vector<ObjectRectangular> obstacleSet)
{
	double  distanceBuf	= 0.;
	for(int cnt = 0 ; cnt <(int)obstacleSet.size() ; cnt ++ )
	{
		// 1] vector obstacle 데이터 검사 ( 유효성 검사 ) *장애물의 폭			
		if(obstacleSet[cnt].Width < MIN_OBSTACLE_WIDTH)		// 지정된 장애물 최소폭 보다 작으면 장애물 처리 안함 
		{
			#ifdef DEBUG_PRINTF
			printf("폭 미달 삭제 =>(%d, w =%lf)\n",cnt,obstacleSet[cnt].Width );
			#endif	
			// 해당 벡터를 지운다 (이후 자동정렬됨)			
			obstacleSet.erase(obstacleSet.begin()+cnt);	
			// 해당 자리를 위하여 같은 구간을 한번더 검색	
			--cnt;											
		}
	}

	for(int cnt = 0 ; cnt < (int)obstacleSet.size() ; cnt ++ )
	{
		
		if((int)obstacleSet.size() <= cnt +1) break;
		// 스캔데이터의 방향성을 고려하여 장애물의 끝 점과 다음장애물의 시작점의 거리를 계산하여 장애물 사이의 거리값을 산출 한다 
		distanceBuf =	pdistance(obstacleSet[cnt].endXY.x,obstacleSet[cnt].endXY.y,obstacleSet[cnt+1].startXY.x,obstacleSet[cnt+1].startXY.y);
		
		// 최소 통과 폭 보다 좁으면 다음장애물의 끝점을 저장시키고 순번을 저장한다.
		if(distanceBuf < MIN_PASS_DISTANCE)					 
		{
#ifdef DEBUG_PRINTF
			printf("size = %d ,  cnt = %d\n",(int)obstacleSet.size(),cnt);
#endif			
			obstacleSet[cnt].endXY.x = (double)obstacleSet[cnt+1].endXY.x;	
			obstacleSet[cnt].endXY.y = (double)obstacleSet[cnt+1].endXY.y;	
			
			#ifdef DEBUG_PRINTF
			printf("fusion !! (%d %d) (%lf %lf) => ",cnt,cnt+1,(double)obstacleSet[cnt].Width,(double)obstacleSet[cnt+1].Width);
			#endif
			
			reSizingData(&obstacleSet[cnt]);
#ifdef DEBUG_PRINTF
			printf("cnt %d %lf \n",cnt,(double)obstacleSet[cnt].Width);
#endif
			obstacleSet.erase(obstacleSet.begin()+cnt+1);							
			--cnt;											// 지워진 자리를 위하여 같은 구간을 한번더 검색 
			//cnt = 0;
		}
	} 
	return obstacleSet;
}

void StubObjectManager::reSizingData(ObjectRectangular* Data)
{
	Data->centerXY.x	=	((double)Data->startXY.x + (double)Data->endXY.x) /2.;	//장애물의 중간점 구함 
	Data->centerXY.y	=	((double)Data->startXY.y + (double)Data->endXY.y) /2.;


	Data->distance	=   (double)pdistance(Data->centerXY.x , Data->centerXY.y,m_north,m_east);	// 센서 기준 거리 -> 기준좌표 (0,0)
	//180 ~ -180
	Data->Ang		=	(double)atan2((double)(Data->centerXY.y- m_east),(double)(Data->centerXY.x- m_north)) * 180. / PI -  (double)(m_heading * 180.);
	
	Data->Width		=	sqrt(	(double)square((double)difference((double)Data->startXY.x,(double)Data->endXY.x)) + 
		(double)square((double)difference((double)Data->startXY.y,(double)Data->endXY.y)));
}

double StubObjectManager::AngCalculator(double Angle)
{
	double ang_buf = Angle;
	bool check = FALSE;

	while(1)
	{
		check = FALSE;
		if(ang_buf>360.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		else if(ang_buf > 180.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		if(check == FALSE) break;
	}
	return ang_buf;
}

void ObstacleMakeID(vector<ObjectRectangular> fusedObstacle)
{

}

void StubObjectManager::debug_datacheck(vector<ObjectRectangular> obstacleSet)
{
	consoleColor(0,0,1);
	printf("! Change %d \n",(int)obstacleSet.size());	

	for(int i = 0 ; i < (int)obstacleSet.size() ; i++)
	{
		consoleColor(1,1,0);
		printf(" x=%.2f y=%.2f w=%.2f a=%.2f d = %.2lf"	,(double)obstacleSet[i].centerXY.y			
			,(double)obstacleSet[i].centerXY.x
			,(double)obstacleSet[i].Width
			,(double)obstacleSet[i].Ang
			,(double)obstacleSet[i].distance);
		consoleColor(0,1,0);
		printf("\n");
	}
	printf("\n");

	for(int i = 0 ; i < (int)obstacleSet.size() ; i++)
	{
		fprintf(m_file," x=%lf,y=%lf,w=%lf,a=%lf, rx=%lf,ry=%lf,"	,(double)obstacleSet[i].centerXY.x			
			,(double)obstacleSet[i].centerXY.y
			,(double)obstacleSet[i].Width
			,(double)obstacleSet[i].Ang
			,(double)m_east
			,(double)m_north);
		fprintf(m_file,"||");
	}
	fprintf(m_file,"\n");
}


vector<ObjectRectangular> StubObjectManager::ObstacleAngleDistanceFilter(vector<ObjectRectangular> fusedObstacle)
{
#ifdef DEBUG_PRINTF
	printf("ObstacleAngleDistanceFilter\n");
#endif	
	// 융합된 장애물 정보를 처음부터 끝까지 검색 
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{
		if((int)fusedObstacle.size() > 1)
		{
			// ABS_ANGLE_USE 보다 장애물이 위치한 각도가 크고 , 거리가 DETECTION_MAX_DISTANCE 보다 멀거나 DETECTION_MIN_DISTANCE 보다 작으면 삭제 
			if(( (int)(abs(fusedObstacle[i].Ang))  >  ABS_ANGLE_USE  )  || (((double)fusedObstacle[i].distance > DETECTION_MAX_DISTANCE)) || (((double)fusedObstacle[i].distance < DETECTION_MIN_DISTANCE)))
			{
				#ifdef DEBUG_PRINTF
				printf("%d , %lf 삭제 \n",(int)fusedObstacle[i].Ang,(double)fusedObstacle[i].distance);
				#endif	

				fusedObstacle.erase(fusedObstacle.begin() + i);
				--i;
			}
			else 
			{
				#ifdef DEBUG_PRINTF
				printf("!!!!!!!!!!!!!!!!!!!!!%d , %lf 유지 \n",(int)fusedObstacle[i].Ang,(double)fusedObstacle[i].distance);
				#endif				
			}
		}
	}
	return fusedObstacle;
}

vector<ObjectRectangular> StubObjectManager::ObstacleGroundFilter(vector<ObjectRectangular> fusedObstacle, double east , double north)
{
#ifdef DEBUG_PRINTF
	printf("ObstacleGroundFilter\n");
#endif	
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{

		if((int)fusedObstacle.size() > 1)
		{
			if((double)fusedObstacle[i].Width < (double)fusedObstacle[i+1].Width)
			{
				fusedObstacle.erase(fusedObstacle.begin() + i);
				--i;
			}
			else if((double)fusedObstacle[i].Width >= (double)fusedObstacle[i+1].Width)
			{
				fusedObstacle.erase(fusedObstacle.begin() + i + 1);
				--i;
			}
		}
		//장애물 전체 사이즈가 1 보다 작으면 정지
		if(((int)fusedObstacle.size()) <= (1) ) 
		{
			#ifdef DEBUG_PRINTF
			printf("GroundFilter 사이즈 부족\n");
			#endif	

			break;
		}
	}	
	//위 For 문을 거치면 결과적으로 결과 값은 하나만 남음
	return fusedObstacle;
}

nerv_err_t StubObjectManager::on_ground_tracking_path_connect( const client_addr_t& in_client_addr,
															  const bool& in_tcp_request,
															  const double& in_elapsed_time,
															  const double& in_limit_time )
{
	printf("connect requirement!!\n");
	return Nerv::c_nerv_err_none;
}

void StubObjectManager::on_ground_tracking_path_shutdown( const client_addr_t& in_client_addr)
{
	printf("proxy shutdown!!\n");
}