/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Time
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:17:58 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Time__Lemon_Data_Time_h
#define Define__Lemon_Data_Time__Lemon_Data_Time_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Time
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : realtime_v1_t
            writer     : Lemonade
            description: 0초=  AD 1년 1월 1일 0시 0분 0초
        ********************************************************************************************************************************************/
        inline const double minrealtime_v1_t() { return double(-9223372036.854775807); }
        inline const double maxrealtime_v1_t() { return double(9223372036.854775807); }
        typedef ::Nerv::real< ::Nerv::int64_t,minrealtime_v1_t,maxrealtime_v1_t > realtime_v1_t;
        typedef template_history_elem<realtime_v1_t> realtime_v1_t_history_elem;
        typedef template_history<realtime_v1_t> realtime_v1_t_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Time__Lemon_Data_Time_h
