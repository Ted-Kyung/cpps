/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Navigation
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Mon Feb 23 19:25:43 2009

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Navigation__Lemon_Data_Navigation_h
#define Define__Lemon_Data_Navigation__Lemon_Data_Navigation_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Mgrs.h>
#include <Lemon_Data_Time.h>
#include <Lemon_Data_Wgs84.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Navigation
{

        using namespace Lemon_Data_Mgrs;
        using namespace Lemon_Data_Time;
        using namespace Lemon_Data_Wgs84;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_linear_velocity_v1
            writer     : Lemonade
            description: size=25
        ********************************************************************************************************************************************/
        struct packed struct_linear_velocity_v1
        {
            /* 0.001 mm/s 단위 */
            static const double min_res() { return double(0.000001); }
            static const double max_res() { return double(4294.967295); }
            ::Nerv::real< ::Nerv::int32_t,min_res,max_res > res;

            /*  */
            static const double min_east() { return double(-2147483647.0); }
            static const double max_east() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_east,max_east > east;

            /*  */
            static const double min_north() { return double(-2147483647.0); }
            static const double max_north() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_north,max_north > north;

            /*  */
            static const double min_down() { return double(-2147483647.0); }
            static const double max_down() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_down,max_down > down;

            /*  */
            static const double min_speed() { return double(0.0); }
            static const double max_speed() { return double(4294967294.0); }
            ::Nerv::real< ::Nerv::int32_t,min_speed,max_speed > speed;

            /*  */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(4294967294.0); }
            ::Nerv::real< ::Nerv::int32_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_linear_velocity_v1> struct_linear_velocity_v1_history_elem;
        typedef template_history<struct_linear_velocity_v1> struct_linear_velocity_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_linear_acceleration_v1
            writer     : Lemonade
            description: size=25
        ********************************************************************************************************************************************/
        struct packed struct_linear_acceleration_v1
        {
            /* 0.000001m/s^2 */
            static const double min_res() { return double(0.000001); }
            static const double max_res() { return double(4294.967295); }
            ::Nerv::real< ::Nerv::int32_t,min_res,max_res > res;

            /*  */
            static const double min_x() { return double(-2147483647.0); }
            static const double max_x() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_x,max_x > x;

            /*  */
            static const double min_y() { return double(-2147483647.0); }
            static const double max_y() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_y,max_y > y;

            /*  */
            static const double min_z() { return double(-2147483647.0); }
            static const double max_z() { return double(2147483647.0); }
            ::Nerv::real< ::Nerv::int32_t,min_z,max_z > z;

            /*  */
            static const double min_acceleratingForce() { return double(0.0); }
            static const double max_acceleratingForce() { return double(4294967294.0); }
            ::Nerv::real< ::Nerv::int32_t,min_acceleratingForce,max_acceleratingForce > acceleratingForce;

            /*  */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(4294967294.0); }
            ::Nerv::real< ::Nerv::int32_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_linear_acceleration_v1> struct_linear_acceleration_v1_history_elem;
        typedef template_history<struct_linear_acceleration_v1> struct_linear_acceleration_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_linear_navigation_v1
            writer     : Lemonade
            description: size=106
        ********************************************************************************************************************************************/
        struct packed struct_linear_navigation_v1
        {
            /*  */
            Lemon_Data_Wgs84::struct_wgs84_v1 wgs84;

            /*  */
            Lemon_Data_Mgrs::struct_mgrs_v1 mgrs;

            /*  */
            Lemon_Data_Navigation::struct_linear_velocity_v1 velocity;

            /*  */
            Lemon_Data_Navigation::struct_linear_acceleration_v1 acceleration;

        };
        typedef template_history_elem<struct_linear_navigation_v1> struct_linear_navigation_v1_history_elem;
        typedef template_history<struct_linear_navigation_v1> struct_linear_navigation_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_attitude_v1
            writer     : Lemonade
            description: size=9
        ********************************************************************************************************************************************/
        struct packed struct_attitude_v1
        {
            /* pi*rad 단위 */
            static const double min_roll() { return double(-1.0); }
            static const double max_roll() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_roll,max_roll > roll;

            /* pi*rad 단위 */
            static const double min_pitch() { return double(-1.0); }
            static const double max_pitch() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_pitch,max_pitch > pitch;

            /* pi*rad 단위 */
            static const double min_heading() { return double(-1.0); }
            static const double max_heading() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_heading,max_heading > heading;

            /* pi*rad */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_attitude_v1> struct_attitude_v1_history_elem;
        typedef template_history<struct_attitude_v1> struct_attitude_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_angular_rate_v1
            writer     : Lemonade
            description: size=13
        ********************************************************************************************************************************************/
        struct packed struct_angular_rate_v1
        {
            /* 0.001 pi*rad/s */
            static const double min_res() { return double(0.001); }
            static const double max_res() { return double(65.535); }
            ::Nerv::real< ::Nerv::int16_t,min_res,max_res > res;

            /*  */
            static const double min_roll() { return double(-32767.0); }
            static const double max_roll() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_roll,max_roll > roll;

            /*  */
            static const double min_pitch() { return double(-32767.0); }
            static const double max_pitch() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_pitch,max_pitch > pitch;

            /*  */
            static const double min_yaw() { return double(-32767.0); }
            static const double max_yaw() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_yaw,max_yaw > yaw;

            /*  */
            static const double min_speed() { return double(0.0); }
            static const double max_speed() { return double(65534.0); }
            ::Nerv::real< ::Nerv::int16_t,min_speed,max_speed > speed;

            /*  */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(65534.0); }
            ::Nerv::real< ::Nerv::int16_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_angular_rate_v1> struct_angular_rate_v1_history_elem;
        typedef template_history<struct_angular_rate_v1> struct_angular_rate_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_anguar_acceleration_v1
            writer     : Lemonade
            description: size=13
        ********************************************************************************************************************************************/
        struct packed struct_anguar_acceleration_v1
        {
            /* 0.001˚/s^2 */
            static const double min_res() { return double(0.001); }
            static const double max_res() { return double(65.535); }
            ::Nerv::real< ::Nerv::int16_t,min_res,max_res > res;

            /*  */
            static const double min_roll() { return double(-32767.0); }
            static const double max_roll() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_roll,max_roll > roll;

            /*  */
            static const double min_pitch() { return double(-32767.0); }
            static const double max_pitch() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_pitch,max_pitch > pitch;

            /*  */
            static const double min_yaw() { return double(-32767.0); }
            static const double max_yaw() { return double(32767.0); }
            ::Nerv::real< ::Nerv::int16_t,min_yaw,max_yaw > yaw;

            /*  */
            static const double min_acceleratingForce() { return double(0.0); }
            static const double max_acceleratingForce() { return double(65534.0); }
            ::Nerv::real< ::Nerv::int16_t,min_acceleratingForce,max_acceleratingForce > acceleratingForce;

            /*  */
            static const double min_rms() { return double(0.0); }
            static const double max_rms() { return double(65534.0); }
            ::Nerv::real< ::Nerv::int16_t,min_rms,max_rms > rms;

        };
        typedef template_history_elem<struct_anguar_acceleration_v1> struct_anguar_acceleration_v1_history_elem;
        typedef template_history<struct_anguar_acceleration_v1> struct_anguar_acceleration_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_rotational_navigation_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_rotational_navigation_v1
        {
            /*  */
            Lemon_Data_Navigation::struct_attitude_v1 attitude;

            /*  */
            Lemon_Data_Navigation::struct_angular_rate_v1 rate;

            /*  */
            Lemon_Data_Navigation::struct_anguar_acceleration_v1 acceleration;

        };
        typedef template_history_elem<struct_rotational_navigation_v1> struct_rotational_navigation_v1_history_elem;
        typedef template_history<struct_rotational_navigation_v1> struct_rotational_navigation_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_navigation_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_navigation_v1
        {
            /*  */
            Lemon_Data_Time::realtime_v1_t timestamp;

            /*  */
            Lemon_Data_Navigation::struct_linear_navigation_v1 linear;

            /*  */
            Lemon_Data_Navigation::struct_rotational_navigation_v1 rotational;

        };
        typedef template_history_elem<struct_navigation_v1> struct_navigation_v1_history_elem;
        typedef template_history<struct_navigation_v1> struct_navigation_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_inertial_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_inertial_v1
        {
            /*  */
            Lemon_Data_Time::realtime_v1_t timestamp;

            /*  */
            ::Nerv::ieee754_64_t x_accel;

            /*  */
            ::Nerv::ieee754_64_t y_accel;

            /*  */
            ::Nerv::ieee754_64_t z_accel;

            /*  */
            ::Nerv::ieee754_64_t r_rate;

            /*  */
            ::Nerv::ieee754_64_t p_rate;

            /*  */
            ::Nerv::ieee754_64_t y_rate;

        };
        typedef template_history_elem<struct_inertial_v1> struct_inertial_v1_history_elem;
        typedef template_history<struct_inertial_v1> struct_inertial_v1_history;

        /********************************************************************************************************************************************
            data name  : struct_bit
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_bit
        {
            /*  */
            ::Nerv::u_int8_t gps :1;

            /*  */
            ::Nerv::u_int8_t imu :1;

            /*  */
            ::Nerv::u_int8_t dru :1;

            /*  */
            ::Nerv::u_int8_t compass :1;

            /*  */
            ::Nerv::u_int8_t computer :1;

            /*  */
            ::Nerv::u_int8_t reserved :3;

        };
        typedef template_history_elem<struct_bit> struct_bit_history_elem;
        typedef template_history<struct_bit> struct_bit_history;

        /********************************************************************************************************************************************
            data name  : enum_mode
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_mode;
        const enum_mode c_nav_mode_integrated = 0; // description:
        const enum_mode c_nav_mode_ins = 3; // description:
        typedef template_history_elem<enum_mode> enum_mode_history_elem;
        typedef template_history<enum_mode> enum_mode_history;

        /********************************************************************************************************************************************
            data name  : enum_alignment
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_alignment;
        const enum_alignment c_align_completion = 0; // description:
        const enum_alignment c_align_being = 3; // description:
        typedef template_history_elem<enum_alignment> enum_alignment_history_elem;
        typedef template_history<enum_alignment> enum_alignment_history;

        /********************************************************************************************************************************************
            data name  : enum_gps_status
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_gps_status;
        const enum_gps_status c_gps_invalid = 0; // description:
        const enum_gps_status c_gps_single = 1; // description:
        const enum_gps_status c_gps_dual = 2; // description:
        typedef template_history_elem<enum_gps_status> enum_gps_status_history_elem;
        typedef template_history<enum_gps_status> enum_gps_status_history;

        /********************************************************************************************************************************************
            data name  : struct_gps_status
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_gps_status
        {
            /*  */
            Lemon_Data_Navigation::enum_alignment alignment :2;

            /*  */
            ::Nerv::u_int8_t reserved :6;

            /*  */
            static const double min_horizontal_align_time() { return double(0.0); }
            static const double max_horizontal_align_time() { return double(255.0); }
            ::Nerv::real< ::Nerv::u_int8_t,min_horizontal_align_time,max_horizontal_align_time > horizontal_align_time;

            /*  */
            Lemon_Data_Navigation::struct_attitude_v1 attitude_align_status;

            /*  */
            static const double min_gps_dop() { return double(0.0); }
            static const double max_gps_dop() { return double(25.5); }
            ::Nerv::real< ::Nerv::int8_t,min_gps_dop,max_gps_dop > gps_dop;

            /*  */
            ::Nerv::u_int8_t gps_satallite_no;

            /*  */
            Lemon_Data_Navigation::enum_gps_status gps_status;

            /*  */
            Lemon_Data_Navigation::enum_mode mode;

            /*  */
            Lemon_Data_Navigation::struct_bit bit;

        };
        typedef template_history_elem<struct_gps_status> struct_gps_status_history_elem;
        typedef template_history<struct_gps_status> struct_gps_status_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Navigation__Lemon_Data_Navigation_h
