/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_General_Component
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:18:15 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_General_Component__Lemon_General_Component_h
#define Define__Lemon_General_Component__Lemon_General_Component_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>

#include <string>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_General_Component
{

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : rank_t
            writer     : Lemonade
            description: 1
        ********************************************************************************************************************************************/
        typedef ::Nerv::u_int8_t rank_t;
        typedef template_history_elem<rank_t> rank_t_history_elem;
        typedef template_history<rank_t> rank_t_history;

        /********************************************************************************************************************************************
            data name  : struct_control_state
            writer     : Lemonade
            description: 2
        ********************************************************************************************************************************************/
        struct packed struct_control_state
        {
            /*  */
            Lemon_General_Component::rank_t rank;

            /*  */
            Lemon_General_Component::rank_t ownerRank;

            /*  */
            ::Nerv::packed_client_addr_t owner;

        };
        typedef template_history_elem<struct_control_state> struct_control_state_history_elem;
        typedef template_history<struct_control_state> struct_control_state_history;

        /********************************************************************************************************************************************
            data name  : enum_primary_operation
            writer     : Lemonade
            description: 9
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_primary_operation;
        const enum_primary_operation c_operation_shutdown = 0; // description:
        const enum_primary_operation c_operation_reset = 1; // description:
        const enum_primary_operation c_operation_standby = 2; // description:
        const enum_primary_operation c_operation_ready = 3; // description:
        const enum_primary_operation c_operation_go = 4; // description:
        const enum_primary_operation c_operation_emergency = 5; // description:
        const enum_primary_operation c_operation_clear_emergency = 6; // description:
        typedef template_history_elem<enum_primary_operation> enum_primary_operation_history_elem;
        typedef template_history<enum_primary_operation> enum_primary_operation_history;

        /********************************************************************************************************************************************
            data name  : enum_secondary_operation
            writer     : Lemonade
            description: 10
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_secondary_operation;
        typedef template_history_elem<enum_secondary_operation> enum_secondary_operation_history_elem;
        typedef template_history<enum_secondary_operation> enum_secondary_operation_history;

        /********************************************************************************************************************************************
            data name  : struct_operation
            writer     : Lemonade
            description: 3
        ********************************************************************************************************************************************/
        struct packed struct_operation
        {
            /*  */
            Lemon_General_Component::enum_primary_operation primary_operation;

            /*  */
            Lemon_General_Component::enum_secondary_operation secondary_operation;

        };
        typedef template_history_elem<struct_operation> struct_operation_history_elem;
        typedef template_history<struct_operation> struct_operation_history;

        /********************************************************************************************************************************************
            data name  : enum_primary_state
            writer     : Lemonade
            description: 6
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_primary_state;
        const enum_primary_state c_state_shutdown = 0; // description:
        const enum_primary_state c_state_initialize = 1; // description:
        const enum_primary_state c_state_standby = 2; // description:
        const enum_primary_state c_state_ready = 3; // description:
        const enum_primary_state c_state_run = 4; // description:
        const enum_primary_state c_state_emergency = 5; // description:
        const enum_primary_state c_state_failure = 6; // description:
        typedef template_history_elem<enum_primary_state> enum_primary_state_history_elem;
        typedef template_history<enum_primary_state> enum_primary_state_history;

        /********************************************************************************************************************************************
            data name  : enum_secondary_state
            writer     : Lemonade
            description: 8
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_secondary_state;
        typedef template_history_elem<enum_secondary_state> enum_secondary_state_history_elem;
        typedef template_history<enum_secondary_state> enum_secondary_state_history;

        /********************************************************************************************************************************************
            data name  : struct_state
            writer     : Lemonade
            description: 4
        ********************************************************************************************************************************************/
        struct packed struct_state
        {
            /*  */
            Lemon_General_Component::enum_primary_state prmState;

            /*  */
            Lemon_General_Component::enum_secondary_state scdState;

        };
        typedef template_history_elem<struct_state> struct_state_history_elem;
        typedef template_history<struct_state> struct_state_history;

        /********************************************************************************************************************************************
            data name  : struct_debug_operation
            writer     : Lemonade
            description: 7
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t struct_debug_operation;
        const struct_debug_operation c_operation_release = 0; // description:
        const struct_debug_operation c_operation_debug = 1; // description:
        typedef template_history_elem<struct_debug_operation> struct_debug_operation_history_elem;
        typedef template_history<struct_debug_operation> struct_debug_operation_history;

        /********************************************************************************************************************************************
            data name  : enum_debug_state
            writer     : Lemonade
            description: 5
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_debug_state;
        const enum_debug_state c_state_release = 0; // description:
        const enum_debug_state c_state_debug = 1; // description:
        const enum_debug_state c_state_debug_stop = 2; // description:
        typedef template_history_elem<enum_debug_state> enum_debug_state_history_elem;
        typedef template_history<enum_debug_state> enum_debug_state_history;

        /********************************************************************************************************************************************
            data name  : struct_error
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_error
        {
            /*  */
            ::Nerv::char_t string[1024];

        };
        typedef template_history_elem<struct_error> struct_error_history_elem;
        typedef template_history<struct_error> struct_error_history;

        /********************************************************************************************************************************************
        data name  : struct_string256
        writer     : Lemonade
        description: 
        ********************************************************************************************************************************************/
        struct packed struct_string256
        {
            /*  */
            ::Nerv::char_t buf[256];

        };
        typedef template_history_elem<struct_string256> struct_string256_history_elem;
        typedef template_history<struct_string256> struct_string256_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : SetDebugOperation
            Input      : Lemon_General_Component::struct_debug_operation
            Output     : Lemon_General_Component::
            Method Type: CALL
            Code       : 0x0001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_SetDebugOperation : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_SetDebugOperation()    : nerv_proxy_object_call(0x0001) {}

            ::Nerv::nerv_err_t SetDebugOperation(Lemon_General_Component::struct_debug_operation* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Lemon_General_Component::struct_debug_operation);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void SetDebugOperation_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_SetDebugOperation : public ::Nerv::nerv_object_call
        {
        public:
            method_SetDebugOperation() :  nerv_object_call(0x0001,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_SetDebugOperation(in_client_addr,(Lemon_General_Component::struct_debug_operation*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_SetDebugOperation_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_SetDebugOperation(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_debug_operation* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_SetDebugOperation_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : GetDebugState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::enum_debug_state
            Method Type: CALL
            Code       : 0x0002
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_GetDebugState : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_GetDebugState()    : nerv_proxy_object_call(0x0002) {}

            ::Nerv::nerv_err_t GetDebugState(Lemon_General_Component::enum_debug_state* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::enum_debug_state);
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void GetDebugState_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_GetDebugState : public ::Nerv::nerv_object_call
        {
        public:
            method_GetDebugState() :  nerv_object_call(0x0002,sizeof(Lemon_General_Component::enum_debug_state)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_GetDebugState(in_client_addr,(Lemon_General_Component::enum_debug_state*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::enum_debug_state);
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_GetDebugState_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_GetDebugState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::enum_debug_state* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_GetDebugState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : ChangedDebugState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::enum_debug_state
            Method Type: EVENT
            Code       : 0x0002
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ChangedDebugState : public ::Nerv::nerv_proxy_object_event
        {
        public:
            proxy_method_ChangedDebugState() : nerv_proxy_object_event(0x0002) {}

            bool ChangedDebugState_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t ChangedDebugState_connect( const double& in_limit_time = 1.0, double* pout_time= 0 )
            {
                return_object_t return_object = async_connect( in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void ChangedDebugState_shutdown() { shutdown(); }

            virtual void on_event( void const * const pin_event_data, const pksize_t& in_event_size, const double&  in_elapsed_time)
            {
                on_ChangedDebugState((Lemon_General_Component::enum_debug_state*)pin_event_data, in_elapsed_time);
            }

        private:
            virtual void on_disconnected() { on_ChangedDebugState_disconnected(); }

            virtual void on_ChangedDebugState(Lemon_General_Component::enum_debug_state* pOut, const double& in_elapsed_time) = 0; //{}

            virtual void on_ChangedDebugState_disconnected() = 0; //{}
        };

        /* stub */
        class method_ChangedDebugState : public ::Nerv::nerv_object_event
        {
        public:
            method_ChangedDebugState() : nerv_object_event(0x0002) {}

            void ChangedDebugState_disconnect(const ::Nerv::client_addr_t client_addr ) { disconnect(client_addr); }

            void ChangedDebugState(Lemon_General_Component::enum_debug_state* pout_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::enum_debug_state);
                event(pout_buf,outSize);
            }

            virtual ::Nerv::nerv_err_t on_connect(  const client_addr_t&   in_client_addr,
                                                    const double&               in_elapsed_time,
                                                    const double&               in_limit_time )
            {
                return on_ChangedDebugState_connect(in_client_addr,in_elapsed_time,in_limit_time);
            }

            virtual void on_shutdown( const ::Nerv::client_addr_t& client_addr) { on_ChangedDebugState_shutdown( client_addr); }

            virtual nerv_err_t on_ChangedDebugState_connect( const client_addr_t& in_client_addr,
                                                       const double& in_elapsed_time,
                                                       const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ChangedDebugState_shutdown(const ::Nerv::client_addr_t& client_addr) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : Alive
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::
            Method Type: DATAGRAM
            Code       : 0x0100
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_Alive : public ::Nerv::nerv_proxy_object_datagram
        {
        public:
            proxy_method_Alive() : nerv_proxy_object_datagram(0x0100)
            {
            }

            void Alive_datagram()
            {
                datagram(0,0);
            }
        };

        /* stub */
        class method_Alive : public ::Nerv::nerv_object_datagram
        {
        public:
            method_Alive() : nerv_object_datagram(0x0100)
            {
            }

            virtual void on_Alive_datagram( const ::Nerv::client_addr_t& in_client_addr) = 0; //{}

            virtual void on_datagram( const struct_client_addr& in_client_addr, void* const pin_buf, const pksize_t& in_size)
            {
                on_Alive_datagram(in_client_addr);
            }
        };

        /********************************************************************************************************************************************
            Method     : RequestControl
            Input      : Lemon_General_Component::rank_t
            Output     : Lemon_General_Component::
            Method Type: CALL
            Code       : 0x0101
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_RequestControl : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_RequestControl()    : nerv_proxy_object_call(0x0101) {}

            ::Nerv::nerv_err_t RequestControl(Lemon_General_Component::rank_t* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Lemon_General_Component::rank_t);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void RequestControl_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_RequestControl : public ::Nerv::nerv_object_call
        {
        public:
            method_RequestControl() :  nerv_object_call(0x0101,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_RequestControl(in_client_addr,(Lemon_General_Component::rank_t*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_RequestControl_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_RequestControl(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::rank_t* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_RequestControl_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : ReleaseControl
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::
            Method Type: CALL
            Code       : 0x0102
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ReleaseControl : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_ReleaseControl()    : nerv_proxy_object_call(0x0102) {}

            ::Nerv::nerv_err_t ReleaseControl( const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void ReleaseControl_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_ReleaseControl : public ::Nerv::nerv_object_call
        {
        public:
            method_ReleaseControl() :  nerv_object_call(0x0102,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_ReleaseControl(in_client_addr,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_ReleaseControl_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_ReleaseControl(const ::Nerv::client_addr_t& client_addr,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ReleaseControl_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : SetRank
            Input      : Lemon_General_Component::rank_t
            Output     : Lemon_General_Component::
            Method Type: CALL
            Code       : 0x0103
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_SetRank : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_SetRank()    : nerv_proxy_object_call(0x0103) {}

            ::Nerv::nerv_err_t SetRank(Lemon_General_Component::rank_t* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Lemon_General_Component::rank_t);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void SetRank_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_SetRank : public ::Nerv::nerv_object_call
        {
        public:
            method_SetRank() :  nerv_object_call(0x0103,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_SetRank(in_client_addr,(Lemon_General_Component::rank_t*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_SetRank_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_SetRank(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::rank_t* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_SetRank_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : GetControlState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::struct_control_state
            Method Type: CALL
            Code       : 0x0104
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_GetControlState : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_GetControlState()    : nerv_proxy_object_call(0x0104) {}

            ::Nerv::nerv_err_t GetControlState(Lemon_General_Component::struct_control_state* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_control_state);
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void GetControlState_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_GetControlState : public ::Nerv::nerv_object_call
        {
        public:
            method_GetControlState() :  nerv_object_call(0x0104,sizeof(Lemon_General_Component::struct_control_state)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_GetControlState(in_client_addr,(Lemon_General_Component::struct_control_state*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_control_state);
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_GetControlState_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_GetControlState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_control_state* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_GetControlState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : ChangedControlState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::struct_control_state
            Method Type: EVENT
            Code       : 0x0104
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ChangedControlState : public ::Nerv::nerv_proxy_object_event
        {
        public:
            proxy_method_ChangedControlState() : nerv_proxy_object_event(0x0104) {}

            bool ChangedControlState_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t ChangedControlState_connect( const double& in_limit_time = 1.0, double* pout_time= 0 )
            {
                return_object_t return_object = async_connect( in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void ChangedControlState_shutdown() { shutdown(); }

            virtual void on_event( void const * const pin_event_data, const pksize_t& in_event_size, const double&  in_elapsed_time)
            {
                on_ChangedControlState((Lemon_General_Component::struct_control_state*)pin_event_data, in_elapsed_time);
            }

        private:
            virtual void on_disconnected() { on_ChangedControlState_disconnected(); }

            virtual void on_ChangedControlState(Lemon_General_Component::struct_control_state* pOut, const double& in_elapsed_time) = 0; //{}

            virtual void on_ChangedControlState_disconnected() = 0; //{}
        };

        /* stub */
        class method_ChangedControlState : public ::Nerv::nerv_object_event
        {
        public:
            method_ChangedControlState() : nerv_object_event(0x0104) {}

            void ChangedControlState_disconnect(const ::Nerv::client_addr_t client_addr ) { disconnect(client_addr); }

            void ChangedControlState(Lemon_General_Component::struct_control_state* pout_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_control_state);
                event(pout_buf,outSize);
            }

            virtual ::Nerv::nerv_err_t on_connect(  const client_addr_t&   in_client_addr,
                                                    const double&               in_elapsed_time,
                                                    const double&               in_limit_time )
            {
                return on_ChangedControlState_connect(in_client_addr,in_elapsed_time,in_limit_time);
            }

            virtual void on_shutdown( const ::Nerv::client_addr_t& client_addr) { on_ChangedControlState_shutdown( client_addr); }

            virtual nerv_err_t on_ChangedControlState_connect( const client_addr_t& in_client_addr,
                                                       const double& in_elapsed_time,
                                                       const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ChangedControlState_shutdown(const ::Nerv::client_addr_t& client_addr) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : SetOperation
            Input      : Lemon_General_Component::struct_operation
            Output     : Lemon_General_Component::
            Method Type: CALL
            Code       : 0x0200
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_SetOperation : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_SetOperation()    : nerv_proxy_object_call(0x0200) {}

            ::Nerv::nerv_err_t SetOperation(Lemon_General_Component::struct_operation* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Lemon_General_Component::struct_operation);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void SetOperation_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_SetOperation : public ::Nerv::nerv_object_call
        {
        public:
            method_SetOperation() :  nerv_object_call(0x0200,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_SetOperation(in_client_addr,(Lemon_General_Component::struct_operation*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_SetOperation_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_SetOperation(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_operation* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_SetOperation_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : GetState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::struct_state
            Method Type: CALL
            Code       : 0x0201
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_GetState : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_GetState()    : nerv_proxy_object_call(0x0201) {}

            ::Nerv::nerv_err_t GetState(Lemon_General_Component::struct_state* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_state);
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void GetState_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_GetState : public ::Nerv::nerv_object_call
        {
        public:
            method_GetState() :  nerv_object_call(0x0201,sizeof(Lemon_General_Component::struct_state)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_GetState(in_client_addr,(Lemon_General_Component::struct_state*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_state);
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_GetState_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_GetState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_state* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_GetState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : ChangedState
            Input      : Lemon_General_Component::
            Output     : Lemon_General_Component::struct_state
            Method Type: EVENT
            Code       : 0x0201
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ChangedState : public ::Nerv::nerv_proxy_object_event
        {
        public:
            proxy_method_ChangedState() : nerv_proxy_object_event(0x0201) {}

            bool ChangedState_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t ChangedState_connect( const double& in_limit_time = 1.0, double* pout_time= 0 )
            {
                return_object_t return_object = async_connect( in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void ChangedState_shutdown() { shutdown(); }

            virtual void on_event( void const * const pin_event_data, const pksize_t& in_event_size, const double&  in_elapsed_time)
            {
                on_ChangedState((Lemon_General_Component::struct_state*)pin_event_data, in_elapsed_time);
            }

        private:
            virtual void on_disconnected() { on_ChangedState_disconnected(); }

            virtual void on_ChangedState(Lemon_General_Component::struct_state* pOut, const double& in_elapsed_time) = 0; //{}

            virtual void on_ChangedState_disconnected() = 0; //{}
        };

        /* stub */
        class method_ChangedState : public ::Nerv::nerv_object_event
        {
        public:
            method_ChangedState() : nerv_object_event(0x0201) {}

            void ChangedState_disconnect(const ::Nerv::client_addr_t client_addr ) { disconnect(client_addr); }

            void ChangedState(Lemon_General_Component::struct_state* pout_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_state);
                event(pout_buf,outSize);
            }

            virtual ::Nerv::nerv_err_t on_connect(  const client_addr_t&   in_client_addr,
                                                    const double&               in_elapsed_time,
                                                    const double&               in_limit_time )
            {
                return on_ChangedState_connect(in_client_addr,in_elapsed_time,in_limit_time);
            }

            virtual void on_shutdown( const ::Nerv::client_addr_t& client_addr) { on_ChangedState_shutdown( client_addr); }

            virtual nerv_err_t on_ChangedState_connect( const client_addr_t& in_client_addr,
                                                       const double& in_elapsed_time,
                                                       const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ChangedState_shutdown(const ::Nerv::client_addr_t& client_addr) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : GetErrorString
            Input      : Nerv::
            Output     : Lemon_General_Component::struct_error
            Method Type: CALL
            Code       : 0x0300
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_GetErrorString : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_GetErrorString()    : nerv_proxy_object_call(0x0300) {}

            ::Nerv::nerv_err_t GetErrorString(Lemon_General_Component::struct_error* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_error);
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void GetErrorString_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_GetErrorString : public ::Nerv::nerv_object_call
        {
        public:
            method_GetErrorString() :  nerv_object_call(0x0300,sizeof(Lemon_General_Component::struct_error)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_GetErrorString(in_client_addr,(Lemon_General_Component::struct_error*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = sizeof(Lemon_General_Component::struct_error);
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_GetErrorString_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_GetErrorString(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_error* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_GetErrorString_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };


    /* Interface Defines ***************************************************************************************************************************/

        /********************************************************************************************************************************************
            Interface Name : Debug
            Writer : Lemonade
            Description : 7
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_Debug : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_method_SetDebugOperation
        , virtual public Lemon_General_Component::proxy_method_ChangedDebugState
        , virtual public Lemon_General_Component::proxy_method_GetDebugState
        {
        };

        /* stub */
        class stub_interface_Debug : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::method_SetDebugOperation
        , virtual public Lemon_General_Component::method_ChangedDebugState
        , virtual public Lemon_General_Component::method_GetDebugState
        {
        };


        /********************************************************************************************************************************************
            Interface Name : RealtimeComponent
            Writer : Lemonade
            Description : 9
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_RealtimeComponent : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_method_SetOperation
        , virtual public Lemon_General_Component::proxy_method_ChangedState
        , virtual public Lemon_General_Component::proxy_method_GetState
        {
        };

        /* stub */
        class stub_interface_RealtimeComponent : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::method_SetOperation
        , virtual public Lemon_General_Component::method_ChangedState
        , virtual public Lemon_General_Component::method_GetState
        {
        };


        /********************************************************************************************************************************************
            Interface Name : DistributedComponent
            Writer : Lemonade
            Description : 8
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_DistributedComponent : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_method_Alive
        , virtual public Lemon_General_Component::proxy_method_RequestControl
        , virtual public Lemon_General_Component::proxy_method_ReleaseControl
        , virtual public Lemon_General_Component::proxy_method_SetRank
        , virtual public Lemon_General_Component::proxy_method_ChangedControlState
        , virtual public Lemon_General_Component::proxy_method_GetControlState
        {
        };

        /* stub */
        class stub_interface_DistributedComponent : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::method_Alive
        , virtual public Lemon_General_Component::method_RequestControl
        , virtual public Lemon_General_Component::method_ReleaseControl
        , virtual public Lemon_General_Component::method_SetRank
        , virtual public Lemon_General_Component::method_ChangedControlState
        , virtual public Lemon_General_Component::method_GetControlState
        {
        };


        /********************************************************************************************************************************************
            Interface Name : GeneralComponent
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_GeneralComponent : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_interface_Debug
        , virtual public Lemon_General_Component::proxy_interface_DistributedComponent
        , virtual public Lemon_General_Component::proxy_interface_RealtimeComponent
        , virtual public Lemon_General_Component::proxy_method_GetErrorString
        {
        };

        /* stub */
        class stub_interface_GeneralComponent : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::stub_interface_Debug
        , virtual public Lemon_General_Component::stub_interface_DistributedComponent
        , virtual public Lemon_General_Component::stub_interface_RealtimeComponent
        , virtual public Lemon_General_Component::method_GetErrorString
        {
        };

        /********************************************************************************************************************************************
        Interface Name : BGeneralComponent
        Writer : fm
        Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_BGeneralComponent : virtual public ::Nerv::nerv_proxy_object,
            virtual public Lemon_General_Component::proxy_interface_GeneralComponent
        {
        public:
            proxy_interface_BGeneralComponent()
                : nerv_proxy_object()
            {
            }
            ~proxy_interface_BGeneralComponent(void) {}
            virtual void on_ChangedDebugState(Lemon_General_Component::enum_debug_state* pOut, const double& in_elapsed_time){}
            virtual void on_ChangedDebugState_disconnected(){}
            virtual void on_ChangedControlState(Lemon_General_Component::struct_control_state* pOut, const double& in_elapsed_time){}
            virtual void on_ChangedControlState_disconnected(){}
            virtual void on_ChangedState(Lemon_General_Component::struct_state* pOut, const double& in_elapsed_time){}
            virtual void on_ChangedState_disconnected(){}
        };

        class stub_interface_BGeneralComponent : virtual public ::Nerv::nerv_object,
            virtual public Lemon_General_Component::stub_interface_GeneralComponent
        {
        protected:
            //typedef std::map<packed_client_addr_t,bool> mapControlStateClient;
            //mapControlStateClient				m_controlStateClient;
            Lemon_General_Component::struct_control_state	m_controlState;
            Lemon_General_Component::struct_state			m_state;
            Lemon_General_Component::struct_state			m_preEmgState;
            Lemon_General_Component::enum_debug_state		m_debug;
            std::string                                     m_comment;
            Lemon_General_Component::struct_string256       m_objectName;
        public:
            //	생성자
            stub_interface_BGeneralComponent() : nerv_object(),
                stub_interface_Debug()
            {
                m_controlState.rank = 0;
                m_controlState.ownerRank = 0;
                //				m_controlState.owner.node.ip.s_addr = 0;
                //				m_controlState.owner.node.port = 0;
                //				m_controlState.owner.obj = 0;
                m_state.prmState = Lemon_General_Component::c_state_initialize;
                m_state.scdState = 0;
                m_debug = Lemon_General_Component::c_state_release;
            }

            //	소멸자
            virtual ~stub_interface_BGeneralComponent(void) {

            }
            virtual nerv_err_t on_ChangedDebugState_connect( const client_addr_t& in_client_addr,
                const double& in_packet_live_time,
                const double& in_packet_limit_time )
            { 
                return ::Nerv::c_nerv_err_none;
            }

            virtual void on_ChangedDebugState_shutdown(const ::Nerv::client_addr_t& client_addr)
            {
            }
            virtual ::Nerv::nerv_err_t on_GetDebugState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::enum_debug_state* pOut,const double& in_elapsed_time,const double& in_limit_time)
            {
                *pOut = m_debug;
                return ::Nerv::c_nerv_err_none; 
            }
            virtual void on_GetDebugState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }


            virtual ::Nerv::nerv_err_t on_SetDebugOperation(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_debug_operation* pIn,const double& in_elapsed_time,const double& in_limit_time)
            {
                m_debug = *pIn;

                ChangedDebugState(&m_debug);
                return Nerv::c_nerv_err_none;
            }
            virtual void on_SetDebugOperation_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }
            virtual void on_Alive_datagram( const ::Nerv::client_addr_t& in_client_addr)
            {
                //if( strcmp(m_objectName.buf,pin_buf->buf) ==0) return;
                //LOG_PUSH(nervlog,"On_Alive_Datagram("<<from<<")");
                //LOG_POP(nervlog);
            }
            virtual ::Nerv::nerv_err_t on_RequestControl(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::rank_t* pIn,const double& in_elapsed_time,const double& in_limit_time)
            { 
                //if( m_controlState.owner==from) {
                //	m_controlState.ownerRank = 0;
                //	m_controlState.owner.node.ip.s_addr = 0;
                //	m_controlState.owner.node.port = 0;
                //	m_controlState.owner.obj = 0;
                //}
                if( m_controlState.rank > *pIn ) {
                    return Nerv::c_nerv_err_unknown;
                }
                //				if( m_controlState.owner.obj!=0 && m_controlState.ownerRank >= *pIn ) {
                if( m_controlState.ownerRank >= *pIn ) {
                    return Nerv::c_nerv_err_unknown;
                }
                //mapControlStateClient::iterator pos = m_controlStateClient.find(from);
                //if( pos ==  m_controlStateClient.end() ) {
                //	return Nerv::c_nerv_err_unknown;
                //}
                //				m_controlState.owner = from;
                m_controlState.ownerRank = *pIn;
                ChangedControlState(&m_controlState);
                return Nerv::c_nerv_err_none;
            }
            virtual void on_RequestControl_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }
            virtual ::Nerv::nerv_err_t on_ReleaseControl(const ::Nerv::client_addr_t& client_addr,const double& in_elapsed_time,const double& in_limit_time)
            { 
                //if( memcmp(&m_controlState.owner,&from,sizeof(packed_client_addr_t)) != 0 ) {
                //	return Nerv::c_nerv_err_unknown;
                //}
                m_controlState.ownerRank = 0;
                //m_controlState.owner.node.ip.s_addr = 0;
                //m_controlState.owner.node.port = 0;
                //m_controlState.owner.obj = 0;
                ChangedControlState(&m_controlState);
                return Nerv::c_nerv_err_none;
            }

            virtual void on_ReleaseControl_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual ::Nerv::nerv_err_t on_SetRank(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::rank_t* pIn,const double& in_elapsed_time,const double& in_limit_time)
            { 
                //if( m_controlState.owner!=from ) {
                //	return Nerv::c_nerv_err_unknown;
                //}
                if( *pIn > m_controlState.ownerRank) {
                    return Nerv::c_nerv_err_unknown;
                }
                m_controlState.rank = *pIn;
                ChangedControlState(&m_controlState);
                return Nerv::c_nerv_err_none;
            }
            virtual void on_SetRank_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual nerv_err_t on_ChangedControlState_connect( const client_addr_t& in_client_addr,
                const double& in_packet_live_time,
                const double& in_packet_limit_time )
            { 
                return ::Nerv::c_nerv_err_none; 
            }
            virtual void on_ChangedControlState_shutdown(const ::Nerv::client_addr_t& client_addr)
            {
                //if( m_controlState.owner==from ) {
                //	m_controlState.ownerRank = 0;
                //	m_controlState.owner.node.ip.s_addr = 0;
                //	m_controlState.owner.node.port = 0;
                //	m_controlState.owner.obj = 0;
                //	ChangedControlState_event(&m_controlState);
                //}
                //m_controlStateClient.erase(from);
            }

            virtual ::Nerv::nerv_err_t on_GetControlState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_control_state* pOut,const double& in_elapsed_time,const double& in_limit_time)
            { 
                *pOut = m_controlState;
                return Nerv::c_nerv_err_none;
            }

            virtual void on_GetControlState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual ::Nerv::nerv_err_t on_SetOperationShutdown(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationReset(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationStandby(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationReady(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationGo(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationEmergency(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;
            virtual ::Nerv::nerv_err_t on_SetOperationClearEmergency(::Nerv::client_addr_t from,Lemon_General_Component::struct_operation* pIn) = 0;

            virtual ::Nerv::nerv_err_t on_SetOperation(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_operation* pIn,const double& in_elapsed_time,const double& in_limit_time)
            {
                //if( m_controlState.owner!=from ) {
                //	return nerv::ERR_UNKNOWN;
                //}
                ::Nerv::nerv_err_t rv;
                switch( pIn->primary_operation ) {
        case Lemon_General_Component::c_operation_shutdown: 		rv = on_SetOperationShutdown(client_addr,pIn);			break;
        case Lemon_General_Component::c_operation_reset:			rv = on_SetOperationReset(client_addr,pIn);		    	break;
        case Lemon_General_Component::c_operation_standby:			rv = on_SetOperationStandby(client_addr,pIn);			break;
        case Lemon_General_Component::c_operation_ready:			rv = on_SetOperationReady(client_addr,pIn);			    break;
        case Lemon_General_Component::c_operation_go:				rv = on_SetOperationGo(client_addr,pIn);				break;
        case Lemon_General_Component::c_operation_emergency:		rv = on_SetOperationEmergency(client_addr,pIn);	    	break;
        case Lemon_General_Component::c_operation_clear_emergency:	rv = on_SetOperationClearEmergency(client_addr,pIn);	break;
        default:
            m_comment = "알수없는 퍼스트 명령입니다.";
            rv = Nerv::c_nerv_err_unknown;
            break;
                }
                return rv;
            }
            virtual void on_SetOperation_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual nerv_err_t on_ChangedState_connect( const client_addr_t& in_client_addr,
                const double& in_packet_live_time,
                const double& in_packet_limit_time )
            { 
                return ::Nerv::c_nerv_err_none; 
            }
            virtual void on_ChangedState_shutdown(const ::Nerv::client_addr_t& client_addr)
            {
            }

            virtual ::Nerv::nerv_err_t on_GetState(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_state* pOut,const double& in_elapsed_time,const double& in_limit_time)
            { 
                *pOut= m_state;
                return Nerv::c_nerv_err_none;
            }
            virtual void on_GetState_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual Nerv::nerv_err_t on_GetErrorString(const ::Nerv::client_addr_t& client_addr,Lemon_General_Component::struct_error* pOut,const double& in_elapsed_time,const double& in_limit_time)
            {
            	sprintf(pOut->string,m_comment.c_str());
            	m_comment.clear();
            	return Nerv::c_nerv_err_none;
            }

            virtual void on_GetErrorString_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            virtual ::Nerv::nerv_err_t on_GetObjectName(::Nerv::client_addr_t client_addr,Lemon_General_Component::struct_string256* pOut)
            { 
                sprintf(pOut->buf,m_objectName.buf);
                return ::Nerv::c_nerv_err_none; 
            }
            virtual void on_GetObjectName_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error)
            {
            }

            //virtual Nerv::nerv_err_t On_GetRelativeCallerAddress(packed_client_addr_t from,packed_client_addr_t* pIn)
            //{
            //	*pIn = from;
            //	return Nerv::c_nerv_err_none;
            //}
            //class The : public Lemon_General_Component::NProxyGetRelativeCallerAddress {
            //public:
            //	The(packed_client_addr_t addr,nerv::NObject* pOwner) : NProxyObject(addr,pOwner)	{}
            //};
            //virtual Nerv::nerv_err_t On_GetTheRelativeAddress(packed_client_addr_t from,packed_client_addr_t* pIn,packed_client_addr_t* pOut)
            //{
            //	The the(*pIn,this);
            //	the.GetRelativeCallerAddress(pOut);
            //	return Nerv::c_nerv_err_none;
            //}
        };


}


#endif // #ifdef Define__Lemon_General_Component__Lemon_General_Component_h
