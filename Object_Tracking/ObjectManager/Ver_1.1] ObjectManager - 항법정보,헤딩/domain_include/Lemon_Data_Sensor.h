/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Sensor
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:17:49 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Sensor__Lemon_Data_Sensor_h
#define Define__Lemon_Data_Sensor__Lemon_Data_Sensor_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Sensor
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_sensor_mount
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_sensor_mount
        {
            /*  */
            static const double min_x() { return double(-2147.483647); }
            static const double max_x() { return double(2147.483647); }
            ::Nerv::real< ::Nerv::int32_t,min_x,max_x > x;

            /*  */
            static const double min_y() { return double(-2147.483647); }
            static const double max_y() { return double(2147.483647); }
            ::Nerv::real< ::Nerv::int32_t,min_y,max_y > y;

            /*  */
            static const double min_z() { return double(-2147.483647); }
            static const double max_z() { return double(2147.483647); }
            ::Nerv::real< ::Nerv::int32_t,min_z,max_z > z;

            /*  */
            static const double min_roll() { return double(-1.0); }
            static const double max_roll() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_roll,max_roll > roll;

            /*  */
            static const double min_pitch() { return double(-1.0); }
            static const double max_pitch() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_pitch,max_pitch > pitch;

            /*  */
            static const double min_yaw() { return double(-1.0); }
            static const double max_yaw() { return double(1.0); }
            ::Nerv::real< ::Nerv::int16_t,min_yaw,max_yaw > yaw;

        };
        typedef template_history_elem<struct_sensor_mount> struct_sensor_mount_history_elem;
        typedef template_history<struct_sensor_mount> struct_sensor_mount_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Sensor__Lemon_Data_Sensor_h
