/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Method_Estimator
    creator              : Lemonade
    creator information  : 
    standardport         : 2334
    description          : 
                                                                                                  by Lemonade, at Mon Feb 09 17:07:32 2009

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Method_Estimator__Lemon_Method_Estimator_h
#define Define__Lemon_Method_Estimator__Lemon_Method_Estimator_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Estimator.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Method_Estimator
{

        using namespace Lemon_Data_Estimator;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 2334;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : obstacle
            Input      : ::
            Output     : Lemon_Data_Estimator::struct_obstacle
            Method Type: INFORMATION
            Code       : 0x0001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_obstacle : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_obstacle() :    nerv_proxy_object_information(0x0001) {}

            bool obstacle_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t obstacle_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void obstacle_shutdown() { shutdown(); }

            void obstacle_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t obstacle_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void obstacle_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t obstacle_read( Lemon_Data_Estimator::struct_obstacle* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Estimator::struct_obstacle);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_obstacle_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_obstacle_sync(in_sync_model,in_interval ,(Lemon_Data_Estimator::struct_obstacle*)pin_buffer );
            }

            virtual void on_obstacle_disconnected() = 0; //{}

            virtual void on_obstacle_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Estimator::struct_obstacle const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_obstacle : public ::Nerv::nerv_object_information
        {

        public:

            method_obstacle(int interval= 0) : nerv_object_information(0x0001) {}

            void obstacle_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void obstacle_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Estimator::struct_obstacle* obstacle_get()
            {
                return (Lemon_Data_Estimator::struct_obstacle*)get(sizeof(Lemon_Data_Estimator::struct_obstacle) );
            }

            void obstacle_cancel()
            {
                cancel();
            }

            nerv_err_t obstacle_post(Lemon_Data_Estimator::struct_obstacle* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Estimator::struct_obstacle);
                return post( outSize);
            }

            nerv_err_t obstacle_write(Lemon_Data_Estimator::struct_obstacle* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Estimator::struct_obstacle);
                return write(pin_buf,outSize);
            }

            letter_case_t obstacle_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void obstacle_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t obstacle_read( Lemon_Data_Estimator::struct_obstacle* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Estimator::struct_obstacle);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_obstacle_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_obstacle_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_obstacle_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_obstacle_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : obstacle_set
            Input      : ::
            Output     : Lemon_Data_Estimator::struct_obstacle_set
            Method Type: INFORMATION
            Code       : 0x0002
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_obstacle_set : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_obstacle_set() :    nerv_proxy_object_information(0x0002) {}

            bool obstacle_set_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t obstacle_set_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void obstacle_set_shutdown() { shutdown(); }

            void obstacle_set_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t obstacle_set_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void obstacle_set_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t obstacle_set_read( Lemon_Data_Estimator::struct_obstacle_set* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Estimator::struct_obstacle_set);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_obstacle_set_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_obstacle_set_sync(in_sync_model,in_interval ,(Lemon_Data_Estimator::struct_obstacle_set*)pin_buffer );
            }

            virtual void on_obstacle_set_disconnected() = 0; //{}

            virtual void on_obstacle_set_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Estimator::struct_obstacle_set const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_obstacle_set : public ::Nerv::nerv_object_information
        {

        public:

            method_obstacle_set(int interval= 0) : nerv_object_information(0x0002) {}

            void obstacle_set_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void obstacle_set_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Estimator::struct_obstacle_set* obstacle_set_get()
            {
                return (Lemon_Data_Estimator::struct_obstacle_set*)get(sizeof(Lemon_Data_Estimator::struct_obstacle_set) );
            }

            void obstacle_set_cancel()
            {
                cancel();
            }

            nerv_err_t obstacle_set_post(Lemon_Data_Estimator::struct_obstacle_set* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_Estimator::struct_obstacle_set*)pin_buf)->get_sizeof();
                return post( outSize);
            }

            nerv_err_t obstacle_set_write(Lemon_Data_Estimator::struct_obstacle_set* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_Estimator::struct_obstacle_set*)pin_buf)->get_sizeof();
                return write(pin_buf,outSize);
            }

            letter_case_t obstacle_set_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void obstacle_set_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t obstacle_set_read( Lemon_Data_Estimator::struct_obstacle_set* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Estimator::struct_obstacle_set);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_obstacle_set_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_obstacle_set_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_obstacle_set_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_obstacle_set_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/

        /********************************************************************************************************************************************
            Interface Name : obstacle_estimator
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_obstacle_estimator : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Estimator::proxy_method_obstacle_set
        {
        };

        /* stub */
        class stub_interface_obstacle_estimator : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Estimator::method_obstacle_set
        {
        };



}


#endif // #ifdef Define__Lemon_Method_Estimator__Lemon_Method_Estimator_h
