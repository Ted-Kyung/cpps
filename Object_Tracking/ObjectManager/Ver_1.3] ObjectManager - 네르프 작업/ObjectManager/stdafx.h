#pragma once
#include <vector>
#include <deque>
#include <math.h>
#include <Windows.h>

#define MIN_OBSTACLE_WIDTH 0.0		// 장애물 폭 최소 거리 (폭이 작으면 장애물 무시)   미터 단위 
#define MIN_PASS_DISTANCE  1.0		// 장애물 사이 간격으로 로봇이 통과할수 있는 폭(못지나가면 합침)    미터 단위 
#define DETECTION_MAX_DISTANCE 20.		// 최대 탐지거리 미터단위
#define DETECTION_MIN_DISTANCE 0.0		// 최소 탐지거리 미터 단위 
#define ABS_ANGLE_USE      180.			// 장애물 발견시 사용할 각도 로봇 정면을 기준 0 도로 햇을때 (ABS 값임) 
#define RESOLUTION 0.5				// 레이저 스캐너 해상도 
#define SENSOR_START_ANG 180.		// 전방을 0도로 햇을때 우측으로 떨어진 각도 센서의 시작 앵글값 
#define ObjectOFFSET 50				// 장애물이 떨어진 간격 로우데이터 단위 
#define MAX_RANGE 6000				// 레이저 최대 거리 (발산시)
#define EXTEND_DISTANCE		2.									// Extend length  : Meter
								

#define START 1				// 장애물 저장 시작
#define END   0				// 장애물 저장 종료 


//#define GROUND_FILTER
#define DEBUG_PRINTF				// 데이터 처리 과정 볼수있음 (주석 삭제시)

#define PI 3.141592
#define DR 180.0 * PI

inline double difference(double x, double _x)
{
	return (double)_x-(double)x;
}
inline double square(double x)
{
	return (double)x*(double)x;
}
inline double pdistance(double fx, double fy,double sx, double sy)
{
	return (double)sqrt((square(difference(fx,sx))) + (square(difference(fy,sy))));
}
struct Rectangular
{
	double x;
	double y;
};

struct ObjectRectangular
{
	Rectangular	startXY;
	Rectangular	endXY;
	Rectangular	centerXY;
	double		Width;
	double		Ang;
	double		distance;
};


/////////////////////////////////////////////////////////////////콘솔 색 입히기 
const HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);

inline int _drgb ( bool d, bool r, bool g, bool b)
{
	return (d << 3) + (r << 2) + (g << 1) + (b << 0);
}

inline int _clr ( int fc, int bc )
{
	return (fc << 0) + (bc << 4);
}

inline void consoleColor(bool r, bool g, bool b)
{
	int fore = _drgb (true,r,g,b);		// 명암값 , R , G  , B 
	int back = _drgb (0,0,0,0);
	int color = _clr (fore,back);
	SetConsoleTextAttribute(h,color);	
}


