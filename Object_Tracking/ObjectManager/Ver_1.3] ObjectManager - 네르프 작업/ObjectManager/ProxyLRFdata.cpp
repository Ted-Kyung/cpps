#include "ProxyLRFdata.h"

ProxyLRFdata::ProxyLRFdata(InterfaceLRF* pOwer)
{
	m_pOwer = pOwer;

	int id;
	char ip[20];	
	char buf[128];
	GetPrivateProfileString("LRF","IP","127.0.0.1",ip,sizeof(ip),".\\ObjectManager.ini");
	GetPrivateProfileString("LRF","ID","41",buf,sizeof(buf),".\\ObjectManager.ini");
	id = atoi(buf);

	nerv_err_t err = this->create(Lemon_Method_LRF::c_standard_port,Nerv::inet_addr(ip),id,1);
	printf("proxy LRF Create => %s IP : %s ID : %d\n",get_err_str(err),ip,id);
}

ProxyLRFdata::~ProxyLRFdata(void)
{
}
void ProxyLRFdata::on_object_create()
{
	nerv_err_t err = this->range_map_connect(false,5.0,0);
	printf("connection => %s \n",get_err_str(err));
}

void ProxyLRFdata::on_object_destroy()
{
	printf("LRF Scaner destroyed\n");
}

void ProxyLRFdata::on_range_map_disconnected()
{

}

void ProxyLRFdata::on_range_map_sync( const sender_sync_model_t& in_sync_model,
									 const double& in_interval,
									 Lemon_Data_LRF::struct_range_map * pin_buffer )
{
	// range_map 데이터 생성 
	lrfData = new Lemon_Data_LRF::struct_range_map;
	// range_map 데이터 복사 
	*(lrfData) = *(pin_buffer);
	
	BITMAPFILEHEADER* bitmapfileheader = (BITMAPFILEHEADER*)lrfData->map();
	BITMAPINFOHEADER* bitmapinfoheader = (BITMAPINFOHEADER*)(bitmapfileheader + 1);
	// 실제 데이터 pin_data (거리데이터)
	unsigned short* pin_data = (unsigned short*)(bitmapinfoheader + 1);
	
	// 데이터 길이
	int width = (int)bitmapinfoheader->biWidth;

	unsigned short* RangeData;

	// 데이터 복사 
	RangeData = new unsigned short[width];
	memcpy(RangeData,pin_data,sizeof(unsigned short) * width);

	// 데이터, 데이터길이, e , n, h 전송
	m_pOwer->lrfrecv(RangeData, width,(double)pin_buffer->sensor_info.navigation.linear.mgrs.east, (double)pin_buffer->sensor_info.navigation.linear.mgrs.north, (double)pin_buffer->sensor_info.navigation.rotational.attitude.heading);

	//현재 벨로다인데이터의 0 도는 차량전방을 기준으로 후방이 0도 90도는 우측 , 180도 전방 270도 좌측 순이다. 인덱스값은 Degree * 2  

	delete lrfData;
	delete RangeData;
}