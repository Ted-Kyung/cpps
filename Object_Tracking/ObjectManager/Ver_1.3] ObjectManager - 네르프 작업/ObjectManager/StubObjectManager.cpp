#include "StubObjectManager.h"
#include <stdio.h>
#include <deque>



StubObjectManager::StubObjectManager(void)
{
	int id;
	char buf[128];
	GetPrivateProfileString("ObstacleManager","ID","41",buf,sizeof(buf),".\\ObjectManager.ini");
	id = atoi(buf);

	nerv_err_t err = this->create(FM_KJH_OBSTACLE_MANAGER::c_standard_port,id,1);
	printf("Ground Tracking Create => %s ID : %d PORT : %d \n",get_err_str(err),id,FM_KJH_OBSTACLE_MANAGER::c_standard_port);
	m_FirstTime = TRUE;
	m_file = fopen("Rord Log.log","w+");

}

StubObjectManager::~StubObjectManager(void)
{
	fclose(m_file);
}

void StubObjectManager::ObstacleData(deque<ObjectRectangular> obstacleSet, double east , double north, double heading)
{
	m_east		= east;
	m_north		= north;
	m_heading	= heading;

	//데이터가 비어있으면 하위 함수 처리 안함 
	if (obstacleSet.empty())	
	{
		return;
	}

	

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleAngleDistanceFilter START\n");
	consoleColor(1,1,1);
#endif

	//가용 Angle 값 과 희망거리값 이상의 장애물 데이터 삭제 
	m_fusion_obstacle = ObstacleAngleDistanceFilter(obstacleSet);

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleAngleDistanceFilter END\n");
	consoleColor(1,1,1);
	//수신데이터 확인용
	debug_datacheck(m_fusion_obstacle);	
#endif

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleFusion 처리 시작 \n");
	consoleColor(1,1,1);
#endif

	
	//통과 불가능한 지역의 장애물을 하나로 만드는 작업을 수행
	m_fusion_obstacle = ObstacleFusion(m_fusion_obstacle);	

	//m_fusion_obstacle = ObstacleExtended(m_fusion_obstacle);

#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("ObstacleFusion 처리 종료 \n");
	consoleColor(1,1,1);
	//수신데이터 확인용
	debug_datacheck(m_fusion_obstacle);	
#endif


#ifdef GROUND_FILTER
	//제일 가까운 장애물중 땅에 가까운 데이터만을 남기고 모두 삭제 하는 루틴 
	m_fusion_obstacle = ObstacleGroundFilter(m_fusion_obstacle,east,north);
	//printf("ObstacleGroundFilter\n");
	debug_datacheck(m_fusion_obstacle);	// 도로의 가운데 점만 저장 할수있도록 한후 데이터 체크 	
#endif



#ifdef GROUND_FILTER
	//데이터 서비스 해줄곳 (좌표 서비스)
	{
		FM_KJH_ON_GROUND_PATH::ObjectRectangular pinbuff;

		pinbuff.startXY.x		= (double)m_fusion_obstacle[0].startXY.x;
		pinbuff.startXY.y		= (double)m_fusion_obstacle[0].startXY.y;
		pinbuff.centerXY.x		= (double)m_fusion_obstacle[0].centerXY.x;
		pinbuff.centerXY.y		= (double)m_fusion_obstacle[0].centerXY.y;
		pinbuff.endXY.x			= (double)m_fusion_obstacle[0].endXY.x;
		pinbuff.endXY.y			= (double)m_fusion_obstacle[0].endXY.y;

		pinbuff.distance		= (double)m_fusion_obstacle[0].distance;
		pinbuff.Ang				= (double)m_fusion_obstacle[0].Ang;
		pinbuff.Width			= (double)m_fusion_obstacle[0].Width;

		ground_tracking_path_write(&pinbuff);
	}
#endif

	


	int obstacle_cnt = (int)m_fusion_obstacle.size();

	// 가변 데이터 포인터로 선언 
	FM_KJH_OBSTACLE_MANAGER::object2obstacle* nerv_obstacle_data;
	nerv_obstacle_data = objectManagerData_get();
	// get ~ post 는 셋트 

	// 가변 데이터 init
	nerv_obstacle_data->init_daynamic_buffer();

	// 가변 데이터의 크기 선언
	nerv_obstacle_data->obstacleData_alloc(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + (sizeof(FM_KJH_OBSTACLE_MANAGER::ObjectRectangular) * obstacle_cnt) +1);

	// BITMAPFILEHEADER 정의
	BITMAPFILEHEADER* m_nerv_obstacle_data_bitmap_file_header = (BITMAPFILEHEADER*)nerv_obstacle_data->obstacleData();

	// BITMAPINFOHEADER 정의
	BITMAPINFOHEADER* m_nerv_obstacle_data_bitmap_header = (BITMAPINFOHEADER*)(m_nerv_obstacle_data_bitmap_file_header+1);

	// 실제 데이터 저장 공간 
	FM_KJH_OBSTACLE_MANAGER::ObjectRectangular* m_nerv_obstacle_data = (FM_KJH_OBSTACLE_MANAGER::ObjectRectangular*)(m_nerv_obstacle_data_bitmap_header+1);

	m_nerv_obstacle_data_bitmap_header->biWidth = (int)obstacle_cnt;
	m_nerv_obstacle_data_bitmap_header->biHeight = 1;
	m_nerv_obstacle_data_bitmap_header->biBitCount = sizeof(FM_KJH_OBSTACLE_MANAGER::ObjectRectangular);

	memset(m_nerv_obstacle_data,0,sizeof(FM_KJH_OBSTACLE_MANAGER::ObjectRectangular) * obstacle_cnt);
	
	if(obstacle_cnt >0)
	{
		//전송할 데이터 복사 
		for(int cpycnt = 0 ; cpycnt < obstacle_cnt ; cpycnt++)
		{	
			m_nerv_obstacle_data[cpycnt].Width			=	(double)m_fusion_obstacle[cpycnt].Width			;  
			m_nerv_obstacle_data[cpycnt].Ang			=	(double)m_fusion_obstacle[cpycnt].Ang			;
			m_nerv_obstacle_data[cpycnt].distance		=	(double)m_fusion_obstacle[cpycnt].distance		;
			m_nerv_obstacle_data[cpycnt].startXY.y		=	(double)m_fusion_obstacle[cpycnt].startXY.y		;		
			m_nerv_obstacle_data[cpycnt].startXY.x		=	(double)m_fusion_obstacle[cpycnt].startXY.x		;
			m_nerv_obstacle_data[cpycnt].centerXY.y		=	(double)m_fusion_obstacle[cpycnt].centerXY.y	;			
			m_nerv_obstacle_data[cpycnt].centerXY.x		=	(double)m_fusion_obstacle[cpycnt].centerXY.x	;		
			m_nerv_obstacle_data[cpycnt].endXY.y		=	(double)m_fusion_obstacle[cpycnt].endXY.y		;		
			m_nerv_obstacle_data[cpycnt].endXY.x		=	(double)m_fusion_obstacle[cpycnt].endXY.x		;
		}
		// nerv 데이터 확인용 
		//printf("Nerv Data print Start\n");
		//for(int i = 0 ; i < (int)obstacle_cnt ; i++)
		//{
		//	consoleColor(1,1,0);
		//	printf("sx=%.2f\tsy=%.2f\tex=%.2f\tey=%.2f\tcx=%.2f\tcy=%.2f \tw=%.2f \ta=%.2f \td = %.2lf\n"	
		//		,(double)m_nerv_obstacle_data[i].startXY.y			
		//		,(double)m_nerv_obstacle_data[i].startXY.x
		//		,(double)m_nerv_obstacle_data[i].endXY.y			
		//		,(double)m_nerv_obstacle_data[i].endXY.x
		//		,(double)m_nerv_obstacle_data[i].centerXY.y			
		//		,(double)m_nerv_obstacle_data[i].centerXY.x
		//		,(double)m_nerv_obstacle_data[i].Width
		//		,(double)m_nerv_obstacle_data[i].Ang
		//		,(double)m_nerv_obstacle_data[i].distance);
		//	consoleColor(0,1,0);
		//}
		//printf("\n");
		//printf("Nerv Data print End\n");
	}
	printf("%d Obstacle Post!\n",obstacle_cnt);
	objectManagerData_post(nerv_obstacle_data);
	//delete nerv_obstacle_data (post 기능으로 삭제하는 부분 미 필요 )

	//사용된 데이터 삭제 
	m_fusion_obstacle.clear();
}
//장애물 확장 
deque<ObjectRectangular> StubObjectManager::ObstacleExtended(deque<ObjectRectangular> fusedObstacle)
{
	if (fusedObstacle.empty())	//데이터가 비어있으면 하위 함수 처리 안함 
	{
		return fusedObstacle;
	}

	double Extend_distance;
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{
		Extend_distance = (fusedObstacle[i].Width / 2.) + EXTEND_DISTANCE;

		//장애물을 바라보는 각도에 의하여 확장 = 정의 별첨 
		fusedObstacle[i].startXY.x	= ((double)Extend_distance * sin((double)((-90. + fusedObstacle[i].Ang/DR)))) +(double)fusedObstacle[i].centerXY.x;
		fusedObstacle[i].startXY.y	= ((double)Extend_distance * cos((double)((-90. + fusedObstacle[i].Ang/DR)))) +(double)fusedObstacle[i].centerXY.y;
		fusedObstacle[i].endXY.x	= ((double)Extend_distance * sin((double)((90. + fusedObstacle[i].Ang/DR))))+(double)fusedObstacle[i].centerXY.x;
		fusedObstacle[i].endXY.y	= ((double)Extend_distance * cos((double)((90. + fusedObstacle[i].Ang/DR))))+(double)fusedObstacle[i].centerXY.y;	

		fusedObstacle[i].Width		= pdistance((double)fusedObstacle[i].startXY.x
			,(double)fusedObstacle[i].startXY.y
			,(double)fusedObstacle[i].endXY.x	
			,(double)fusedObstacle[i].endXY.y);
	}
	return fusedObstacle;
}

deque<ObjectRectangular>  StubObjectManager::ObstacleFusion(deque<ObjectRectangular> obstacleSet)
{
	double  distanceBuf	= 0.;
	for(int cnt = 0 ; cnt <(int)obstacleSet.size() ; cnt ++ )
	{
		// 1] deque obstacle 데이터 검사 ( 유효성 검사 ) *장애물의 폭			
		if(obstacleSet[cnt].Width < MIN_OBSTACLE_WIDTH)		// 지정된 장애물 최소폭 보다 작으면 장애물 처리 안함 
		{
			#ifdef DEBUG_PRINTF
			printf("폭 미달 삭제 =>(%d, w =%lf)\n",cnt,obstacleSet[cnt].Width );
			#endif	
			// 해당 벡터를 지운다 (이후 자동정렬됨)			
			obstacleSet.erase(obstacleSet.begin()+cnt);	
			// 해당 자리를 위하여 같은 구간을 한번더 검색	
			--cnt;											
		}
	}

	for(int cnt = 0 ; cnt < (int)obstacleSet.size() ; cnt ++ )
	{
		
		if((int)obstacleSet.size() <= cnt +1) break;
		// 스캔데이터의 방향성을 고려하여 장애물의 끝 점과 다음장애물의 시작점의 거리를 계산하여 장애물 사이의 거리값을 산출 한다 
		distanceBuf =	pdistance(obstacleSet[cnt].endXY.x,obstacleSet[cnt].endXY.y,obstacleSet[cnt+1].startXY.x,obstacleSet[cnt+1].startXY.y);
		
		// 최소 통과 폭 보다 좁으면 다음장애물의 끝점을 저장시키고 순번을 저장한다.
		if(distanceBuf < MIN_PASS_DISTANCE)					 
		{
#ifdef DEBUG_PRINTF
			printf("size = %d ,  cnt = %d\n",(int)obstacleSet.size(),cnt);
#endif			
			obstacleSet[cnt].endXY.x = (double)obstacleSet[cnt+1].endXY.x;	
			obstacleSet[cnt].endXY.y = (double)obstacleSet[cnt+1].endXY.y;	
			
			#ifdef DEBUG_PRINTF
			printf("fusion !! (%d %d) (%lf %lf) => ",cnt,cnt+1,(double)obstacleSet[cnt].Width,(double)obstacleSet[cnt+1].Width);
			#endif
			
			reSizingData(&obstacleSet[cnt]);
#ifdef DEBUG_PRINTF
			printf("cnt %d %lf \n",cnt,(double)obstacleSet[cnt].Width);
#endif
			obstacleSet.erase(obstacleSet.begin()+cnt+1);							
			--cnt;											// 지워진 자리를 위하여 같은 구간을 한번더 검색 
			//cnt = 0;
		}
	} 
	return obstacleSet;
}

void StubObjectManager::reSizingData(ObjectRectangular* Data)
{
	Data->centerXY.x	=	((double)Data->startXY.x + (double)Data->endXY.x) /2.;	//장애물의 중간점 구함 
	Data->centerXY.y	=	((double)Data->startXY.y + (double)Data->endXY.y) /2.;


	Data->distance	=   (double)pdistance(Data->centerXY.x , Data->centerXY.y,m_north,m_east);	// 센서 기준 거리 -> 기준좌표 (0,0)
	//180 ~ -180
	Data->Ang		=	(double)atan2((double)(Data->centerXY.y- m_east),(double)(Data->centerXY.x- m_north)) * 180. / PI -  (double)(m_heading * 180.);
	
	Data->Width		=	sqrt(	(double)square((double)difference((double)Data->startXY.x,(double)Data->endXY.x)) + 
		(double)square((double)difference((double)Data->startXY.y,(double)Data->endXY.y)));
}

double StubObjectManager::AngCalculator(double Angle)
{
	double ang_buf = Angle;
	bool check = FALSE;

	while(1)
	{
		check = FALSE;
		if(ang_buf>360.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		else if(ang_buf > 180.)
		{
			ang_buf = ang_buf - 360.;
			check = TRUE;
		}
		if(check == FALSE) break;
	}
	return ang_buf;
}

void ObstacleMakeID(deque<ObjectRectangular> fusedObstacle)
{

}

void StubObjectManager::debug_datacheck(deque<ObjectRectangular> obstacleSet)
{
	consoleColor(0,0,1);
	printf("! Change %d \n",(int)obstacleSet.size());	

	for(int i = 0 ; i < (int)obstacleSet.size() ; i++)
	{
		consoleColor(1,1,0);
		printf(" x=%.2f y=%.2f w=%.2f a=%.2f d = %.2lf"	,(double)obstacleSet[i].centerXY.y			
			,(double)obstacleSet[i].centerXY.x
			,(double)obstacleSet[i].Width
			,(double)obstacleSet[i].Ang
			,(double)obstacleSet[i].distance);
		consoleColor(0,1,0);
		printf("\n");
	}
	printf("\n");

#ifdef DEBUG_PRINTF

	for(int i = 0 ; i < (int)obstacleSet.size() ; i++)
	{
		fprintf(m_file," x=%lf,y=%lf,w=%lf,a=%lf, rx=%lf,ry=%lf,"	,(double)obstacleSet[i].centerXY.x			
			,(double)obstacleSet[i].centerXY.y
			,(double)obstacleSet[i].Width
			,(double)obstacleSet[i].Ang
			,(double)m_east
			,(double)m_north);
		fprintf(m_file,"||");
	}
	fprintf(m_file,"\n");
#endif
}


deque<ObjectRectangular> StubObjectManager::ObstacleAngleDistanceFilter(deque<ObjectRectangular> fusedObstacle)
{
#ifdef DEBUG_PRINTF
	printf("ObstacleAngleDistanceFilter\n");
#endif	
	// 융합된 장애물 정보를 처음부터 끝까지 검색 
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{
		if((int)fusedObstacle.size() > 1)
		{
			// ABS_ANGLE_USE 보다 장애물이 위치한 각도가 크고 , 거리가 DETECTION_MAX_DISTANCE 보다 멀거나 DETECTION_MIN_DISTANCE 보다 작으면 삭제 
			if(( (int)(abs(fusedObstacle[i].Ang))  >  ABS_ANGLE_USE  )  || (((double)fusedObstacle[i].distance > DETECTION_MAX_DISTANCE)) || (((double)fusedObstacle[i].distance < DETECTION_MIN_DISTANCE)))
			{
				#ifdef DEBUG_PRINTF
				printf("%d , %lf 삭제 \n",(int)fusedObstacle[i].Ang,(double)fusedObstacle[i].distance);
				#endif	

				fusedObstacle.erase(fusedObstacle.begin() + i);
				--i;
			}
			else 
			{
				#ifdef DEBUG_PRINTF
				printf("!!!!!!!!!!!!!!!!!!!!!%d , %lf 유지 \n",(int)fusedObstacle[i].Ang,(double)fusedObstacle[i].distance);
				#endif				
			}
		}
	}
	return fusedObstacle;
}

deque<ObjectRectangular> StubObjectManager::ObstacleGroundFilter(deque<ObjectRectangular> fusedObstacle, double east , double north)
{
#ifdef DEBUG_PRINTF
	printf("ObstacleGroundFilter\n");
#endif	
	for(int i = 0 ; i < (int)fusedObstacle.size() ; i++)
	{

		if((int)fusedObstacle.size() > 1)
		{
			if((double)fusedObstacle[i].Width < (double)fusedObstacle[i+1].Width)
			{
				fusedObstacle.erase(fusedObstacle.begin() + i);
				--i;
			}
			else if((double)fusedObstacle[i].Width >= (double)fusedObstacle[i+1].Width)
			{
				fusedObstacle.erase(fusedObstacle.begin() + i + 1);
				--i;
			}
		}
		//장애물 전체 사이즈가 1 보다 작으면 정지
		if(((int)fusedObstacle.size()) <= (1) ) 
		{
			#ifdef DEBUG_PRINTF
			printf("GroundFilter 사이즈 부족\n");
			#endif	

			break;
		}
	}	
	//위 For 문을 거치면 결과적으로 결과 값은 하나만 남음
	return fusedObstacle;
}

nerv_err_t StubObjectManager::on_objectManagerData_connect( const client_addr_t& in_client_addr,
												const bool& in_tcp_request,
												const double& in_elapsed_time,
												const double& in_limit_time )
{
	printf("objectManagerData_connect => return err_none\n");
	return Nerv::c_nerv_err_none;
}

void StubObjectManager::on_objectManagerData_shutdown( const client_addr_t& in_client_addr)
{
	printf("on_objectManagerData_shutdown\n");
}
