#include "RangeDataDivide.h"
#include "StubObjectManager.h"
#include <stdio.h>
#include <conio.h>



void main(void)
{
	
	RangeDataDivide*	datadivide		= new RangeDataDivide;
	while(1)
	{
		if(_kbhit() == 0)
		{
			if(_getch() == 'q')break;
		}
	}
	delete datadivide;
}

RangeDataDivide::RangeDataDivide(void)
{
	proxylrf		= new ProxyLRFdata(this);
	ObjectManager	= new StubObjectManager;

	char buf[128];
	GetPrivateProfileString("DENOMINATION","METER ","999",buf,sizeof(buf),".\\ObjectManager.ini");
	denomination = (double)atof(buf);
	printf("단위 보정값 = %lf\n",(double)denomination);
}

RangeDataDivide::~RangeDataDivide(void)
{
	obstacle.clear();
	delete proxylrf;
	delete ObjectManager;
}


void RangeDataDivide::lrfrecv(unsigned short* lrfdata, int width, double east , double north , double heading)
{
	// 현재 LRF 데이터를 저장 중인지 아닌지를 판별 
	objectcount = 0;														
	startend	= END;

	m_east		= east;
	m_north		= north;
	m_heading   = heading;

	for(int i = 0 ; i < width ; i++)
	{
		// 시작데이터가 최대 감지거리보다 짧을때. 장애물로 인식하고 저장 시작. 시작데이터 예외 처림 	
		if((MAX_RANGE > (int)lrfdata[i]) && (i == 0))						
		{															
			StartObject((int)lrfdata[i],width,i);						
		}		
		if(i>0 && (abs((int)lrfdata[i]-(int)lrfdata[i-1]) > ObjectOFFSET))
		{
			unsigned short aa = (int)lrfdata[i];
			switch (startend)
			{
			case 0:
				// 이전 데이터와 현재 데이터를 비교하여 오프셋보다 거리가 멀고  
				StartObject((int)lrfdata[i],width,i);						
				break;													
			case 1:
				// 이전 데이터와 현재 데이터를 비교하여 오프셋보다 거리가 멀고
				// 장애물 저장중이면 장애물 종료점으로 판단(저장값은 이전값 데이터를 저장)
				EndObject((int)lrfdata[i-1],width,i-1);						
				if((int)lrfdata[i] != MAX_RANGE)
				{
					StartObject(lrfdata[i],width,i);
				}
				break;													
			}
		}
		if(((int)lrfdata[i] == MAX_RANGE) && (startend ==START))
		{
			EndObject(lrfdata[i-1],width,i-1);
		}
		if((obstacle.size() >= 0) &&(i== (width-1)) && startend==START)			
		{
			EndObject((int)lrfdata[i],width,i);
		}
		else if((i == (width-1)) && (startend==START))
		{
			EndObject((int)lrfdata[i],width,i);				
		}
	}
#ifdef DEBUG_PRINTF
	consoleColor(1,0,0);
	printf("data Divide Debuging START\n");
	consoleColor(1,1,1);
	for(int cp = 0; cp < objectcount ; cp ++)
	{
		printf("%d: (e %.2lf ,n %.2lf ,a %.2lf)"	,(int)cp			//데이터 확인용 
													,obstacle[cp].centerXY.x 
													,obstacle[cp].centerXY.y
													,obstacle[cp].Ang);
		printf("d %3.2lf w %3.2lf\n",obstacle[cp].distance,obstacle[cp].Width);
	}
	consoleColor(1,0,0);
	printf("data Divide Debuging END\n");
		consoleColor(1,1,1);
#endif
		
	if(objectcount > 0)								
	{
		// lrf 에서 구분된 장애물이 있을경우 ObstacleData 호출 
		consoleColor(1,1,1);
#ifdef DEBUG_PRINT
		printf("Divide Object %d>\n",objectcount);	
#endif			
		ObjectManager->ObstacleData(obstacle,east,north,heading);
	}
	else
	{
		//장애물이 하나도 없을때-> 장애물 없음 
		consoleColor(1,0,0);
		printf("LRF Data Clear(object none)\n");
	}
	obstacle.clear();
}

void RangeDataDivide::StartObject(int Data, int TotalAngle,int res)
{
	//startAng = (double)(res * RESOLUTION)  -  (double)(TotalAngle * RESOLUTION )/ 2.;

	double bufang =  (double)(res * RESOLUTION);

	startAng = 	 SENSOR_START_ANG	- (double)(res * RESOLUTION) ;

	double buf_heading = (double)startAng + (m_heading * 180);

	if(buf_heading > 180.0)
	{
		buf_heading =  buf_heading - 360.;
	}
	else if(buf_heading < -180.0)
	{
		buf_heading =  buf_heading + 360.;
	}

	 	
	O_buffer.startXY.x	= (double)(Data/ denomination) * cos((double)(buf_heading/DR));  	//  /denomination. = 미터단위로 표시하기 위해서  
	O_buffer.startXY.y	= (double)(Data/ denomination) * sin((double)(buf_heading/DR));  	//  /denomination. = 미터단위로 표시하기 위해서  

	O_buffer.startXY.x	= O_buffer.startXY.x + m_north;
	O_buffer.startXY.y	= O_buffer.startXY.y + m_east;

	startend = START;
}
void RangeDataDivide::EndObject(int Data, int TotalAngle,int res)
{
	double bufang =  (double)(res * RESOLUTION);
	double e_endAng =0.0;
	
	endAng = 	 SENSOR_START_ANG	- (double)(res * RESOLUTION) ;
	
	double buf_heading = (double)endAng + (m_heading * 180);

	if(buf_heading > 180.0)
	{
		buf_heading =  buf_heading - 360.;
	}
	else if(buf_heading < -180.0)
	{
		buf_heading =  buf_heading + 360.;
	}

	O_buffer.endXY.x	=	(double)(Data/ denomination) * cos((double)(buf_heading/DR)) ;		//  /DENOMINATION. = 미터단위로 표시하기 위해서  
	O_buffer.endXY.y	=	(double)(Data/ denomination) * sin((double)(buf_heading/DR)) ;		//  /DENOMINATION. = 미터단위로 표시하기 위해서  
	O_buffer.endXY.x	=	O_buffer.endXY.x + m_north;
	O_buffer.endXY.y	=	O_buffer.endXY.y + m_east;

	O_buffer.centerXY.x	=	((double)O_buffer.startXY.x + (double)O_buffer.endXY.x) /2.;	//장애물의 중간점 구함 
	O_buffer.centerXY.y =	((double)O_buffer.startXY.y + (double)O_buffer.endXY.y) /2.;
	O_buffer.distance	=   (double)pdistance(O_buffer.centerXY.x , O_buffer.centerXY.y,m_north,m_east);	// 센서 기준 거리 -> 기준좌표 (0,0) 

	O_buffer.Ang		=	(startAng + endAng )/2.;
	O_buffer.Width		= sqrt((double)square((double)difference((double)O_buffer.startXY.x,(double)O_buffer.endXY.x)) + 
							(double)square((double)difference((double)O_buffer.startXY.y,(double)O_buffer.endXY.y)));
	objectcount++;
	startend = END;
 	obstacle.push_back(O_buffer);
}
