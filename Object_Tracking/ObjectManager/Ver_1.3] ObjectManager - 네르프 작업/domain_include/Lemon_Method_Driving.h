/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Method_Driving
    creator              : Lemonade
    creator information  : 
    standardport         : 1300
    description          : 
                                                                                                  by Lemonade, at Tue Sep 09 02:20:08 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Method_Driving__Lemon_Method_Driving_h
#define Define__Lemon_Method_Driving__Lemon_Method_Driving_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Driving.h>
#include <Lemon_General_Component.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Method_Driving
{

        using namespace Lemon_Data_Driving;
        using namespace Lemon_General_Component;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 1300;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : wrench_effort_cmd
            Input      : Lemon_Data_Driving::struct_wrench_effort
            Output     : Lemon_Method_Driving::
            Method Type: COMMAND
            Code       : 0x1001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_wrench_effort_cmd : public ::Nerv::nerv_proxy_object_command
        {
        public:
            proxy_method_wrench_effort_cmd() :  nerv_proxy_object_command(0x1001) {}

            bool wrench_effort_cmd_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t wrench_effort_cmd_connect( const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void wrench_effort_cmd_shutdown() { shutdown(); }

            void wrench_effort_cmd_set_sync_model ( const bool& in_using_udp,
                                  const sender_sync_model_t& in_sync_model,
                                  const double& in_interval)
            {
                set_sync_model ( in_using_udp, in_sync_model,in_interval);
            }

            Lemon_Data_Driving::struct_wrench_effort* wrench_effort_cmd_get()
            {
                return (Lemon_Data_Driving::struct_wrench_effort*)get(sizeof(Lemon_Data_Driving::struct_wrench_effort));
            }

            void wrench_effort_cmd_cancel()
            {
                cancel();
            }

            nerv_err_t wrench_effort_cmd_post(Lemon_Data_Driving::struct_wrench_effort* pin_buf  )
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return post(outSize);
            }

            nerv_err_t wrench_effort_cmd_write( Lemon_Data_Driving::struct_wrench_effort const* const& pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return write(pin_buf,outSize);
            }

            letter_case_t wrench_effort_cmd_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void wrench_effort_cmd_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t wrench_effort_cmd_read( Lemon_Data_Driving::struct_wrench_effort* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_wrench_effort_cmd_disconnected();
            }

            virtual void on_wrench_effort_cmd_disconnected() = 0; //{}
        };

        /* stub */
        class method_wrench_effort_cmd : public ::Nerv::nerv_object_command
        {
        public:
            method_wrench_effort_cmd() : nerv_object_command(0x1001) {}

            void wrench_effort_cmd_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }


            void wrench_effort_cmd_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t wrench_effort_cmd_see_history( const client_addr_t& in_client_addr, const size_t& in_size)
            {
                return see_history(in_client_addr, in_size);
            }

            void wrench_effort_cmd_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            unsigned int wrench_effort_cmd_get_client_count()
            {
                return get_client_count();
            }

            void wrench_effort_cmd_free_client_addr_list(client_addr_list_t* p_list)
            {
                free_client_addr_list(p_list);
            }

            nerv_err_t wrench_effort_cmd_read(const client_addr_t& in_client, Lemon_Data_Driving::struct_wrench_effort* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return read(in_client, pin_data, &in_size );
            }

        private:

            virtual void on_sync( const client_addr_t& in_client,
                                  const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_wrench_effort_cmd_sync(in_client, in_sync_model,in_interval,(Lemon_Data_Driving::struct_wrench_effort*)pin_buffer);
            }

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_wrench_effort_cmd_connect( in_client_addr,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_wrench_effort_cmd_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_wrench_effort_cmd_connect( const client_addr_t& in_client_addr,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_wrench_effort_cmd_shutdown( const client_addr_t& in_client_addr) = 0; //{}

            virtual void on_wrench_effort_cmd_sync( const client_addr_t& in_client,
                                             const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                              Lemon_Data_Driving::struct_wrench_effort* pin_buffer ) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : wrench_effort_state
            Input      : Lemon_Method_Driving::
            Output     : Lemon_Data_Driving::struct_wrench_effort
            Method Type: INFORMATION
            Code       : 0x1002
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_wrench_effort_state : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_wrench_effort_state() :    nerv_proxy_object_information(0x1002) {}

            bool wrench_effort_state_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t wrench_effort_state_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void wrench_effort_state_shutdown() { shutdown(); }

            void wrench_effort_state_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t wrench_effort_state_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void wrench_effort_state_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t wrench_effort_state_read( Lemon_Data_Driving::struct_wrench_effort* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_wrench_effort_state_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_wrench_effort_state_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::struct_wrench_effort*)pin_buffer );
            }

            virtual void on_wrench_effort_state_disconnected() = 0; //{}

            virtual void on_wrench_effort_state_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::struct_wrench_effort const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_wrench_effort_state : public ::Nerv::nerv_object_information
        {

        public:

            method_wrench_effort_state(int interval= 0) : nerv_object_information(0x1002) {}

            void wrench_effort_state_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void wrench_effort_state_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::struct_wrench_effort* wrench_effort_state_get()
            {
                return (Lemon_Data_Driving::struct_wrench_effort*)get(sizeof(Lemon_Data_Driving::struct_wrench_effort) );
            }

            void wrench_effort_state_cancel()
            {
                cancel();
            }

            nerv_err_t wrench_effort_state_post(Lemon_Data_Driving::struct_wrench_effort* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return post( outSize);
            }

            nerv_err_t wrench_effort_state_write(Lemon_Data_Driving::struct_wrench_effort* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return write(pin_buf,outSize);
            }

            letter_case_t wrench_effort_state_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void wrench_effort_state_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t wrench_effort_state_read( Lemon_Data_Driving::struct_wrench_effort* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_wrench_effort);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_wrench_effort_state_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_wrench_effort_state_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_wrench_effort_state_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_wrench_effort_state_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : waypoint_cmd
            Input      : Lemon_Data_Driving::struct_waypoint
            Output     : ::
            Method Type: COMMAND
            Code       : 0x1004
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_waypoint_cmd : public ::Nerv::nerv_proxy_object_command
        {
        public:
            proxy_method_waypoint_cmd() :  nerv_proxy_object_command(0x1004) {}

            bool waypoint_cmd_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t waypoint_cmd_connect( const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void waypoint_cmd_shutdown() { shutdown(); }

            void waypoint_cmd_set_sync_model ( const bool& in_using_udp,
                                  const sender_sync_model_t& in_sync_model,
                                  const double& in_interval)
            {
                set_sync_model ( in_using_udp, in_sync_model,in_interval);
            }

            Lemon_Data_Driving::struct_waypoint* waypoint_cmd_get()
            {
                return (Lemon_Data_Driving::struct_waypoint*)get(sizeof(Lemon_Data_Driving::struct_waypoint));
            }

            void waypoint_cmd_cancel()
            {
                cancel();
            }

            nerv_err_t waypoint_cmd_post(Lemon_Data_Driving::struct_waypoint* pin_buf  )
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_waypoint);
                return post(outSize);
            }

            nerv_err_t waypoint_cmd_write( Lemon_Data_Driving::struct_waypoint const* const& pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_waypoint);
                return write(pin_buf,outSize);
            }

            letter_case_t waypoint_cmd_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void waypoint_cmd_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t waypoint_cmd_read( Lemon_Data_Driving::struct_waypoint* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_waypoint);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_waypoint_cmd_disconnected();
            }

            virtual void on_waypoint_cmd_disconnected() = 0; //{}
        };

        /* stub */
        class method_waypoint_cmd : public ::Nerv::nerv_object_command
        {
        public:
            method_waypoint_cmd() : nerv_object_command(0x1004) {}

            void waypoint_cmd_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }


            void waypoint_cmd_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t waypoint_cmd_see_history( const client_addr_t& in_client_addr, const size_t& in_size)
            {
                return see_history(in_client_addr, in_size);
            }

            void waypoint_cmd_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            unsigned int waypoint_cmd_get_client_count()
            {
                return get_client_count();
            }

            void waypoint_cmd_free_client_addr_list(client_addr_list_t* p_list)
            {
                free_client_addr_list(p_list);
            }

            nerv_err_t waypoint_cmd_read(const client_addr_t& in_client, Lemon_Data_Driving::struct_waypoint* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_waypoint);
                return read(in_client, pin_data, &in_size );
            }

        private:

            virtual void on_sync( const client_addr_t& in_client,
                                  const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_waypoint_cmd_sync(in_client, in_sync_model,in_interval,(Lemon_Data_Driving::struct_waypoint*)pin_buffer);
            }

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_waypoint_cmd_connect( in_client_addr,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_waypoint_cmd_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_waypoint_cmd_connect( const client_addr_t& in_client_addr,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_waypoint_cmd_shutdown( const client_addr_t& in_client_addr) = 0; //{}

            virtual void on_waypoint_cmd_sync( const client_addr_t& in_client,
                                             const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                              Lemon_Data_Driving::struct_waypoint const* const pin_buffer ) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : get_driving_path
            Input      : Lemon_Data_Driving::
            Output     : Lemon_Data_Driving::struct_driving_path
            Method Type: CALL
            Code       : 0x1005
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_get_driving_path : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_get_driving_path()    : nerv_proxy_object_call(0x1005) {}

            ::Nerv::nerv_err_t get_driving_path(Lemon_Data_Driving::struct_driving_path* pOut,  const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_driving_path);
                return_object_t return_object = async_call(0,0, in_limit_time);
                return async_return( return_object ,pOut,&outSize, pout_time );
            }

            void get_driving_path_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_get_driving_path : public ::Nerv::nerv_object_call
        {
        public:
            method_get_driving_path() :  nerv_object_call(0x1005,sizeof(Lemon_Data_Driving::struct_driving_path)) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_get_driving_path(in_client_addr,(Lemon_Data_Driving::struct_driving_path*)pout_buf,in_elapsed_time,in_limit_time);
                ::Nerv::pksize_t outSize = ((Lemon_Data_Driving::struct_driving_path*)pout_buf)->get_sizeof();
                *pout_buf_size = outSize;
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_get_driving_path_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_get_driving_path(const ::Nerv::client_addr_t& client_addr,Lemon_Data_Driving::struct_driving_path* pOut,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_get_driving_path_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : set_driving_path
            Input      : Lemon_Data_Driving::struct_driving_path
            Output     : Lemon_Data_Driving::
            Method Type: CALL
            Code       : 0x1006
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_set_driving_path : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_set_driving_path()    : nerv_proxy_object_call(0x1006) {}

            ::Nerv::nerv_err_t set_driving_path(Lemon_Data_Driving::struct_driving_path* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = pIn->get_sizeof();
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void set_driving_path_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_set_driving_path : public ::Nerv::nerv_object_call
        {
        public:
            method_set_driving_path() :  nerv_object_call(0x1006,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_set_driving_path(in_client_addr,(Lemon_Data_Driving::struct_driving_path*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_set_driving_path_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_set_driving_path(const ::Nerv::client_addr_t& client_addr,Lemon_Data_Driving::struct_driving_path* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_set_driving_path_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : set_commander
            Input      : Nerv::packed_client_addr_t
            Output     : Nerv::
            Method Type: CALL
            Code       : 0x1007
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_set_commander : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_set_commander()    : nerv_proxy_object_call(0x1007) {}

            ::Nerv::nerv_err_t set_commander(Nerv::packed_client_addr_t* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Nerv::packed_client_addr_t);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void set_commander_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_set_commander : public ::Nerv::nerv_object_call
        {
        public:
            method_set_commander() :  nerv_object_call(0x1007,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_set_commander(in_client_addr,(Nerv::packed_client_addr_t*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_set_commander_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_set_commander(const ::Nerv::client_addr_t& client_addr,Nerv::packed_client_addr_t* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_set_commander_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : set_output_driver
            Input      : Nerv::packed_client_addr_t
            Output     : Lemon_Data_Driving::
            Method Type: CALL
            Code       : 0x1008
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_set_output_driver : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_set_output_driver()    : nerv_proxy_object_call(0x1008) {}

            ::Nerv::nerv_err_t set_output_driver(Nerv::packed_client_addr_t* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Nerv::packed_client_addr_t);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void set_output_driver_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_set_output_driver : public ::Nerv::nerv_object_call
        {
        public:
            method_set_output_driver() :  nerv_object_call(0x1008,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_set_output_driver(in_client_addr,(Nerv::packed_client_addr_t*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_set_output_driver_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_set_output_driver(const ::Nerv::client_addr_t& client_addr,Nerv::packed_client_addr_t* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_set_output_driver_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : set_feedback_driver
            Input      : Nerv::packed_client_addr_t
            Output     : Lemon_Data_Driving::
            Method Type: CALL
            Code       : 0x1009
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_set_feedback_driver : public ::Nerv::nerv_proxy_object_call
        {
        public:
            proxy_method_set_feedback_driver()    : nerv_proxy_object_call(0x1009) {}

            ::Nerv::nerv_err_t set_feedback_driver(Nerv::packed_client_addr_t* pIn, const double& in_limit_time = 1.0 ,double* pout_time= 0)
            {
                ::Nerv::pksize_t inSize = sizeof(Nerv::packed_client_addr_t);
                return_object_t return_object = async_call(pIn,inSize, in_limit_time);
                return async_return( return_object ,0,0, pout_time );
            }

            void set_feedback_driver_cancel(const nerv_err_t& in_error)
            {
                cancel(in_error);
            }
        };

        /* stub */
        class method_set_feedback_driver : public ::Nerv::nerv_object_call
        {
        public:
            method_set_feedback_driver() :  nerv_object_call(0x1009,0) {}

        protected:

            virtual ::Nerv::nerv_err_t on_call ( const client_addr_t& in_client_addr,
                                                 void* const          pin_buf,
                                                 const pksize_t&      in_buf_size,
                                                 void*                pout_buf,
                                                 pksize_t*            pout_buf_size,
                                                 const double&        in_elapsed_time,
                                                 const double&        in_limit_time )
            {
                ::Nerv::nerv_err_t r = on_set_feedback_driver(in_client_addr,(Nerv::packed_client_addr_t*)pin_buf,in_elapsed_time,in_limit_time);
                return r;
            }

            virtual void on_call_cancel( const client_addr_t& in_client_addr, const nerv_err_t& in_error)
            {
                on_set_feedback_driver_cancel(in_client_addr,in_error);
            }

            virtual ::Nerv::nerv_err_t on_set_feedback_driver(const ::Nerv::client_addr_t& client_addr,Nerv::packed_client_addr_t* pIn,const double& in_elapsed_time,const double& in_limit_time) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_set_feedback_driver_cancel(const ::Nerv::client_addr_t& in_client_addr, const ::Nerv::nerv_err_t& in_error) = 0; //{}
        };

        /********************************************************************************************************************************************
            Method     : collision_state
            Input      : ::
            Output     : Lemon_Data_Driving::struct_collision
            Method Type: INFORMATION
            Code       : 0x100A
            Writer     : root
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_collision_state : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_collision_state() :    nerv_proxy_object_information(0x100A) {}

            bool collision_state_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t collision_state_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void collision_state_shutdown() { shutdown(); }

            void collision_state_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t collision_state_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void collision_state_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t collision_state_read( Lemon_Data_Driving::struct_collision* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_collision);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_collision_state_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_collision_state_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::struct_collision*)pin_buffer );
            }

            virtual void on_collision_state_disconnected() = 0; //{}

            virtual void on_collision_state_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::struct_collision const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_collision_state : public ::Nerv::nerv_object_information
        {

        public:

            method_collision_state(int interval= 0) : nerv_object_information(0x100A) {}

            void collision_state_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void collision_state_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::struct_collision* collision_state_get()
            {
                return (Lemon_Data_Driving::struct_collision*)get(sizeof(Lemon_Data_Driving::struct_collision) );
            }

            void collision_state_cancel()
            {
                cancel();
            }

            nerv_err_t collision_state_post(Lemon_Data_Driving::struct_collision* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_collision);
                return post( outSize);
            }

            nerv_err_t collision_state_write(Lemon_Data_Driving::struct_collision* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_collision);
                return write(pin_buf,outSize);
            }

            letter_case_t collision_state_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void collision_state_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t collision_state_read( Lemon_Data_Driving::struct_collision* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_collision);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_collision_state_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_collision_state_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_collision_state_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_collision_state_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : waypoint_tracer_debug
            Input      : ::
            Output     : Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG
            Method Type: INFORMATION
            Code       : 0xF000
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_waypoint_tracer_debug : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_waypoint_tracer_debug() :    nerv_proxy_object_information(0xF000) {}

            bool waypoint_tracer_debug_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t waypoint_tracer_debug_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void waypoint_tracer_debug_shutdown() { shutdown(); }

            void waypoint_tracer_debug_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t waypoint_tracer_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void waypoint_tracer_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t waypoint_tracer_debug_read( Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_waypoint_tracer_debug_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_waypoint_tracer_debug_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG*)pin_buffer );
            }

            virtual void on_waypoint_tracer_debug_disconnected() = 0; //{}

            virtual void on_waypoint_tracer_debug_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_waypoint_tracer_debug : public ::Nerv::nerv_object_information
        {

        public:

            method_waypoint_tracer_debug(int interval= 0) : nerv_object_information(0xF000) {}

            void waypoint_tracer_debug_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void waypoint_tracer_debug_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG* waypoint_tracer_debug_get()
            {
                return (Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG*)get(sizeof(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG) );
            }

            void waypoint_tracer_debug_cancel()
            {
                cancel();
            }

            nerv_err_t waypoint_tracer_debug_post(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG);
                return post( outSize);
            }

            nerv_err_t waypoint_tracer_debug_write(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG);
                return write(pin_buf,outSize);
            }

            letter_case_t waypoint_tracer_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void waypoint_tracer_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t waypoint_tracer_debug_read( Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::WAYPOINT_TRACER_DEBUG);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_waypoint_tracer_debug_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_waypoint_tracer_debug_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_waypoint_tracer_debug_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_waypoint_tracer_debug_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : path_tracer_debug
            Input      : ::
            Output     : Lemon_Data_Driving::PATH_TRACER_DEBUG
            Method Type: INFORMATION
            Code       : 0xF001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_path_tracer_debug : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_path_tracer_debug() :    nerv_proxy_object_information(0xF001) {}

            bool path_tracer_debug_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t path_tracer_debug_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void path_tracer_debug_shutdown() { shutdown(); }

            void path_tracer_debug_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t path_tracer_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void path_tracer_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t path_tracer_debug_read( Lemon_Data_Driving::PATH_TRACER_DEBUG* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::PATH_TRACER_DEBUG);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_path_tracer_debug_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_path_tracer_debug_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::PATH_TRACER_DEBUG*)pin_buffer );
            }

            virtual void on_path_tracer_debug_disconnected() = 0; //{}

            virtual void on_path_tracer_debug_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::PATH_TRACER_DEBUG const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_path_tracer_debug : public ::Nerv::nerv_object_information
        {

        public:

            method_path_tracer_debug(int interval= 0) : nerv_object_information(0xF001) {}

            void path_tracer_debug_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void path_tracer_debug_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::PATH_TRACER_DEBUG* path_tracer_debug_get()
            {
                return (Lemon_Data_Driving::PATH_TRACER_DEBUG*)get(sizeof(Lemon_Data_Driving::PATH_TRACER_DEBUG) );
            }

            void path_tracer_debug_cancel()
            {
                cancel();
            }

            nerv_err_t path_tracer_debug_post(Lemon_Data_Driving::PATH_TRACER_DEBUG* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::PATH_TRACER_DEBUG);
                return post( outSize);
            }

            nerv_err_t path_tracer_debug_write(Lemon_Data_Driving::PATH_TRACER_DEBUG* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::PATH_TRACER_DEBUG);
                return write(pin_buf,outSize);
            }

            letter_case_t path_tracer_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void path_tracer_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t path_tracer_debug_read( Lemon_Data_Driving::PATH_TRACER_DEBUG* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::PATH_TRACER_DEBUG);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_path_tracer_debug_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_path_tracer_debug_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_path_tracer_debug_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_path_tracer_debug_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : yawrate_steering
            Input      : ::
            Output     : Lemon_Data_Driving::struct_steering_data
            Method Type: INFORMATION
            Code       : 0xF002
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_yawrate_steering : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_yawrate_steering() :    nerv_proxy_object_information(0xF002) {}

            bool yawrate_steering_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t yawrate_steering_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void yawrate_steering_shutdown() { shutdown(); }

            void yawrate_steering_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t yawrate_steering_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void yawrate_steering_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t yawrate_steering_read( Lemon_Data_Driving::struct_steering_data* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_steering_data);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_yawrate_steering_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_yawrate_steering_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::struct_steering_data*)pin_buffer );
            }

            virtual void on_yawrate_steering_disconnected() = 0; //{}

            virtual void on_yawrate_steering_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::struct_steering_data const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_yawrate_steering : public ::Nerv::nerv_object_information
        {

        public:

            method_yawrate_steering(int interval= 0) : nerv_object_information(0xF002) {}

            void yawrate_steering_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void yawrate_steering_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::struct_steering_data* yawrate_steering_get()
            {
                return (Lemon_Data_Driving::struct_steering_data*)get(sizeof(Lemon_Data_Driving::struct_steering_data) );
            }

            void yawrate_steering_cancel()
            {
                cancel();
            }

            nerv_err_t yawrate_steering_post(Lemon_Data_Driving::struct_steering_data* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_steering_data);
                return post( outSize);
            }

            nerv_err_t yawrate_steering_write(Lemon_Data_Driving::struct_steering_data* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_steering_data);
                return write(pin_buf,outSize);
            }

            letter_case_t yawrate_steering_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void yawrate_steering_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t yawrate_steering_read( Lemon_Data_Driving::struct_steering_data* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_steering_data);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_yawrate_steering_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_yawrate_steering_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_yawrate_steering_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_yawrate_steering_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : estimate_debug
            Input      : ::
            Output     : Lemon_Data_Driving::struct_estimate_debug
            Method Type: INFORMATION
            Code       : 0xF003
            Writer     : root
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_estimate_debug : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_estimate_debug() :    nerv_proxy_object_information(0xF003) {}

            bool estimate_debug_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t estimate_debug_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void estimate_debug_shutdown() { shutdown(); }

            void estimate_debug_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t estimate_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void estimate_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t estimate_debug_read( Lemon_Data_Driving::struct_estimate_debug* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_estimate_debug);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_estimate_debug_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_estimate_debug_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::struct_estimate_debug*)pin_buffer );
            }

            virtual void on_estimate_debug_disconnected() = 0; //{}

            virtual void on_estimate_debug_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::struct_estimate_debug const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_estimate_debug : public ::Nerv::nerv_object_information
        {

        public:

            method_estimate_debug(int interval= 0) : nerv_object_information(0xF003) {}

            void estimate_debug_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void estimate_debug_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::struct_estimate_debug* estimate_debug_get()
            {
                return (Lemon_Data_Driving::struct_estimate_debug*)get(sizeof(Lemon_Data_Driving::struct_estimate_debug) );
            }

            void estimate_debug_cancel()
            {
                cancel();
            }

            nerv_err_t estimate_debug_post(Lemon_Data_Driving::struct_estimate_debug* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_estimate_debug);
                return post( outSize);
            }

            nerv_err_t estimate_debug_write(Lemon_Data_Driving::struct_estimate_debug* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Driving::struct_estimate_debug);
                return write(pin_buf,outSize);
            }

            letter_case_t estimate_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void estimate_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t estimate_debug_read( Lemon_Data_Driving::struct_estimate_debug* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_estimate_debug);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_estimate_debug_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_estimate_debug_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_estimate_debug_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_estimate_debug_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };

        /********************************************************************************************************************************************
            Method     : ptca_debug
            Input      : ::
            Output     : Lemon_Data_Driving::struct_ptca_debug
            Method Type: INFORMATION
            Code       : 0xF004
            Writer     : root
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_ptca_debug : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_ptca_debug() :    nerv_proxy_object_information(0xF004) {}

            bool ptca_debug_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t ptca_debug_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void ptca_debug_shutdown() { shutdown(); }

            void ptca_debug_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t ptca_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void ptca_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t ptca_debug_read( Lemon_Data_Driving::struct_ptca_debug* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_ptca_debug);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_ptca_debug_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_ptca_debug_sync(in_sync_model,in_interval ,(Lemon_Data_Driving::struct_ptca_debug*)pin_buffer );
            }

            virtual void on_ptca_debug_disconnected() = 0; //{}

            virtual void on_ptca_debug_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Driving::struct_ptca_debug * pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_ptca_debug : public ::Nerv::nerv_object_information
        {

        public:

            method_ptca_debug(int interval= 0) : nerv_object_information(0xF004) {}

            void ptca_debug_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void ptca_debug_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Driving::struct_ptca_debug* ptca_debug_get()
            {
                return (Lemon_Data_Driving::struct_ptca_debug*)get(sizeof(Lemon_Data_Driving::struct_ptca_debug) );
            }

            void ptca_debug_cancel()
            {
                cancel();
            }

            nerv_err_t ptca_debug_post(Lemon_Data_Driving::struct_ptca_debug* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_Driving::struct_ptca_debug*)pin_buf)->get_sizeof();
                return post( outSize);
            }

            nerv_err_t ptca_debug_write(Lemon_Data_Driving::struct_ptca_debug* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((Lemon_Data_Driving::struct_ptca_debug*)pin_buf)->get_sizeof();
                return write(pin_buf,outSize);
            }

            letter_case_t ptca_debug_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void ptca_debug_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t ptca_debug_read( Lemon_Data_Driving::struct_ptca_debug* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Driving::struct_ptca_debug);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_ptca_debug_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_ptca_debug_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_ptca_debug_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_ptca_debug_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/

        /********************************************************************************************************************************************
            Interface Name : driver
            Writer : Lemonade
            Description : 1
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_interface_BGeneralComponent
        , virtual public Lemon_Method_Driving::proxy_method_set_commander
        {
        };

        /* stub */
        class stub_interface_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::stub_interface_BGeneralComponent
        , virtual public Lemon_Method_Driving::method_set_commander
        {
        };


        /********************************************************************************************************************************************
            Interface Name : ackerman_driver
            Writer : Lemonade
            Description :
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_ackerman_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_driver
        , virtual public Lemon_Method_Driving::proxy_method_wrench_effort_cmd
        , virtual public Lemon_Method_Driving::proxy_method_wrench_effort_state
        {
        };

        /* stub */
        class stub_interface_ackerman_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_driver
        , virtual public Lemon_Method_Driving::method_wrench_effort_cmd
        , virtual public Lemon_Method_Driving::method_wrench_effort_state
        {
        };


        /********************************************************************************************************************************************
            Interface Name : primitive_driver
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_primitive_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_ackerman_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_output_driver
        {
        };

        /* stub */
        class stub_interface_primitive_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_ackerman_driver
        , virtual public Lemon_Method_Driving::method_set_output_driver
        {
        };


        /********************************************************************************************************************************************
            Interface Name : waypoint_driver
            Writer : Lemonade
            Description :
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_waypoint_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_driver
        , virtual public Lemon_Method_Driving::proxy_method_waypoint_cmd
        {
        };

        /* stub */
        class stub_interface_waypoint_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_driver
        , virtual public Lemon_Method_Driving::method_waypoint_cmd
        {
        };


        /********************************************************************************************************************************************
            Interface Name : reflexive_driver
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_reflexive_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_ackerman_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_output_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_feedback_driver
        {
        };

        /* stub */
        class stub_interface_reflexive_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_ackerman_driver
        , virtual public Lemon_Method_Driving::method_set_output_driver
        , virtual public Lemon_Method_Driving::method_set_feedback_driver
        {
        };


        /********************************************************************************************************************************************
            Interface Name : waypoint_tracer
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_waypoint_tracer : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_waypoint_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_output_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_feedback_driver
        , virtual public Lemon_Method_Driving::proxy_method_waypoint_tracer_debug
        {
        };

        /* stub */
        class stub_interface_waypoint_tracer : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_waypoint_driver
        , virtual public Lemon_Method_Driving::method_set_output_driver
        , virtual public Lemon_Method_Driving::method_set_feedback_driver
        , virtual public Lemon_Method_Driving::method_waypoint_tracer_debug
        {
        };


        /********************************************************************************************************************************************
            Interface Name : path_driver
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_path_driver : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_driver
        , virtual public Lemon_Method_Driving::proxy_method_get_driving_path
        , virtual public Lemon_Method_Driving::proxy_method_set_driving_path
        {
        };

        /* stub */
        class stub_interface_path_driver : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_driver
        , virtual public Lemon_Method_Driving::method_get_driving_path
        , virtual public Lemon_Method_Driving::method_set_driving_path
        {
        };


        /********************************************************************************************************************************************
            Interface Name : path_tracer
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_path_tracer : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_interface_path_driver
        , virtual public Lemon_Method_Driving::proxy_method_set_output_driver
        , virtual public Lemon_Method_Driving::proxy_method_path_tracer_debug
        {
        };

        /* stub */
        class stub_interface_path_tracer : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::stub_interface_path_driver
        , virtual public Lemon_Method_Driving::method_set_output_driver
        , virtual public Lemon_Method_Driving::method_path_tracer_debug
        {
        };


        /********************************************************************************************************************************************
            Interface Name : steering_data_process
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_steering_data_process : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_General_Component::proxy_interface_BGeneralComponent
        , virtual public Lemon_Method_Driving::proxy_method_wrench_effort_cmd
        , virtual public Lemon_Method_Driving::proxy_method_yawrate_steering
        {
        };

        /* stub */
        class stub_interface_steering_data_process : virtual public ::Nerv::nerv_object
        , virtual public Lemon_General_Component::stub_interface_BGeneralComponent
        , virtual public Lemon_Method_Driving::method_wrench_effort_cmd
        , virtual public Lemon_Method_Driving::method_yawrate_steering
        {
        };


        /********************************************************************************************************************************************
            Interface Name : ptca_debug
            Writer : root
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_ptca_debug : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Driving::proxy_method_ptca_debug
        {
        };

        /* stub */
        class stub_interface_ptca_debug : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Driving::method_ptca_debug
        {
        };



}


#endif // #ifdef Define__Lemon_Method_Driving__Lemon_Method_Driving_h
