/****************************************************************************************************************************************************

    Document Header

    domain               : FM_KJH_OBSTACLE_MANAGER
    creator              : Lemonade
    creator information  : 
    standardport         : 8509
    description          : 
                                                                                                  by Lemonade, at Fri Jul 13 20:15:16 2012

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__FM_KJH_OBSTACLE_MANAGER__FM_KJH_OBSTACLE_MANAGER_h
#define Define__FM_KJH_OBSTACLE_MANAGER__FM_KJH_OBSTACLE_MANAGER_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace FM_KJH_OBSTACLE_MANAGER
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 8509;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : Rectangular
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed Rectangular
        {
            /*  */
            static const double min_x() { return double(-100000.0); }
            static const double max_x() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_x,max_x > x;

            /*  */
            static const double min_y() { return double(-100000.0); }
            static const double max_y() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_y,max_y > y;

        };
        typedef template_history_elem<Rectangular> Rectangular_history_elem;
        typedef template_history<Rectangular> Rectangular_history;

        /********************************************************************************************************************************************
            data name  : ObjectRectangular
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed ObjectRectangular
        {
            /*  */
            FM_KJH_OBSTACLE_MANAGER::Rectangular startXY;

            /*  */
            FM_KJH_OBSTACLE_MANAGER::Rectangular endXY;

            /*  */
            FM_KJH_OBSTACLE_MANAGER::Rectangular centerXY;

            /*  */
            static const double min_Width() { return double(-100000.0); }
            static const double max_Width() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_Width,max_Width > Width;

            /*  */
            static const double min_Ang() { return double(-100000.0); }
            static const double max_Ang() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_Ang,max_Ang > Ang;

            /*  */
            static const double min_distance() { return double(-100000.0); }
            static const double max_distance() { return double(100000.0); }
            ::Nerv::real< ::Nerv::int32_t,min_distance,max_distance > distance;

        };
        typedef template_history_elem<ObjectRectangular> ObjectRectangular_history_elem;
        typedef template_history<ObjectRectangular> ObjectRectangular_history;

        /********************************************************************************************************************************************
            data name  : object2obstacle
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed object2obstacle
        {
            /*  */
            variable_array_field_dctrl_t obstacleData_dctrl;
            FM_KJH_OBSTACLE_MANAGER::ObjectRectangular* obstacleData() { return (FM_KJH_OBSTACLE_MANAGER::ObjectRectangular*)(dynamicbuffer+obstacleData_dctrl.dindex); }
            bool obstacleData_alloc(unsigned int array_size)
            {
                obstacleData_dctrl.dindex = dynamicsize;
                obstacleData_dctrl.dsize = array_size*sizeof(FM_KJH_OBSTACLE_MANAGER::ObjectRectangular);
                if(_maxDynamicSize < dynamicsize + obstacleData_dctrl.dsize) return false;
                dynamicsize += obstacleData_dctrl.dsize;
                return true;
            }

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(FM_KJH_OBSTACLE_MANAGER::ObjectRectangular)*100};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(object2obstacle)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<object2obstacle> object2obstacle_history_elem;
        typedef template_history<object2obstacle> object2obstacle_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : objectManagerData
            Input      : ::
            Output     : FM_KJH_OBSTACLE_MANAGER::object2obstacle
            Method Type: INFORMATION
            Code       : 0xFF04
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_objectManagerData : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_objectManagerData() :    nerv_proxy_object_information(0xFF04) {}

            bool objectManagerData_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t objectManagerData_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void objectManagerData_shutdown() { shutdown(); }

            void objectManagerData_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t objectManagerData_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void objectManagerData_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t objectManagerData_read( FM_KJH_OBSTACLE_MANAGER::object2obstacle* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(FM_KJH_OBSTACLE_MANAGER::object2obstacle);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_objectManagerData_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_objectManagerData_sync(in_sync_model,in_interval ,(FM_KJH_OBSTACLE_MANAGER::object2obstacle*)pin_buffer );
            }

            virtual void on_objectManagerData_disconnected() = 0; //{}

            virtual void on_objectManagerData_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             FM_KJH_OBSTACLE_MANAGER::object2obstacle const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_objectManagerData : public ::Nerv::nerv_object_information
        {

        public:

            method_objectManagerData(int interval= 0) : nerv_object_information(0xFF04) {}

            void objectManagerData_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void objectManagerData_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            FM_KJH_OBSTACLE_MANAGER::object2obstacle* objectManagerData_get()
            {
                return (FM_KJH_OBSTACLE_MANAGER::object2obstacle*)get(sizeof(FM_KJH_OBSTACLE_MANAGER::object2obstacle) );
            }

            void objectManagerData_cancel()
            {
                cancel();
            }

            nerv_err_t objectManagerData_post(FM_KJH_OBSTACLE_MANAGER::object2obstacle* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((FM_KJH_OBSTACLE_MANAGER::object2obstacle*)pin_buf)->get_sizeof();
                return post( outSize);
            }

            nerv_err_t objectManagerData_write(FM_KJH_OBSTACLE_MANAGER::object2obstacle* pin_buf)
            {
                ::Nerv::pksize_t outSize = ((FM_KJH_OBSTACLE_MANAGER::object2obstacle*)pin_buf)->get_sizeof();
                return write(pin_buf,outSize);
            }

            letter_case_t objectManagerData_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void objectManagerData_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t objectManagerData_read( FM_KJH_OBSTACLE_MANAGER::object2obstacle* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(FM_KJH_OBSTACLE_MANAGER::object2obstacle);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_objectManagerData_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_objectManagerData_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_objectManagerData_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_objectManagerData_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__FM_KJH_OBSTACLE_MANAGER__FM_KJH_OBSTACLE_MANAGER_h
