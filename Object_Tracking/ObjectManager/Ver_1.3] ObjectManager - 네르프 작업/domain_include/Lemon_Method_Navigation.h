/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Method_Navigation
    creator              : Lemonade
    creator information  : 
    standardport         : 14654
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:30:18 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Method_Navigation__Lemon_Method_Navigation_h
#define Define__Lemon_Method_Navigation__Lemon_Method_Navigation_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Navigation.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Method_Navigation
{

        using namespace Lemon_Data_Navigation;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 14654;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/

        /********************************************************************************************************************************************
            Method     : navigation_v1
            Input      : ::
            Output     : Lemon_Data_Navigation::struct_navigation_v1
            Method Type: INFORMATION
            Code       : 0x0001
            Writer     : Lemonade
            Description: 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_method_navigation_v1 : public ::Nerv::nerv_proxy_object_information
        {
        public:

            proxy_method_navigation_v1() :    nerv_proxy_object_information(0x0001) {}

            bool navigation_v1_is_connected() { return is_connected(); }

            ::Nerv::nerv_err_t navigation_v1_connect( const bool& in_udp_request = false , const double& in_limit_time = 1.0 , double* pout_time = 0)
            {
                return_object_t return_object = async_connect( in_udp_request , in_limit_time );
                return async_connect_return( return_object, pout_time );
            }

            void navigation_v1_shutdown() { shutdown(); }

            void navigation_v1_set_sync(const bool& in_use_on_sync) { set_sync(in_use_on_sync); }

            letter_case_t navigation_v1_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void navigation_v1_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t navigation_v1_read( Lemon_Data_Navigation::struct_navigation_v1* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Navigation::struct_navigation_v1);
                return read(pin_data, &in_size );
            }

        private:

            virtual void on_disconnected()
            {
                on_navigation_v1_disconnected();
            }

            virtual void on_sync( const sender_sync_model_t& in_sync_model,
                                  const double& in_interval,
                                  void const* const pin_buffer,
                                  const pksize_t in_size)
            {
                on_navigation_v1_sync(in_sync_model,in_interval ,(Lemon_Data_Navigation::struct_navigation_v1*)pin_buffer );
            }

            virtual void on_navigation_v1_disconnected() = 0; //{}

            virtual void on_navigation_v1_sync( const sender_sync_model_t& in_sync_model,
                                             const double& in_interval,
                                             Lemon_Data_Navigation::struct_navigation_v1 const* const pin_buffer ) = 0; //{}
        };

        /* stub */
        class method_navigation_v1 : public ::Nerv::nerv_object_information
        {

        public:

            method_navigation_v1(int interval= 0) : nerv_object_information(0x0001) {}

            void navigation_v1_set_sync_model( const sender_sync_model_t& in_sync_model, const double& in_interval)
            {
                set_sync_model(in_sync_model,in_interval);
            }

            void navigation_v1_disconnect( const client_addr_t& in_client_addr )
            {
                disconnect( in_client_addr );
            }

            Lemon_Data_Navigation::struct_navigation_v1* navigation_v1_get()
            {
                return (Lemon_Data_Navigation::struct_navigation_v1*)get(sizeof(Lemon_Data_Navigation::struct_navigation_v1) );
            }

            void navigation_v1_cancel()
            {
                cancel();
            }

            nerv_err_t navigation_v1_post(Lemon_Data_Navigation::struct_navigation_v1* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Navigation::struct_navigation_v1);
                return post( outSize);
            }

            nerv_err_t navigation_v1_write(Lemon_Data_Navigation::struct_navigation_v1* pin_buf)
            {
                ::Nerv::pksize_t outSize = sizeof(Lemon_Data_Navigation::struct_navigation_v1);
                return write(pin_buf,outSize);
            }

            letter_case_t navigation_v1_see_history( const size_t& in_size)
            {
                return see_history( in_size);
            }

            void navigation_v1_set_history_size( const size_t& in_history_size)
            {
                set_history_size( in_history_size);
            }

            nerv_err_t navigation_v1_read( Lemon_Data_Navigation::struct_navigation_v1* pin_data)
            {
                Nerv::pksize_t in_size = sizeof(Lemon_Data_Navigation::struct_navigation_v1);
                return read(pin_data, &in_size );
            }

        private:

            virtual nerv_err_t on_connect( const client_addr_t& in_client_addr,
                                           const bool& in_tcp_request,
                                           const double& in_elapsed_time,
                                           const double& in_limit_time )
            {
                return on_navigation_v1_connect( in_client_addr,in_tcp_request,in_elapsed_time,in_limit_time );
            }

            virtual void on_shutdown( const client_addr_t& in_client_addr)
            {
                on_navigation_v1_shutdown(in_client_addr);
            }

            virtual nerv_err_t on_navigation_v1_connect( const client_addr_t& in_client_addr,
                                            const bool& in_tcp_request,
                                            const double& in_elapsed_time,
                                            const double& in_limit_time ) = 0; //{ return ::Nerv::c_nerv_err_not_implement; }

            virtual void on_navigation_v1_shutdown( const client_addr_t& in_client_addr) = 0; //{}

        };


    /* Interface Defines ***************************************************************************************************************************/

        /********************************************************************************************************************************************
            Interface Name : navigation
            Writer : Lemonade
            Description : 
        ********************************************************************************************************************************************/
        /* proxy */
        class proxy_interface_navigation : virtual public ::Nerv::nerv_proxy_object
        , virtual public Lemon_Method_Navigation::proxy_method_navigation_v1
        {
        };

        /* stub */
        class stub_interface_navigation : virtual public ::Nerv::nerv_object
        , virtual public Lemon_Method_Navigation::method_navigation_v1
        {
        };



}


#endif // #ifdef Define__Lemon_Method_Navigation__Lemon_Method_Navigation_h
