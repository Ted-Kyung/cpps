/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Driving
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Tue Sep 09 01:15:39 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Driving__Lemon_Data_Driving_h
#define Define__Lemon_Data_Driving__Lemon_Data_Driving_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/
#include <Lemon_Data_Estimator.h>
#include <Lemon_Data_LRF.h>
#include <Lemon_Data_Mgrs.h>
#include <Lemon_Data_Navigation.h>
#include <Lemon_General_Component.h>


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Driving
{

        using namespace Lemon_Data_Estimator;
        using namespace Lemon_Data_LRF;
        using namespace Lemon_Data_Mgrs;
        using namespace Lemon_Data_Navigation;
        using namespace Lemon_General_Component;

    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : struct_linear_estimate_ca
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_linear_estimate_ca
        {
            /*  */
            ::Nerv::ieee754_64_t cmd_velocity;

            /*  */
            ::Nerv::ieee754_64_t cmd_radius;

            /*  */
            ::Nerv::ieee754_64_t max_radius;

        };
        typedef template_history_elem<struct_linear_estimate_ca> struct_linear_estimate_ca_history_elem;
        typedef template_history<struct_linear_estimate_ca> struct_linear_estimate_ca_history;

        /********************************************************************************************************************************************
            data name  : travel_speed_t
            writer     : Lemonade
            description: 주행속도 m/s
        ********************************************************************************************************************************************/
        inline const double mintravel_speed_t() { return double(0.0); }
        inline const double maxtravel_speed_t() { return double(655.34); }
        typedef ::Nerv::real< ::Nerv::int16_t,mintravel_speed_t,maxtravel_speed_t > travel_speed_t;
        typedef template_history_elem<travel_speed_t> travel_speed_t_history_elem;
        typedef template_history<travel_speed_t> travel_speed_t_history;

        /********************************************************************************************************************************************
            data name  : enum_waypoint_state
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_waypoint_state;
        const enum_waypoint_state c_wp_stop = 0; // description:
        const enum_waypoint_state c_wp_start = 1; // description:
        typedef template_history_elem<enum_waypoint_state> enum_waypoint_state_history_elem;
        typedef template_history<enum_waypoint_state> enum_waypoint_state_history;

        /********************************************************************************************************************************************
            data name  : enum_waypoint_mission
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_waypoint_mission;
        typedef template_history_elem<enum_waypoint_mission> enum_waypoint_mission_history_elem;
        typedef template_history<enum_waypoint_mission> enum_waypoint_mission_history;

        /********************************************************************************************************************************************
            data name  : enum_waypoint_type
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_waypoint_type;
        typedef template_history_elem<enum_waypoint_type> enum_waypoint_type_history_elem;
        typedef template_history<enum_waypoint_type> enum_waypoint_type_history;

        /********************************************************************************************************************************************
            data name  : enum_waypoint_direction
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_waypoint_direction;
        const enum_waypoint_direction c_wd_forward = 0; // description:
        const enum_waypoint_direction c_wd_reverse = 1; // description:
        typedef template_history_elem<enum_waypoint_direction> enum_waypoint_direction_history_elem;
        typedef template_history<enum_waypoint_direction> enum_waypoint_direction_history;

        /********************************************************************************************************************************************
            data name  : enum_obstacle_aboid_type
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_obstacle_aboid_type;
        const enum_obstacle_aboid_type c_oat_wait = 0; // description:필수 경로점, 장애물 회피 않함
        const enum_obstacle_aboid_type c_oat_avoid = 1; // description:
        typedef template_history_elem<enum_obstacle_aboid_type> enum_obstacle_aboid_type_history_elem;
        typedef template_history<enum_obstacle_aboid_type> enum_obstacle_aboid_type_history;

        /********************************************************************************************************************************************
            data name  : struct_waypoint
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_waypoint
        {
            /*  */
            Lemon_Data_Driving::enum_waypoint_mission mission :4;

            /*  */
            Lemon_Data_Driving::enum_waypoint_type type :2;

            /*  */
            Lemon_Data_Driving::enum_waypoint_direction direction :1;

            /*  */
            Lemon_Data_Driving::enum_obstacle_aboid_type obstacle_avoid :1;

            /*  */
            Lemon_Data_Mgrs::struct_mgrs_v1 point;

            /*  */
            Lemon_Data_Driving::travel_speed_t travel_speed;

            /*  */
            Lemon_Data_Driving::enum_waypoint_state state;

            /*  */
            static const double min_safe_distance() { return double(0.0); }
            static const double max_safe_distance() { return double(655.34); }
            ::Nerv::real< ::Nerv::int16_t,min_safe_distance,max_safe_distance > safe_distance;

        };
        typedef template_history_elem<struct_waypoint> struct_waypoint_history_elem;
        typedef template_history<struct_waypoint> struct_waypoint_history;

        /********************************************************************************************************************************************
            data name  : struct_driving_path
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_driving_path
        {
            /*  */
            variable_array_field_dctrl_t waypoint_dctrl;
            Lemon_Data_Driving::struct_waypoint* waypoint() { return (Lemon_Data_Driving::struct_waypoint*)(dynamicbuffer+waypoint_dctrl.dindex); }
            bool waypoint_alloc(unsigned int array_size)
            {
                waypoint_dctrl.dindex = dynamicsize;
                waypoint_dctrl.dsize = array_size*sizeof(Lemon_Data_Driving::struct_waypoint);
                if(_maxDynamicSize < dynamicsize + waypoint_dctrl.dsize) return false;
                dynamicsize += waypoint_dctrl.dsize;
                return true;
            }

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(Lemon_Data_Driving::struct_waypoint)*1000000};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(struct_driving_path)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<struct_driving_path> struct_driving_path_history_elem;
        typedef template_history<struct_driving_path> struct_driving_path_history;

        /********************************************************************************************************************************************
            data name  : struct_wrench_effort
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_wrench_effort
        {
            /*  */
            static const double min_propulsive_linear_x() { return double(-100.0); }
            static const double max_propulsive_linear_x() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_linear_x,max_propulsive_linear_x > propulsive_linear_x;

            /*  */
            static const double min_propulsive_linear_y() { return double(-100.0); }
            static const double max_propulsive_linear_y() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_linear_y,max_propulsive_linear_y > propulsive_linear_y;

            /*  */
            static const double min_propulsive_linear_z() { return double(-100.0); }
            static const double max_propulsive_linear_z() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_linear_z,max_propulsive_linear_z > propulsive_linear_z;

            /*  */
            static const double min_propulsive_rotational_x() { return double(-100.0); }
            static const double max_propulsive_rotational_x() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_rotational_x,max_propulsive_rotational_x > propulsive_rotational_x;

            /*  */
            static const double min_propulsive_rotational_y() { return double(-100.0); }
            static const double max_propulsive_rotational_y() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_rotational_y,max_propulsive_rotational_y > propulsive_rotational_y;

            /*  */
            static const double min_propulsive_rotational_z() { return double(-100.0); }
            static const double max_propulsive_rotational_z() { return double(100.0); }
            ::Nerv::real< ::Nerv::int16_t,min_propulsive_rotational_z,max_propulsive_rotational_z > propulsive_rotational_z;

            /*  */
            static const double min_resistive_linear_x() { return double(0.0); }
            static const double max_resistive_linear_x() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_linear_x,max_resistive_linear_x > resistive_linear_x;

            /*  */
            static const double min_resistive_linear_y() { return double(0.0); }
            static const double max_resistive_linear_y() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_linear_y,max_resistive_linear_y > resistive_linear_y;

            /*  */
            static const double min_resistive_linear_z() { return double(0.0); }
            static const double max_resistive_linear_z() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_linear_z,max_resistive_linear_z > resistive_linear_z;

            /*  */
            static const double min_resistive_rotational_x() { return double(0.0); }
            static const double max_resistive_rotational_x() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_rotational_x,max_resistive_rotational_x > resistive_rotational_x;

            /*  */
            static const double min_resistive_rotational_y() { return double(0.0); }
            static const double max_resistive_rotational_y() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_rotational_y,max_resistive_rotational_y > resistive_rotational_y;

            /*  */
            static const double min_resistive_rotational_z() { return double(0.0); }
            static const double max_resistive_rotational_z() { return double(100.0); }
            ::Nerv::real< ::Nerv::int8_t,min_resistive_rotational_z,max_resistive_rotational_z > resistive_rotational_z;

        };
        typedef template_history_elem<struct_wrench_effort> struct_wrench_effort_history_elem;
        typedef template_history<struct_wrench_effort> struct_wrench_effort_history;

        /********************************************************************************************************************************************
            data name  : enum_driving_mode
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_driving_mode;
        const enum_driving_mode c_driving_mode_manual = 0; // description:
        const enum_driving_mode c_driving_mode_remote = 1; // description:
        const enum_driving_mode c_driving_mode_auto = 2; // description:
        const enum_driving_mode c_driving_mode_follow = 3; // description:
        typedef template_history_elem<enum_driving_mode> enum_driving_mode_history_elem;
        typedef template_history<enum_driving_mode> enum_driving_mode_history;

        /********************************************************************************************************************************************
            data name  : struct_vehicle_state
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_vehicle_state
        {
            /*  */
            Lemon_General_Component::struct_state state;

            /*  */
            Lemon_General_Component::struct_control_state control_state;

            /*  */
            Lemon_General_Component::enum_debug_state debug_state;

            /*  */
            Lemon_Data_Navigation::struct_linear_navigation_v1 navigation;

            /*  */
            Lemon_Data_Driving::travel_speed_t travel_speed;

            /*  */
            Lemon_Data_Driving::struct_wrench_effort wrench_effort;

        };
        typedef template_history_elem<struct_vehicle_state> struct_vehicle_state_history_elem;
        typedef template_history<struct_vehicle_state> struct_vehicle_state_history;

        /********************************************************************************************************************************************
            data name  : DRIVING_DATA
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed DRIVING_DATA
        {
            /*  */
            ::Nerv::ieee754_64_t x;

            /*  */
            ::Nerv::ieee754_64_t y;

            /*  */
            ::Nerv::ieee754_64_t head;

        };
        typedef template_history_elem<DRIVING_DATA> DRIVING_DATA_history_elem;
        typedef template_history<DRIVING_DATA> DRIVING_DATA_history;

        /********************************************************************************************************************************************
            data name  : RADIUS
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed RADIUS
        {
            /*  */
            ::Nerv::ieee754_64_t r;

            /*  */
            ::Nerv::char_t status;

            /*  */
            ::Nerv::char_t direction;

        };
        typedef template_history_elem<RADIUS> RADIUS_history_elem;
        typedef template_history<RADIUS> RADIUS_history;

        /********************************************************************************************************************************************
            data name  : FPID_GAIN
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed FPID_GAIN
        {
            /*  */
            ::Nerv::ieee754_64_t fgain;

            /*  */
            ::Nerv::ieee754_64_t pgain;

            /*  */
            ::Nerv::ieee754_64_t igain;

            /*  */
            ::Nerv::ieee754_64_t dgain;

        };
        typedef template_history_elem<FPID_GAIN> FPID_GAIN_history_elem;
        typedef template_history<FPID_GAIN> FPID_GAIN_history;

        /********************************************************************************************************************************************
            data name  : WAYPOINT_TRACER_OPTION
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed WAYPOINT_TRACER_OPTION
        {
            /*  */
            Lemon_Data_Driving::FPID_GAIN steer_gain;

            /*  */
            Lemon_Data_Driving::FPID_GAIN accel_gain;

        };
        typedef template_history_elem<WAYPOINT_TRACER_OPTION> WAYPOINT_TRACER_OPTION_history_elem;
        typedef template_history<WAYPOINT_TRACER_OPTION> WAYPOINT_TRACER_OPTION_history;

        /********************************************************************************************************************************************
            data name  : WAYPOINT_TRACER_PID_STATUS
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed WAYPOINT_TRACER_PID_STATUS
        {
            /*  */
            ::Nerv::ieee754_64_t err_tm_0;

            /*  */
            ::Nerv::ieee754_64_t err_tm_1;

            /*  */
            ::Nerv::ieee754_64_t sum_err;

        };
        typedef template_history_elem<WAYPOINT_TRACER_PID_STATUS> WAYPOINT_TRACER_PID_STATUS_history_elem;
        typedef template_history<WAYPOINT_TRACER_PID_STATUS> WAYPOINT_TRACER_PID_STATUS_history;

        /********************************************************************************************************************************************
            data name  : WAYPOINT_TRACER_DEBUG
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed WAYPOINT_TRACER_DEBUG
        {
            /*  */
            Lemon_Data_Driving::struct_waypoint wp_command;

            /*  */
            Lemon_Data_Navigation::struct_navigation_v1 nav_feedback;

            /*  */
            Lemon_Data_Driving::struct_wrench_effort we_feedback;

            /*  */
            Lemon_Data_Driving::WAYPOINT_TRACER_PID_STATUS steer_pid;

            /*  */
            Lemon_Data_Driving::WAYPOINT_TRACER_PID_STATUS accel_pid;

            /*  */
            Lemon_Data_Driving::struct_wrench_effort we_output;

            /*  */
            Lemon_Data_Driving::DRIVING_DATA robot;

            /*  */
            Lemon_Data_Driving::DRIVING_DATA center;

            /*  */
            Lemon_Data_Driving::RADIUS radius;

        };
        typedef template_history_elem<WAYPOINT_TRACER_DEBUG> WAYPOINT_TRACER_DEBUG_history_elem;
        typedef template_history<WAYPOINT_TRACER_DEBUG> WAYPOINT_TRACER_DEBUG_history;

        /********************************************************************************************************************************************
            data name  : PATH_TRACER_DEBUG
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed PATH_TRACER_DEBUG
        {
            /*  */
            ::Nerv::u_int32_t waypoint_index;

            /*  */
            Lemon_Data_Navigation::struct_navigation_v1 nav_feedback;

            /*  */
            ::Nerv::ieee754_64_t look_distance;

            /*  */
            ::Nerv::ieee754_64_t turning_radius;

            /*  */
            Lemon_Data_Driving::DRIVING_DATA robot;

            /*  */
            Lemon_Data_Driving::DRIVING_DATA waypoint;

            /*  */
            Lemon_Data_Driving::DRIVING_DATA center;

        };
        typedef template_history_elem<PATH_TRACER_DEBUG> PATH_TRACER_DEBUG_history_elem;
        typedef template_history<PATH_TRACER_DEBUG> PATH_TRACER_DEBUG_history;

        /********************************************************************************************************************************************
            data name  : struct_steering_data
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_steering_data
        {
            /*  */
            ::Nerv::char_t steering_data[181];

        };
        typedef template_history_elem<struct_steering_data> struct_steering_data_history_elem;
        typedef template_history<struct_steering_data> struct_steering_data_history;

        /********************************************************************************************************************************************
            data name  : struct_estimate_debug
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_estimate_debug
        {
            /*  */
            ::Nerv::ieee754_64_t first_candidature_radius;

            /*  */
            ::Nerv::ieee754_64_t second_candidature_radius;

            /*  */
            Lemon_Data_Estimator::struct_obstacle_set obstacle_set;

            /*  */
            Lemon_Data_Driving::struct_linear_estimate_ca cmd_radius;

        };
        typedef template_history_elem<struct_estimate_debug> struct_estimate_debug_history_elem;
        typedef template_history<struct_estimate_debug> struct_estimate_debug_history;

        /********************************************************************************************************************************************
            data name  : enum_obstacle_state
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_obstacle_state;
        const enum_obstacle_state c_obstacle = 1; // description:
        const enum_obstacle_state c_obstacle_clear = 0; // description:
        typedef template_history_elem<enum_obstacle_state> enum_obstacle_state_history_elem;
        typedef template_history<enum_obstacle_state> enum_obstacle_state_history;

        /********************************************************************************************************************************************
            data name  : struct_collision
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_collision
        {
            /*  */
            Lemon_Data_Estimator::struct_obstacle obstacle;

            /*  */
            Lemon_Data_Driving::enum_obstacle_state b_obstacle;

        };
        typedef template_history_elem<struct_collision> struct_collision_history_elem;
        typedef template_history<struct_collision> struct_collision_history;

        /********************************************************************************************************************************************
            data name  : struct_point
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_point
        {
            /*  */
            ::Nerv::ieee754_64_t x;

            /*  */
            ::Nerv::ieee754_64_t y;

        };
        typedef template_history_elem<struct_point> struct_point_history_elem;
        typedef template_history<struct_point> struct_point_history;

        /********************************************************************************************************************************************
            data name  : struct_ptca_debug
            writer     : root
            description: 
        ********************************************************************************************************************************************/
        struct packed struct_ptca_debug
        {
            /*  */
            ::Nerv::ieee754_64_t look_distance;

            /*  */
            ::Nerv::ieee754_64_t delete_field;

            /*  */
            Lemon_Data_Driving::struct_point select_point;

            /*  */
            Lemon_Data_Navigation::struct_navigation_v1 navigation;

			/*  */
            variable_array_field_dctrl_t modified_path_dctrl;
            Lemon_Data_Driving::struct_waypoint* modified_path() { return (Lemon_Data_Driving::struct_waypoint*)(dynamicbuffer+modified_path_dctrl.dindex); }
            bool modified_path_alloc(unsigned int array_size)
            {
                modified_path_dctrl.dindex = dynamicsize;
                modified_path_dctrl.dsize = array_size*sizeof(Lemon_Data_Driving::struct_waypoint);
                if(_maxDynamicSize < dynamicsize + modified_path_dctrl.dsize) return false;
                dynamicsize += modified_path_dctrl.dsize;
                return true;
            }

            /*  */
            variable_array_field_dctrl_t command_path_dctrl;
            Lemon_Data_Driving::struct_waypoint* command_path() { return (Lemon_Data_Driving::struct_waypoint*)(dynamicbuffer+command_path_dctrl.dindex); }
            bool command_path_alloc(unsigned int array_size)
            {
                command_path_dctrl.dindex = dynamicsize;
                command_path_dctrl.dsize = array_size*sizeof(Lemon_Data_Driving::struct_waypoint);
                if(_maxDynamicSize < dynamicsize + command_path_dctrl.dsize) return false;
                dynamicsize += command_path_dctrl.dsize;
                return true;
            }

            /*  */
            variable_array_field_dctrl_t obstacle_dctrl;
            Lemon_Data_Estimator::struct_obstacle* obstacle() { return (Lemon_Data_Estimator::struct_obstacle*)(dynamicbuffer+obstacle_dctrl.dindex); }
            bool obstacle_alloc(unsigned int array_size)
            {
                obstacle_dctrl.dindex = dynamicsize;
                obstacle_dctrl.dsize = array_size*sizeof(Lemon_Data_Estimator::struct_obstacle);
                if(_maxDynamicSize < dynamicsize + obstacle_dctrl.dsize) return false;
                dynamicsize += obstacle_dctrl.dsize;
                return true;
            }

            /* 가변 데이터 버퍼 */
            enum {_maxDynamicSize=0+sizeof(Lemon_Data_Driving::struct_waypoint)*10000+sizeof(Lemon_Data_Driving::struct_waypoint)*10000+sizeof(Lemon_Data_Estimator::struct_obstacle)*300};
            void init_daynamic_buffer() { dynamicsize = 0; }
            ::Nerv::pksize_t get_sizeof() { return sizeof(struct_ptca_debug)- _maxDynamicSize + dynamicsize; }
            ::Nerv::pksize_t dynamicsize;
            ::Nerv::u_int8_t dynamicbuffer[_maxDynamicSize];    // dynamic buffer
        };
        typedef template_history_elem<struct_ptca_debug> struct_ptca_debug_history_elem;
        typedef template_history<struct_ptca_debug> struct_ptca_debug_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Driving__Lemon_Data_Driving_h
