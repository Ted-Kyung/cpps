/****************************************************************************************************************************************************

    Document Header

    domain               : Lemon_Data_Image
    creator              : Lemonade
    creator information  : 
    standardport         : 0
    description          : 
                                                                                                  by Lemonade, at Thu Aug 21 23:17:29 2008

****************************************************************************************************************************************************/


/* Prevent Duplicated Including ********************************************************************************************************************/
#ifndef Define__Lemon_Data_Image__Lemon_Data_Image_h
#define Define__Lemon_Data_Image__Lemon_Data_Image_h


/* Include Nerv Sdk Document ***********************************************************************************************************************/
#include <nerv/Nerv.h>


/* Include Parent Domains Cocument *****************************************************************************************************************/


/* Domain Define ***********************************************************************************************************************************/
namespace Lemon_Data_Image
{


    /* Standard Domain Port Constance **************************************************************************************************************/

        const ::Nerv::domain_t c_standard_port = 0;


    /* Data Defines ********************************************************************************************************************************/
    #pragma pack(1)
    #pragma warning(disable: 4200)

        /********************************************************************************************************************************************
            data name  : enum_image_format_v1
            writer     : Lemonade
            description: 
        ********************************************************************************************************************************************/
        typedef ::Nerv::enum8_t enum_image_format_v1;
        const enum_image_format_v1 c_image_format_null = 0; // description:
        const enum_image_format_v1 c_image_format_bitmap = 1; // description:
        const enum_image_format_v1 c_image_format_jpg = 2; // description:
        const enum_image_format_v1 c_image_format_png = 3; // description:
        const enum_image_format_v1 c_image_format_tiff = 4; // description:
        typedef template_history_elem<enum_image_format_v1> enum_image_format_v1_history_elem;
        typedef template_history<enum_image_format_v1> enum_image_format_v1_history;

    #pragma warning(default : 4200)
    #pragma pack()


    /* Method Defines ******************************************************************************************************************************/


    /* Interface Defines ***************************************************************************************************************************/


}


#endif // #ifdef Define__Lemon_Data_Image__Lemon_Data_Image_h
