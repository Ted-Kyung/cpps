#pragma once
#include <stdio.h>
#include <time.h>

class CRandom
{
public:
	CRandom(void);
	~CRandom(void);

	double GetProbability(void);
	double GetProbability(int Range);
};

