
// DNA_AlgmDlg.h : 헤더 파일
//

#pragma once
#include "DNA_Algorizm.h"

// CDNA_AlgmDlg 대화 상자
class CDNA_AlgmDlg : public CDialogEx
{
// 생성입니다.
public:
	CDNA_AlgmDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DNA_ALGM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CDNA_Algorizm* mc_dna;
public:
	afx_msg void OnBnClickedBtnTest();
};
