#include "StdAfx.h"
#include "Random.h"


CRandom::CRandom(void)
{
	srand((unsigned int)time(NULL));
}


CRandom::~CRandom(void)
{
}

double CRandom::GetProbability(void)
{
	double returnProbability  = 0.0;
	returnProbability = rand() % 100;
	return returnProbability/100;
}
double CRandom::GetProbability(int Range)
{
	double returnProbability  = 0.0;
	returnProbability = rand() % Range +1;
	return returnProbability;
}