#pragma once
#include "Random.h"

struct DNA
{
	char	autosomalgene;
	double	fitness;
	double	fitnessPercentage;
};

class CDNA_Algorizm
{
public:
	CDNA_Algorizm(void);
	~CDNA_Algorizm(void);

	CRandom* mc_random;

	int m_SolutionGroupCount;
	DNA* m_SolutionGroup;
	DNA* m_NextGeneration_SolutionGroup;
	double m_Totalfitness;

	double CrossOverPercentage;
	double MutationPercentage;
	int ValueArea;

	void SetMakeSolutionGroup(int N);
	void NextGeneration(void);
private:
	void MakeSolutionValue();
	void MakeFitness();
	double Solution(double x);
	int DefineRoulette(double Roulette);
	void CrossOver(int x, int y, int cnt);
	void Mutation(int cnt);
};

