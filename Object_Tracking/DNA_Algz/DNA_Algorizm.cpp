#include "StdAfx.h"
#include "DNA_Algorizm.h"


CDNA_Algorizm::CDNA_Algorizm(void)
{
	mc_random = new CRandom;

	m_SolutionGroupCount = 0;
	m_SolutionGroup = NULL;
	m_NextGeneration_SolutionGroup = NULL;
	CrossOverPercentage = 70.0;
	MutationPercentage = 0.1;
	ValueArea = 63;
}

CDNA_Algorizm::~CDNA_Algorizm(void)
{
	if(mc_random) delete mc_random;  
	m_SolutionGroupCount = 0;
	if(m_SolutionGroup) delete  m_SolutionGroup;
	if(m_NextGeneration_SolutionGroup) delete  m_NextGeneration_SolutionGroup;
}
void CDNA_Algorizm::SetMakeSolutionGroup(int N)
{
	m_SolutionGroupCount = N;
	m_SolutionGroup = new DNA[N];
	m_NextGeneration_SolutionGroup = new DNA[N];
	MakeSolutionValue();
}
void CDNA_Algorizm::MakeSolutionValue()
{
	printf("\n");
	for(int i = 0 ; i< m_SolutionGroupCount ; i++)
	{
		m_SolutionGroup[i].autosomalgene = (int)(mc_random->GetProbability(ValueArea));
	}
}

void CDNA_Algorizm::MakeFitness(void)
{
	m_Totalfitness = 0.0;

	for(int i = 0 ; i< m_SolutionGroupCount ; i++)
	{
		m_SolutionGroup[i].fitness = Solution((double)m_SolutionGroup[i].autosomalgene);
		m_Totalfitness += m_SolutionGroup[i].fitness;
	}

	for(int i = 0 ; i< m_SolutionGroupCount ; i++)
	{
		m_SolutionGroup[i].fitnessPercentage = m_SolutionGroup[i].fitness / m_Totalfitness * 100.0;
		printf("[%d]:%d\t%.1lf\t%.1lf\n"
			,i
			,m_SolutionGroup[i].autosomalgene
			,m_SolutionGroup[i].fitness
			,m_SolutionGroup[i].fitnessPercentage);
	}
}

double CDNA_Algorizm::Solution(double x)
{
	return (double)ValueArea*x - (x*x);
}

void CDNA_Algorizm::NextGeneration(void)
{
	int firstGene = 0, secondGene = 0;
	double buf = 0.0;
	MakeFitness();

	// 교차할 두 염색체 선정 
	for(int i = 0 ; i < m_SolutionGroupCount ; i+=2)
	{
		buf = mc_random->GetProbability(99);
		firstGene = DefineRoulette(buf);
		while (1)
		{
			buf = mc_random->GetProbability(99);
			secondGene = DefineRoulette(buf);
			if(firstGene != secondGene)break;
		}
		//printf("%d %d \n",firstGene,secondGene);

		//교차 유무 판정
		CrossOver(firstGene,secondGene,i);
	}	
	for(int i = 0 ; i < m_SolutionGroupCount ; i++)
	{
		//변이 유무 판정
		Mutation(i);
		m_SolutionGroup[i].autosomalgene = m_NextGeneration_SolutionGroup[i].autosomalgene;
		m_SolutionGroup[i].fitness = 0.0;
		m_SolutionGroup[i].fitnessPercentage = 0.0;
	}

}
int CDNA_Algorizm::DefineRoulette(double Roulette)
{
	double Area = 0.0;
	for(int i = 0 ; i < m_SolutionGroupCount; i++)
	{ 
		if(i == 0)
		{
			if ((Roulette >= 0)&&(Roulette <= m_SolutionGroup[i].fitnessPercentage))
			{
				return i;
			}
			Area = Area + m_SolutionGroup[i].fitnessPercentage;
		}
		else
		{
			if ((Roulette >= Area)&&(Roulette <= Area + m_SolutionGroup[i].fitnessPercentage))
			{
				return i;
			}
			Area = Area + m_SolutionGroup[i].fitnessPercentage;
		}
	}
}
void CDNA_Algorizm::CrossOver(int x, int y, int cnt)
{
	int bufx = 0 , bufy = 0;
	if( mc_random->GetProbability(100) > CrossOverPercentage )
	{
		m_NextGeneration_SolutionGroup[cnt].autosomalgene = m_SolutionGroup[x].autosomalgene;
		m_NextGeneration_SolutionGroup[cnt+1].autosomalgene = m_SolutionGroup[y].autosomalgene;
	}
	else
	{
		int CrossOverValue = (int)(mc_random->GetProbability(63));

		int _x = m_SolutionGroup[x].autosomalgene & CrossOverValue;
		int _y = m_SolutionGroup[y].autosomalgene & CrossOverValue;

		bufx = m_SolutionGroup[x].autosomalgene - _x;
		bufy = m_SolutionGroup[y].autosomalgene - _y;

		bufx = m_SolutionGroup[x].autosomalgene | _y;
		bufy = m_SolutionGroup[y].autosomalgene | _x;

		m_NextGeneration_SolutionGroup[cnt].autosomalgene = bufx;
		m_NextGeneration_SolutionGroup[cnt+1].autosomalgene = bufy;
	}
}
void CDNA_Algorizm::Mutation(int cnt)
{	
	double MutationPercentageValue = mc_random->GetProbability(10000)/100;

	if( MutationPercentageValue < MutationPercentage )
	{
		int MutationValue = 1;
		int shift = mc_random->GetProbability(5);
		int buf = 0;
		MutationValue = MutationValue << shift;
		buf = m_NextGeneration_SolutionGroup[cnt].autosomalgene & MutationValue;
		
		if(buf == 0)
		{
			m_NextGeneration_SolutionGroup[cnt].autosomalgene = m_NextGeneration_SolutionGroup[cnt].autosomalgene + MutationValue;
		}
		else
		{
			m_NextGeneration_SolutionGroup[cnt].autosomalgene = m_NextGeneration_SolutionGroup[cnt].autosomalgene - MutationValue;
		}	
	}
}