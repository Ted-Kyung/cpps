// Virture_LRFDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "Fix_object.h"
#include "Moving_object.h"
#include "resource.h"
//#include "Stub_Virtual_LRF.h"


// CVirture_LRFDlg 대화 상자
class CVirture_LRFDlg : public CDialog , public FixObjectInterface , public MoveObjectInterface/* , public GridmanagerInterface*/
{
// 생성입니다.
public:
	CVirture_LRFDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIRTURE_LRF_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public: 
	//Stub_Virtual_LRF* nervlrf;

public: 
	CDC*			m_imageDC;			// 그릴화면 DC
	CDC				m_mDC;				// 더블 버퍼링 -> 메모리 DC
	CBitmap			m_bitmap;			// 비트맵 -> 더블 버퍼링
	BITMAP			m_bit_info;			// 비트맵 정보 받아올 변수 
	CRect			m_imageRect;		// 그릴화면 DC 의 크기저장
	CStatic			image;				// 그릴 IDC_IMAGE 변수 
	CPoint			m_pline;			// 라인을 그리기 위해서
	CPoint			m_pCenter;			// 화면의 중심점
	COLORREF		m_color;			// 픽셀의 컬러값.

	double			m_resolution;		// 배율을 위한 변수 

	CPen   pen;

	LARGE_INTEGER	mt_buf;				// Hz 검사를 위한 변수 mt_xx 
	double			mt_res;
	double			mt_t;

	unsigned char*	lpBits;				// 장애물 검사를 위한 변수 

	unsigned int	m_count;			// 화면 60Hz 이하로 낮출 변수 
	double*			vLRF_distance;

	CPoint*			m_pfixobject;			// Fix Object  
	CPoint*			m_pMovingobject;	// Moving Object  

	Moving_object*	m_object;			// Fix Object manager Class
	Fix_object*		f_object;			// Moving Object manager Class

	int			m_fixobjectCheck;
	int			m_MoveobjectCheck;
	int			m_controler;
	BOOL		cc;
	BOOL		cc1;

//	GridmanagerInterface* m_pGridOwer;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

	void object_create(CPoint* fix_point,CPoint* move_point);
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

public:
	virtual void FixObject_manager(vector<CPoint> fpoint);
	virtual void MoveObject_manager(vector<CPoint> mpoint);
	virtual void Object_Movemanager(vector<CPoint> mpoint);

};
