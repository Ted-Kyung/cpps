#include "StdAfx.h"
#include "Stub_Virtual_LRF.h"

Stub_Virtual_LRF::Stub_Virtual_LRF(double width)
{
	m_width = (int)width;
	nerv_err_t err = this->create(JH_Virtual_LRF::c_standard_port,30,1);
	printf("nerv object create !! => %s\n",get_err_str(err));	
}

Stub_Virtual_LRF::~Stub_Virtual_LRF(void)
{
}

nerv_err_t Stub_Virtual_LRF::on_scan_data_connect( const client_addr_t& in_client_addr,
										const bool& in_tcp_request,
										const double& in_elapsed_time,
										const double& in_limit_time )
{
	return Nerv::c_nerv_err_none;
}

void Stub_Virtual_LRF::on_scan_data_shutdown( const client_addr_t& in_client_addr)
{

}

void Stub_Virtual_LRF::lrfDataSend(double* Data)
{

	lrfdata = new struct_lrf_data; 

	lrfdata->init_daynamic_buffer();
	lrfdata->lrf_data_alloc(sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER) + sizeof(unsigned short)*m_width);
	bitmapfileheader = (BITMAPFILEHEADER*)lrfdata->lrf_data();
	bitmapinfoheader = (BITMAPINFOHEADER*)(bitmapfileheader +1 );
	range = (unsigned short*)(bitmapinfoheader + 1 );

	bitmapinfoheader->biWidth = (unsigned int)m_width;
	bitmapinfoheader->biHeight = 1;
	bitmapinfoheader->biBitCount = 16;


	for (int lrf=0;lrf<=m_width;lrf++)
	{
		range[lrf] =(unsigned short)Data[lrf];											// 데이터 저장 
	}
	this->scan_data_write(lrfdata);

	delete lrfdata;
}