#include "StdAfx.h"
#include "Fix_object.h"

Fix_object::Fix_object(FixObjectInterface* pOwer)
{
	m_pOwer = pOwer;
}

Fix_object::~Fix_object(void)
{
	if(!ob_vector.empty())
	{
		ob_vector.clear();
	}
}

void Fix_object::add_fix_object(CPoint point)
{
	ob_vector.push_back(point);
	m_pOwer->FixObject_manager(ob_vector);
}

void Fix_object::clearAll(void)
{
	ob_vector.clear();
	m_pOwer->FixObject_manager(ob_vector);
}