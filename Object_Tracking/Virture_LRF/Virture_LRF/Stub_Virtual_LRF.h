#pragma once
#include <JH_Virtual_LRF.h>

using namespace JH_Virtual_LRF;

class Stub_Virtual_LRF : public JH_Virtual_LRF::method_scan_data
{
public:
	Stub_Virtual_LRF(double width);
	~Stub_Virtual_LRF(void);


public:
	struct_lrf_data*	lrfdata;
	BITMAPFILEHEADER*	bitmapfileheader;
	BITMAPINFOHEADER*	bitmapinfoheader;
	unsigned short*		range;

	int m_width;
	

	virtual nerv_err_t on_scan_data_connect( const client_addr_t& in_client_addr,
		const bool& in_tcp_request,
		const double& in_elapsed_time,
		const double& in_limit_time );

	virtual void on_scan_data_shutdown( const client_addr_t& in_client_addr);

public:
	void lrfDataSend(double* Data);
};
