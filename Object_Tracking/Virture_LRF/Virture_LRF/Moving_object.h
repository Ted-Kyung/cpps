#pragma once
#include <stdio.h>
#include <vector>
using namespace std;

#define OBJECT_SIZE 30

class MoveObjectInterface
{
public:
	virtual void MoveObject_manager(vector<CPoint> mpoint) = 0;
	virtual void Object_Movemanager(vector<CPoint> mpoint) = 0;
};

class Moving_object
{
public:
	Moving_object(MoveObjectInterface* pOwer, CRect imageRect);
	~Moving_object(void);

public:
	MoveObjectInterface*	m_pOwer;
	CRect					m_imageRect;	// window DC rect
	vector<int>						mx;				// 이동성 장애물 이동포인트 x 
	vector<int>						my;				// 이동성 장애물 이동포인트 y 
	vector<CPoint>			ob_vector;

public:
	void add_move_object(CPoint point);
	vector<CPoint> moving(vector<CPoint> move_point);
	void moving(void);
	void clearAll(void);

};
