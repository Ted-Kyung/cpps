#pragma once
#include <stdio.h>
#include <vector>
using namespace std;

class FixObjectInterface
{
public:
	virtual void FixObject_manager(vector<CPoint> fpoint) = 0;
};

class Fix_object
{
public:
	Fix_object(FixObjectInterface* pOwer);
	~Fix_object(void);

	
	
protected:
	FixObjectInterface*		m_pOwer;
	vector<CPoint>			ob_vector;

public:
	
	void add_fix_object(CPoint point);
	void clearAll(void);

};
