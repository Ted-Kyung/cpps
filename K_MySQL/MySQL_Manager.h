#pragma once

#include <stdio.h>
#include <winsock2.h>
#include <mysql.h>
#include <stdlib.h>

#pragma comment(lib, "libmysql.lib")
#pragma comment(lib, "ws2_32.lib")

const HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
inline int _drgb ( bool d, bool r, bool g, bool b){	return (d << 3) + (r << 2) + (g << 1) + (b << 0);}
inline int _clr ( int fc, int bc ){	return (fc << 0) + (bc << 4); }
inline void consoleColor(bool r, bool g, bool b) 
{
	int fore = _drgb (true,r,g,b);		
	int back = _drgb (0,0,0,0);
	int color = _clr (fore,back);
	SetConsoleTextAttribute(h,color);	
}
#define C_RED consoleColor(1,0,0);
#define C_BLUE consoleColor(0,0,1);
#define C_YELLOW consoleColor(1,1,0);
#define C_WHITE consoleColor(1,1,1);
#define C_GREEN consoleColor(0,1,0);
#define C_PUFFLE consoleColor(1,0,1);

class CallBack_MySQL_GetDB
{
public:
	virtual void on_MySQLGetDB(char* data) = 0;
};

class CallBackMySQL_interface : public CallBack_MySQL_GetDB
{
	public:
};
class MySQL_Manager
{
public:
	//No use CallBack
	MySQL_Manager(void);

	//if you want callback..
	//inheritance CallBack_MySQL_GetDB class .. 
	MySQL_Manager(CallBack_MySQL_GetDB* pcallback);
	//if you want callback..
	//inheritance CallBackMySQL_interface class .. 
	MySQL_Manager(CallBackMySQL_interface* pcallback);
	~MySQL_Manager(void);
	
	CallBackMySQL_interface*	mp_CallbackInterface;

	//mysql ���� ���� 
	MYSQL	m_mysql;
	MYSQL*	m_mysqlERR;

	char m_using_db_query[256];
	char m_query_Str[256];

	//IP Default "localhost" portNum Default 3306
	//return 1 is success
	bool MySQL_Connect(char* ID , char* passWd, char* dataBaseName);

	//IP Default "localhost"
	//return 1 is success
	bool MySQL_Connect(char* ID , char* passWd, char* dataBaseName,int portNum);

	//User define
	//return 1 is success
	bool MySQL_Connect(char* IP , char* ID, char* passWd, char* dataBaseName,int portNum);

	//MySql close
	void MySQL_DisConnect();

	//Select name element Load 
	//if you inheritance CallBack_MySQL_GetDB class .. 
	void GetMySQL_DBdata(char* TableName, char* whereName, char* whereValue);

	//Select name element Load 
	//if you inheritance CallBack_MySQL_GetDB class .. 
	void GetMySQL_DBdata(char* TableName, char* whereName, char* whereValue, int index);

	//Select name element Load 
	//if you inheritance CallBack_MySQL_GetDB class .. 
	void GetMySQL_RowAllDBdata(char* TableName,int RowNumber);

	//Select name element Load 
	//if you inheritance CallBack_MySQL_GetDB class .. 
	void GetMySQL_AllDBdata(char* TableName);

	//Modify DB data 
	void SetModify_MySQLdata(char* TableName ,char* columName, char* whereName, char* whereValue, char* Value);
	
	//delete DB data
	void SetDelete_MySQLdata(char* TableName, char* whereName, char* whereValue);
	//delete DB Alldata
	void SetDelete_AllMySQLdata(char* TableName);

	//insert DB data
	//plz input HeadLine variableName1, variableName2,......
	//data input caution -> string required 'string'
	void SetInsert_MySQLdata(char* TableName, char* HeadLine, char* data);

	//Create Table
	//HeadLine colummName1 dataform1(int & char(x) & double) , colummName2 dataform2(int & char(x) & double)
	void SetCreate_MySQLTable(char* TableName, char* HeadLine);
};

