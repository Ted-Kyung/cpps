#include "MySQL_Manager.h"

MySQL_Manager::MySQL_Manager(void)
{
	memset((char*)&m_mysql,NULL,sizeof(m_mysql));
	m_mysqlERR = NULL;
	C_YELLOW;printf("MySQL_Manager create \n");	C_WHITE;
}
MySQL_Manager::MySQL_Manager(CallBack_MySQL_GetDB* pcallback)
{
	memset((char*)&m_mysql,NULL,sizeof(m_mysql));
	mp_CallbackInterface = (CallBackMySQL_interface*)pcallback;
	m_mysqlERR = NULL;
	C_YELLOW;printf("MySQL_Manager create \n");	C_WHITE;
}
MySQL_Manager::MySQL_Manager(CallBackMySQL_interface* pcallback)
{
	memset((char*)&m_mysql,NULL,sizeof(m_mysql));
	mp_CallbackInterface = (CallBackMySQL_interface*)pcallback;
	m_mysqlERR = NULL;
	C_YELLOW;printf("MySQL_Manager create \n");	C_WHITE;
}

MySQL_Manager::~MySQL_Manager(void)
{
	if(m_mysql.user) mysql_close(&m_mysql);
	C_RED;printf("~MySQL_Manager destroy \n");C_WHITE;
}

bool MySQL_Manager::MySQL_Connect(char* ID , char* passWd, char* dataBaseName)
{
	return MySQL_Connect("localhost", ID, passWd, dataBaseName, 3306);
}
bool MySQL_Manager::MySQL_Connect(char* ID , char* passWd, char* dataBaseName,int portNum)
{
	return MySQL_Connect("localhost", ID, passWd, dataBaseName, portNum);
}
bool MySQL_Manager::MySQL_Connect(char* IP, char* ID , char* passWd , char* dataBaseName,int portNum)
{
	sprintf(m_using_db_query,"use %s;",dataBaseName);

	// mysql 변수 초기화 
	mysql_init(&m_mysql);
	C_YELLOW;
	printf("mysql_init connect ID = %s\n",ID);
	printf("mysql_init connect PW = %s\n",passWd);
	C_WHITE;

	m_mysql.options.unix_socket		= NULL;
	m_mysql.options.port			= 0;
	m_mysql.options.compress		= 0;
	m_mysql.options.connect_timeout	= 30;

	m_mysqlERR = mysql_real_connect(&m_mysql, IP,ID,passWd,dataBaseName,portNum,0,0);

	if (m_mysqlERR == NULL)
	{
		C_RED;printf("Step [connection] : connection Err Code is => %s \n",mysql_error(&m_mysql));C_WHITE;
		return false;
	}
	else
	{
		C_GREEN;printf("Step [connection] : connection Success Code is => err_none \n",mysql_error(&m_mysql));C_WHITE;
		return true;
	}
}

void MySQL_Manager::MySQL_DisConnect()
{
	if(m_mysql.user) mysql_close(&m_mysql);
}
 
void MySQL_Manager::GetMySQL_AllDBdata(char* TableName)
{
	sprintf(m_query_Str,"select * from %s;",TableName);
	if(mysql_real_query(&m_mysql,m_using_db_query,strlen(m_using_db_query)) !=0)
	{
		C_RED;printf("Table Select Fail...\n");C_WHITE;
	}
	else
	{
		C_GREEN;printf("Table Select Success\n");C_WHITE;

		char char_query[512];
		strcpy(&char_query[0], &m_query_Str[0]);
		if(mysql_real_query(&m_mysql,char_query,strlen(m_query_Str)) != 0)
		{
			C_RED;printf("Step [mysql_real_query] :Code is => %s\n",mysql_error(&m_mysql));C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");C_WHITE;

			MYSQL_RES* resultRes = mysql_store_result(&m_mysql);
			if(resultRes == NULL)
			{
				C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
			}
			else
			{
				C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;

				mysql_next_result(&m_mysql);

				MYSQL_ROW resultRow;
				char buf[2048];				
				while(resultRow = mysql_fetch_row(resultRes))
				{		
					memset(buf,NULL,sizeof(buf));
					int nCnt = mysql_num_fields(resultRes);
					for(int i = 0 ; i < nCnt ; i++)
					{
						strcat(buf,resultRow[i]);
						strcat(buf,",");
					}
					mp_CallbackInterface->on_MySQLGetDB(buf);
				}		
				C_PUFFLE;printf("infomation Road Complate\n");C_WHITE;
				mysql_free_result(resultRes);
			}
		}
	}
}
void MySQL_Manager::GetMySQL_RowAllDBdata(char* TableName,int RowNumber)
{
	sprintf(m_query_Str,"select * from %s;",TableName);
	if(mysql_real_query(&m_mysql,m_using_db_query,strlen(m_using_db_query)) !=0)
	{
		C_RED;printf("DB Select Fail...\n");C_WHITE;
	}
	else
	{
		C_GREEN;printf("DB Select Success\n");C_WHITE;

		char char_query[512];
		strcpy(&char_query[0], &m_query_Str[0]);
		if(mysql_real_query(&m_mysql,char_query,strlen(m_query_Str)) != 0)
		{
			C_RED;printf("Step [mysql_real_query] :Code is => %s\n",mysql_error(&m_mysql));C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");C_WHITE;

			MYSQL_RES* resultRes = mysql_store_result(&m_mysql);
			if(resultRes == NULL)
			{
				C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
			}
			else
			{
				C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;

				mysql_next_result(&m_mysql);

				MYSQL_ROW resultRow;
				while(resultRow = mysql_fetch_row(resultRes))
				{		
					mp_CallbackInterface->on_MySQLGetDB(resultRow[RowNumber]);
				}		
				C_PUFFLE;printf("infomation Road Complate\n");C_WHITE;
				mysql_free_result(resultRes);
			}
		}
	}
}

void MySQL_Manager::SetModify_MySQLdata(char* TableName ,char* columName, char* whereName, char* whereValue,char* Value)
{
	char query_str[512]; 
	sprintf(query_str,"update %s set %s='%s' where %s=%s;",TableName,columName,Value,whereName,whereValue);
	printf("%s\n",query_str);
	mysql_real_query(&m_mysql,query_str,strlen(query_str));
}
void MySQL_Manager::SetDelete_MySQLdata(char* TableName, char* whereName, char* whereValue)
{
	char query_str[512]; 
	sprintf(query_str,"delete from %s where %s=%s;",TableName,whereName,whereValue);
	printf("%s\n",query_str);
	mysql_real_query(&m_mysql,query_str,strlen(query_str));
}

void MySQL_Manager::SetDelete_AllMySQLdata(char* TableName)
{
	char query_str[512]; 
	sprintf(query_str,"delete from %s;",TableName);
	printf("%s\n",query_str);
	mysql_real_query(&m_mysql,query_str,strlen(query_str));
}
void MySQL_Manager::SetInsert_MySQLdata(char* TableName,char* HeadLine,char* data)
{
	char query_str[512]; 
	sprintf(query_str,"Insert into %s (%s) values (%s);",TableName,HeadLine,data);
	printf("%s",query_str);
	mysql_real_query(&m_mysql,query_str,strlen(query_str));
}
void MySQL_Manager::SetCreate_MySQLTable(char* TableName, char* HeadLine)
{
	char query_str[512]; 
	sprintf(query_str,"create table %s(%s);",TableName,HeadLine);
	printf("%s\n",query_str);
	mysql_real_query(&m_mysql,query_str,strlen(query_str));
}
void MySQL_Manager::GetMySQL_DBdata(char* TableName, char* whereName, char* whereValue)
{
	char query_str[512]; 
	sprintf(query_str,"select * from %s where %s=%s;",TableName,whereName,whereValue);
	printf("%s\n",query_str);
	if(mysql_real_query(&m_mysql,query_str,strlen(query_str)) !=0)
	{
		C_RED;printf("DB Select Fail...\n");C_WHITE;
	}
	else
	{
		C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");C_WHITE;

		MYSQL_RES* resultRes = mysql_store_result(&m_mysql);
		if(resultRes == NULL)
		{
			C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;

			mysql_next_result(&m_mysql);

			MYSQL_ROW resultRow;
			resultRow = mysql_fetch_row(resultRes);

			char buf[2048];
			memset(buf,NULL,sizeof(buf));
			int nCnt = mysql_num_fields(resultRes);
			for(int i = 0 ; i < nCnt ; i++)
			{
				strcat(buf,resultRow[i]);
				strcat(buf,",");
			}
			mp_CallbackInterface->on_MySQLGetDB(buf);

			C_PUFFLE;printf("infomation Road Complate\n");C_WHITE;
			mysql_free_result(resultRes);
		}		
	}
}
void MySQL_Manager::GetMySQL_DBdata(char* TableName, char* whereName, char* whereValue, int index)
{
	char query_str[512]; 
	sprintf(query_str,"select * from %s where %s=%s;",TableName,whereName,whereValue);
	printf("%s\n",query_str);
	if(mysql_real_query(&m_mysql,query_str,strlen(query_str)) !=0)
	{
		C_RED;printf("DB Select Fail...\n");C_WHITE;
	}
	else
	{
		C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");C_WHITE;

		MYSQL_RES* resultRes = mysql_store_result(&m_mysql);
		if(resultRes == NULL)
		{
			C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;

			mysql_next_result(&m_mysql);

			MYSQL_ROW resultRow;
			resultRow = mysql_fetch_row(resultRes);
			mp_CallbackInterface->on_MySQLGetDB(resultRow[index]);

			C_PUFFLE;printf("infomation Road Complate\n");C_WHITE;
			mysql_free_result(resultRes);
		}		
	}
}