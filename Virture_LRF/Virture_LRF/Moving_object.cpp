#include "StdAfx.h"
#include "Moving_object.h"
#include <time.h>
#include <stdlib.h>

Moving_object::Moving_object(MoveObjectInterface* pOwer, CRect imageRect)
{
	m_pOwer = pOwer;
	m_imageRect = imageRect;	
}

Moving_object::~Moving_object(void)
{
	if(!ob_vector.empty())
	{
		ob_vector.clear();
	}
}

void Moving_object::add_move_object(CPoint point)
{
	ob_vector.push_back(point);
	mx.push_back(5);
	my.push_back(5);

	ob_vector = moving(ob_vector);

	m_pOwer->MoveObject_manager(ob_vector);
}

vector<CPoint> Moving_object::moving(vector<CPoint> move_point)
{
	int random;
	CPoint movp;
	vector<CPoint> vmovp;

	for (int mo = 0; mo < move_point.size() ; mo++)
	{
		if(move_point[mo].x + OBJECT_SIZE > m_imageRect.right)
		{
			mx[mo] = -5;
		}
		if(move_point[mo].y + OBJECT_SIZE > m_imageRect.bottom)
		{
			my[mo] = -5;
		}
		if(move_point[mo].x < m_imageRect.left)
		{
			mx[mo] = 5;
		}
		if(move_point[mo].y < m_imageRect.top)
		{
			my[mo] = 5;
		}

		srand((long)time(NULL));		//난수를 초기화..
		random = rand() % 5;			//1~5사이의 난수를 발생

		if(random%2 == 0)
		{
			random = random * -1;
		}

		movp.x = move_point[mo].x + mx[mo] + random;
		movp.y = move_point[mo].y + my[mo] - random;

		vmovp.push_back(movp);
	}

	return vmovp;
}

void Moving_object::moving(void)
{
	ob_vector = moving(ob_vector);	
	m_pOwer->Object_Movemanager(ob_vector);
}
void Moving_object::clearAll(void)
{
	ob_vector.clear();
	m_pOwer->Object_Movemanager(ob_vector);
}