// Virture_LRFDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "Virture_LRF.h"
#include "Virture_LRFDlg.h"
#include <math.h>
#include <stdlib.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define PI 3.141592
#define DR 180.0 * PI

#define LRF_ANG			300			//240											//virtuer LRF 감지 각도 
#define LRF_DIRECTION	700.			//200											//virtuer LRF 감지 거리 (pixel 당 5cm) 200 => 10m 
#define LRF_RESOLUTION	1.0			//1.5											//virtuer LRF 감지 각도 사잇각 



#pragma comment (lib,"winmm.lib")

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

CVirture_LRFDlg::CVirture_LRFDlg(CWnd* pParent /*=NULL*/)
: CDialog(CVirture_LRFDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVirture_LRFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE, image);
}

BEGIN_MESSAGE_MAP(CVirture_LRFDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_DESTROY()
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

BOOL CVirture_LRFDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);		

	m_controler = 2;
	//nervlrf = new Stub_Virtual_LRF(LRF_ANG);


	m_imageDC = image.GetWindowDC();														// 실제 사용될 DC
	image.GetWindowRect(&m_imageRect);														// 실제 사용될 DC 의 사각형 정보


	m_imageRect.bottom	= m_imageRect.bottom - m_imageRect.top;								// 사각형 정보 재정의
	m_imageRect.right	= m_imageRect.right - m_imageRect.left;								// 사각형 정보 재정의
	m_imageRect.top		= 0;																// 사각형 정보 재정의
	m_imageRect.left	= 0;																// 사각형 정보 재정의

	m_pCenter.x = m_imageRect.right		/ 2;												//중심점 잡기 x
	m_pCenter.y = m_imageRect.bottom	/ 2;												//중심점 잡기 y

	m_pline.x = m_pCenter.x;
	m_pline.y = m_pCenter.y;

	m_mDC.CreateCompatibleDC(m_imageDC);													// 사용되는 DC 와 호환되는 메모리 DC
	m_bitmap.CreateCompatibleBitmap(m_imageDC , m_imageRect.right , m_imageRect.bottom);	// 메모리 DC 와 호환되는 bitmap
	m_bitmap.GetBitmap(&m_bit_info);														// bitmap 정보 받아오기 

	lpBits = (unsigned char*)malloc(m_bit_info.bmWidthBytes * m_bit_info.bmHeight);			// bitmap 의 픽셀 RGB 정보를 받아오기위한 변수 

	m_mDC.SelectObject(&m_bitmap);															// 메모리 DC 가 bitmap에 그릴것을 설정
	m_mDC.PatBlt(m_imageRect.left , m_imageRect.top , m_imageRect.right 
		,m_imageRect.bottom , BLACKNESS);											// bitmap 의 초기 설정 



	m_resolution = 0.4;																		//배율 변수 초기화 

	m_count = 0;
	printf("더블버퍼링 초기 값 완료\n");

	QueryPerformanceFrequency(&mt_buf);
	mt_res = (double)mt_buf.QuadPart;


	vLRF_distance = new double[LRF_ANG + 1 ];

	m_object = new Moving_object(this,m_imageRect);
	f_object = new Fix_object(this);

	m_fixobjectCheck	 = 0;
	m_MoveobjectCheck	 = 0;
	SetTimer(0 , 100 , NULL);
	SetTimer(1 , 80 , NULL);

	printf("타이머 동작시작\n");

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CVirture_LRFDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CVirture_LRFDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CVirture_LRFDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVirture_LRFDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 0)
	{
		int count = 0;																						// 각도 저장	
		object_create(m_pfixobject , m_pMovingobject);															// 장애물 생성

		CPen		m_pen;
		double		ticktimer;
		double		ang = -1.0 * LRF_ANG/2;
		double		p = LRF_DIRECTION;	
		CPoint		bufPoint;

		bufPoint = m_pCenter;

		m_pen.CreatePen(0,1,RGB(0,255,0));
		m_mDC.SelectObject(m_pen);


		while(1)																			// 한라인을 LRF_DIRECTION 픽셀만큼 나눠서 그림
		{																					// LRF_DIRECTION * (360 / LRF_RESOLUTION) 으로 그림 	
			bufPoint = m_pCenter;
			p=LRF_DIRECTION;

			while(p--)
			{
				if(m_imageRect.top		>	bufPoint.y || 
					m_imageRect.left		>	bufPoint.x ||
					m_imageRect.bottom	<	bufPoint.y ||  
					m_imageRect.right	<	bufPoint.x ) break;
				// Memory Bitmap 픽셀 색깔 구별 부분.
				unsigned char R = (unsigned int)lpBits[ (bufPoint.x*4 + (bufPoint.y * m_bit_info.bmWidthBytes) ) + 0];	
				unsigned char G = (unsigned int)lpBits[ (bufPoint.x*4 + (bufPoint.y * m_bit_info.bmWidthBytes) ) + 1];
				unsigned char B = (unsigned int)lpBits[ (bufPoint.x*4 + (bufPoint.y * m_bit_info.bmWidthBytes) ) + 2];			
				if(R  == 100 && G == 100 && B == 100) break;

				
				if (m_controler == 2)
				{
					lpBits[ (m_pline.x*4 + (m_pline.y * m_bit_info.bmWidthBytes) ) + 0] = 0;	
					lpBits[ (m_pline.x*4 + (m_pline.y * m_bit_info.bmWidthBytes) ) + 1] = 255;
					lpBits[ (m_pline.x*4 + (m_pline.y * m_bit_info.bmWidthBytes) ) + 2] = 0;	
				}
				
				if(p<=0.0)break;

				// LRF_DIRECTION 길이만큼 픽셀을 나누어서 선을 그림 LineTo() 사용 
				m_pline.x	=	m_pCenter.x		+ 	((LRF_DIRECTION-(p)) * sin((double)(ang / DR))*m_resolution);
				m_pline.y	=	m_pCenter.y		-	((LRF_DIRECTION-(p)) * cos((double)(ang / DR))*m_resolution);			

				bufPoint = m_pline;													// 이전 포인트를 buf 로 옮김 (중첩하여 그리는걸 방지 ) 
			}
			ang = ang + LRF_RESOLUTION;
			double dis = (double)(((int)LRF_DIRECTION - p));
			vLRF_distance[count] = 	dis;					// 데이터 저장 pixel 당 거리 5cm
			count++;


			if(ang / DR >= (LRF_ANG/2) / DR) break;												//원하는 설정값 만큼만 그리고 정지
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		//for (int z = 0 ; z <= count -1  ; z++)
		//{
		//	printf("\n\r");																		// 저장된 데이터 확인용
		//	printf("%lf\t",(double)vLRF_distance[z]);
		//}
		m_bitmap.SetBitmapBits( m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,lpBits);
		m_imageDC->BitBlt(0,0,m_imageRect.right,m_imageRect.bottom, &m_mDC , 0 , 0 , SRCCOPY);	// Memory Bitmap에 그려진 정보를 실제 DC 에 복사 

		//nervlrf->lrfDataSend(vLRF_distance);

		QueryPerformanceCounter(&mt_buf);														//Hz 측정부분
		ticktimer = ((mt_buf.QuadPart - mt_t)/mt_res);
		printf("data Save = %d ,  Hz = %lf\r",(int)count,(double)1/ticktimer);
		mt_t	= (double)mt_buf.QuadPart;														// Hz 측정 완료 

	}
	if(nIDEvent == 1 && m_MoveobjectCheck>0)
	{
		m_object->moving();
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CVirture_LRFDlg::OnDestroy()
{

	CDialog::OnDestroy();
	free(lpBits);
	delete vLRF_distance;
	delete f_object;
	delete m_object;
	delete m_pfixobject;
	delete m_pMovingobject;

	//delete nervlrf;

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CVirture_LRFDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.


	if(zDelta >= 0)
	{
		m_resolution = m_resolution + 0.1;
		if(m_resolution >= 2.)
		{
			m_resolution = 2.0;
		}
		m_mDC.PatBlt(m_imageRect.left , m_imageRect.top , m_imageRect.right , m_imageRect.bottom , BLACKNESS);
	}

	if(zDelta <= -1.)
	{
		m_resolution = m_resolution - 0.1;
		if(m_resolution <= 0.1)
		{
			m_resolution = 0.1;
		}
		m_mDC.PatBlt(m_imageRect.left , m_imageRect.top , m_imageRect.right , m_imageRect.bottom , BLACKNESS);
	}

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}


BOOL CVirture_LRFDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN)
	{
		switch((int)pMsg->wParam)
		{
		case 49 : m_object->clearAll();
			       f_object->clearAll();
			break;
		case 50 : m_controler = 2;
			break;
		case 51 : m_controler = 3;
			break;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CVirture_LRFDlg::object_create(CPoint* fix_point,CPoint* move_point)
{	

	m_mDC.PatBlt(m_imageRect.left , m_imageRect.top , m_imageRect.right , m_imageRect.bottom , BLACKNESS);

	//ZoomIn ZoomOut text Display
	{
		char buf[50];
		CRect text_rect;
		text_rect.top = 10;
		text_rect.left = 10;
		text_rect.bottom = 30;
		text_rect.right = 85;
		sprintf_s(buf,"zoom = %.1lf",m_resolution);
		m_mDC.DrawText((CString)buf,text_rect,DT_CENTER);
	}
	
	if(m_fixobjectCheck > 0)		// Fixed Object Grid
	{	
		//char rectbuf[50];
		//CRect text_rect;

		for(int f = 0 ; f < m_fixobjectCheck ; f++)
		{
			m_mDC.FillSolidRect(fix_point[f].x - OBJECT_SIZE/2,fix_point[f].y - OBJECT_SIZE/2,OBJECT_SIZE,OBJECT_SIZE,RGB(100,100,100));


			//sprintf_s(rectbuf,"(%d,%d)",fix_point[f].x - m_pCenter.x,m_pCenter.y-fix_point[f].y );

			//text_rect.top = fix_point[f].y - OBJECT_SIZE;
			//text_rect.left = fix_point[f].x + OBJECT_SIZE;
			//text_rect.bottom = fix_point[f].y+ OBJECT_SIZE;
			//text_rect.right = fix_point[f].x - OBJECT_SIZE;
			//
			//m_mDC.SetTextColor(RGB(255,0,0));
			//m_mDC.SetBkColor(RGB(255,255,255));
			//m_mDC.DrawText((CString)rectbuf,text_rect,DT_CENTER);
		}
	}


	// moving Object Grid 
	if(m_MoveobjectCheck > 0)		// Moved Object Grid
	{
		CBrush	br;
		CPen	pen;
		pen.CreatePen(0,1,RGB(0,0,0));
		br.CreateSolidBrush(RGB(100,100,100));
		CBrush* poldb = (CBrush*)m_mDC.SelectObject(br);
		CPen* poldp = (CPen*)m_mDC.SelectObject(pen);
		for(int m = 0 ; m < m_MoveobjectCheck; m++)
		{
			m_mDC.Ellipse(move_point[m].x,move_point[m].y,move_point[m].x+OBJECT_SIZE,move_point[m].y+OBJECT_SIZE);
		}
		m_mDC.SelectObject(&poldb);
		m_mDC.SelectObject(&poldp);
		pen.DeleteObject();
		br.DeleteObject();

	}

	//m_imageDC->BitBlt(0,0,m_imageRect.right,m_imageRect.bottom, &m_mDC , 0 , 0 , SRCCOPY);	
	m_bitmap.GetBitmapBits(m_bit_info.bmWidthBytes * m_bit_info.bmHeight, lpBits);	
}


void CVirture_LRFDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	printf("이동 장애물 출현 \n");
	m_object->add_move_object(point);
	CDialog::OnRButtonDown(nFlags, point);
}

void CVirture_LRFDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	printf("고정 장애물 출현 \n");
	f_object->add_fix_object(point);
	CDialog::OnLButtonDown(nFlags, point);	
}

void CVirture_LRFDlg::FixObject_manager(vector<CPoint> fpoint)
{
	printf("\nFix Interface Data Receive\n");

	m_fixobjectCheck = (int)fpoint.size();
	m_pfixobject = new CPoint[fpoint.size()];

	for(int i = 0 ; i < fpoint.size() ; ++i)
	{
		m_pfixobject[i] = (CPoint)fpoint[i];
	}
}

void CVirture_LRFDlg::MoveObject_manager(vector<CPoint> mpoint)
{
	printf("\nMove Interface Data Receive\n");

	m_MoveobjectCheck = (int)mpoint.size();
	m_pMovingobject = new CPoint[mpoint.size()];

	for(int i = 0 ; i < mpoint.size() ; ++i)
	{
		m_pMovingobject[i] = (CPoint)mpoint[i];
	}
}
void CVirture_LRFDlg::Object_Movemanager(vector<CPoint> mpoint)
{
	m_MoveobjectCheck = (int)mpoint.size();

	for(int i = 0 ; i < mpoint.size() ; ++i)
	{
		m_pMovingobject[i] = (CPoint)mpoint[i];
	}
}

