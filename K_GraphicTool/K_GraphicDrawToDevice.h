#pragma once
#include "D:\\2_����\\2013 ��\\HandSol\\resource.h"

class K_GraphicDrawToDevice : public CWnd
{
public:
	K_GraphicDrawToDevice(CWnd* mainwindow, int nID);
	~K_GraphicDrawToDevice(void);
	
	void SetPosition(int posX, int posY, int Width, int Height);
	void SetPosition(int posX, int posY);

	void CreateDoubleBuffer();

	void Draw(unsigned char* lowData,BITMAPINFO bitInfo);
	void Draw(int shift_x, int shift_y, unsigned char* lowData,BITMAPINFO bitInfo);
	void SetZoom(int zoomm);
private:
	void	AllDelete();

private:
	CDC*			m_imageDC;			
	CDC				m_mDC;				
	CBitmap			m_bitmap;			
	BITMAP			m_bit_info;				
	CStatic*		image;				

	CWnd*			m_mainWindow;
	int				m_nID;


	int				m_width;
	int				m_height;

	int				res_bitCnt;
	int				totalBitCnt;

	BITMAPINFO		bInfo;
	double			zoom;

	int				center_x;
	int				center_y;
public:
	int				m_posX;
	int				m_posY;
};

