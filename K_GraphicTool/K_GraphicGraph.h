#pragma once
#include "K_GraphicElements.h"

class K_GraphicGraph : public K_GraphicElements
{
public:
	K_GraphicGraph(void);
	~K_GraphicGraph(void);
	//Default Max width & Max height (return true = success )
	bool SetGraphArea(int width, int height);
	//Default Color WHITE OutSide Line (return true = success )
	bool SetGraphArea(int x, int y, int width, int height);
	//User Define (return true = success )
	bool SetGraphArea(int x, int y, int width, int height, COLORREF clr);
	// Get err code 
	char* GetErrCode();
	// Setting Graph Y axis Scale
	bool SetScaleYaxis(double minValue, double maxValue);
	// Set graph Color
	bool SetGraphColor(COLORREF clr);
	// New Data Input
	bool SetDataInput(double data);
	bool SetLogDataLength(int length);
	void GraphAreaRedraw();

private:
	int centorLineStart_X , centorLineStart_Y;
	int centorLineEnd_X , centorLineEnd_Y;
	char m_errCode[1024];
	double m_YaxisMax, m_YaxisMin;
	double m_Yscale;
	int m_graphWidth, m_graphHeight;

	int m_viewDataLenght;
	int m_logDataLenght;

	COLORREF m_areaClr;
	COLORREF m_graphClr;
	bool initcheck;
};
