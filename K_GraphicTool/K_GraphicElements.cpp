#include "StdAfx.h"
#include "K_GraphicElements.h"
#include <stdio.h>
#include <math.h>

K_GraphicElements::K_GraphicElements(void)
{
}
K_GraphicElements::~K_GraphicElements(void)
{
}
void K_GraphicElements::DrawClear(int BLACK_WHITE)
{
	if(BLACK_WHITE == 0)
	{
		memset(GetImageParm().FusionlpBits,0,GetImageParm().TotalBit);
	}
	memset(GetImageParm().FusionlpBits,255,GetImageParm().TotalBit);
}
void K_GraphicElements::DrawPoint(int x, int y, COLORREF clr)
{
	DrawPoint(x, y, 1, clr);
}
void K_GraphicElements::DrawPoint(int x, int y, int size, COLORREF clr)
{
	int			red		= (int)(0x000000FF & (BYTE)(clr));
	int			green	= (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue	= (int)(0x000000FF & (BYTE)(clr>>16));

	if((x >= GetImageParm().PixelSize.width) || (y >= GetImageParm().PixelSize.height)) return;
	if((x < 0) || (y < 0)) return;
	y = GetImageParm().PixelSize.height - y;
	GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 2] = red;
	GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 1] = green;
	GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 0] = blue;
}
void K_GraphicElements::DrawLine(int x , int y , int toX, int toY, COLORREF clr)
{
	DrawLine(x ,y ,toX,toY,1,clr);
}
void K_GraphicElements::DrawLine(int x , int y , int toX, int toY, int size, COLORREF clr)
{
	double		bufX = 0.0;
	double		bufY = 0.0;
	int			distx	= toX - x; 
	int			disty	= toY - y;
	double		dist	= (double)sqrt((double)(distx*distx)+(double)(disty*disty));
	double		mov_x = (double)distx/(double)dist;
	double		mov_y = (double)disty/(double)dist;

	int			red		= (int)(0x000000FF & (BYTE)(clr));
	int			green	= (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue	= (int)(0x000000FF & (BYTE)(clr>>16));

	bufX = x;
	bufY = y;

	bool chkx = false , chky = false;
	while(1)
	{
		DrawPoint((int)bufX,(int)bufY,size,clr);
		if(abs(bufX-(double)toX) <= 1.0)
		{
			chkx = true;
		}
		else
		{
			bufX = (double)bufX + (double)mov_x;
		}
		if(abs(bufY-(double)toY) <= 1.0)
		{
			chky = true;
		}
		else
		{
			bufY = (double)bufY + (double)mov_y;
		}
		if((chkx & chky) == true) break;
	}
}
void K_GraphicElements::DrawCircle(int x , int y , int radius, COLORREF clr)
{
	int			red		= (int)(0x000000FF & (BYTE)(clr));
	int			green	= (int)(0x000000FF & (BYTE)(clr>>8));
	int			blue	= (int)(0x000000FF & (BYTE)(clr>>16));

	double fx = 0.0, fy = 0.0;
	double rad = 0.0;

	for(int i = 0 ; i < 628 ; i++)
	{
		fx = radius * cos(rad);
		fy = radius * sin(rad);
		rad = rad + 0.01;
		DrawPoint((int)(fx + x ),(int)( fy + y),clr);
	}
}
void K_GraphicElements::DrawRect(int x , int y , int width, int height, COLORREF clr)
{
	DrawLine(x, y, width, y, clr);
	DrawLine(x, y, x, height, clr);
	DrawLine(width, height, width, y, clr);
	DrawLine(width, height, x, height, clr);
}

void K_GraphicElements::ChangePointRGB(int x1, int y1, int x2, int y2)
{
	BuflpBits[ (x1*GetImageParm().Bitcount + (y1 * GetImageParm().BitSize.width) ) + 2] = GetImageParm().FusionlpBits[ (x2*GetImageParm().Bitcount + (y2 * GetImageParm().BitSize.width) ) + 2];
	BuflpBits[ (x1*GetImageParm().Bitcount + (y1 * GetImageParm().BitSize.width) ) + 1] = GetImageParm().FusionlpBits[ (x2*GetImageParm().Bitcount + (y2 * GetImageParm().BitSize.width) ) + 1];
	BuflpBits[ (x1*GetImageParm().Bitcount + (y1 * GetImageParm().BitSize.width) ) + 0] = GetImageParm().FusionlpBits[ (x2*GetImageParm().Bitcount + (y2 * GetImageParm().BitSize.width) ) + 0];
}
COLORREF K_GraphicElements::GetPointColor(int x, int y)
{
	int red, green , blue;
	red = GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 2];
	green = GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 1];
	blue = GetImageParm().FusionlpBits[ (x*GetImageParm().Bitcount + (y * GetImageParm().BitSize.width) ) + 0];

	return RGB(red,green,blue);
}