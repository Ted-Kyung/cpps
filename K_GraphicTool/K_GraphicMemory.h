#pragma once
#include <deque>
using namespace std;

struct sBitSize
{
	int width;
	int height;
};
struct sPixelSize
{
	int width;
	int height;
};
struct sMemoryparm
{
	int				Bitcount;
	sPixelSize		PixelSize;
	sBitSize		BitSize;
	unsigned int	TotalBit;
	BITMAPINFO		bInfo;
	unsigned char*  FusionlpBits;
};

class K_GraphicMemory
{
public:
	K_GraphicMemory(void);
	K_GraphicMemory(int width, int height);
	~K_GraphicMemory(void);

	void			ClearAll();
	void			CreateMemory(int width, int height);
	void			CreateMemory(int width, int height, int bitcont);
	void			CreateMemory(int width, int height, int bitcont, int reverse);
	sMemoryparm		GetImageParm();	
	void			FillBitmapInfo(BITMAPINFO* bmi, int width, int height, int bpp, int origin);

	unsigned char*  BuflpBits;
	double*			m_dequeData;
private:	
	sMemoryparm		m_Parm;
	int				DeviceBitCount;	
	void			FusionImageLpBit();
	void			FreeMemory();	
};

