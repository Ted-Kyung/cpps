#include "StdAfx.h"
#include "K_GraphicPhoto.h"


K_GraphicPhoto::K_GraphicPhoto(void)
{
	//*(GetImageParm().FusionlpBits) = NULL;
	//BuflpBits = NULL;
}


K_GraphicPhoto::~K_GraphicPhoto(void)
{
}

bool K_GraphicPhoto::LoadPhoto(char* Path)
{
	FILE* m_pictureFile;
	m_pictureFile = NULL;
	m_pictureFile = fopen(Path,"rb");
	if(m_pictureFile == NULL) 
	{
		printf("file open faild\n");
		return false;
	}
	BITMAPFILEHEADER m_pictureBitFileHead;
	BITMAPINFOHEADER m_pictureBitInfo;
	fread(&m_pictureBitFileHead,sizeof(BITMAPFILEHEADER),1,m_pictureFile);
	fread(&m_pictureBitInfo,sizeof(BITMAPINFOHEADER),1,m_pictureFile);
	CreateMemory(m_pictureBitInfo.biWidth, m_pictureBitInfo.biHeight, m_pictureBitInfo.biBitCount, 1);
	GetImageParm().bInfo.bmiHeader = m_pictureBitInfo;
	fread(GetImageParm().FusionlpBits,GetImageParm().TotalBit, 1, m_pictureFile);

	fclose(m_pictureFile);

	return true;
}
bool K_GraphicPhoto::DeletePhoto()
{
	DrawClear(0);
	return true;
}