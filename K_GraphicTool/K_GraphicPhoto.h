#pragma once
#include "K_GraphicElements.h"
class K_GraphicPhoto : public K_GraphicElements
{
public:
	K_GraphicPhoto(void);
	~K_GraphicPhoto(void);

	bool LoadPhoto(char* Path);
	bool DeletePhoto();

};

