#include "StdAfx.h"
#include "K_GraphicMemory.h"

K_GraphicMemory::K_GraphicMemory(void)
{
	m_Parm.FusionlpBits	= NULL;
	BuflpBits			= NULL;
	DeviceBitCount		= 32;	
}
K_GraphicMemory::K_GraphicMemory(int width, int height)
{
	CreateMemory(width, height);
}
K_GraphicMemory::~K_GraphicMemory(void)
{
	FreeMemory();
}
void K_GraphicMemory::ClearAll()
{
	memset(m_Parm.FusionlpBits,NULL,m_Parm.TotalBit);
	memset(BuflpBits,NULL,m_Parm.TotalBit);
}
void K_GraphicMemory::CreateMemory(int width, int height)
{
	CreateMemory(width, height, DeviceBitCount);
}
void K_GraphicMemory::CreateMemory(int width, int height, int bitcont)
{
	CreateMemory(width, height, DeviceBitCount, 1);
}
void K_GraphicMemory::CreateMemory(int width, int height, int bitcont, int reverse)
{	
	FreeMemory();
	m_Parm.Bitcount				= bitcont / 8;
	m_Parm.PixelSize.width		= width;
	m_Parm.PixelSize.height		= height;
	m_Parm.BitSize.width		= m_Parm.PixelSize.width * m_Parm.Bitcount;
	m_Parm.BitSize.height		= m_Parm.PixelSize.height;
	m_Parm.TotalBit				= m_Parm.BitSize.width * m_Parm.BitSize.height;

	m_Parm.FusionlpBits		= new unsigned char[m_Parm.TotalBit+1];
	BuflpBits				= new unsigned char[m_Parm.TotalBit+1];

	memset(m_Parm.FusionlpBits,NULL,m_Parm.TotalBit);
	memset(BuflpBits,NULL,m_Parm.TotalBit);


	FillBitmapInfo(&m_Parm.bInfo, m_Parm.PixelSize.width, m_Parm.PixelSize.height, bitcont, reverse);
}

sMemoryparm	K_GraphicMemory::GetImageParm()
{	
	return m_Parm;
}

void K_GraphicMemory::FillBitmapInfo(BITMAPINFO* bmi, int width, int height, int bpp, int origin)
{
	_ASSERT(bmi && width >= 0 && height >=0 && (bpp == 8 || bpp ==24 || bpp ==32));
	BITMAPINFOHEADER* bmih = &(bmi->bmiHeader);
	memset(bmih, 0, sizeof(*bmih));
	bmih->biSize = sizeof(BITMAPINFOHEADER);
	bmih->biWidth = width;
	bmih->biHeight = origin ? abs(height) : -abs(height);
	bmih->biPlanes = 1;
	bmih->biBitCount = (unsigned short)bpp;
	bmih->biCompression = BI_RGB;
	if(bpp == 8)
	{
		RGBQUAD* palette = bmi->bmiColors;
		int i;
		for(i=0;i<256;i++)
		{
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}

}
void K_GraphicMemory::FreeMemory()
{
	if(BuflpBits != NULL)
	{
		delete BuflpBits;
		BuflpBits			= NULL;
	}
	if(m_Parm.FusionlpBits != NULL)
	{
		delete m_Parm.FusionlpBits;
		m_Parm.FusionlpBits		= NULL;
	}

}