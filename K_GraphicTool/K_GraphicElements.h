#pragma once
#include "K_GraphicMemory.h"
class K_GraphicElements : public K_GraphicMemory
{
public:
	K_GraphicElements(void);
	~K_GraphicElements(void);

	void	DrawClear(int BLACK_WHITE);
	void	ChangePointRGB(int x1, int y1, int x2, int y2);
	void	DrawPoint(int x, int y, COLORREF clr);
	void	DrawPoint(int x, int y, int size, COLORREF clr);
	void	DrawLine(int x , int y , int toX, int toY, COLORREF clr);
	void	DrawLine(int x , int y , int toX, int toY, int size, COLORREF clr);
	void	DrawCircle(int x , int y , int radius, COLORREF clr);
	void	DrawRect(int x , int y , int width, int height, COLORREF clr);
	COLORREF	GetPointColor(int x, int y);
};

