#include "StdAfx.h"
#include "K_GraphicDrawToDevice.h"

K_GraphicDrawToDevice::K_GraphicDrawToDevice(CWnd* mainwindow, int nID)
{
	m_imageDC			= NULL;	
	image				= NULL;				
	m_nID				= nID;
	m_mainWindow		= mainwindow;
	zoom				= 1.0 ;
	image = (CStatic *)(m_mainWindow->GetDlgItem(m_nID));
}
K_GraphicDrawToDevice::~K_GraphicDrawToDevice(void)
{
	AllDelete();
}

void K_GraphicDrawToDevice::SetPosition(int posX, int posY)
{
	CRect rect;
	image->GetWindowRect(&rect);
		
	SetPosition(rect.right - rect.left,rect.bottom - rect.top,m_posX,m_posY);
}
void K_GraphicDrawToDevice::SetPosition(int posX, int posY, int Width, int Height)
{
	m_posX = posX;
	m_posY = posY;
	m_width = Width;
	m_height = Height;

	image->MoveWindow(m_posX,m_posY,m_width,m_height);	
	CreateDoubleBuffer();
}

void K_GraphicDrawToDevice::Draw(unsigned char* lowData,BITMAPINFO bitInfo)
{
	Draw(center_x,center_y,lowData,bitInfo);
}

void K_GraphicDrawToDevice::Draw(int shift_x, int shift_y, unsigned char* lowData,BITMAPINFO bitInfo)
{
	m_imageDC->SetStretchBltMode(HALFTONE);
	
	int sx = (bitInfo.bmiHeader.biWidth * zoom ) - bitInfo.bmiHeader.biWidth;
	int sy =(bitInfo.bmiHeader.biHeight *  zoom) - bitInfo.bmiHeader.biHeight;
	int ex= bitInfo.bmiHeader.biWidth - sx;
	int ey = bitInfo.bmiHeader.biHeight - sy;

	center_x = (sx+ex)/2;
	center_y = (sy+ey)/2;

	shift_x = shift_x - center_x;
	shift_y = center_y - shift_y;

	StretchDIBits(m_imageDC->GetSafeHdc()
		, 0
		, 0
		, m_width
		, m_height 
		, sx - sx/2 + shift_x
		, sy - sy/2 + shift_y
		, ex
		, ey
		, lowData,&bitInfo,DIB_RGB_COLORS,SRCCOPY);
}
void K_GraphicDrawToDevice::AllDelete()
{
	m_imageDC			= NULL;	
	m_bitmap.DeleteObject();
	m_mDC.DeleteDC();
}
void K_GraphicDrawToDevice::CreateDoubleBuffer()
{
	CRect m_imageRect;
	m_imageDC = image->GetWindowDC();													
	image->GetClientRect(&m_imageRect);													

	m_mDC.CreateCompatibleDC(m_imageDC);													
	m_bitmap.CreateCompatibleBitmap(m_imageDC , m_imageRect.right , m_imageRect.bottom);
	m_bitmap.GetBitmap(&m_bit_info);	

	m_mDC.SelectObject(&m_bitmap);														
	m_mDC.PatBlt(m_imageRect.left, m_imageRect.top, m_imageRect.right,m_imageRect.bottom , BLACKNESS);		
}
void K_GraphicDrawToDevice::SetZoom(int zoomm)
{
	zoom += ((double)zoomm / 3600.0);
	if(zoom < 1.0) zoom = 1.0;
	if(zoom > 2.0) zoom = 2.0;
}

//	BLACKNESS
//	이미지를 원본의 색이 아닌 검은색으로 채웁니다.
//
//	DSTINVERT
//	지정된 이미지와는 상관없이 화면의 색상을 반전시킵니다.
//
//	MERGECOPY
//	이미지의 색상과 현재 선택된 브러시를 AND 연산자를 사용하여 병합합니다.
//
//	MERGEPAINT
//	반전된 이미지와 화면의 색을 OR 연산자를 사용하여 병합합니다.
//
//	NOMIRRORBITMAP
//	Prevents the bitmap from being mirrored.
//
//	NOTSRCCOPY
//	이미지의 색상을 반전시킵니다.
//
//	NOTSRCERASE
//	화면과 이미지를 OR 연산자를 사용하여 합친 다음 색상을 반전시킵니다.
//
//	PATCOPY
//	현재 선택된 브러시를 출력합니다.
//
//	PATINVERT
//	현재 선택된 브러시 색과 화면을 XOR 연산자를 사용하여 결합합니다.
//
//	PATPAINT
//	현재 선택된 브러시 색과 반전된 원본 이미지를 OR 연산자를 사용하여 결합합니다.
//
//	그다음 OR 연산자를 사용하여 화면과 결합합니다.
//
//	SRCAND
//	화면과 이미지를 AND 연산자를 사용하여 결합합니다.
//
//	SRCCOPY
//	원본 이미지를 출력합니다.
//
//	SRCERASE
//	반전된 화면과 이미지를 AND 연산자를 사용하여 결합합니다.
//
//	SRCINVERT
//	화면과 이미지를 XOR 연산자를 사용하여 결합합니다.
//
//	SRCPAINT
//	화면과 이미지를 OR 연산자를 사용하여 결합합니다.
//
//	WHITENESS
//	이미지를 원본의 색이 아닌 하얀색으로 채웁니다.

