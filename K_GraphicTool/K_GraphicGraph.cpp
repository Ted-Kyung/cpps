#include "StdAfx.h"
#include "K_GraphicGraph.h"
#include <memory.h>

K_GraphicGraph::K_GraphicGraph()
{
	centorLineStart_X = 0, centorLineStart_Y = 0;
	centorLineEnd_X = 0 , centorLineEnd_Y= 0;
	memset(m_errCode,0,1024);

	m_Yscale = 0.0, m_YaxisMax = 0.0, m_YaxisMin = 0.0;
	m_graphWidth = 0, m_graphHeight = 0;

	m_viewDataLenght = 0;
	m_logDataLenght = 0;
	m_dequeData = NULL;
	initcheck = false;
	m_graphClr =  RGB(255,0,0);
}
K_GraphicGraph::~K_GraphicGraph()
{
	if(m_dequeData != NULL) delete [] m_dequeData;
}
bool K_GraphicGraph::SetGraphArea(int width, int height)
{
	return SetGraphArea(0,0,width,height,RGB(0,0,0));
}
//Default Color WHITE OutSide Line
bool K_GraphicGraph::SetGraphArea(int x, int y, int width, int height)
{
	return SetGraphArea(x,y,width,height,RGB(0,0,0));
}
//User Define
bool K_GraphicGraph::SetGraphArea(int x, int y, int width, int height, COLORREF clr)
{
	if ((x < 0) || (y < 0) || (width <= 0) || (height <= 0))
	{
		char buf[1024] = "coordinate Setting Error\n";
		memcpy(m_errCode,buf,1024);
		return false;
	}

	CreateMemory(width,height,32,1);

	DrawRect(x,y,width,height,clr); 

	m_graphWidth = width;
	m_graphHeight = height;
	m_viewDataLenght = width;
	m_logDataLenght = width;
	m_areaClr = clr;

	m_dequeData = new double[m_logDataLenght];

	centorLineStart_X = x;
	centorLineStart_Y = (height / 2) + y;
	centorLineEnd_X = width + x ;
	centorLineEnd_Y = (height  / 2) + y ;
	initcheck = true;
	DrawLine(centorLineStart_X,centorLineStart_Y,centorLineEnd_X,centorLineEnd_Y,clr);
	return true;
}
// Get err code 
char* K_GraphicGraph::GetErrCode()
{
	return m_errCode;
}
// Setting Graph Y axis Scale
bool K_GraphicGraph::SetScaleYaxis(double minValue, double maxValue)
{
	if (minValue > maxValue)
	{
		char buf[1024] = "Y Axis scale Setting Error\n";
		memcpy(m_errCode,buf,1024);
		return false;
	}
	m_YaxisMax = maxValue;
	m_YaxisMin = minValue;

	if ((m_YaxisMax > 0) && (m_YaxisMin >= 0))
	{
		m_Yscale = (double)((m_YaxisMax - m_YaxisMin) / m_graphHeight);
	}
	else if ((m_YaxisMax > 0) && (m_YaxisMin < 0))
	{
		m_Yscale = (double)((m_YaxisMax + (m_YaxisMin * -1.0)) / m_graphHeight);
	}
	else if ((m_YaxisMax <= 0) && (m_YaxisMin < 0))
	{
		m_Yscale = (double)(((m_YaxisMax * -1.0) + (m_YaxisMin * -1.0)) / m_graphHeight);
	}	
	return true;
}
bool K_GraphicGraph::SetLogDataLength(int length)
{
	initcheck = false;
	if(length < m_graphWidth)
	{
		char buf[1024] = "Log data Length Error (length < graph Width)\n";
		memcpy(m_errCode,buf,1024);
		return false;
	}

	m_logDataLenght = length;
	
	if (m_dequeData != NULL) delete m_dequeData;

	m_dequeData = new double[m_logDataLenght];

	initcheck = true;
	return true;
}

// Set graph Color
bool K_GraphicGraph::SetGraphColor(COLORREF clr)
{
	m_graphClr = clr;
	return true;
}
// New Data Input
bool K_GraphicGraph::SetDataInput(double data)
{
	if((data < m_YaxisMin) || (data > m_YaxisMax) || (initcheck == false))
	{
		char buf[1024] = "input data Over&Under\n";
		memcpy(m_errCode,buf,1024);
		return false;
	}
	for(int y = 1; y < m_logDataLenght ; y++)
	{
		m_dequeData[y-1] = m_dequeData[y];
	}
	m_dequeData[m_logDataLenght -1] = (data / m_Yscale) + centorLineStart_Y;

	ClearAll();
	GraphAreaRedraw();

	for(int i = (int)(m_logDataLenght - m_viewDataLenght) ; i < (int)m_viewDataLenght ; i ++)
	{
		DrawPoint((int)i+centorLineStart_X,GetImageParm().PixelSize.height - (int)m_dequeData[i],m_graphClr);
	}
	return true;
}
void K_GraphicGraph::GraphAreaRedraw()
{
	DrawRect(centorLineStart_X
		    ,centorLineStart_Y - (m_graphHeight / 2)
			,m_graphWidth
			,m_graphHeight
			,m_areaClr);
	DrawLine(centorLineStart_X,centorLineStart_Y,centorLineEnd_X,centorLineEnd_Y,m_areaClr);
}