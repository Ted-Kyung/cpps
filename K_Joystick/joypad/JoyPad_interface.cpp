#include ".\joypad_interface.h"

JoyPad_interface::JoyPad_interface(void)
{
	HandleofThread = NULL;
	joy_init();
}

JoyPad_interface::~JoyPad_interface(void)
{

}

void JoyPad_interface::joy_init(void)
{
	//init joystick ID	
	joystickUserCnt = 0;
	joystickid = new UINT[JOYSTICK_USER_CNT];
	for(int cnt = 0 ; cnt < JOYSTICK_USER_CNT ; cnt ++)	joystickid[cnt] = 0;

	devcnt = joyGetNumDevs();

	if(joyGetNumDevs() == 0)	
	{	// 0 == err 
		printf("[%s]JoyStick not Found\nPlz restart Program\n",get_err_code(ERR));
		return;
	}
	else
	{
		printf("[%s]JoyStick Found\n",get_err_code(ERR_NONE));
	}


	memset(&joypad_info,0,sizeof(JOYINFOEX));
	joypad_info.dwSize = sizeof(JOYINFOEX);
	joypad_info.dwFlags = JOY_RETURNBUTTONS | JOY_RETURNX | JOY_RETURNY | JOY_RETURNALL;

	if( joyGetPosEx(JOYSTICKID1, &joypad_info) != JOYERR_UNPLUGGED )
	{
		printf("[%s]JOYSTICKID%d plugged\n",get_err_code(ERR_NONE),joystickUserCnt);
		joystickid[joystickUserCnt] = JOYSTICKID1;
	}
	else if (joyGetPosEx(JOYSTICKID2, &joypad_info) != JOYERR_UNPLUGGED)
	{
		joystickUserCnt++;
		printf("[%s]JOYSTICKID%d plugged\n",get_err_code(ERR_NONE),joystickUserCnt);
		joystickid[joystickUserCnt] = JOYSTICKID2;		
	}
	else
	{
		printf("[%s]JOYSTICK not plugged\n",get_err_code(ERR));
		joystickid[joystickUserCnt] = 0;
		return;
	}

	if( HandleofThread != NULL)
	{
		TerminateThread(HandleofThread, 0);
		CloseHandle(HandleofThread);
		HandleofThread = NULL;
	}
	//joystick data receive Trhead Create
	HandleofThread = CreateThread(NULL,NULL,joystick_get,(LPVOID)this,NULL,NULL);

}

DWORD WINAPI JoyPad_interface::joystick_get(LPVOID ptr)
{
	JoyPad_interface* fptr = (JoyPad_interface*)ptr;
	return fptr->joystickDataOutput();
}
DWORD JoyPad_interface::joystickDataOutput(void)
{
	int joyerr = -1;
	bool chk = false;
	while(1)
	{
		joyerr = joyGetPosEx(joystickid[joystickUserCnt], &joypad_info);
		if(joyerr == JOYERR_NOERROR)
		{
			printf("%d\t%d\t%d\t%d\t\t\t\r",(int)joypad_info.dwXpos,(int)joypad_info.dwYpos,(int)joypad_info.dwZpos,(int)joypad_info.dwRpos);
		}
		else
		{
			printf("Wait..Search joyStick\t\t\n");
			joyConfigChanged(0);
			Sleep(2500);
		}
	}
	
	return 0;
}
