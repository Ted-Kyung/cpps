#include <stdio.h>
#include <conio.h>

#include "JoyPad_interface.h"

void main(int argc, char* argv[])
{
	char cmd;
	JoyPad_interface* joypad = new JoyPad_interface;
	while (1)
	{
		if(_kbhit() == 0)
		{
			cmd = _getch();
			if(cmd == 'q')
			{
				printf("\n");
				break;
			}
			else if(cmd == 's')
			{
				joypad->joy_init();
			}
		}
	}
}