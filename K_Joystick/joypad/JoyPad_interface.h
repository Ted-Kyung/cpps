#pragma once
#include <stdio.h>
#include <Windows.h>
#include <MMSystem.h>
#include <dinput.h>
#pragma comment(lib,"winmm.lib")

#define INIT_ID				 0
#define ERR_NONE			 0
#define ERR					 1
#define ERR_UNKNOWN			-1
#define JOYSTICK_USER_CNT	 2

class JoyPad_interface
{
public:
	JoyPad_interface(void);
	~JoyPad_interface(void);

private: //joystick init 		
	UINT*			joystickid;					//many joystick 		
	JOYINFOEX		joypad_info;				//joystick data
 
	// MMSystem inside ....WINMMAPI MMRESULT WINAPI joyGetPosEx( IN UINT uJoyID, OUT LPJOYINFOEX pji);	
	int joystickUserCnt;
	static DWORD WINAPI joystick_get(LPVOID ptr);
public:
	void joy_init(void);
	DWORD joystickDataOutput();

	HANDLE HandleofThread;
	int devcnt;
};


static char* get_err_code(int err)
{
	switch(err)
	{
	case 0 : return "err_none";
		break;
	case 1 : return "error!";
		break;
	default: return "err unknown";
		break;
	}
}