//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GPP_OptimazingPath.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GPP_OPTIMAZINGPATH_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDC_IMG_MAP                     1000
#define IDC_BUTTON1                     1001
#define IDC_BUTTON2                     1002
#define IDC_BUTTON3                     1003
#define IDC_COMBO                       1007
#define IDC_BTN_MAPCHOICE               1008
#define IDC_BTN_MAPSELECT               1009
#define IDC_STATIC_NEAST                1010
#define IDC_STATIC_NNORTH               1011
#define IDC_STATIC_GEAST                1012
#define IDC_STATIC_GNORTH2              1013
#define IDC_STATIC_TIMESTAMP            1015
#define IDC_EDIT_INTEGRALDIS            1016
#define IDC_EDIT3                       1017
#define IDC_EDIT_CALTIME                1017
#define IDC_STATIC_ERR1                 1018
#define IDC_STATIC_ERR2                 1019
#define IDC_STATIC_ERR3                 1020
#define IDC_STATIC_ERR4                 1021
#define IDC_STATIC_ERR5                 1022
#define IDC_STATIC_ERR6                 1023
#define IDC_STATIC_ERR7                 1024
#define IDC_STATIC_ERR8                 1025
#define IDC_STATIC_ERR9                 1026
#define IDC_STATIC_ERR10                1027
#define IDC_STATIC_ERR11                1028
#define IDC_STATIC_ERR12                1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
