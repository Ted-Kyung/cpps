#include "StdAfx.h"
#include "GPP_Data_Struct.h"


GPP_Data_Struct::GPP_Data_Struct()
{
	iter_point = point.begin();
	buf_point.parentNum = -1;
	buf_point.pointNum = -1;
	buf_point.state = NONE;
	buf_point.east = -1;
	buf_point.north = -1;
	buf_point.connection.resize(0);
	trashpoint.push_front(buf_point);
}
GPP_Data_Struct::~GPP_Data_Struct(void)
{
	point.clear();
	point.resize(0);
}
void GPP_Data_Struct::RemoveAllPoint(void)
{
	point.clear();
}
void GPP_Data_Struct::AddPoint(int pointNum, double east, double north)
{
	if(CheckSameNum(pointNum))return;

	buf_point.parentNum = -1;
	buf_point.pointNum = pointNum;
	buf_point.state = NONE;
	buf_point.east = east;
	buf_point.north = north;
	buf_point.connection.resize(0);
	buf_point.IntegralParent2Distance = 0.0;
	buf_point.Goal2Distance = 0.0;
	
	point.push_back(buf_point);
}
void GPP_Data_Struct::setState(int state, int pointNum)
{
	if(state == START)
	{
		SetStartPoint(pointNum);
	}
	if(state == END)
	{
		SetEndPoint(pointNum);
	}
}
int GPP_Data_Struct::getState(int pointNum)
{
	return (findPointNumIter(pointNum))->state;
}
void GPP_Data_Struct::RemovePoint(int pointNum)
{
	point.erase(findPointNumIter(pointNum));
}
int	GPP_Data_Struct::GetALLPointSize(void)
{
	return point.size();
}
bool GPP_Data_Struct::CheckSameNum(int pointNum)
{
	for(int bCnt = 0 ; bCnt < GetALLPointSize(); bCnt++)
	{
		if (point[bCnt].pointNum == pointNum)
		{
			return true;
		}
	}
	return false;
}
deque<GPR_DATA>::iterator GPP_Data_Struct::findPointNumIter(int pointNum)
{
	for(int bCnt = 0 ; bCnt < GetALLPointSize(); bCnt++)
	{
		if (point[bCnt].pointNum == pointNum)
		{
			return point.begin() + bCnt;
		}
	}
	return trashpoint.begin();
}
void GPP_Data_Struct::SetConnection(int pointNum1, int pointNum2)
{
	if(CheckSameConnection(pointNum1,pointNum2)) return;
	
	 (findPointNumIter(pointNum1))->connection.push_back(pointNum2);
	 (findPointNumIter(pointNum2))->connection.push_back(pointNum1);
}
bool GPP_Data_Struct::CheckSameConnection(int pointNum1, int pointNum2)
{
	int connecsize = GetConnectionSize(pointNum1);
	buf_iter_iter_point = GetConnection(pointNum1);

	for(int chkcnt = 0; chkcnt < connecsize ; chkcnt++)
	{
		if(buf_iter_iter_point[chkcnt]->pointNum == pointNum2) return true;
	}
	connecsize = GetConnectionSize(pointNum2);
	buf_iter_iter_point = GetConnection(pointNum2);

	for(int chkcnt = 0; chkcnt < connecsize ; chkcnt++)
	{
		if(buf_iter_iter_point[chkcnt]->pointNum == pointNum1) return true;
	}
	return false;
}
int  GPP_Data_Struct::GetConnectionSize(int myNum)
{
	return (findPointNumIter(myNum))->connection.size();
}
deque<deque<GPR_DATA>::iterator> GPP_Data_Struct::GetConnection(int myNum)
{
	int connectionSize = GetConnectionSize(myNum);
	iter_point = findPointNumIter(myNum);

	iter_iter_point.clear();

	for(int cntcnt = 0 ; cntcnt < connectionSize ; cntcnt++)
	{
		iter_iter_point.push_back(findPointNumIter((int)(iter_point->connection[cntcnt])));
	}
	return iter_iter_point;
}
void GPP_Data_Struct::SetMyParent(int myNum, int parentNum)
{
	(findPointNumIter(myNum))->parentNum = parentNum;
}
deque<GPR_DATA>::iterator	GPP_Data_Struct::GetMyParent(int myNum)
{
	return findPointNumIter((findPointNumIter(myNum))->parentNum);
}
void GPP_Data_Struct::SetStartPoint(int myNum)
{
	BeforeStartPointRemove();
	mp_datacallback->callback_ERRNONE_NotStartPoint();
	iter_point = findPointNumIter(myNum);
	iter_point->state = START;
	iter_point->parentNum = myNum;
	iter_start_point = iter_point;
}
void GPP_Data_Struct::BeforeStartPointRemove(void)
{
	for(int chkcnt = 0 ; chkcnt < (int)GetALLPointSize() ; chkcnt++)
	{
		if(point[chkcnt].state == START) 
		{
			point[chkcnt].state = NONE;
			return ;
		}
	}
}
deque<GPR_DATA>::iterator GPP_Data_Struct::getStartNum(void)
{
	return iter_start_point;
}
void GPP_Data_Struct::SetEndPoint(int myNum)
{
	BeforeEndPointRemove();
	mp_datacallback->callback_ERRNONE_NotEndPoint();
	iter_point = findPointNumIter(myNum);
	iter_point->state = END;
	iter_end_point = iter_point;
}
void GPP_Data_Struct::BeforeEndPointRemove(void)
{
	for(int chkcnt = 0 ; chkcnt < (int)GetALLPointSize() ; chkcnt++)
	{
		if(point[chkcnt].state == END) 
		{
			point[chkcnt].state = NONE;
			return ;
		}
	}
}
deque<GPR_DATA>::iterator GPP_Data_Struct::getEndpoint(void)
{
	return iter_end_point;
}
deque<GPR_DATA>::iterator GPP_Data_Struct::getALLpoint(void)
{
	return point.begin();
}
deque<GPR_DATA>::iterator GPP_Data_Struct::getMyNum(int myNum)
{
	return findPointNumIter(myNum);
}
int GPP_Data_Struct::SearchNearPointNum(double east, double north)
{
	double d_east = 0;
	double d_north = 0;

	double cal_dist = 0.0;
	double c_distance = 99999999.0;

	int nearpointNum = 0;

	for(int chkcnt = 0 ; chkcnt < (int)GetALLPointSize() ; chkcnt++)
	{
		if(point[chkcnt].connection.size() >= 1)
		{
			d_east = east - point[chkcnt].east;
			d_north = north - point[chkcnt].north;
			cal_dist = sqrt((double)((d_east*d_east)+(d_north*d_north)));

			if(c_distance > cal_dist)
			{
				c_distance = cal_dist;
				nearpointNum = point[chkcnt].pointNum;
			}
		}
	}
	return nearpointNum;
}