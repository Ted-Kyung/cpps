
// GPP_OptimazingPathDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "GPP_OptimazingPath.h"
#include "GPP_OptimazingPathDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}
BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

CGPP_OptimazingPathDlg::CGPP_OptimazingPathDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGPP_OptimazingPathDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}
void CGPP_OptimazingPathDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}
BEGIN_MESSAGE_MAP(CGPP_OptimazingPathDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_DESTROY()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_BN_CLICKED(IDC_BUTTON1, &CGPP_OptimazingPathDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CGPP_OptimazingPathDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CGPP_OptimazingPathDlg::OnBnClickedButton3)
	ON_CBN_SELCHANGE(IDC_COMBO, &CGPP_OptimazingPathDlg::OnCbnSelchangeCombo)
	ON_BN_CLICKED(IDC_BTN_MAPCHOICE, &CGPP_OptimazingPathDlg::OnBnClickedBtnMapchoice)
	ON_BN_CLICKED(IDC_BTN_MAPSELECT, &CGPP_OptimazingPathDlg::OnBnClickedBtnMapselect)
END_MESSAGE_MAP()


// CGPP_OptimazingPathDlg 메시지 처리기

BOOL CGPP_OptimazingPathDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	mImgMap					= NULL;
	m_mysql_manager			= NULL;
	m_searchOptimizingPath	= NULL;

	m_combo = (CComboBox*)GetDlgItem(IDC_COMBO);

	m_combo->InsertString(0,"창원 ADD");
	m_combo->InsertString(1,"대전 ADD");
	m_combo->SetCurSel(0);	

	m_mapSetting	= (CButton*)GetDlgItem(IDC_BTN_MAPCHOICE);
	m_mapSelect		= (CButton*)GetDlgItem(IDC_BTN_MAPSELECT); 

	m_combo->EnableWindow(FALSE);
	m_mapSetting->EnableWindow(TRUE);
	m_mapSelect->EnableWindow(FALSE);

	m_static_err1	= (CStatic*)GetDlgItem(IDC_STATIC_ERR1);
	m_static_err2	= (CStatic*)GetDlgItem(IDC_STATIC_ERR2);
	m_static_err3	= (CStatic*)GetDlgItem(IDC_STATIC_ERR3);
	m_static_err4	= (CStatic*)GetDlgItem(IDC_STATIC_ERR4);
	m_static_err5	= (CStatic*)GetDlgItem(IDC_STATIC_ERR5);
	m_static_err6	= (CStatic*)GetDlgItem(IDC_STATIC_ERR6);
	m_static_err7	= (CStatic*)GetDlgItem(IDC_STATIC_ERR7);
	m_static_err8	= (CStatic*)GetDlgItem(IDC_STATIC_ERR8);
	m_static_err9	= (CStatic*)GetDlgItem(IDC_STATIC_ERR9);
	m_static_err10	= (CStatic*)GetDlgItem(IDC_STATIC_ERR10);
	m_static_err11	= (CStatic*)GetDlgItem(IDC_STATIC_ERR11);
	m_static_err12	= (CStatic*)GetDlgItem(IDC_STATIC_ERR12);

	m_integraldistance	= (CEdit*)GetDlgItem(IDC_EDIT_INTEGRALDIS);
	m_calcultime		= (CEdit*)GetDlgItem(IDC_EDIT_CALTIME);

	m_integraldistance->SetWindowTextA("0.0");
	m_calcultime->SetWindowTextA("0.0");

	m_static_err1->EnableWindow(TRUE);
	m_static_err2->EnableWindow(TRUE);
	m_static_err3->EnableWindow(TRUE);
	m_static_err4->EnableWindow(TRUE);
	m_static_err5->EnableWindow(TRUE);
	m_static_err6->EnableWindow(TRUE);
	m_static_err7->EnableWindow(TRUE);
	m_static_err8->EnableWindow(TRUE);
	m_static_err9->EnableWindow(TRUE);
	m_static_err10->EnableWindow(TRUE);
	m_static_err11->EnableWindow(TRUE);
	m_static_err12->EnableWindow(TRUE);

	int usemapNum;
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString("USE_MAP","MAP","None_UseMAP",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		usemapNum = atoi(buf);
	}

	mf_ProgramInit(usemapNum);
	
	return TRUE;  
}
void CGPP_OptimazingPathDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}
void CGPP_OptimazingPathDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}
HCURSOR CGPP_OptimazingPathDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CGPP_OptimazingPathDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_LBpush = true;
	mf_DCpoint2Globalpoint_Convert(point.x,point.y);
	printf("[DC2Global] %.2lf %.2lf\n",m_mouseEast,m_mouseNorth);

	int nearpointNum = m_searchOptimizingPath->SearchNearPointNum(m_mouseEast,m_mouseNorth);

	if (mb_StartBtn)
	{
		mb_StartClick = true;
		mb_StartBtn = false;
		m_searchOptimizingPath->SetStartPoint(nearpointNum);
		printf("SetStartPoint Num is [%d]\n",nearpointNum);

		mf_Circle(m_searchOptimizingPath->getMyNum(nearpointNum)->east, m_searchOptimizingPath->getMyNum(nearpointNum)->north, 5, RGB(255,0,255));
		mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
		mImgMap->ReDrawDC();
		mImgMap->SetBackGroundBIT();
	} 
	else if(mb_GoalBtn)
	{
		mb_GoalClick = true;
		mb_GoalBtn = false;
		m_searchOptimizingPath->SetEndPoint(nearpointNum);
		printf("SetEndPoint Num is [%d]\n",nearpointNum);

		mf_Circle(m_searchOptimizingPath->getMyNum(nearpointNum)->east, m_searchOptimizingPath->getMyNum(nearpointNum)->north, 5, RGB(255,0,255));
		mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
		mImgMap->ReDrawDC();
		mImgMap->SetBackGroundBIT();
	}
	CDialogEx::OnLButtonDown(nFlags, point);
}
void CGPP_OptimazingPathDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_LBpush = false;
	CDialogEx::OnLButtonUp(nFlags, point);
}
void CGPP_OptimazingPathDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	m_RBpush = true;
	StShiftX = point.x;
	StShiftY = point.y;
	__super::OnRButtonDown(nFlags, point);
}
void CGPP_OptimazingPathDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	m_RBpush = false;
	__super::OnRButtonUp(nFlags, point);
}
void CGPP_OptimazingPathDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	int difx = abs(StShiftX - point.x);
	int dify = abs(point.y - StShiftY);

	int fzoom = (int)((double)((double)zoom / (double)ZOOM_MAX) * (double)100.0);
	fzoom = 100 - fzoom;
	fzoom = (int)((double)fzoom/(double)15.0);
	if(fzoom <= 0) fzoom = 1;

	if(m_RBpush)
	{
		shiftX += (int)(StShiftX - point.x) * fzoom;
		shiftY += (int)(point.y - StShiftY) * fzoom;

		if(shiftX < 0) shiftX = 0;
		if(shiftX + zoom > mImgMap->GetPicWidth()) shiftX = mImgMap->GetPicWidth() - zoom;
		if(shiftY < 0) shiftY = 0;
		if(shiftY + zoom > mImgMap->GetPicHeight()) shiftY = mImgMap->GetPicHeight() - zoom;

		mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
		mImgMap->ReDrawDC();
		mImgMap->SetBackGroundBIT();
	}

	StShiftX = point.x;
	StShiftY = point.y;

	CDialogEx::OnMouseMove(nFlags, point);
}
BOOL CGPP_OptimazingPathDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	m_wheel = true;
	zoom += zDelta* -1;
	if(zoom < ZOOM_MIN) zoom = ZOOM_MIN;
	if(zoom > ZOOM_MAX) zoom = ZOOM_MAX;
	mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
	mImgMap->ReDrawDC();
	mImgMap->SetBackGroundBIT();
	m_wheel = false;
	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}

void CGPP_OptimazingPathDlg::callback_Addpoint(int pointNum,double East, double North)
{
	m_searchOptimizingPath->AddPoint(pointNum,East,North);
}
void CGPP_OptimazingPathDlg::callback_SetConnettion(int pointNum,int pointNum2)
{
	m_searchOptimizingPath->SetConnection(pointNum,pointNum2);
} 
void CGPP_OptimazingPathDlg::callback_DrawPath(deque<GPR_DATA> Path)
{
	CString sbuf;
	int path_cnt = Path.size();
	for(int dcnt = 1 ; dcnt < path_cnt ; dcnt++)
	{
		mf_Line(Path[dcnt].east,Path[dcnt].north,Path[dcnt-1].east,Path[dcnt-1].north,RGB(255,0,0));
	}

	sbuf.Format("%.2lf",Path[Path.size()-1].IntegralParent2Distance);
	m_integraldistance->SetWindowTextA(sbuf);

	//출발점은 제외 -1
	sbuf.Format("%d",Path.size()-1);
	m_calcultime->SetWindowTextA(sbuf);

	mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
	mImgMap->ReDrawDC();
	mImgMap->SetBackGroundBIT();
}

void CGPP_OptimazingPathDlg::OnBnClickedButton1()
{
	// start point
	mb_StartBtn = true;
	mb_GoalBtn = false;
	C_YELLOW;printf("Start point Select Plz...\n");C_WHITE;
}
void CGPP_OptimazingPathDlg::OnBnClickedButton2()
{
	// end point
	mb_StartBtn = false;
	mb_GoalBtn = true;
	C_YELLOW;printf("Goal point Select Plz...\n");C_WHITE;
}
void CGPP_OptimazingPathDlg::OnBnClickedButton3()
{
	// path create
	if((mb_StartClick == true ) && (mb_GoalClick == true))
	{
		C_BLUE;	m_searchOptimizingPath->StartCreateObtimizerPath();	C_WHITE;
	}
	else
	{
		C_RED; printf("Check Plz StartPoint & GoalPoint Select\n"); C_WHITE;
	}

	mb_StartClick = false;
	mb_GoalClick = false;
}
void CGPP_OptimazingPathDlg::OnCbnSelchangeCombo()
{
	m_combo->EnableWindow(FALSE);
	m_mapSetting->EnableWindow(FALSE);
	m_mapSelect->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::OnBnClickedBtnMapchoice()
{
	m_combo->EnableWindow(TRUE);
	m_mapSetting->EnableWindow(FALSE);
	m_mapSelect->EnableWindow(FALSE);
}
void CGPP_OptimazingPathDlg::OnBnClickedBtnMapselect()
{
	m_combo->EnableWindow(FALSE);
	m_mapSetting->EnableWindow(TRUE);
	m_mapSelect->EnableWindow(FALSE);
	this->SetFocus();

	int selectMapNum = m_combo->GetCurSel();
	selectMapNum = selectMapNum + 1;
	m_combo->EnableWindow(false);

	mf_ProgramInit(selectMapNum);
}

void CGPP_OptimazingPathDlg::mf_DrawPoint()
{
	m_searchOptimizingPath->Reset();
	double east_dist = (double)m_mapEastEnd - (double)m_mapEastStart;
	double North_dist = (double)m_mapNorthStart - (double)m_mapNorthEnd;

	m_mapEast_offset = east_dist / mImgMap->GetPicWidth();
	m_mapNorth_offset = North_dist / mImgMap->GetPicHeight();
	int pointCnt = 	m_searchOptimizingPath->GetALLPointSize();
	deque<GPR_DATA>::iterator points = m_searchOptimizingPath->getALLpoint();
	int connectionpoint;
	deque<deque<GPR_DATA>::iterator> connections;

	for(int dcnt = 0 ; dcnt < pointCnt ; dcnt++)
	{
		mf_Circle((points + dcnt)->east,(points + dcnt)->north,4,RGB(120,100,0));
		connectionpoint = m_searchOptimizingPath->GetConnectionSize((points + dcnt)->pointNum);
		connections = m_searchOptimizingPath->GetConnection((points + dcnt)->pointNum);
		for(int ccnt = 0 ; ccnt < connectionpoint ; ccnt++)
		{
			mf_Line((points + dcnt)->east,(points + dcnt)->north,(connections[ccnt])->east,(connections[ccnt])->north,RGB(0,255,0));
		}
	}
}
void CGPP_OptimazingPathDlg::mf_DCpoint2Globalpoint_Convert(int x, int y)
{
	y = (int)m_windowHeight - y;

	m_xViewdistance = zoom * m_mapEast_offset;
	m_yViewdistance = zoom * m_mapNorth_offset;

	m_xoffSet  = m_xViewdistance / m_windowWidth;
	m_yoffSet  = m_yViewdistance / m_windowHeight;

	m_mouseEast	= (double)(shiftX * m_mapEast_offset) + (x * m_xoffSet) + m_mapEastStart;
	m_mouseNorth	= (double)(shiftY * m_mapNorth_offset) + (y * m_yoffSet) + m_mapNorthEnd;
}
void CGPP_OptimazingPathDlg::mf_Line(double east1, double north1,double east2, double north2, COLORREF clr)
{
	double east_dist = m_mapEastEnd - m_mapEastStart;
	double North_dist = m_mapNorthStart - m_mapNorthEnd;

	m_mapEast_offset = east_dist / mImgMap->GetPicWidth();
	m_mapNorth_offset = North_dist / mImgMap->GetPicHeight();

	double buf_east , buf_north;
	int pixel_offset_east,pixel_offset_north;

	double line_buf_east, line_buf_north;
	int line_offset_east, line_offset_north;

	buf_east = east1;
	buf_north = north1;

	buf_east = buf_east - m_mapEastStart;
	buf_north = m_mapNorthStart - buf_north;

	pixel_offset_east		= 	(int)(buf_east / m_mapEast_offset);
	pixel_offset_north		=   (int)(buf_north / m_mapNorth_offset);

	line_buf_east = east2;
	line_buf_north = north2;

	line_buf_east = line_buf_east - m_mapEastStart;
	line_buf_north = m_mapNorthStart - line_buf_north;

	line_offset_east		= 	(int)(line_buf_east / m_mapEast_offset);
	line_offset_north		=   (int)(line_buf_north / m_mapNorth_offset);
	mImgMap->StaticLine(pixel_offset_east,pixel_offset_north,(int)line_offset_east,(int)line_offset_north,clr);
}
void CGPP_OptimazingPathDlg::mf_Circle(double east1, double north1,int r, COLORREF clr)
{
	double f_bufeast = 0.0;
	double f_bufnorth = 0.0;
	int f_pixel_offset_east, f_pixel_offset_north;
	f_bufeast = east1;
	f_bufnorth = north1;
	f_bufeast = f_bufeast - m_mapEastStart;
	f_bufnorth = m_mapNorthStart - f_bufnorth;
	f_pixel_offset_east			= 	(int)(f_bufeast / m_mapEast_offset);
	f_pixel_offset_north		=   (int)(f_bufnorth / m_mapNorth_offset);
	mImgMap->StaticCircle(f_pixel_offset_east, f_pixel_offset_north, r, clr);
}
void CGPP_OptimazingPathDlg::mf_ProgramInit(int mapNum)
{
	KillTimer(1);

	char f_usemap[32];
	char f_mysql_id[32];
	char f_mysql_pw[32];
	char f_mysql_DB[32];
	char f_map_path[256];

	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString("DEBUG","USE","0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		if(atoi(buf) == 1)
		{
			AllocConsole();
			freopen("CONIN$","rt",stdin);
			freopen("CONOUT$","wt",stdout);
			//#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")	
		}
			
	}

	sprintf(f_usemap,"MAP_%d",mapNum);

	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString("MySQL","ID","None_UserID",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		memcpy(f_mysql_id,buf,sizeof(f_mysql_id));
		C_GREEN;printf("select MySQL ID  [ %s ]\n",f_mysql_id);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString("MySQL","PW","None_UserPW",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		memcpy(f_mysql_pw,buf,sizeof(f_mysql_pw));
		C_GREEN;printf("select MySQL PW  [ %s ]\n",f_mysql_pw);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"DB","None_DBname",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		memcpy(f_mysql_DB,buf,sizeof(f_mysql_DB));
		C_GREEN;printf("select MySQL DataBaseName  [ %s ]\n",f_mysql_DB);C_WHITE;
	}	
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"BITMAP_PATH","None_BITMAP",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		memcpy(f_map_path,buf,sizeof(f_map_path));
		C_GREEN;printf("select ImagePath  [ %s ]\n",f_map_path);C_WHITE;
	}
	//////
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"MAP_START_EAST","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		m_mapEastStart = atof(buf);
		C_YELLOW;printf("EastStart [ %.2lf ]\n",m_mapEastStart);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"MAP_END_EAST","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		m_mapEastEnd = atof(buf);
		C_YELLOW;printf("EastEnd [ %.2lf ]\n",m_mapEastEnd);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"MAP_START_NORTH","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		m_mapNorthStart = atof(buf);
		C_YELLOW;printf("NorthStart [ %.2lf ]\n",m_mapNorthStart);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"MAP_END_NORTH","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		m_mapNorthEnd = atof(buf);
		C_YELLOW;printf("NorthEnd [ %.2lf ]\n",m_mapNorthEnd);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"ZOOMMIN","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		ZOOM_MIN = atoi(buf);
		C_YELLOW;printf("ZOOM_MIN [ %d ]\n",ZOOM_MIN);C_WHITE;
	}
	{
		char buf[256];
		memset(buf,NULL,sizeof(buf));
		GetPrivateProfileString(f_usemap,"ZOOMMAX","-1.0",buf,sizeof(buf),".\\GPP_OptimizerPath.ini");
		ZOOM_MAX = atoi(buf);
		C_YELLOW;printf("ZOOM_MIN [ %d ]\n",ZOOM_MAX);C_WHITE;
	}
	
	if(mImgMap)					delete mImgMap;
	if(m_mysql_manager)			delete m_mysql_manager;
	if(m_searchOptimizingPath)	delete m_searchOptimizingPath;

	mImgMap					= NULL;
	m_mysql_manager			= NULL;
	m_searchOptimizingPath	= NULL;

	//GPP Calculat Class Create
	m_searchOptimizingPath	= new SearchOptimizingPath(this);

	// MySQL Management
	m_mysql_manager				= new MySQL_Manager(this);
	m_mysql_manager->MySQL_Connect(f_mysql_id,f_mysql_pw,f_mysql_DB);
	m_mysql_manager->MySQL_GetDB();

	//마우스 조작 
	m_LBpush		= false;
	m_RBpush		= false;
	m_wheel			= false;

	//지도 조작
	shiftX			= 0;
	shiftY			= 0;
	zoom			= 0;
	StShiftX		= 0;
	StShiftY		= 0;

	//Map 그리는 영역 이미지 관리 클래스로 관리 
	mImgMap = new imageProcessing(this,IDC_IMG_MAP,this);
	mImgMap->SetImageInit(0,0);	
	mImgMap->setStaticBITPicture(f_map_path);
	mf_DrawPoint();

	shiftX			= 0;
	shiftY			= 0;
	zoom   = ZOOM_MAX;

	mImgMap->StaticBITPictureMove(shiftX,shiftY,zoom);
	mImgMap->ReDrawDC();
	mImgMap->SetBackGroundBIT();

	m_windowWidth = mImgMap->GetImageParm().dcRight;
	m_windowHeight = mImgMap->GetImageParm().dcBottem;

	m_mouseEast = 0.0;
	m_mouseNorth = 0.0;
	m_xoffSet = 0.0;
	m_yoffSet = 0.0;

	mb_StartBtn = false;
	mb_GoalBtn = false;
	mb_StartClick = false;
	mb_GoalClick = false;

	//전체 영역에 대하여 ReDraw Timer 생성
	SetTimer(1,10,NULL);
}

void CGPP_OptimazingPathDlg::OnTimer(UINT_PTR nIDEvent)
{
	if((m_LBpush == false)&&(m_LBpush == false)&&(m_wheel == false))
	{
		mImgMap->BackGroundDraw();
		mImgMap->ReDrawDC();
	}	
	CDialogEx::OnTimer(nIDEvent);
}
void CGPP_OptimazingPathDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(1);
	if(mImgMap)					delete mImgMap;
	if(m_mysql_manager)			delete m_mysql_manager;
	if(m_searchOptimizingPath)	delete m_searchOptimizingPath;
}



void CGPP_OptimazingPathDlg::callback_ERR_MysqlConnection(void)
{
	m_static_err3->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_MysqlConnection(void)
{
	m_static_err3->EnableWindow(FALSE);
}

void CGPP_OptimazingPathDlg::callback_ERR_MysqlDBfind(void)
{
	m_static_err4->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_MysqlDBfind(void)
{
	m_static_err4->EnableWindow(FALSE);
}

void CGPP_OptimazingPathDlg::callback_ERR_NotConnectionPath(void)
{
	m_static_err8->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_NotConnectionPath(void)
{
	m_static_err8->EnableWindow(FALSE);
}

void CGPP_OptimazingPathDlg::callback_ERR_NotStartPoint(void)
{
	m_static_err6->EnableWindow(TRUE);	
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_NotStartPoint(void)
{
	m_static_err6->EnableWindow(FALSE);	
}

void CGPP_OptimazingPathDlg::callback_ERR_NotEndPoint(void)
{
	m_static_err7->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_NotEndPoint(void)
{
	m_static_err7->EnableWindow(FALSE);
}

void CGPP_OptimazingPathDlg::callback_ERR_MapFileOpen(void)
{
	m_static_err5->EnableWindow(TRUE);
}
void CGPP_OptimazingPathDlg::callback_ERRNONE_MapFileOpen(void)
{
	m_static_err5->EnableWindow(FALSE);
}



