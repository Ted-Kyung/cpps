#pragma once
#include "ImageCtrl.h"
#include <Vfw.h>
#pragma comment (lib,"vfw32.lib")

class ImageCameraCtrl : public ImageCtrl
{
public:
	ImageCameraCtrl(void);
	ImageCameraCtrl(CWnd* mainwindow, int nID);
	~ImageCameraCtrl(void);

	bool			init_Cam(void);
	unsigned char*	getCapture();
	int				Getcap_width();
	int				Getcap_height();
	BITMAPINFO		Getcap_BITMAPINFO();

	CWnd* m_Cwnd;
	HWND m_hWndCap;
		
	
	int buf_size;
	static LRESULT CALLBACK capCallbackOnFrame(HWND hWnd, LPVIDEOHDR lpVHdr);
	
};
