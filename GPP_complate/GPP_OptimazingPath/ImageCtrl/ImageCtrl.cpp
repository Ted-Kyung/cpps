#include "StdAfx.h"
#include "ImageCtrl.h"
#include <math.h>

ImageCtrl::ImageCtrl(void)
{
	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	image				= NULL;				
	lpBits				= NULL;
}
ImageCtrl::ImageCtrl(CWnd* mainwindow, int nID)
{
	m_nID = nID;
	m_mainWindow		= mainwindow;
	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	image				= NULL;				
	lpBits				= NULL;

	image = (CStatic *)(m_mainWindow->GetDlgItem(m_nID));
}
ImageCtrl::~ImageCtrl(void)
{
	if(lpBits)free(lpBits);
	if(BackGroundlpBits)free(BackGroundlpBits);
	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	lpBits				= NULL;
	BackGroundlpBits	= NULL;
	image				= NULL;	
	m_bitmap.DeleteObject();
	m_mDC.DeleteDC();
}
void ImageCtrl::CreateDC(CWnd* mainwindow, int nID)
{
	m_nID = nID;
	m_mainWindow		= mainwindow;
	m_imageDC			= NULL;	
	m_imageRect			= NULL;		
	image				= NULL;				
	lpBits				= NULL;

	image				= (CStatic *)(m_mainWindow->GetDlgItem(m_nID));
}
CDC* ImageCtrl::GetImageDC()
{
	return				m_imageDC;
}

CDC* ImageCtrl::GetmDC()
{
	return				&m_mDC;
}
void ImageCtrl::SetBackGroundBIT(void)
{
	memcpy(BackGroundlpBits,lpBits,GetBitSize());
}
void ImageCtrl::BackGroundDraw(void)
{
	m_bitmap.SetBitmapBits( m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,BackGroundlpBits);
	m_bitmap.GetBitmapBits(m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,lpBits);
}
bool ImageCtrl::SetImageInit(int posX, int posY, int Width, int Height)
{		
	m_Parm.dcLeft		= 0;
	m_Parm.dcRight		= Width;
	m_Parm.dcTop		= 0;
	m_Parm.dcBottem		= Height;
	
	m_Parm.dcXoffset	= posX;
	m_Parm.dcYoffset	= posY;

	SetImageSize(m_Parm.dcRight,m_Parm.dcBottem);
	SetImagePos(m_Parm.dcXoffset,m_Parm.dcYoffset);

	return true;
}
int	ImageCtrl::GetXcentor()
{
	return (int)(m_Parm.dcRight/2);
}
int	ImageCtrl::GetYcentor()
{
	return (int)(m_Parm.dcBottem/2);
}
bool ImageCtrl::SetImageInit(int posX, int posY)
{
	CRect rect;
	image->GetWindowRect(&rect);

	m_Parm.dcLeft		= 0;
	m_Parm.dcRight		= rect.right - rect.left;
	m_Parm.dcTop		= 0;
	m_Parm.dcBottem		= rect.bottom - rect.top;

	m_Parm.dcXoffset	= posX;
	m_Parm.dcYoffset	= posY;

	SetImageSize(m_Parm.dcRight,m_Parm.dcBottem);
	SetImagePos(m_Parm.dcXoffset,m_Parm.dcYoffset);

	return true;
}

bool ImageCtrl::SetImageSize(int width,int height)
{
	bitcnt_size = GetDeviceCaps((m_mainWindow->GetDC())->m_hDC, BITSPIXEL)/8;

	int f_res_count		= 0;
	m_Image_Width		= width;
	m_Image_Height		= height;
	m_Image_bitWidth	= m_Image_Width * bitcnt_size;
	f_res_count			= m_Image_bitWidth % 4;
	m_Image_bitWidth	= m_Image_bitWidth + f_res_count;
	m_Image_bitHeight	= m_Image_Height;
	bitSize				= m_Image_bitHeight * m_Image_bitWidth;

	return true;
}
bool ImageCtrl::SetImagePos(int x,int y)
{
	image->MoveWindow(x,y,m_Image_Width,m_Image_Height);
	saveSetting();
	return true;
}
int ImageCtrl::GetBitSize()
{
	return				bitSize;
}
unsigned char* ImageCtrl::GetImageLpBit()
{
	return				lpBits;
}
sDCparm ImageCtrl::GetImageParm()
{
	return				m_Parm;
}

int ImageCtrl::GetDCwidth()
{
	return m_Image_Width;
}

int ImageCtrl::GetDCheight()
{
	return m_Image_Height;
}

void ImageCtrl::saveSetting(void)
{
	if (lpBits != NULL)
	{
		free(lpBits);
		m_imageDC		= NULL;	
		m_imageRect		= NULL;		
		lpBits			= NULL;
		image = (CStatic *)(m_mainWindow->GetDlgItem(m_nID));
		m_bitmap.DeleteObject();
		m_mDC.DeleteDC();
	}
	m_imageDC = image->GetWindowDC();														// 실제 사용될 DC
	image->GetClientRect(&m_imageRect);														// 실제 사용될 DC 의 사각형 정보

	m_mDC.CreateCompatibleDC(m_imageDC);													// 사용되는 DC 와 호환되는 메모리 DC
	m_bitmap.CreateCompatibleBitmap(m_imageDC , m_imageRect.right , m_imageRect.bottom);	// 메모리 DC 와 호환되는 bitmap
	m_bitmap.GetBitmap(&m_bit_info);	
													// bitmap 정보 받아오기 

	res_bitCount = m_bit_info.bmWidthBytes % 4;

	lpBits = (unsigned char*)malloc((m_bit_info.bmWidthBytes + res_bitCount) * m_bit_info.bmHeight);			// bitmap 의 픽셀 RGB 정보를 받아오기위한 변수 
	BackGroundlpBits = (unsigned char*)malloc((m_bit_info.bmWidthBytes + res_bitCount) * m_bit_info.bmHeight);	
	
	m_mDC.SelectObject(&m_bitmap);															// 메모리 DC 가 bitmap에 그릴것을 설정
	m_mDC.PatBlt(m_imageRect.left , m_imageRect.top , m_imageRect.right 
		,m_imageRect.bottom , BLACKNESS);													// bitmap 의 초기 설정 

	memset(lpBits,0,sizeof(unsigned char) * (m_bit_info.bmWidthBytes * m_bit_info.bmHeight) );
}
void ImageCtrl::DrawDC()
{
	m_bitmap.SetBitmapBits( m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,lpBits);
	m_imageDC->BitBlt(0,0,m_imageRect.right,m_imageRect.bottom, &m_mDC , 0 , 0 , SRCCOPY);	// Memory Bitmap에 그려진 정보를 실제 DC 에 복사 
}

void ImageCtrl::ReDrawDC()
{
	m_bitmap.GetBitmapBits(m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,lpBits);
	m_bitmap.SetBitmapBits( m_bit_info.bmHeight * m_bit_info.bmWidthBytes ,lpBits);
	m_imageDC->BitBlt(0,0,m_imageRect.right,m_imageRect.bottom, &m_mDC , 0 , 0 , SRCCOPY);	// Memory Bitmap에 그려진 정보를 실제 DC 에 복사 
}
void ImageCtrl::StrechDrawDC(int startX, int startY,int endX, int endY)
{
	m_imageDC->SetStretchBltMode(HALFTONE);
	m_imageDC->StretchBlt(0,0,m_Image_Width,m_Image_Height,&m_mDC,startX,startY,endX,endY,SRCCOPY);
}
void ImageCtrl::SetImageLpBit(unsigned char* bitData)
{
	memcpy(lpBits, bitData,sizeof(unsigned char) * GetBitSize());
}
int	ImageCtrl::GetresourceID()
{
	return m_nID;
}