#pragma once
#include "../stdafx.h"
#include "../resource.h"

struct sDCparm
{
	int dcLeft;
	int dcRight;
	int dcTop;
	int dcBottem;
	int dcXoffset;
	int dcYoffset;
};

class ImageCtrl : public CWnd
{
public:
	ImageCtrl(void);
	ImageCtrl(CWnd* mainwindow, int nID);
	~ImageCtrl(void);

	void			CreateDC(CWnd* mainwindow, int nID);
	bool			SetImageInit(int posX, int posY, int Width, int Height);
	bool			SetImageInit(int posX, int posY);
	void			SetBackGroundBIT(void);

	void			DrawDC();
	void			ReDrawDC();	
	void			StrechDrawDC(int startX, int startY,int endX, int endY);
	void			SetImageLpBit(unsigned char* bitData);
	void			BackGroundDraw(void);

	unsigned char*	GetImageLpBit();
	CDC*			GetImageDC();
	CDC*			GetmDC();
	int				GetBitSize();
	int				GetDCwidth();
	int				GetDCheight();
	sDCparm			GetImageParm();
	int				GetresourceID();
	int				GetXcentor();
	int				GetYcentor();

protected:
	bool	SetImageSize(int width,int height);
	bool	SetImagePos(int x,int y);
	void	saveSetting(void);

	int		m_Image_Width;
	int		m_Image_Height;
	int		m_Image_bitWidth;
	int		m_Image_bitHeight;
	CWnd*	m_mainWindow;
	int		m_nID;
	int		bitSize;
	int		bitcnt_size;
	int		res_bitCount;
	sDCparm m_Parm;

protected:
	CDC*			m_imageDC;			// 그릴화면 DC
	CDC				m_mDC;				// 더블 버퍼링 -> 메모리 DC
	CBitmap			m_bitmap;			// 비트맵 -> 더블 버퍼링
	BITMAP			m_bit_info;			// 비트맵 정보 받아올 변수 
	CRect			m_imageRect;		// 그릴화면 DC 의 크기저장
	CStatic*		image;				// 그릴 IDC_IMAGE 변수 
	CPoint			m_pline;			// 라인을 그리기 위해서
	unsigned char*  lpBits;
	unsigned char*  BackGroundlpBits;
};

