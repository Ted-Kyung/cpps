#include "StdAfx.h"
#include "SearchOptimizingPath.h"
#include <math.h>
#include <conio.h>
#define MAX_DISTANCE 999999.0

SearchOptimizingPath::SearchOptimizingPath(Callback_SearchOptimizingPath* callback)
{
	m_callback = callback;
	mp_datacallback = callback;
	gppdata_object = new GPP_Data_Struct;

	endpointeast			= 0.0;
	endpointnorth			= 0.0;

	now_east				= 0.0;
	now_north				= 0.0;

	cost					= 0.0;
	lest_cost				= 0.0;
	guess_lest_cost			= 0.0;

	now_point				= -1;
}


SearchOptimizingPath::~SearchOptimizingPath(void)
{
	delete gppdata_object;
}

void SearchOptimizingPath::StartCreateObtimizerPath(void)
{
	//start point 와 end point 가 존재 하는지 확인 
	if(CheckStartGoalPoint()) 
	{
		C_RED;printf("Check to StartPoint & EndPoint\n");C_WHITE;
		return;
	}

	InitObtimizer();

	int selectNum = -1;
	double buf_east = 0.0;
	double buf_north = 0.0;

	bool notCandidate = false;
	
	double parent_east = 0.0;
	double parent_north = 0.0;
	double parent_distance = 0.0;

	double chk_low_distance = MAX_DISTANCE;

	int release_candidate_point_cnt = 0;
	int candidate_point_cnt = GetConnectionSize(now_point);
	candidate_point = GetConnection(now_point);
	double chkdis = 0.0;
	int bbs = 0;
	while(1)
	{
		if(_kbhit() != 0)
		{
			if(_getch() == 'q')break;
		}
		candidate_point_cnt = GetConnectionSize(now_point);
		candidate_point = GetConnection(now_point);
		for(int bcnt = 0; bcnt < candidate_point_cnt ; bcnt++)
		{			
			if((((candidate_point[bcnt])->pointNum) != parent_point)&&((candidate_point[bcnt])->parentNum == -1))
			{
				//0 부모 포인트를 지정해 준다 
				(candidate_point[bcnt])->parentNum = now_point;
				//1 후보 점들과 골사이의 거리를 계산 한다. 
				buf_east = (candidate_point[bcnt])->east;
				buf_north = (candidate_point[bcnt])->north;
				(candidate_point[bcnt])->Goal2Distance = p2GoalDistance(buf_east,buf_north);
				//2 부모의 누적 거리 + 부모가 나로부터 떨어진 거리를 계산 
				parent_east = (getMyNum(candidate_point[bcnt]->parentNum))->east;
				parent_north = (getMyNum(candidate_point[bcnt]->parentNum))->north;
				parent_distance = (getMyNum(candidate_point[bcnt]->parentNum))->IntegralParent2Distance;
				(candidate_point[bcnt])->IntegralParent2Distance = parent_distance + p2pDistance(buf_east,buf_north,parent_east,parent_north);

				release_candidate_point.push_back(getMyNum((candidate_point[bcnt])->pointNum));
			}			
		}
		release_candidate_point_cnt = release_candidate_point.size();
		if(getMyNum(now_point)->state == END) break;
		if(release_candidate_point_cnt == 0) 
		{
			notCandidate = true;
			break;
		}
		for(int bcnt = 0; bcnt < release_candidate_point_cnt ; bcnt++)
		{
			// 공개 후보 경로점 중에서 가장 짧은 거리를 추출
			chkdis = (release_candidate_point[bcnt])->Goal2Distance + (release_candidate_point[bcnt])->IntegralParent2Distance;
			if(chk_low_distance > chkdis)
			{
				selectNum = (release_candidate_point[bcnt])->pointNum;
				chk_low_distance = chkdis;
				bbs = bcnt;
			}
		}
		//선택된 점은 공개 후보점에서 삭제 
		release_candidate_point.erase(release_candidate_point.begin() + bbs);
		chk_low_distance = MAX_DISTANCE;
		parent_point = now_point;
		now_point = selectNum;
		selectNum = -1;		
	}
	printf("Path Select Success!\n");
	if(notCandidate == false)
	{
		m_callback->callback_ERRNONE_NotConnectionPath();
		ReverseEstimationPath();
	}
	else
	{
		m_callback->callback_ERR_NotConnectionPath();
		C_RED;printf("Not Connect Goal Point.. Path of End!!\n");C_WHITE;
	}
}
void SearchOptimizingPath::ReverseEstimationPath(void)
{
	int f_nowpoint = getEndpoint()->pointNum;
	C_NONE;printf("ReverseEstimationPath.");C_WHITE;
	while(1)
	{
		C_NONE;printf(".");C_WHITE;
		if(_kbhit() != 0)
		{
			if(_getch() == 'q')break;
		}
		buff_point.pointNum = getMyNum(f_nowpoint)->pointNum;
		buff_point.state = getMyNum(f_nowpoint)->state;
		buff_point.east = getMyNum(f_nowpoint)->east;
		buff_point.north = getMyNum(f_nowpoint)->north;
		buff_point.parentNum = getMyNum(f_nowpoint)->parentNum;
		buff_point.IntegralParent2Distance = getMyNum(f_nowpoint)->IntegralParent2Distance;
		buff_point.Goal2Distance = getMyNum(f_nowpoint)->Goal2Distance;

		Path.push_front(buff_point);
		if(getMyNum(f_nowpoint)->state == START) break;
		f_nowpoint = getMyNum(f_nowpoint)->parentNum;	
	}
	C_NONE;printf(".\n");C_WHITE;

	m_callback->callback_ERR_NotEndPoint();
	m_callback->callback_ERR_NotStartPoint();	
	m_callback->callback_DrawPath(Path);
}

double SearchOptimizingPath::p2GoalDistance(double e1,double n1)
{
	double buf_distance = 0.0;
	double e = e1 - endpointeast;
	double n = n1 - endpointnorth;

	buf_distance = sqrt((e*e)+(n*n));
	return buf_distance;
}

double SearchOptimizingPath::p2pDistance(double e1,double n1,double e2, double n2)
{
	double buf_distance = 0.0;
	double e = e1 - e2;
	double n = n1 - n2;

	buf_distance = sqrt((e*e)+(n*n));
	return buf_distance;
}

void SearchOptimizingPath::InitObtimizer(void)
{
	Reset();
	candidate_point.resize(0);
	release_candidate_point.resize(0);
	Path.resize(0);
	
	endpointeast = (getEndpoint())->east;
	endpointnorth = (getEndpoint())->north;

	startpointeast = (getStartNum())->east;
	startpointnorth = (getStartNum())->north;

	now_point = (getStartNum())->pointNum;
	now_east = (getStartNum())->east;
	now_north = (getStartNum())->north;
	(getStartNum())->parentNum = now_point;
	parent_point = now_point;

	lest_cost = p2pDistance(now_east,now_north,endpointeast,endpointnorth);
	(getStartNum())->Goal2Distance = lest_cost;
	(getStartNum())->IntegralParent2Distance = 0.0;
}

void SearchOptimizingPath::Reset(void)
{
	int reset_point_cnt = GetALLPointSize();
	reset_point = getALLpoint();

	for (int resetcnt = 0 ; resetcnt < reset_point_cnt ; resetcnt++)
	{
		(reset_point+resetcnt)->parentNum = -1;
		(reset_point+resetcnt)->Goal2Distance = 0.0;
		(reset_point+resetcnt)->IntegralParent2Distance = 0.0;
	}
}
bool SearchOptimizingPath::CheckStartGoalPoint(void)
{
	int reset_point_cnt = GetALLPointSize();
	int checksum = 0;

	for (int resetcnt = 0 ; resetcnt < reset_point_cnt ; resetcnt++)
	{
		if ((getALLpoint()+resetcnt)->state == START)
		{
			checksum = checksum + 1;
		}
		else if ((getALLpoint()+resetcnt)->state == END)
		{
			checksum = checksum + 20;
		}
	}

	if(checksum == 21)
	{
		m_callback->callback_ERRNONE_NotEndPoint();
		m_callback->callback_ERRNONE_NotStartPoint();
		return false;
	}

	m_callback->callback_ERR_NotEndPoint();
	m_callback->callback_ERR_NotStartPoint();	
	return true;
}