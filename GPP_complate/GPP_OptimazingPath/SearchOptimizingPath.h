#pragma once
#include "GPP_Data_Struct.h"
#include <vector>
using namespace std;
#define START 1
#define END	2
#define NONE 0

class Callback_SearchOptimizingPath : public Callback_GPP_Data_Struct
{
public : 
	virtual void callback_DrawPath(deque<GPR_DATA> Path) = 0;
	virtual void callback_ERR_NotConnectionPath(void) = 0;
	virtual void callback_ERRNONE_NotConnectionPath(void) = 0;
};

class SearchOptimizingPath : public GPP_Data_Struct
{
public:
	SearchOptimizingPath(Callback_SearchOptimizingPath* callback);
	~SearchOptimizingPath(void);

	Callback_SearchOptimizingPath* m_callback;

	GPP_Data_Struct* gppdata_object;
	
public:
	void StartCreateObtimizerPath(void);	
	void Reset(void);
private:
	double endpointeast;
	double endpointnorth;

	double startpointeast;
	double startpointnorth;

	double now_east;
	double now_north;
	
	double cost;
	double lest_cost;
	double guess_lest_cost;

	int	parent_point;
	int now_point;
	deque<GPR_DATA>::iterator reset_point;
	deque<deque<GPR_DATA>::iterator> candidate_point;
	deque<deque<GPR_DATA>::iterator> release_candidate_point;

	GPR_DATA buff_point;
	deque<GPR_DATA> Path;
	
	double p2GoalDistance(double e,double n);
	double p2pDistance(double e1,double n1,double e2, double n2);
	void InitObtimizer(void);	
	void ReverseEstimationPath(void);
	bool CheckStartGoalPoint(void);
};

