
// GPP_OptimazingPathDlg.h : 헤더 파일
//

#pragma once
#include "imageProcessing.h"
#include "MySQL_Manager.h"
#include "SearchOptimizingPath.h"
#include <Windows.h>

// CGPP_OptimazingPathDlg 대화 상자
class CGPP_OptimazingPathDlg : public CDialogEx, public MySql_CallBack , public Callback_SearchOptimizingPath , public ImageProcessing_CallBack
{
// 생성입니다.
public:
	CGPP_OptimazingPathDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GPP_OPTIMAZINGPATH_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	//마우스 조작 /
	bool m_LBpush;
	bool m_RBpush;
	bool m_wheel;

	//map 조작 변수
	int StShiftX;
	int StShiftY;
	int shiftX;
	int shiftY;
	int zoom;
	int ZOOM_MAX;
	int ZOOM_MIN;

	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnDestroy();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);

	CComboBox*	m_combo;
	CButton*	m_mapSetting;
	CButton*	m_mapSelect;

	CEdit*		m_integraldistance;
	CEdit*		m_calcultime;

	CStatic*	m_static_err1;
	CStatic*	m_static_err2;
	CStatic*	m_static_err3;
	CStatic*	m_static_err4;
	CStatic*	m_static_err5;
	CStatic*	m_static_err6;
	CStatic*	m_static_err7;
	CStatic*	m_static_err8;
	CStatic*	m_static_err9;
	CStatic*	m_static_err10;
	CStatic*	m_static_err11;
	CStatic*	m_static_err12;

	//Image Control
	imageProcessing* mImgMap;

	//mysql manage Class 
	MySQL_Manager* m_mysql_manager;
	void callback_Addpoint(int pointNum,double East, double North);
	void callback_SetConnettion(int pointNum,int pointNum2);

	//Search optimizing Path class 
	SearchOptimizingPath* m_searchOptimizingPath;
	void callback_DrawPath(deque<GPR_DATA> Path);
	deque<GPR_DATA>::iterator iter_GPPpoint;

	double m_mapEastStart;
	double m_mapEastEnd;

	double m_mapNorthStart;
	double m_mapNorthEnd;

	double m_mapEast_offset ;
	double m_mapNorth_offset;

	//DC 와 Global 좌표 계산용 
	double m_mouseEast;
	double m_mouseNorth;
	double m_windowWidth;
	double m_windowHeight;
	double m_xoffSet;
	double m_yoffSet;
	double m_xViewdistance;
	double m_yViewdistance;
	
	//test 용 버튼 컨트롤 
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();

	bool mb_StartBtn;
	bool mb_GoalBtn;

	bool mb_StartClick;
	bool mb_GoalClick;

	//그림 함수 
	void mf_Line(double east1, double north1,double east2, double north2, COLORREF clr);
	void mf_Circle(double east1, double north1,int r, COLORREF clr);
	void mf_DrawPoint();
	void mf_DCpoint2Globalpoint_Convert(int x, int y);

	// GPP Initializing 
	void mf_ProgramInit(void);
	void mf_ProgramInit(int mapNum);
	afx_msg void OnCbnSelchangeCombo();
	afx_msg void OnBnClickedBtnMapchoice();
	afx_msg void OnBnClickedBtnMapselect();

	//error callback
	void callback_ERR_MysqlConnection(void);
	void callback_ERR_MysqlDBfind(void);
	void callback_ERR_NotConnectionPath(void);
	void callback_ERR_NotStartPoint(void);
	void callback_ERR_NotEndPoint(void);
	void callback_ERR_MapFileOpen(void);
	void callback_ERRNONE_MapFileOpen(void);
	void callback_ERRNONE_MysqlConnection(void);
	void callback_ERRNONE_MysqlDBfind(void);
	void callback_ERRNONE_NotConnectionPath(void);
	void callback_ERRNONE_NotStartPoint(void);
	void callback_ERRNONE_NotEndPoint(void);

	//calculator time

};
