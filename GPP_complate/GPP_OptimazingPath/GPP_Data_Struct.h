#pragma once
#include <deque>
#include <vector>

#define START 1
#define END	2
#define NONE 0
using namespace std;

struct GPR_DATA
{
	int									parentNum;
	double								IntegralParent2Distance;
	int									pointNum;
	int									state;
	double								east;
	double								north;
	double								Goal2Distance;
	deque<int>							connection;
};

class Callback_GPP_Data_Struct
{
public : 
	virtual void callback_ERR_NotStartPoint(void) = 0;
	virtual void callback_ERR_NotEndPoint(void) = 0;
	virtual void callback_ERRNONE_NotStartPoint(void) = 0;
	virtual void callback_ERRNONE_NotEndPoint(void) = 0;
};

class GPP_Data_Struct
{
public:
	GPP_Data_Struct();
	~GPP_Data_Struct(void);

public:
	//Remove All Point 
	void RemoveAllPoint(void);	
	//ADD 1 point 
	void AddPoint(int pointNum, double east, double north);
	//Set 1 point State
	void setState(int state, int pointNum);
	//return 1point State
	int getState(int pointNum);
	//Remove 1 point
	void RemovePoint(int pointNum);
	//Connection point1, point2 
	void SetConnection(int pointNum1, int pointNum2);
	//return myNum's Connect Count
	int  GetConnectionSize(int myNum);
	//return Pointer(*) Connect points
	deque<deque<GPR_DATA>::iterator> GetConnection(int myNum);
	//Save Parent Point
	void SetMyParent(int myNum, int parentNum);
	//return myNum's Parent Pointer 
	deque<GPR_DATA>::iterator GetMyParent(int myNum);
	//return ALL point Count
	int GetALLPointSize(void);
	//return ALL point variable
	deque<GPR_DATA>::iterator getALLpoint(void);
	//return End Point's pointer 
	deque<GPR_DATA>::iterator getEndpoint(void);
	//return StartPoint's pointer
	deque<GPR_DATA>::iterator getStartNum(void);
	deque<GPR_DATA>::iterator getMyNum(int myNum);
	int SearchNearPointNum(double east, double north);
	void BeforeEndPointRemove(void);
	void BeforeStartPointRemove(void);
	//Save Set Start point's pointer
	void SetStartPoint(int myNum);
	//Save Set End point's pointer
	void SetEndPoint(int myNum);

	Callback_GPP_Data_Struct* mp_datacallback;

private:
	GPR_DATA buf_point;
	deque<GPR_DATA> point;
	deque<GPR_DATA> trashpoint;
	deque<GPR_DATA>::iterator iter_point;
	deque<deque<GPR_DATA>::iterator> iter_iter_point;
	deque<deque<GPR_DATA>::iterator> buf_iter_iter_point;

	deque<GPR_DATA>::iterator iter_start_point;
	deque<GPR_DATA>::iterator iter_end_point;
	deque<GPR_DATA>::iterator findPointNumIter(int pointNum);
	bool CheckSameNum(int pointNum);
	bool CheckSameConnection(int pointNum1, int pointNum2);	
};

