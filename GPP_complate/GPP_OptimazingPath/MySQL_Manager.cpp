#include "StdAfx.h"
#include "MySQL_Manager.h"


MySQL_Manager::MySQL_Manager(MySql_CallBack* pcallback)
{
	m_pcallback = pcallback;
	m_mysqlERR = NULL;
	C_YELLOW;printf("MySQL_Manager create \n");	C_WHITE;
}

MySQL_Manager::~MySQL_Manager(void)
{
	mysql_close(&m_mysql);
	C_RED;printf("~MySQL_Manager destroy \n");C_WHITE;
}

bool MySQL_Manager::MySQL_Connect(char* ID , char* passWd , char* dataBaseName)
{
	m_using_db_query = "use ";
	m_using_db_query = m_using_db_query + dataBaseName;
	m_using_db_query = m_using_db_query + ";";

	// mysql 변수 초기화 
	mysql_init(&m_mysql);
	C_YELLOW;
	printf("mysql_init connect ID = %s\n",ID);
	printf("mysql_init connect PW = %s\n",passWd);
	C_WHITE;

	m_mysql.options.unix_socket		= NULL;
	m_mysql.options.port			= 0;
	m_mysql.options.compress		= 0;
	m_mysql.options.connect_timeout	= 30;

	m_mysqlERR = mysql_real_connect(&m_mysql, "localhost",ID,passWd,dataBaseName,3306,0,0);

	if (m_mysqlERR == NULL)
	{
		C_RED;printf("Step [connection] : connection Err Code is => %s \n",mysql_error(&m_mysql));C_WHITE;
		m_pcallback->callback_ERR_MysqlConnection();
		return false;
	}
	else
	{
		m_pcallback->callback_ERRNONE_MysqlConnection();
		C_GREEN;printf("Step [connection] : connection Success Code is => err_none \n",mysql_error(&m_mysql));C_WHITE;
		return true;
	}
}

bool MySQL_Manager::MySQL_GetDB()
{
	if(mysql_real_query(&m_mysql,m_using_db_query,m_using_db_query.GetLength()) !=0)
	{
		m_pcallback->callback_ERR_MysqlDBfind();
		C_RED;printf("DB Select Fail...\n");C_WHITE;
	}
	else
	{
		m_pcallback->callback_ERRNONE_MysqlDBfind();
		C_GREEN;printf("DB Select Success\n");C_WHITE;
		char char_query[512];
		m_query_Str = "select * from coordinate;";
		strcpy(&char_query[0], m_query_Str.GetBuffer(0));

		if(mysql_real_query(&m_mysql,char_query,m_query_Str.GetLength()) != 0)
		{
			C_RED;printf("Step [mysql_real_query] :Code is => %s\n",mysql_error(&m_mysql));C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");C_WHITE;
		}
		MYSQL_RES* coordinate_Res = mysql_store_result(&m_mysql);
		if(coordinate_Res == NULL)
		{
			C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
			return false;
		}
		else
		{
			C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;
		}

		mysql_next_result(&m_mysql);

		// coordinate 정보를 끝까지 읽어 오기 
		MYSQL_ROW coordinate_Row;
		while(coordinate_Row = mysql_fetch_row(coordinate_Res))
		{		
			//if(atoi(coordinate_Row[4]) == 1)
				m_pcallback->callback_Addpoint(atoi(coordinate_Row[0]),atof(coordinate_Row[2]),atof(coordinate_Row[3]));
		}		
		C_PUFFLE;printf("coordinate(point, East, North)infomation Road Complate\n");C_WHITE;
		mysql_free_result(coordinate_Res);
		////////////////////////////////////////////////////////////////////////////////////////////////////

		m_query_Str = "select * from polygon;";
		strcpy(&char_query[0], m_query_Str.GetBuffer(0));

		if(mysql_real_query(&m_mysql,char_query,m_query_Str.GetLength()) != 0)
		{
			C_RED;printf("Step [mysql_real_query] :Code is => %s\n",mysql_error(&m_mysql));	C_WHITE;
		}
		else
		{
			C_GREEN;printf("Step [mysql_real_query] :Query Success code is => err_none\n");	C_WHITE;
		}
		MYSQL_RES* connection_Res = mysql_store_result(&m_mysql);
		if(connection_Res == NULL)
		{
			C_RED;printf("Step [mysql_store_result]Coordinate Sort Faild\n");C_WHITE;
			return false;
		}
		else
		{
			C_GREEN;printf("Step [mysql_store_result] :Coordinate Sort Success code is => err none\n");C_WHITE;
		}

		mysql_next_result(&m_mysql);

		// coordinate 정보를 끝까지 읽어 오기 
		MYSQL_ROW connection_Row;
		while(connection_Row = mysql_fetch_row(connection_Res))
		{	
			//if(atoi(connection_Row[1]) != -1) 
				m_pcallback->callback_SetConnettion(atoi(connection_Row[2]),atoi(connection_Row[3]));
		}		
		C_PUFFLE;printf("connection(point, point2)infomation Road Complate\n");C_WHITE;
		mysql_free_result(connection_Res);
	}
	return true;
}