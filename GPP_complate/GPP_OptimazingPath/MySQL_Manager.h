#pragma once

#include <winsock2.h>
#include <mysql.h>

#pragma comment(lib, "libmysql.lib")
#pragma comment(lib, "ws2_32.lib")

class MySql_CallBack
{
public:
	virtual void callback_Addpoint(int pointNum,double East, double North) = 0;
	virtual void callback_SetConnettion(int pointNum,int pointNum2) = 0;
	virtual void callback_ERR_MysqlConnection(void) = 0;
	virtual void callback_ERR_MysqlDBfind(void) = 0;
	virtual void callback_ERRNONE_MysqlConnection(void) = 0;
	virtual void callback_ERRNONE_MysqlDBfind(void) = 0;
};

class MySQL_Manager
{
public:
	MySQL_Manager(MySql_CallBack* pcallback);
	~MySQL_Manager(void);

	MySql_CallBack* m_pcallback;

	//mysql ���� ���� 
	MYSQL	m_mysql;
	MYSQL*	m_mysqlERR;

	CString m_using_db_query;
	CString m_query_Str;

	bool MySQL_Connect(char* ID , char* passWd, char* dataBaseName);
	bool MySQL_GetDB();
};

